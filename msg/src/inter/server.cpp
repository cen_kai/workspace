#include <signal.h>

#include <sched.h>

#include <locale.h>
#include <time.h>

#ifdef LOGGER
#include <log4cplus/configurator.h>
using namespace log4cplus;
#endif

#include <string>
#include <sstream>
using namespace std;

#include "server/ServerApp.h"

#include "server/ServiceThread.h"
#include "client/ClientSession.h"
#include "msg_transmit/StoreCache.h"

#include "TaskProcessThreads.h"

ServerApp g_app;

ServiceThread< ClientSession > * g_client_service;
TaskProcessThreads *g_task_service;

static log4cplus::Logger _logger = log4cplus::Logger::getInstance("server");

void sign_t(int sign)
{
    printf("error!! in thread %u\n", (unsigned int) pthread_self());
    abort();
    //kill(0, SIGKILL);
}



int main(int argc, char* argv[])
{
    fprintf(stderr, "start.....\n");
#ifdef LOGGER
    PropertyConfigurator::doConfigure("../conf/log4cpluscplus.properties");
#endif

    srand(time(NULL));
    setlocale(LC_ALL, "");

    signal(SIGSEGV, sign_t);
        
    g_app.set_config_file("../conf/msg_transmit_svr.conf");
    if (!g_app.load_conf())
    {
        fprintf(stderr, "failed to load configuration.\n");
        return -1;
    }

    
    g_task_service = new TaskProcessThreads(8);
    g_task_service->startThreads();

    string server_addr = g_app.get_conf_string("server.bind.ip", "0.0.0.0");
    int server_timeout_ms = g_app.get_conf_int("server.overdue.sec", 0) * 1000;    
    int server_port = g_app.get_conf_int("server.bind.port", 8080);    
    int thread_num = g_app.get_conf_int("server.process.num", 8);
    g_client_service = new ServiceThread< ClientSession >(server_addr, server_port, thread_num, server_timeout_ms);
    g_client_service->start();

    g_client_service->wait_terminate();

    return 0;
}

