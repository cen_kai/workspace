#include "TaskProcessThreads.h"
#include "server/ServiceThread.h"
#include <vector>
#include <cstdlib>
#include <sstream>
#include <cstdarg>
#include <map>
using namespace std ;

extern ServiceThread< ClientSession > * g_client_service;

#include "msg_transmit/SessionCache.h"
#include "msg_transmit/RecordCache.h"
#include "msg_transmit/common.h"
#include "msg_transmit/json_util.hpp"
#include "msg_transmit/json_util.cpp"
#include "server/timedelt.h"
#include "msg_transmit/StoreCache.h"

extern ServerApp g_app;


IMPL_LOGGER(TaskProcessThreads, logger);
map<string,int> TaskProcessThreads::m_task_type;


void TaskProcessThreads::init_task_type()
{
    m_task_type["conf"] = PipeEvent::PIPE_TYPE_CLIENT_CONF | PipeEvent::PIPE_TYPE_FLAG_NO_NEED_VERIFY;
    m_task_type["query"] = PipeEvent::PIPE_TYPE_CLIENT_QUERY | PipeEvent::PIPE_TYPE_FLAG_NO_NEED_VERIFY;
    m_task_type["clear"] = PipeEvent::PIPE_TYPE_CLIENT_CLEAR;
    m_task_type["delete"] = PipeEvent::PIPE_TYPE_CLIENT_DELETE;
    m_task_type["report"] = PipeEvent::PIPE_TYPE_CLIENT_REPORT;
    m_task_type["page"] = PipeEvent::PIPE_TYPE_CLIENT_PAGE;
    m_task_type["store"] = PipeEvent::PIPE_TYPE_CLIENT_STORE_REPORT | PipeEvent::PIPE_TYPE_FLAG_SERVER_MESSAGE ;
}


int TaskProcessThreads::get_task_type(const string& cmd)
{
    map<string,int>::iterator iter = m_task_type.find(cmd);

    if(iter == m_task_type.end())
    {
        return -1;
    }
    else
    {
        return iter->second;
    }
}


TaskProcessThreads::~TaskProcessThreads()
{
    delete _tasks;
}

TaskProcessThreads::TaskProcessThreads(size_t thread_count) : SDThreadPool(thread_count)
{
    LOG4CPLUS_DEBUG(logger, "init");
    _tasks = new SDQueue<PipeEvent>(10000);
    LOG4CPLUS_DEBUG(logger, "init 2");
}

bool TaskProcessThreads::add_task(PipeEvent &event)
{
    return _tasks->push(event,100);
}

void TaskProcessThreads::doIt()
{
    while(true)
    {
        PipeEvent task;
        if(!_tasks->pop(task, 100)){
            continue;
        }

        LOG4CPLUS_DEBUG(logger, "try to handle one task: " << task.dump());
        
        SessionCacheImp ssch_imp;
        
        if (false == check_session(task,ssch_imp))
        {
            LOG4CPLUS_DEBUG(logger, "not pass check ");
            task.resp = new string("{code:-1,info:\"not pass check,illegal user\"}");
        }
        else
        {
            switch(task.m_type & PipeEvent::PIPE_TYPE_WITHOUT_FLAG)
            {
                case PipeEvent::PIPE_TYPE_CLIENT_CONF:

                    get_conf(task);
                    
                    break;
                    
                case PipeEvent::PIPE_TYPE_CLIENT_QUERY:
                    
                    query_record(task,ssch_imp);
                    
                    break;

                case PipeEvent::PIPE_TYPE_CLIENT_REPORT:

                    report_record(task,ssch_imp);

                    break;

                case PipeEvent::PIPE_TYPE_CLIENT_DELETE:
                    
                    delete_record(task,ssch_imp);

                    break;

                case PipeEvent::PIPE_TYPE_CLIENT_CLEAR:
                    
                    clear_record(task,ssch_imp);

                    break;

                    
                case PipeEvent::PIPE_TYPE_CLIENT_VERIFY:

                    continue;

                case PipeEvent::PIPE_TYPE_CLIENT_PAGE:
                    get_page(task,ssch_imp);
                    break;

                case PipeEvent::PIPE_TYPE_CLIENT_STORE_REPORT:
                    store_record(task );
                    break;

                default:
                    task.resp = new string("Wrong Command");
                    break;
                    
            }   
        }   

        //make_call_back(task);
        
        if(task.m_data != NULL) 
        {
            LOG4CPLUS_DEBUG(logger, "delete 1");
            delete (char*)task.m_data;
            LOG4CPLUS_DEBUG(logger, "delete 2");
            task.m_data = NULL;
        }

        return_result(task);
    }
}


bool TaskProcessThreads::check_session(PipeEvent& task, SessionCacheImp& ssch_imp)
{
    static SessionCache* session_cache = SessionCache::Instance();
    //  �ڲ�������ͨ�ŵĲ���Ҫ��֤
    if( (int)(task.m_type & PipeEvent::PIPE_TYPE_FLAG_SERVER_MESSAGE) == PipeEvent::PIPE_TYPE_FLAG_SERVER_MESSAGE )
    {
        return true;
    }
    
    if(task.m_type == PipeEvent::PIPE_TYPE_CLIENT_VERIFY)
    {
        SessionCacheImp* ssch_ptr = (SessionCacheImp*)task.m_data;

        bool result = session_cache->check_session(*ssch_ptr);
        delete (SessionCacheImp*)task.m_data;
        task.m_data = NULL;
        return result;
    }
        
    bool flag = false;
    if(!get_ssch_imp((HttpCmd*)task.m_data,ssch_imp))
    {
        LOG4CPLUS_DEBUG(logger, "Not Get Session Cache Imp");
        task.m_type = PipeEvent::PIPE_TYPE_WRONG_CMD ;
        return false;
    }
    
    if(ssch_imp.m_state == SessionCacheImp::PASS)
    {
         LOG4CPLUS_DEBUG(logger, "Pass a request");
         flag = true;
    }
    else if(ssch_imp.m_state == SessionCacheImp::ILLEGLE)
    {
        flag = false;
    }
    else if((int)(task.m_type&0xF0000000) == PipeEvent::PIPE_TYPE_FLAG_NO_NEED_VERIFY)
    {
        if(ssch_imp.m_state == SessionCacheImp::VERIFYING)
        {
            add_verify(task,ssch_imp);
            flag = true;
        }
    }
    else
    {
        if(!session_cache->check_session(ssch_imp))
        {
            flag = false;
        }
        else
        {
            flag = true;
        }
    }   
    
    
    return flag;
}


bool TaskProcessThreads::get_ssch_imp(HttpCmd* cmd,SessionCacheImp& ssch_imp)
{
    stringstream ss;
    string tmp;
    if(!cmd->get_cmd_string("userid",tmp))
    {
        LOG4CPLUS_ERROR(logger,"No userid input");
        return false;
    }

    _u64 userid = 0;
    ss<<tmp;
    ss>>userid;

    string sessionid;
    if(!cmd->get_cmd_string("sessionid",sessionid))
    {
        LOG4CPLUS_ERROR(logger,"No sessionid input");
        return false;
    }

    if(sessionid.empty()){
        LOG4CPLUS_ERROR(logger,"No sessionid input");
        return false;
    }
        
    SessionCheck ssck(userid,sessionid);
    static SessionCache* session_cache = SessionCache::Instance();
    
    if(!session_cache->get_checked_session(ssck,ssch_imp))
    {
        LOG4CPLUS_DEBUG(logger, "Not Get Session Cache Imp In cache");
        string logintype;
        cmd->get_cmd_string("logintype",logintype);
        SessionCacheImp::ClientType ctype;
        _u64 bussinessid = 0;
        
        if( (logintype == "Client") || (logintype == "client") )
        {
            ctype = SessionCacheImp::CLIENT;
            
            if(!cmd->get_cmd_ulvalue("bussinessid",bussinessid))
            {
                LOG4CPLUS_ERROR(logger,"No bussinessid input");
                return false;
            }
        }
        else if( (logintype == "Web") || (logintype == "web") )
        {
            ctype = SessionCacheImp::WEB;
        }
        else
        {
            LOG4CPLUS_ERROR(logger,"Wrong login type");
            return false;
        }
    

        ssch_imp.set(userid,sessionid,ctype,bussinessid);
        ssch_imp.m_state = SessionCacheImp::VERIFYING;


        session_cache->add_cache(ssck,ssch_imp);
        LOG4CPLUS_DEBUG(logger, "Add cache uid:"<<ssck.m_userid<<" sessionid:"<<ssck.m_sessionid << " bussinessid:"<<bussinessid );
        LOG4CPLUS_DEBUG(logger, "Session cache size:"<<session_cache->get_cache_size());
    
    }

    return true;
    
}




void TaskProcessThreads::add_verify(PipeEvent& task,SessionCacheImp& ssch_imp)
{
    PipeEvent event(task);
    event.m_type = PipeEvent::PIPE_TYPE_CLIENT_VERIFY;
    event.m_data = new SessionCacheImp(ssch_imp);
    add_task(event);
}

void TaskProcessThreads::get_conf(PipeEvent& task)
{
    stringstream ss;
    vector<json_pair> vec ;
    vec.push_back(json_pair("code",(_u64)0));
    vec.push_back(json_pair("info","ok"));

    static string interval = g_app.get_conf_string("conf.interval", "60");
    vec.push_back(json_pair("interval",interval.c_str()));

    static string redirect = g_app.get_conf_string("conf.server.address", "131.23.2.122:8080");
    vec.push_back(json_pair("redirect",redirect.c_str()));
    
    make_json(ss,NULL,0,vec);
    task.resp = new string(ss.str());
}


void TaskProcessThreads::query_record(PipeEvent& task,const SessionCacheImp& ssch_imp)
{
    LOG4CPLUS_DEBUG(logger, "Query user:"<<ssch_imp.m_userid<<" record");
   
    HttpCmd *cmd = (HttpCmd*)task.m_data ;
       
    string platform ;
    if(!cmd->get_cmd_string("devicetype",platform))
    {
           task.resp = new string(err_json("err devicetype"));
           return ;    
    }
    
    string app_name;
    if(!cmd->get_cmd_string("product",app_name))
    {
           task.resp = new string(err_json("err app name"));
           return ;    
    }

    uint64_t  ver=1;
    if(!cmd->get_cmd_ulvalue("protocolver",ver)){
        ver=1;
    }
    
    _u64 num = 0;
    if(!cmd->get_cmd_ulvalue("size",num))
    {
        task.resp = new string(err_json("size err"));
        return ;    
    }
    
    vector<PlayRecord*>  records ;
    static RecordCache* record_cache = RecordCache::GetInstance();
    const uint64_t GET_DATA=1;
    uint64_t flag=0;
    flag |= GET_DATA;
    if(1==ver)
    {
            flag |= 1<<VIEW_TYPE_LONG;
    }else{
        flag |= 1<<VIEW_TYPE_LONG;
        flag |= 1<<VIEW_TYPE_MUSIC;
        flag |= 1<<VIEW_TYPE_SHORT;
        if("xmp"==app_name){
                flag |= 1<<VIEW_TYPE_LOCAL;
                flag |= 1<<VIEW_TYPE_CLOUD;
        }
    }
    int ret = record_cache->Query(ssch_imp.m_userid,platform,flag,app_name,records);
    
    if(ret==-1)
    {
        task.resp = new string(err_json("server query err"));
        return ;
    }
    
    LOG4CPLUS_DEBUG(logger, "Query record size:"<<records.size()<<" need num:"<<num);
    
    
    vector<PlayRecord*>  records_copy ;
    vector<PlayRecord*>::iterator end_iter;
    if(num>records.size())
        end_iter=records.end();
    else
        end_iter=records.begin()+num;
    copy(records.begin(),end_iter,back_inserter(records_copy));
    stringstream  ss;
    vector<struct json_pair>  json_vec ;
    json_vec.push_back(json_pair("code",(_u64)0));
    json_vec.push_back(json_pair("info","ok"));
    json_vec.push_back(json_pair("size",(_u64)records_copy.size()));
    make_json(ss,&records_copy,num,json_vec);
    vector<PlayRecord*>::iterator iter=records.begin();
    for (;iter!=records.end();++iter)
    {
        if(*iter)
        {
            delete *iter ;
        }
    }
    task.resp = new string(ss.str());
}

void TaskProcessThreads::get_page(PipeEvent& task,const SessionCacheImp& ssch_imp)
{
    LOG4CPLUS_DEBUG(logger, "get page user:"<<ssch_imp.m_userid<<" record");
   
    HttpCmd *cmd = (HttpCmd*)task.m_data ;
       
    string platform ;
    if(!cmd->get_cmd_string("devicetype",platform))
    {
           task.resp = new string(err_json("err devicetype"));
           return ;    
    }
       
    string app_name;
    if(!cmd->get_cmd_string("product",app_name))
    {
           task.resp = new string(err_json("err app name"));
           return ;    
    }

    _u64 pagesize = 0;
    if(!cmd->get_cmd_ulvalue("pagesize",pagesize) || pagesize==0 )
    {
        task.resp = new string(err_json("no page size or page size err"));
        return ;    
    }

    _u64 pageindex = 0;
    if(!cmd->get_cmd_ulvalue("pageindex",pageindex))
    {
        task.resp = new string(err_json("no page index or err"));
        return ;    
    }

    uint64_t  ver=1;
    if(!cmd->get_cmd_ulvalue("protocolver",ver)){
        ver=1;
    }
    
    vector<PlayRecord*>  records ;
    static RecordCache* record_cache = RecordCache::GetInstance();
    const uint64_t GET_DATA=1;
    uint64_t flag=0;
    flag |= GET_DATA;
    if(1==ver)
    {
            flag |= 1<<VIEW_TYPE_LONG;
    }else{
        flag |= 1<<VIEW_TYPE_LONG;
        flag |= 1<<VIEW_TYPE_LOCAL;
        flag |= 1<<VIEW_TYPE_CLOUD;
        flag |= 1<<VIEW_TYPE_MUSIC;
        flag |= 1<<VIEW_TYPE_SHORT;
    }
    int ret = record_cache->Query(ssch_imp.m_userid,platform,flag,app_name,records);
    
    if(ret==-1)
    {
        task.resp = new string(err_json("server query err"));
        return ;
    }

    _u64 pagenum=(records.size()+pagesize-1)/pagesize;
    LOG4CPLUS_DEBUG(logger, "Get page  record size:"<<records.size()<<" page num:"<<pagenum <<" page size:"<<pagesize << " page index:"<<pageindex);
    
    if(pageindex>=pagenum && pagenum!=0)
    {
        task.resp = new string(err_json("pageindex greater than pagenum"));
        vector<PlayRecord*>::iterator iter=records.begin();
        for (;iter!=records.end();++iter)
        {
            if(*iter)
            {
                delete *iter ;
            }
        }
        return ;
    }
        
    vector<PlayRecord*>  records_copy ;
    vector<PlayRecord*>::iterator start_iter,end_iter;
    start_iter = records.begin()+pagesize*pageindex;
    if((pageindex+1)*pagesize<records.size())
        end_iter = start_iter+pagesize;
    else
        end_iter = records.end();
    
    copy(start_iter,end_iter,back_inserter(records_copy));
    sort(records_copy.begin(), records_copy.end(),greater_record_pointer);
    stringstream  ss;
    vector<struct json_pair>  json_vec ;
    json_vec.push_back(json_pair("code",(_u64)0));
    json_vec.push_back(json_pair("info","ok"));
    json_vec.push_back(json_pair("pagenum",pagenum));
    json_vec.push_back(json_pair("pageindex",pageindex));
    json_vec.push_back(json_pair("size",(_u64)records.size()));
    make_json(ss,&records_copy,records_copy.size(),json_vec);
    vector<PlayRecord*>::iterator iter=records.begin();
    for (;iter!=records.end();++iter)
    {
        if(*iter)
        {
            delete *iter ;
        }
    }
    task.resp = new string(ss.str());
}


void TaskProcessThreads::make_json(stringstream &ss,vector < PlayRecord*> *records,_u64 num,vector<struct json_pair> &info)
{
    ss<<"{";
    vector<json_pair>::const_iterator iter = info.begin();
    bool first=true;
    for(; iter!=info.end(); ++iter)
    {
        if(!first)
            ss<<",";
        if(iter->type == TYPE_CHAR)
        {
            ToJsonPair(iter->cvalue,ss,iter->key);
        }
        else if(iter->type == TYPE_INT)
        {
            ToJsonPair(iter->ivalue,ss,iter->key);
        }
        first=false;
    }
    if( records )
    {
        ss<<",";
        ToJsonPair(*records,ss,"records");
    }
    ss<<"}";
}

void TaskProcessThreads::report_record(PipeEvent& task,const SessionCacheImp& ssch_imp)
{
    static RecordCache* record_cache = RecordCache::GetInstance();
    HttpCmd  *cmd = (HttpCmd*)task.m_data ;


    string type;
    if(!cmd->get_cmd_string("type",type))
    {
        task.resp = new string(err_json("no type or error type"));
        return ;
    }
    
    
    if(type=="local")
    {
        string err ;
        LocalPlayRecord* p_record = LocalPlayRecord::TryCreate(cmd,err);
        if(p_record == NULL)
        {
            task.resp = new string(err_json(err.c_str()));
            return ;
        }
        
        p_record->userid=ssch_imp.m_userid;
        record_cache->UpdateLocalPlayRecord(*p_record);
        delete p_record;
    }
    else if(type == "online" || type == "long" )
    {
        string err ,message;
        if(cmd->get_cmd_string("notified",message)){
            PlayRecordKey  video ;
            string version;
            uint64_t userid,movieid,charge;
            if(!cmd->get_cmd_ulvalue("userid",userid)){
                task.resp = new string(err_json("no userid"));
                return ;
            }
            if(!cmd->get_cmd_ulvalue("movieid",movieid)){
                task.resp = new string(err_json("no movieid"));
                return ;
            }
            if(!cmd->get_cmd_string("version",version)){
                task.resp = new string(err_json("no version"));
                return ;
            }
            if(!cmd->get_cmd_ulvalue("charge",charge)){
                task.resp = new string(err_json("no charge"));
                return ;
            }
            video.type = VIEW_TYPE_LONG;
            video.charge = (ChargeType)charge;
            video.movieid = movieid;
            video.gcid="";
            video.url_hash="";
            record_cache->NotifiedOneVodRecord(userid,video,version);
        }else{
                string err ;
                VodPlayRecord* p_record = VodPlayRecord::TryCreate(cmd,err);
                if(p_record == NULL)
                {
                    task.resp = new string(err_json(err.c_str()));
                    return ;
                }
                
                p_record->userid=ssch_imp.m_userid;
                unsigned long timetake ;
                {
                    Timedelt  gettime(timetake);
                    record_cache->UpdateOnlinePlayRecord(p_record);
                }
                delete p_record;
                LOG4CPLUS_DEBUG(logger,"it takes "<<timetake<<"time");
        }
        
    }
    else if( type == "short" ){
        string err ;
        ShortPlayRecord *p_record = ShortPlayRecord::TryCreate(cmd,err);
        if(p_record == NULL)
        {
            task.resp = new string(err_json(err.c_str()));
            return ;
        }
        p_record->userid = ssch_imp.m_userid;
        {
            record_cache->UpdateOnlinePlayRecord(static_cast<PlayRecord *>(p_record));
        }
        delete p_record;
    }
    else if( type == "music")
    {
        string err ;
        MusicPlayRecord *p_record = MusicPlayRecord::TryCreate(cmd,err);
        if(p_record == NULL)
        {
            task.resp = new string(err_json(err.c_str()));
            return ;
        }
        p_record->userid = ssch_imp.m_userid;
        {
            record_cache->UpdateOnlinePlayRecord(static_cast<PlayRecord *>(p_record));
        }
        delete p_record;    
    }
    else if(type == "cloud"){
        string err ;
        CloudPlayRecord* p_record = CloudPlayRecord::TryCreate(cmd,err);
        if(p_record == NULL)
        {
            task.resp = new string(err_json(err.c_str()));
            return ;
        }

        p_record->userid=ssch_imp.m_userid;
        record_cache->UpdateCloudPlayRecord(*p_record);
        delete p_record;
    }
    else{
        task.resp = new string(err_json("type err"));
        return;
    }

    stringstream ss;
    vector<json_pair> vec ;
    vec.push_back(json_pair("code",(_u64)0));
    vec.push_back(json_pair("info","ok"));
    make_json(ss,NULL,0,vec);
    task.resp = new string(ss.str());
}

void TaskProcessThreads::store_record(PipeEvent& task)
{
    static StoreCache *store_record = StoreCache::GetInstance(StoreCache::STORE_INST);
    static StoreCache *subscibe_record = StoreCache::GetInstance(StoreCache::SUBSCRIBE_INST);
    HttpCmd *cmd = (HttpCmd*)task.m_data ; 
    //�ڲ�������ĸ�svr����  store or subscribe
    string svr;
    if(!cmd->get_cmd_string("svr",svr)){
        task.resp = new string(err_json("no svr field"));
        return ;
    }
    
    string notified;
    if(cmd->get_cmd_string("notified",notified)){
        if( svr=="store" ){
                store_record->NotifiedAllRecord(StoreCache::STORE_INST);
        }else if( svr=="subscribe" ){
                subscibe_record->NotifiedAllRecord(StoreCache::SUBSCRIBE_INST);
        }
    }else{
        uint64_t userid;
        if(!cmd->get_cmd_ulvalue("userid",userid)){
            task.resp = new string(err_json("no userid"));
            return ;
        }
        uint64_t movieid;
        if(!cmd->get_cmd_ulvalue("movieid",movieid)){
            task.resp = new string(err_json("no movieid"));
            return ;
        }
        uint64_t charge;
        if(!cmd->get_cmd_ulvalue("charge",charge)){
            task.resp = new string(err_json("no charge"));
            return ;
        }

        PlayRecordKey  key;
        key.movieid = movieid;
        key.gcid="";
        key.charge=(ChargeType)charge;
        key.url_hash="";
        key.type = VIEW_TYPE_LONG;
        StoreKey  store_key(userid,key);
        if( svr=="store" ){
          store_record->AddStoreRecord(store_key);
        }else{
          subscibe_record->AddStoreRecord(store_key);
        }
    }
    
    stringstream ss;
    ss<<"{";
    ToJsonPair(0,ss,"code");
    ss<<",";
    ToJsonPair("ok",ss,"info");
    ss<<"}";
    task.resp = new string(ss.str());
}

void TaskProcessThreads::delete_record(PipeEvent& task,const SessionCacheImp& ssch_imp)
{
    
    static RecordCache* record_cache = RecordCache::GetInstance();
    HttpCmd  *cmd = (HttpCmd*)task.m_data ;

    string platform ;
    if(!cmd->get_cmd_string("devicetype",platform))
    {
        task.resp = new string(err_json("err or no devicetype"));
        return ;    
    }
    
    string appversion;
    if(!cmd->get_cmd_string("productversion",appversion))
    {
        task.resp = new string(err_json("err or no productversion"));
        return ;        
    }

    string app_name;
    if(!cmd->get_cmd_string("product",app_name))
    {
           task.resp = new string(err_json("err app name"));
           return ;    
    }

    uint64_t  ver=1;
    if(!cmd->get_cmd_ulvalue("protocolver",ver)){
            ver=1;
    }
    
    PlayRecordKey video;
    //��Э�� ����Ƶ������շ�
    if(1==ver){
        uint64_t  movieid;
        if(!cmd->get_cmd_ulvalue("movieid",movieid))
        {
            task.resp = new string(err_json("no movieid"));
            return ;
        }
        // �ϵİ汾��֧�ֳ���Ƶ ���ɾ��
        video.type = VIEW_TYPE_LONG;
        video.charge = VIDEO_FREE;
        video.movieid = movieid;
        video.gcid="";
        video.url_hash="";
    }else if(2==ver){ //��Э��
        string viewtype;
        if(!cmd->get_cmd_string("viewtype",viewtype)){
            task.resp = new string(err_json("no viewtype"));
            return ;
        }
        std::map<string,int>::iterator iter=record_type_map.find(viewtype);
        if(iter == record_type_map.end())
        {
            task.resp = new string(err_json("err viewtype"));
            return ;
        }
        video.type = (PlayRecordType)iter->second;
        uint64_t videouid=0;
        string gcid;
        string urlhash;

        if("long"==viewtype || "short"==viewtype || "online"==viewtype || "music"==viewtype){
            uint64_t charge;            
            if(!cmd->get_cmd_ulvalue("charge",charge)){
                task.resp = new string("no charge ");
                return ;
            }
            video.charge=(ChargeType)charge;

        }
        if("long"==viewtype || "online"==viewtype){
            if(!cmd->get_cmd_ulvalue("movieid",videouid)){
                task.resp = new string(err_json("no movieid"));
                return ;
            }
        }else if("short"==viewtype){
            if(!cmd->get_cmd_ulvalue("videoid",videouid)){
                task.resp = new string(err_json("no videoid"));
                return ;
            }   
        }else if("music"==viewtype){
            if(!cmd->get_cmd_ulvalue("albumid",videouid)){
                task.resp = new string(err_json("no albumid"));
                return ;
            }
        }else if("local"==viewtype || "cloud"==viewtype ){
            if(!cmd->get_cmd_string("gcid",gcid)){
                task.resp = new string(err_json("no gcid"));
                return ;
            }
            if("cloud"==viewtype){
                if(!cmd->get_cmd_string("urlhash",urlhash)){
                    task.resp = new string(err_json("no urlhash"));
                    return ;
                }
                gcid="";
            }
        }
        else{
            LOG4CPLUS_INFO(logger,"delete unkown type");
        }
        video.movieid = videouid;
        video.gcid = gcid;
        video.url_hash = urlhash;
    }else{
        task.resp = new string(err_json("err protocolver"));
        return ;
    }
    record_cache->DeleteOneVodRecord(ssch_imp.m_userid, video);
    
    stringstream ss;
    ss<<"{";
    ToJsonPair(0,ss,"code");
    ss<<",";
    ToJsonPair("ok",ss,"info");
    ss<<"}";
    task.resp = new string(ss.str());
}

void TaskProcessThreads::clear_record(PipeEvent& task,const SessionCacheImp& ssch_imp)
{
    static RecordCache* record_cache = RecordCache::GetInstance();
    record_cache->Clear(ssch_imp.m_userid);

    
    stringstream ss;
    ss<<"{";
    ToJsonPair(0,ss,"code");
    ss<<",";
    ToJsonPair("ok",ss,"info");
    ss<<"}";
    task.resp = new string(ss.str());
    return ;
}


string TaskProcessThreads::err_json(const char *pinfo)
{
    stringstream ss;
    ss<<"{";
    ToJsonPair(-1,ss,"code");
    if(pinfo)
    {
        ss<<",";
        ToJsonPair(static_cast<const char*>(pinfo),ss,"info");
        ss<<"}";
    }
    return ss.str();
}

void TaskProcessThreads::return_result(PipeEvent& task)
{
    task.m_type = PipeEvent::PIPE_TYPE_CLIENT_RESP;
    LOG4CPLUS_DEBUG(logger, "resp one task: " << task.dump());
    if(!g_client_service->add_pipe_event(task))
    {
        LOG4CPLUS_ERROR(logger, "add_PipeEvent to io server failed.");
           
        if(task.m_data != NULL) 
        {
            delete ((HttpCmd*)task.m_data);
            task.m_data = NULL;
        }
     }
}


void TaskProcessThreads::make_call_back(PipeEvent& task)
{
    HttpCmd *cmd = (HttpCmd*)task.m_data ;
       
    string callback ;
    string obj;
    if(cmd)
    {
        if(cmd->get_cmd_string("obj",obj))
        {
            obj = obj + "=" + (*task.resp);
        }
        if(cmd->get_cmd_string("callback",callback))
        {
            callback = callback + "(" + (*task.resp) + ")";
        }
    }
    string retstr;
    if(task.resp){
        retstr = *task.resp;
    }
    delete task.resp;
    if( !obj.empty())
        retstr = obj;
    else if(!callback.empty())
        retstr = callback;
    task.resp = new string(retstr);
}


void TaskProcessThreads::return_error(PipeEvent& task,const string& err)
{
    if(task.resp != NULL) 
    {
        delete task.resp;
        task.resp = NULL;
    }
    task.resp = new string(err);
    return_result(task);
}


