#ifndef __SESSION_THREAD_DATA_H_20100713_1522
#define  __SESSION_THREAD_DATA_H_20100713_1522
#include "common/IOEventServer.h"
#include "common/SeqNOGenerator.h"
#include "server/Datagram.h"
#include "client/Command.h"

#include <string>

using std::string;

class AddSession
{
public:
    int m_fd;
    int m_event_type;
    HttpCmd *m_wait_deal_cmd;
    _u32 m_app_type;
};


class PipeEvent
{
public:
    const static int PIPE_TYPE_ADD_SESSION = 0;
    const static int PIPE_TYPE_CLIENT_QUERY = 1;
    const static int PIPE_TYPE_CLIENT_CONF = 2;
    const static int PIPE_TYPE_CLIENT_REPORT = 3;
    const static int PIPE_TYPE_CLIENT_DELETE = 4;
    const static int PIPE_TYPE_CLIENT_CLEAR = 5;
    const static int PIPE_TYPE_CLIENT_VERIFY = 6;
    const static int PIPE_TYPE_CLIENT_RESP = 7;
    const static int PIPE_TYPE_CLIENT_PAGE = 8;
    const static int PIPE_TYPE_CLIENT_STORE_REPORT = 9 ;
    const static int PIPE_TYPE_WRONG_CMD = 0xffffffff ;

    const static int PIPE_TYPE_FLAG = 0xF0000000;
    const static int PIPE_TYPE_WITHOUT_FLAG = 0x0FFFFFFF;
    
    const static int PIPE_TYPE_FLAG_NO_NEED_VERIFY = 0x10000000;    
    const static int PIPE_TYPE_FLAG_SERVER_MESSAGE = 0x20000000;

    const static int STATUS_TIMEOUT = -1;
    const static int STATUS_QUEUE = 0;
    const static int STATUS_ABORT = 1;
    const static int STATUS_DONE = 3;
public:
    PipeEvent(int type = PIPE_TYPE_ADD_SESSION)
    {
        m_fd = 0;
        m_data = NULL;
        m_type = type;
        m_thread_type = 0;
        status = STATUS_QUEUE;
        timestamp = 0;
        seq = 0;
        resp = NULL;
    }

    PipeEvent(const PipeEvent &event)
    {
        m_type = event.m_type;
        m_fd = event.m_fd;
        m_thread_type = event.m_thread_type;
        m_data = event.m_data;
        timestamp = event.timestamp;
        status = event.status;
        seq = event.seq;
        resp = event.resp;
    }

    PipeEvent & operator = (const PipeEvent &event)
    {
        m_type = event.m_type;
        m_fd = event.m_fd;
        m_thread_type = event.m_thread_type;
        m_data = event.m_data;
        timestamp = event.timestamp;
        status = event.status;
        seq = event.seq;
        resp = event.resp;
        return *this;
    }
    
    int m_type;
    int m_fd;
    _u32 m_thread_type;
    seq_t seq;
    void *m_data;
    int m_data_len;
    time_t timestamp;
    int status;
    string* resp;
    
public:
    string dump() const
    {
        stringstream out;
        out << "type=" << m_type << ", "
            << "fd=" << m_fd << ", "
            << "m_thread_type=" << m_thread_type << ", ";
            
        out << "timestamp=" << timestamp << ", ";
        out << "seq=" << seq << ", ";
        if(m_data){
            out << "m_data=" << m_data << ", ";
        }else{
            out << "m_data=NULL" << ", ";
        }
        out << "status=" << status;
        return out.str();
    }
};

#endif

