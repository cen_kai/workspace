//============================================================
// Timer.h : interface of class Timer and ITimerTask
//                           (thunder P2P upload server)
// Author: JeffLuo
// Created: 2006-08-31
//============================================================

#ifndef __TIMER_TASK_H_20060831_23
#define  __TIMER_TASK_H_20060831_23

#include <string>
#include <vector>
#include "SDThread.h"

using namespace std;

class ITimerTask
{
public:
    virtual ~ITimerTask() { }

    virtual void exec_timer_task() = 0;

    virtual string get_task_name() const = 0;

    virtual int get_run_intervals() = 0;
};

class Timer : public SDThread
{
public:
    Timer(int check_interval);

    bool register_timer_task(ITimerTask* task);
    bool unregister_timer_task(ITimerTask* task);

    void stop_timer() { request_stop(); }

protected:
    virtual void run();

private:    
    vector<ITimerTask*> m_timer_task_list;    
};

#endif // #ifndef __TIMER_TASK_H_20060831_23

