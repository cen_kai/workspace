//==============================================================
// LRUHashtable.h : interface of class LRUHashtable
//               (Least Recent Used(LRU) hashtable)
//
// Author:  Jeffluo, qiuzhicong
// Created: 2006-09-23
//===============================================================

#ifndef __LRU_HASHTABLE_H_20060923_12
#define  __LRU_HASHTABLE_H_20060923_12

#include <ext/hash_map>
#include "common/common.h"
#include <vector>

using namespace __gnu_cxx;
using std::vector;

namespace __gnu_cxx
{
template <> struct hash<_u64> {
    size_t operator()(_u64 _x) const { return (_u32)((_x >> 32) & 0xFFFFFFFF) ^ (_u32)(_x & 0xFFFFFFFF); }
};
} // namespace __gnu_cxx

template <class _Key, class _Data, class _HashFcn  = hash<_Key> >
class LRUHashtable
{
public:
    typedef  _Key key_type;
    typedef _Data data_type;    

private:
    struct DataExt;

    typedef hash_map<key_type, DataExt, _HashFcn> hashtable;
    typedef pair<const key_type, DataExt> node_type;    

    struct DataExt
    {
        data_type m_real_data;
        node_type* m_prev;
        node_type* m_next;
    };

public:
    LRUHashtable(int list_size) : m_elements_ht(list_size), m_max_size(list_size), m_head(NULL), m_tail(NULL) { }

    bool add_element(const key_type& key, const data_type& data);
    bool get_element(const key_type& key, data_type& data);
    bool get_most_recent_element(data_type& data);
    bool get_least_recent_element(data_type& data);
    _u32 size() const { return m_elements_ht.size(); }
    void clear();
    
    // this function is for debug purpose, ordered by most-recent ordering
    void get_ordered_keys(vector<key_type>& keys);    
private:
    hashtable m_elements_ht;
    _u32 m_max_size;
    node_type* m_head;
    node_type* m_tail;
};



template <class _Key, class _Data, class _HashFcn>
bool LRUHashtable<_Key, _Data, _HashFcn>::add_element(const key_type & key, const data_type & data)
{
    bool inserted = false;

    typename hashtable::iterator it = m_elements_ht.find(key); // find
    
    if (it == m_elements_ht.end()) // add new node
    {
        inserted = true;
        
        DataExt data_ext;
        data_ext.m_real_data = data;
        data_ext.m_prev = data_ext.m_next = NULL;

        pair<typename hashtable::iterator, bool> ins_result
                = m_elements_ht.insert(node_type(key, data_ext)); // insert
        if (ins_result.second) // insert seccuss
        {
            ins_result.first->second.m_next = m_head;
            if (m_head)
                m_head->second.m_prev = &(*(ins_result.first));
            m_head = &(*(ins_result.first));;
            if (!m_tail)
                m_tail = m_head;
        }
        
    }
    else // add old node
    {
        inserted = false;
        it->second.m_real_data = data; // update the data

        if (m_head != &(*it)) // update the link
        {
            node_type* ptr_prev = it->second.m_prev;
            node_type* ptr_next = it->second.m_next;

            ptr_prev->second.m_next = ptr_next;    
            it->second.m_next = m_head;
            it->second.m_prev = NULL;
            m_head->second.m_prev = &(*it);
            m_head = &(*it);

            if (ptr_next)
                ptr_next->second.m_prev = ptr_prev;
            if (m_tail == &*(it))
                m_tail = ptr_prev;            
        }
    }

    if (m_elements_ht.size() > m_max_size) // remove the lease recent node
    {
        node_type* ptr = m_tail;
        m_tail = ptr->second.m_prev;
        m_tail->second.m_next = NULL;
        m_elements_ht.erase(ptr->first);
    }

    return inserted;
}

template <class _Key, class _Data, class _HashFcn>
bool LRUHashtable<_Key, _Data, _HashFcn>::get_element(const key_type& key, data_type& data)
{
    typename hashtable::iterator it = m_elements_ht.find(key); // find
    if (it == m_elements_ht.end()) // not found
        return false;

    data = it->second.m_real_data; // get the data
    
    if (m_head != &(*it)) // update the link
    {
        node_type* ptr_prev = it->second.m_prev;
        node_type* ptr_next = it->second.m_next;

        ptr_prev->second.m_next = ptr_next;    
        it->second.m_next = m_head;
        it->second.m_prev = NULL;
        m_head->second.m_prev = &(*it);
        m_head = &(*it);

        if (ptr_next)
            ptr_next->second.m_prev = ptr_prev;
        if (m_tail == &*(it))
            m_tail = ptr_prev;
    }

    return true;
}

template <class _Key, class _Data, class _HashFcn>
bool LRUHashtable<_Key, _Data, _HashFcn>::get_most_recent_element(data_type& data)
{
    if (m_head == NULL)
        return false;

    data = m_head->second.m_real_data;
    return true;
}

template <class _Key, class _Data, class _HashFcn>
bool LRUHashtable<_Key, _Data, _HashFcn>::get_least_recent_element(data_type& data)
{
    if (m_tail == NULL)
        return false;

    data = m_tail->second.m_real_data;
    return true;
}

template <class _Key, class _Data, class _HashFcn>
void LRUHashtable<_Key, _Data, _HashFcn>::clear()
{
    m_elements_ht.clear();
    m_head = NULL;
    m_tail = NULL;
}

template <class _Key, class _Data, class _HashFcn>
void LRUHashtable<_Key, _Data, _HashFcn>::get_ordered_keys(vector<key_type>& keys)
{
    keys.clear();
    for (node_type* p=m_head; p; p=p->second.m_next)
    {
        keys.push_back(p->first);
    }
}

#endif // #ifndef __LRU_HASHTABLE_H_20060923_12

