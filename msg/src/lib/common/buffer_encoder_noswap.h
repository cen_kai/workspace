/*
* Author: 
        yezhihui@xunlei.com
* Function: 
        二进制协议编解码模块
* Todo: 
        buffer越界检查，抛异常处理
*/

#ifndef BUFFER_ENCODER_NOSWAP_H
#define BUFFER_ENCODER_NOSWAP_H

#include <string.h>
#include <string>
#include "FileUtil.h"

using namespace std;

class LightString
{
public:
    LightString(uint8_t *buf, int len) : m_data_buf(buf), m_data_len(len){}
    uint8_t *m_data_buf;
    int m_data_len;
};

class EncoderNoSwap
{
public:
    EncoderNoSwap(char* buffer, size_t size);
    ~EncoderNoSwap();

    template<typename T>
    EncoderNoSwap& operator&(T& t)
    {
        this->save(t);
        return *this;
    }

    template<typename T>
    void save(T& t)
    {
        t.serialize(*this);
    }

    void save(int n);
    void save(int16_t n);
    void save(int64_t n);
    void save(char n);
    void save(uint8_t n);
    void save(uint16_t n);
    void save(uint32_t n);
    void save(uint64_t n);
    void save(string& str);
    void save(const string& str);
    void save(const char *str);
    void save(double n);
    void save(CIDWrapper &cid);
    void save(const CIDWrapper &cid);
    void save(LightString &cid);
    void save(const LightString &cid);

    template<typename T>
    void operator<<(const T& t)
    {
        memcpy(_buffer+_pos, &t, sizeof(t));
        _pos+=sizeof(t);
    }
    void operator<<(const string& str)
    {
        memcpy(_buffer+_pos, str.c_str(), str.length());
        _pos+=str.length();
    }

public:
    char* _buffer;
    size_t _size;
    size_t _pos;
};

class DecoderNoSwap
{
public:
    DecoderNoSwap(char* buffer, size_t size);
    ~DecoderNoSwap();
    
    template<typename T>
    DecoderNoSwap& operator&(T& t)
    {
        this->load(t);
        return *this;
    }

    template<typename T>
    void load(T& t)
    {
        t.serialize(*this);
    }

    void load(int &n);
    void load(int16_t &n);
    void load(int64_t &n);
    void load(char &n);
    void load(uint8_t &n);
    void load(uint16_t &n);
    void load(uint32_t &n);
    void load(uint64_t &n);
    void load(string& str);
    void load(char *str);
    void load(double &n);
    void load(CIDWrapper &cid);
    void load(LightString &cid);

    template<typename T>
    void operator>>(T& t)
    {
        memcpy(&t, _buffer+_pos, sizeof(t));
        _pos+=sizeof(t);
    }

public:
    char* _buffer;
    size_t _size;
    size_t _pos;
};

#endif


