#include <exception>
#include "SDThread.h"

using namespace std;

IMPL_LOGGER(SDThread, logger)

SDThread::SDThread(int stack_size)
{
    m_thread_id = 0;
    m_stack_size = stack_size;
    m_should_stop = false;
    m_is_started = false;

    m_detachable = false;

    m_thread_counter = NULL;
}

SDThread::~SDThread()
{

}

bool SDThread::start(bool force_restart)
{
    if(m_is_started && !force_restart)
        return false;

    pthread_attr_t attr;
    pthread_attr_init(&attr);

    // set stack size
    if(m_stack_size > 0)
        pthread_attr_setstacksize(&attr, m_stack_size);
    if(m_detachable)
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); 

    if(0 != pthread_create(&m_thread_id, &attr, threadProc, (void*)this))
        return false;

    m_is_started = true;

    pthread_attr_destroy(&attr);

    return true;
}

void SDThread::wait_terminate()
{
    if(m_detachable)
        return;
    else
        pthread_join(m_thread_id, NULL);
}

void* SDThread::threadProc(void* param)
{
    SDThread* me = (SDThread*) param;
    //modify by sch
#if 0
    try
    {
        me->run();
    }
    catch (std::exception& e) 
    {
        // report to monitor
        LOG4CPLUS_ERROR(logger, "Thread exception: "<<e.what());
    }
    catch(...)
    {
        // report to monitor
        LOG4CPLUS_ERROR(logger, "Unknown exception in thread");
    }
#endif

    me->run();
    me->m_is_started = false;
    
    return NULL;
}
