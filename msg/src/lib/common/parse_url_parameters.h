#ifndef __PARSE_URL_PARAMETERS_H_201201041730__
#define __PARSE_URL_PARAMETERS_H_201201041730__
#include <stdlib.h>
#include <map>

using namespace std;
enum cgi_param_type{
    APPLICATION_WWW_FORM_URLENCODED=0,
    MULTIPART_FORM_DATA,
    APPLICATION_JSON,
    TEXT_XML,
    CGI_PARAM_NOTYPE
};
extern int read_cgi_input(map<string, string> &key_value, const char *input,cgi_param_type type);

#endif

