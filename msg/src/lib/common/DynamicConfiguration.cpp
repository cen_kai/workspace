//============================================================
// DynamicConfiguration.cpp : implementation of class DynamicConfiguration
//                           (dynamic configuration based on filepath of key)
// Author: JeffLuo
// Created: 2006-11-25
//============================================================

#include "DynamicConfiguration.h"
#include "Utility.h"

IMPL_LOGGER(DynamicConfiguration, logger);

const char* DynamicConfiguration::DEFAULT_CONF_FILE_SUFFIX = ".conf";

DynamicConfiguration::DynamicConfiguration(const char * root_conf_path, const char * default_dir_conf_filename)
{
    m_root_cfg.m_loaded = true;

    ConfigReader* cfg_reader = new ConfigReader(root_conf_path);
    if(!cfg_reader->load())
    {
        LOG4CPLUS_THIS_WARN(logger, "failed to load root dynamic config file: " << root_conf_path);

//        return;
    }

    m_root_cfg.m_cfg_reader = cfg_reader;

    if(default_dir_conf_filename)
        m_dir_conf_filename = default_dir_conf_filename;
    else
        m_dir_conf_filename = Utility::extract_filename(root_conf_path);

    if(m_dir_conf_filename.empty())
    {
        LOG4CPLUS_THIS_WARN(logger, "dir default config filename is empty!!");
    }
}

bool DynamicConfiguration::add_config_file(const char * conf_file_path, bool use_parent_conf)
{
    if(conf_file_path == NULL)
        return false;

    string path = conf_file_path;
    if(!Utility::ends_with(path.c_str(), DEFAULT_CONF_FILE_SUFFIX))
        path += DEFAULT_CONF_FILE_SUFFIX;    
    LOG4CPLUS_THIS_DEBUG(logger, "try to load dynamic config file '" << path << "'");
    
    if(m_cfg_list.find(path) != m_cfg_list.end())
    {
        LOG4CPLUS_THIS_INFO(logger, "config file '" << conf_file_path << "' already loaded into memory.");
        return false;
    }

    DynaConfigItem* dyna_conf = new DynaConfigItem();
    dyna_conf->m_loaded = true;
    if(Utility::file_exists(path.c_str()))
    {
        ConfigReader* cfg_reader = new ConfigReader(path.c_str());
        if(!cfg_reader->load())
        {        
            LOG4CPLUS_THIS_WARN(logger, "failed to load config file: " << path);

            delete cfg_reader;
        }
        else
        {
            dyna_conf->m_cfg_reader = cfg_reader;
        }
    }
    else
    {
        LOG4CPLUS_THIS_DEBUG(logger, "config file '" << path << "' not exists!");
    }

    pair<map_iterator, bool > result = m_cfg_list.insert(make_pair(path, dyna_conf));
    if(!result.second)
    {
        LOG4CPLUS_THIS_WARN(logger, "!! impossible result when insert item to map.");
        return false;
    }

    if(!use_parent_conf || m_dir_conf_filename.empty())
        return true;

    LOG4CPLUS_THIS_DEBUG(logger, "try to load parents of dynamic config file '" << path << "'");
    DynaConfigItem** child_conf = &(result.first->second);
    // load parents conf recursively
    load_parents_conf(path.c_str(), child_conf);

    return true;
}

bool DynamicConfiguration::update_config_file(const char * conf_file_path, bool /* use_parent_conf */)
{
    if(conf_file_path == NULL)
        return false;

    string path = conf_file_path;
    if(!Utility::ends_with(path.c_str(), DEFAULT_CONF_FILE_SUFFIX))
        path += DEFAULT_CONF_FILE_SUFFIX;    
    LOG4CPLUS_THIS_DEBUG(logger, "try to update dynamic config file '" << path << "'");
    
    map_iterator it = m_cfg_list.find(path);
    if(it == m_cfg_list.end())
    {
        // config file not found !!
        LOG4CPLUS_THIS_WARN(logger, "dynamic config file '" << path << "' not loaded before!!");
        return false;
    }

    DynaConfigItem* dyna_conf = it->second;
    if(dyna_conf->m_cfg_reader == NULL)
    {
        if(Utility::file_exists(path.c_str()))
        {
            ConfigReader* cfg_reader = new ConfigReader(path.c_str());
            if(!cfg_reader->load())
            {
                LOG4CPLUS_THIS_WARN(logger, "failed to load config file: " << path);
                delete cfg_reader;
            }
            else
            {
                dyna_conf->m_cfg_reader = cfg_reader;
            }
        }
        else
        {
            LOG4CPLUS_THIS_DEBUG(logger, "config file '" << path << "' not exists!");
        }        
    }
    else
    {
        bool succ = dyna_conf->m_cfg_reader->load();
        if(!succ)
        {
            LOG4CPLUS_THIS_WARN(logger, "reload dynamic confile file '" << path << "' failed!");
        }
    }
    
    return true;
}


void DynamicConfiguration::load_parents_conf(const char * child_conf_path, DynaConfigItem * * child_conf)
{
    if(child_conf_path == NULL || child_conf == NULL)
        return;

    string cur_path = child_conf_path;
    string parent_dir = Utility::extract_parent_dir(child_conf_path);
    while(parent_dir != "/")
    {
        string parent_conf_path(parent_dir + m_dir_conf_filename);
        if(Utility::file_exists(parent_conf_path.c_str()))
        {
            DynaConfigItem* parent_item = NULL;
        
            map_iterator it = m_dir_cfg_list.find(parent_conf_path);
            if(it != m_dir_cfg_list.end())
            {
                // already loaded
                parent_item = it->second;
            }
            else
            {
                // not loaded yet
                ConfigReader* cfg_reader = new ConfigReader(parent_conf_path.c_str());
                if(!cfg_reader->load())
                {
                    LOG4CPLUS_THIS_WARN(logger, "failed to load config file: " << parent_conf_path);
                    delete cfg_reader;
                }
                else
                {
                    parent_item = new DynaConfigItem();
                    parent_item->m_loaded = true;
                    parent_item->m_cfg_reader = cfg_reader;
                    
                    LOG4CPLUS_THIS_DEBUG(logger, "config item '" << child_conf_path << "' parent is '" << parent_conf_path << "'");
                    m_dir_cfg_list.insert(make_pair(parent_conf_path, parent_item));
                }
            }

            if(parent_item != NULL)
            {
                (*child_conf)->m_parent = parent_item;

                load_parents_conf(parent_dir.c_str(), &parent_item);
                return;
            }
        }        

        cur_path = parent_dir;
        parent_dir = Utility::extract_parent_dir(cur_path.c_str());
    }

    LOG4CPLUS_THIS_DEBUG(logger, "config item '" << child_conf_path << "' parent is 'root'");
    // up to root dir (no conf loaded)
    (*child_conf)->m_parent = &m_root_cfg;
}

string DynamicConfiguration::get_conf_string(const char * conf_path, const char * key, const char * default_value)
{
    if(key == NULL)
        return default_value ? default_value : "";

    if(conf_path == NULL)
    {
        // resort to root conf
        if(m_root_cfg.m_cfg_reader == NULL)
            return default_value ? default_value : "";
        else
            return m_root_cfg.m_cfg_reader->get_string(key, default_value);
    }

    string path = conf_path;
    if(!Utility::ends_with(path.c_str(), DEFAULT_CONF_FILE_SUFFIX))
        path += DEFAULT_CONF_FILE_SUFFIX;
    
    map_iterator it = m_cfg_list.find(path);
    if(it == m_cfg_list.end())
    {
        // conf item not found !!
        LOG4CPLUS_THIS_WARN(logger, "dynamic config item for '" << path << "' not found!!");
        return default_value ? default_value : "";
    }

    string ret_val;
    bool fetched =false;
    DynaConfigItem* dyna_conf = it->second;
    while(!fetched)
    {
        if(dyna_conf->m_cfg_reader != NULL)
        {
            ret_val = dyna_conf->m_cfg_reader->get_string(key, default_value, false);
            if((default_value != NULL && ret_val != default_value) || (default_value == NULL && !ret_val.empty()))
            {
                LOG4CPLUS_THIS_DEBUG(logger, "found config item in context '" << dyna_conf->m_cfg_reader->config_file() << "'");
                fetched = true;
            }
        }

        if(!fetched)
        {
            if(dyna_conf->m_parent != NULL)
                dyna_conf = dyna_conf->m_parent;
            else
                break;
        }
    }

    LOG4CPLUS_THIS_DEBUG(logger, "config item: " << key << "=" << ret_val << " in context '" << conf_path << "'");

    return ret_val;
}

int DynamicConfiguration::get_conf_int(const char * conf_path, const char * key, int default_value)
{
    if(key == NULL)
        return default_value;

    if(conf_path == NULL)
    {
        // resort to root conf
        if(m_root_cfg.m_cfg_reader == NULL)
            return default_value;
        else
            return m_root_cfg.m_cfg_reader->get_int(key, default_value);
    }

    string path = conf_path;
    if(!Utility::ends_with(path.c_str(), DEFAULT_CONF_FILE_SUFFIX))
        path += DEFAULT_CONF_FILE_SUFFIX;
    
    map_iterator it = m_cfg_list.find(path);
    if(it == m_cfg_list.end())
    {
        // conf item not found !!
        LOG4CPLUS_THIS_WARN(logger, "dynamic config item for '" << path << "' not found!!");
        return default_value;
    }

    int ret_val = default_value;
    bool fetched =false;
    DynaConfigItem* dyna_conf = it->second;
    while(!fetched)
    {
        if(dyna_conf->m_cfg_reader != NULL)
        {
            ret_val = dyna_conf->m_cfg_reader->get_int(key, default_value, false);
            if(ret_val != default_value)
            {
                LOG4CPLUS_THIS_DEBUG(logger, "found config item in context '" << dyna_conf->m_cfg_reader->config_file() << "'");            
                fetched = true;
            }
        }

        if(!fetched)
        {
            if(dyna_conf->m_parent != NULL)
                dyna_conf = dyna_conf->m_parent;
            else
                break;
        }
    }

    LOG4CPLUS_THIS_DEBUG(logger, "config item: " << key << "=" << ret_val << " in context '" << conf_path << "'");    

    return ret_val;
}


