//============================================================
// common.h : common includes files, type definitions, etc
//                          
// Author: JeffLuo
// Created: 2006-10-21
//============================================================

#ifndef _COMMON_HEADER_H_20061021_16
#define  _COMMON_HEADER_H_20061021_16

/**  common include files **/
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string>


/** typedefs  **/
typedef uint64_t _u64;
typedef uint32_t _u32;
typedef uint16_t _u16;
typedef int64_t __int64;
typedef int __int32;
typedef uint8_t byte;

#define DIST_SERVER_VERSION    "1.0.0.20"
#define COLL_SERVER_VERSION    "1.0.0.20"
#define DIST_SERVER_VER_MAJOR    1
#define DIST_SERVER_VER_BUILD    20
#define DIST_SERVER_ID 2

const int REPORT_FROM_XUNLEI = 0;
const int REPORT_FROM_PARTER = 1;

/**  macros **/
#define MAX_CID_LEN        20
#define MAX_PATH_LEN    512
#define MAX_PEERID_LEN    24
#define MAX_FILE_BLOCKS    128
#define COMMON_FILE_MODE    S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH

/** file type **/
#define FILE_INDEX             10001
#define FILE_PARTS            10002

/** ver **/
#define HEAD_VER_10001         10001
#define HEAD_VER_10002         10002


#define PART_FILE_MAGIC    0x1327B5C4

#define NORMALFILE            -2
#define HAVE_PART_FD        1234567890

#define MAX_UDP_PACKET_SIZE  2048

#ifdef __LOG_OPEN__
#define LOG_OPEN(filename,avg...) true ? ({ \
    int fd = open(filename, avg); \
    LOG4CPLUS_WARN(logger, "file:" << __FILE__ << ",line:" << __LINE__ <<", open file:" << filename << ", fd:" << fd); \
    (int)fd; \
}) : 0
#else
#define LOG_OPEN(filename,avg...) open(filename, avg)
#endif

#ifdef __LOG_CLOSE__
#define LOG_CLOSE(fd) true ? ({ \
    int ret = close(fd); \
    LOG4CPLUS_WARN(logger, "file:" << __FILE__ << ",line:" << __LINE__ << ", close fd:" << fd); \
    (int)ret; \
}) : 0
#else
#define LOG_CLOSE(fd) close(fd)
#endif

#pragma pack (4) 
struct HeadFile
{
    int m_magic_num;
    int m_ver;
    int m_type;
    int m_headlength;
};

struct PartFile_V10001
{
    int m_part_num;  //begin 0
    uint8_t m_cid[MAX_CID_LEN];
    uint8_t m_gcid[MAX_CID_LEN];
    _u64 m_total_filesize;
    _u64 m_begin_pos;
    _u64 m_cur_filesize;
    int m_bcid_num;    // 只有本片的BCID
};

struct PartFile_V10002
{
    int m_part_num;  //begin 0
    uint8_t m_cid[MAX_CID_LEN];
    uint8_t m_gcid[MAX_CID_LEN];
    _u64 m_total_filesize;
    _u64 m_begin_pos;
    _u64 m_cur_filesize;
    int m_bcid_num;   // 所有分片的BCID
};
#pragma pack ()

struct ServerAddr
{
    std::string m_ip;
    int m_port;
};

#endif // #ifndef _COMMON_HEADER_H_20061021_16

