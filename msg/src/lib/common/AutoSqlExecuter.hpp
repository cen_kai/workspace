#ifndef AUTO_SQL_EXECUTER__H
#define AUTO_SQL_EXECUTER__H

class AutoSqlExecuter{
public:
    AutoSqlExecuter(SDDBConnectionPool * pool, const string & sql, bool read=true)
    {
        _pool = pool;
        _records = NULL;
        _code = 0;
        _need_read = read;
        Execute(sql);
    }

    ~AutoSqlExecuter()
    {
        if(_records){
            _records->release();
            delete _records;
        }
        if(_pool && _mysql){
            _pool->release_sql(_mysql);
        }
    }

    bool Good() const
    {
        return _code == 0;
    }

	bool Empty()const
	{
		return _records->is_Null();
	}
	
    int Code() const
    {
        return _code;
    }
    
    SDRES_RESULT * Records() 
    {
        return _records;
    }
 
private:
    SDDBConnectionPool * _pool;
    int _code;
    SDRES_RESULT * _records;
    SDMYSQL * _mysql;
    bool _need_read;

    void Execute(const string & sql)
    {
        _mysql = _pool->get_sql(true);
        if(!_mysql){
            _code = -1;
            return;
        }
        if(!_mysql->query(sql.c_str())){
            _code = -2;
            return;
        }
        if(_need_read){
            _records = new SDRES_RESULT(_mysql);
        }
    }
};

#endif
