//***************************************************
// SDMutextLock.h : interface for pthread mutex lock
// JeffLuo, 2006-08-03
//***************************************************

#ifndef __SD_MUTEXTLOCK_H_20060803_11
#define  __SD_MUTEXTLOCK_H_20060803_11

#include <pthread.h>

class SDMutexLock
{
public:
    SDMutexLock()
    {
        pthread_mutex_init(&m_lock, NULL);
    }
    ~SDMutexLock()
    {
        pthread_mutex_destroy(&m_lock);
    }
    void lock()
    {
        pthread_mutex_lock(&m_lock);    
    }
    void unlock()
    {
        pthread_mutex_unlock(&m_lock);        
    }
    
    pthread_mutex_t raw_lock() const
    {
        return m_lock;
    }

    pthread_mutex_t* raw_lock_ptr()
    {
        return &m_lock;
    }    

private:
    pthread_mutex_t m_lock;
};

class SDAutoLock
{
public:
    SDAutoLock(const pthread_mutex_t& lock)
    {
        m_lock_p = const_cast<pthread_mutex_t*>(&lock);
        pthread_mutex_lock(m_lock_p);
    }
    SDAutoLock(const pthread_mutex_t* lock)
    {
        m_lock_p = const_cast<pthread_mutex_t*>(lock);
        pthread_mutex_lock(m_lock_p);
    }    

    ~SDAutoLock()
    {
        pthread_mutex_unlock(m_lock_p);
    }

    
private:
    pthread_mutex_t* m_lock_p;
};

//by tianming 20091211
class SDAutoRDLock
{
public:
    SDAutoRDLock(const pthread_rwlock_t& lock)
    {
        m_lock_p = const_cast<pthread_rwlock_t*>(&lock);
        pthread_rwlock_rdlock(m_lock_p);
    }
    SDAutoRDLock(const pthread_rwlock_t* lock)
    {
        m_lock_p = const_cast<pthread_rwlock_t*>(lock);
        pthread_rwlock_rdlock(m_lock_p);
    }    

    ~SDAutoRDLock()
    {
        pthread_rwlock_unlock(m_lock_p);
    }
    
private:
    pthread_rwlock_t* m_lock_p;
};

class SDAutoWRLock
{
public:
    SDAutoWRLock(const pthread_rwlock_t& lock)
    {
        m_lock_p = const_cast<pthread_rwlock_t*>(&lock);
        pthread_rwlock_wrlock(m_lock_p);
    }
    SDAutoWRLock(const pthread_rwlock_t* lock)
    {
        m_lock_p = const_cast<pthread_rwlock_t*>(lock);
        pthread_rwlock_wrlock(m_lock_p);
    }    

    ~SDAutoWRLock()
    {
        pthread_rwlock_unlock(m_lock_p);
    }
    
private:
    pthread_rwlock_t* m_lock_p;
};

#endif // #ifndef __SD_MUTEXTLOCK_H_20060803_11


