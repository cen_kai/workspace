/********************************************************************
    created:        2005/08/15
    created:        15:8:2005   12:32
    filename:         d:\work\newcvs\chub\impl\src\common\sdqueue.h
    file path:        d:\work\newcvs\chub\impl\src\common
    file base:        sdqueue
    file ext:        h
    author:            lifeng
    description:    this is a fixed size queue; class T had better be a pointer
*********************************************************************/
#ifndef SANDAI_C_QUEUE_H_200508151110
#define SANDAI_C_QUEUE_H_200508151110
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include <exception>
#include "common/SDMutexLock.h"
#include <set>
#include <iostream>
#include <vector>
using std::set;
using std::vector;
class SDIQueueInfo
{
public:
    virtual ~SDIQueueInfo() { };
    virtual int getCapacity() = 0;
    virtual int getSize() = 0;
    virtual int getMaxSize(bool resetMax = true) = 0;
};
template <class T> 
class SDQueue : public SDIQueueInfo{
    T*  m_buffer;
    int m_capacity;
    int m_start;
    int m_size;
    int m_max_size;
    pthread_mutex_t m_lock;
    pthread_cond_t    m_cond_not_full;
    pthread_cond_t    m_cond_not_empty;
public:
    SDQueue(int capacity)
    {
        m_buffer = NULL;
        m_start = 0;
        m_size = 0;
        m_max_size = 0;
        m_capacity = capacity;
        pthread_mutex_init(&m_lock, NULL);
        pthread_cond_init(&m_cond_not_full, NULL);
        pthread_cond_init(&m_cond_not_empty, NULL);
    }
    virtual ~SDQueue()
    {
        freeBuffer(m_buffer);
        pthread_mutex_destroy(&m_lock);
        pthread_cond_destroy(&m_cond_not_full);
        pthread_cond_destroy(&m_cond_not_empty);
    }
    bool pop(T& retVal, int waitMs)
    {
        bool ret_val = false;
        SDAutoLock lock(m_lock);
//        pthread_mutex_lock(&m_lock);
        if(!checkBuffer())
            return false;
        try
        {
            if(m_size == 0)
            {//empty
                if(waitMs == 0)
                {//wait infinitely
                    while(m_size == 0)
                        pthread_cond_wait(&m_cond_not_empty, &m_lock);
                }
                else
                {//wait for a given timeout
                    struct timespec timespot;
                    if(waitMs > 0)
                    {
                        getAbsTimeout(waitMs, timespot);
                    }
                    else
                    {//need not call gettimeofday
                        timespot.tv_sec = 0;
                        timespot.tv_nsec = 0;
                    }
                    pthread_cond_timedwait(&m_cond_not_empty, &m_lock, &timespot);
                }
            }
            if(m_size > 0)
            {
                retVal = m_buffer[m_start];
                ret_val = true;
                if(++m_start >= m_capacity)
                    m_start -= m_capacity;
                m_size --;
                pthread_cond_signal(&m_cond_not_full);
            }
        }
        catch (std::exception& ) {
            //exception
            ret_val = false;
        }
        return ret_val;
    }
    bool pop_ex(set<T>& retVal, int waitMs)
    {
        bool ret_val = false;
        SDAutoLock lock(m_lock);
        if(!checkBuffer())
            return false;
        try
        {
            if(m_size == 0)
            {//empty
                if(waitMs == 0)
                {//wait infinitely
                    while(m_size == 0)
                        pthread_cond_wait(&m_cond_not_empty, &m_lock);
                }
                else
                {//wait for a given timeout
                    struct timespec timespot;
                    if(waitMs > 0)
                    {
                        getAbsTimeout(waitMs, timespot);
                    }
                    else
                    {//need not call gettimeofday
                        timespot.tv_sec = 0;
                        timespot.tv_nsec = 0;
                    }
                    pthread_cond_timedwait(&m_cond_not_empty, &m_lock, &timespot);
                }
            }
            if(m_size > 0)
            {
                //modify by sunchanghai
                //retVal.clear();
                retVal.insert(m_buffer[m_start]);
                int i(1);
                int remain_i(i);
                for( ; i < m_size; i++)
                {    
                    int pos(m_start + i);
                    if(pos >= m_capacity)
                        pos -= m_capacity;
//modify by sunchanghai
#if 0
                    if(!m_buffer[m_start].same_file(m_buffer[pos]))
                    {
                        if(remain_i != i)
                        {
                            int remain_pos(m_start + remain_i);
                            if(remain_pos >= m_capacity)
                                remain_pos -= m_capacity;
                            m_buffer[remain_pos] = m_buffer[pos];
                        }
                        remain_i++;
                    }
                    else
#endif
                    {
                        retVal.insert(m_buffer[pos]);
                    }
                }
                if(++m_start >= m_capacity)
                    m_start -= m_capacity;
                m_size = remain_i - 1;
                ret_val = true;
                pthread_cond_signal(&m_cond_not_full);
            }
        }
        catch (std::exception& ) {
            //exception
            ret_val = false;
        }
        return ret_val;
    }
    //added by tianming,20071224
    bool pop_ext(vector<T>& retVal, int waitMs)
    {
        bool ret_val = false;
        SDAutoLock lock(m_lock);
        if(!checkBuffer())
            return false;
        try
        {
            if(m_size == 0)
            {//empty
                if(waitMs == 0)
                {//wait infinitely
                    while(m_size == 0)
                        pthread_cond_wait(&m_cond_not_empty, &m_lock);
                }
                else
                {//wait for a given timeout
                    struct timespec timespot;
                    if(waitMs > 0)
                    {
                        getAbsTimeout(waitMs, timespot);
                    }
                    else
                    {//need not call gettimeofday
                        timespot.tv_sec = 0;
                        timespot.tv_nsec = 0;
                    }
                    pthread_cond_timedwait(&m_cond_not_empty, &m_lock, &timespot);
                }
            }
            if(m_size > 0)
            {
                //++pop the sequence task,added by tianming,20071224
                retVal.clear();
                retVal.push_back(m_buffer[m_start]);
                ret_val = true;
                int counter = 1;
                for (int i = 1; i <= m_size - 1; ++i)
                {
                    int curpos = (m_start + i)%m_capacity;
                    int beforepos = (m_start + i - 1)%m_capacity;
                    //we should implement the adjacent function
                    if (m_buffer[curpos].adjacent(m_buffer[beforepos]))
                    {
                        retVal.push_back(m_buffer[curpos]);
                        ++counter;
                    }
                    else 
                    {
                        break;
                    }
                }
                m_start = (m_start + counter)%m_capacity;
                m_size -= counter;
                pthread_cond_signal(&m_cond_not_full);
                //--pop the sequence task,added by tianming,20071224
                /*
                retVal = m_buffer[m_start];
                ret_val = true;
                if(++m_start >= m_capacity)
                    m_start -= m_capacity;
                m_size --;
                pthread_cond_signal(&m_cond_not_full);
                */
            }
        }
        catch (std::exception& ) {
            //exception
            ret_val = false;
        }
        return ret_val;
    }
    bool pop_tail(T& retVal, int waitMs)
    {
        bool ret_val = false;
        SDAutoLock lock(m_lock);
        if(!checkBuffer())
            return false;
        try
        {
            if(m_size == 0)
            {//empty
                if(waitMs == 0)
                {//wait infinitely
                    while(m_size == 0)
                        pthread_cond_wait(&m_cond_not_empty, &m_lock);
                }
                else
                {//wait for a given timeout
                    struct timespec timespot;
                    if(waitMs > 0)
                    {
                        getAbsTimeout(waitMs, timespot);
                    }
                    else
                    {//need not call gettimeofday
                        timespot.tv_sec = 0;
                        timespot.tv_nsec = 0;
                    }
                    pthread_cond_timedwait(&m_cond_not_empty, &m_lock, &timespot);
                }
            }
            if(m_size > 0)
            {
                // �����׵�����ȴ�ʱ�䳬����ֵ��Ӷ���ȡ
                if(m_buffer[m_start].out_of_time())
                {
                    retVal = m_buffer[m_start];
                    if(++m_start >= m_capacity)
                        m_start -= m_capacity;
                }
                else
                {
                    int tail_pos = m_start + m_size - 1;
                    if(tail_pos >= m_capacity)
                        tail_pos -= m_capacity;
                    retVal = m_buffer[tail_pos];
                }
                ret_val = true;
                m_size--;
                pthread_cond_signal(&m_cond_not_full);
            }
        }
        catch (std::exception& ) {
            //exception
            ret_val = false;
        }
        return ret_val;
    }
    bool pop_tail_ex(set<T>& retVal, int waitMs)
    {
        bool ret_val = false;
        SDAutoLock lock(m_lock);
        if(!checkBuffer())
            return false;
        try
        {
            if(m_size == 0)
            {//empty
                if(waitMs == 0)
                {//wait infinitely
                    while(m_size == 0)
                        pthread_cond_wait(&m_cond_not_empty, &m_lock);
                }
                else
                {//wait for a given timeout
                    struct timespec timespot;
                    if(waitMs > 0)
                    {
                        getAbsTimeout(waitMs, timespot);
                    }
                    else
                    {//need not call gettimeofday
                        timespot.tv_sec = 0;
                        timespot.tv_nsec = 0;
                    }
                    pthread_cond_timedwait(&m_cond_not_empty, &m_lock, &timespot);
                }
            }
            if(m_size > 0)
            {
                retVal.clear();
                // �����׵�����ȴ�ʱ�䳬����ֵ��Ӷ���ȡ
                if(m_buffer[m_start].out_of_time())
                {
                    retVal.insert(m_buffer[m_start]);
                    int i(1);
                    int remain_i(i);
                    for( ; i < m_size; i++)
                    {    
                        int pos(m_start + i);
                        if(pos >= m_capacity)
                            pos -= m_capacity;
                        if(!m_buffer[m_start].same_file(m_buffer[pos]))
                        {
                            if(remain_i != i)
                            {
                                int remain_pos(m_start + remain_i);
                                if(remain_pos >= m_capacity)
                                    remain_pos -= m_capacity;
                                m_buffer[remain_pos] = m_buffer[pos];
                            }
                            remain_i++;
                        }
                        else
                        {
                            retVal.insert(m_buffer[pos]);
                        }
                    }
                    if(++m_start >= m_capacity)
                        m_start -= m_capacity;
                    m_size = remain_i - 1;
                }
                else
                {
                    int tail_pos = m_start + (--m_size);
                    if(tail_pos >= m_capacity)
                        tail_pos -= m_capacity;
                    retVal.insert(m_buffer[tail_pos]);
                    int i(0);
                    int remain_i(i);
                    for( ; i < m_size; i++)
                    {    
                        int pos(m_start + i);
                        if(pos >= m_capacity)
                            pos -= m_capacity;
                        if(!m_buffer[tail_pos].same_file(m_buffer[pos]))
                        {
                            if(remain_i != i)
                            {
                                int remain_pos(m_start + remain_i);
                                if(remain_pos >= m_capacity)
                                    remain_pos -= m_capacity;
                                m_buffer[remain_pos] = m_buffer[pos];
                            }
                            remain_i++;
                        }
                        else
                        {
                            retVal.insert(m_buffer[pos]);
                        }
                    }
                    m_size = remain_i;
                }
                ret_val = true;
                pthread_cond_signal(&m_cond_not_full);
            }
        }
        catch (std::exception& ) {
            //exception
            ret_val = false;
        }
        return ret_val;
    }
    bool push(const T& element, int waitMs)
    {
        bool retVal = false;
        try    
        {
            SDAutoLock lock(m_lock);
            if(!checkBuffer())
                return false;
            if(m_size >= m_capacity)
            {//full
                if(waitMs == 0)
                {//wait infinitely
                    while(m_size >= m_capacity)
                        pthread_cond_wait(&m_cond_not_full, &m_lock);
                }
                else
                {//wait for a given timeout
                    struct timespec timespot;
                    if(waitMs > 0)
                    {
                        getAbsTimeout(waitMs, timespot);
                    }
                    else
                    {//need not call gettimeofday
                        timespot.tv_sec = 0;
                        timespot.tv_nsec = 0;
                    }
                    pthread_cond_timedwait(&m_cond_not_full, &m_lock, &timespot);
                }
            }
            if(m_size < m_capacity)
            {
                int newpos = m_start + m_size;
                if(newpos >= m_capacity)
                    newpos -= m_capacity;
                m_buffer[newpos] = element;
                m_size ++;
                if(m_max_size < m_size)
                    m_max_size = m_size;
                pthread_cond_signal(&m_cond_not_empty);
                retVal = true;
            }
        }
        catch (std::exception&) {
            //exception
            retVal = false;
        }
        return retVal;
    }
    bool pop(T& retVal)
    {
        return pop(retVal, -1);
    }
    bool push(T element)
    {
        return push(element, -1);
    }
    bool push_ex(T element)
    {
        return push_ex(element, -1);
    }
    int getCapacity()
    {
        return m_capacity;
    }
    int getSize()
    {
        return m_size;
    }
    int getMaxSize(bool resetMax = true)
    {
        int maxSize = m_max_size;
        if(resetMax)
        {
            pthread_mutex_lock(&m_lock);    
            m_max_size = m_size;
            pthread_mutex_unlock(&m_lock);        
        }
        return maxSize;        
    }
    void export_subset_list(vector<T> &sub_list)
    {
        SDAutoLock lock(m_lock);
        if(!checkBuffer())
            return;
        try
        {
            if(m_size == 0)
            {//empty
                return;
            }
            if(m_size > 0)
            {
                for(int i(0); i < m_size; i++)
                {
                    int pos(m_start + i);
                    if(pos >= m_capacity)
                        pos -= m_capacity;
                    {
                        sub_list.push_back(m_buffer[pos]);
                    }
                }
            }
        }
        catch (std::exception& ) {
            //exception
            return;
        }
    }
protected:
    virtual T* allocBuffer(int capacity)
    {
        return new T[capacity];
    }
    virtual void freeBuffer(T* pBuffer)
    {    
        if(pBuffer)
            delete []pBuffer;
    }
private:
    bool checkBuffer()
    {
        if(m_buffer == NULL)
        {
            m_buffer = allocBuffer(m_capacity);
        }
        return m_buffer != NULL;
    }
    void getAbsTimeout(int timeout, struct timespec& abstimeout)
    {
        struct timeval now;
        gettimeofday(&now, NULL);
        timeout += (now.tv_usec / 1000);
        abstimeout.tv_sec = now.tv_sec + timeout / 1000;
        abstimeout.tv_nsec = (timeout % 1000) * 1000 * 1000;
    }
};
#endif
