//============================================================
// SimpleStack.h : interface and implementation of class SimpleStack
//                  (simple but efficent stack implementation, based on fixed-size array, it's NOT thread-safe) 
// Author: JeffLuo
// Created: 2006-10-05
//============================================================

#ifndef __SIMPLE_STACK_H_20061005_16
#define  __SIMPLE_STACK_H_20061005_16

template <class T>
class SimpleStack
{
private:
    T* m_buffer;
    int m_max_capacity;
//    int m_start;
    int m_index;

public:
    SimpleStack(int max_capacity) 
    {
        if(max_capacity > 0)
        {
            m_max_capacity = max_capacity;
            m_buffer = new T[max_capacity];
            m_index = -1;
        }
        else
        {
            m_buffer = NULL;
            m_max_capacity = 0;
            m_index = -1;
        }
    }

    ~SimpleStack()
    {
        if(m_buffer)
        {
            delete []m_buffer;
        }
    }

    bool push(const T& val)
    {
        bool ret = false;
        
        if(m_index + 1 < m_max_capacity)
        {
            m_buffer[++m_index] = val;
            
            ret = true;
        }

        return ret;
    }

    bool pop(T& val)
    {
        bool ret = false;
        
        if(m_index >= 0)
        {
            val = m_buffer[m_index--];
            
            ret = true;
        }        

        return ret;
    }

    int size()
    {
        return m_index + 1;
    }

    int max_capacity()
    {
        return m_max_capacity;
    }
};

#endif // #ifndef __SIMPLE_STACK_H_20061005_16

