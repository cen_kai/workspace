//============================================================
// TextSheetLoader.cpp: implementation of class TextSheetLoader
//        (Text-sheet represents rowset of text data)
//
// Author: JeffLuo
// Created: 2006-09-28
//============================================================

#include "TextSheetLoader.h"
#include "common/Utility.h"

IMPL_LOGGER(TextSheetLoader, logger);

TextSheetLoader::TextSheetLoader(const char * sheet_file, const char * col_delimitor)
{
    if(sheet_file != NULL)
        strncpy(m_sheet_file, sheet_file, MAX_FILE_PATH);
    else
        m_sheet_file[0] = '\0';

    if(col_delimitor != NULL)
        strncpy(m_col_delimitor, col_delimitor, MAX_DELIMITOR_LEN);
    else
        strcpy(m_col_delimitor, "\t");
}

//读取一个文件的数据，但要去掉空行和以"#" 开头的评价行
bool TextSheetLoader::load(int ignore_flag)
{
    if(strlen(m_sheet_file) == 0)
    {
        LOG4CPLUS_THIS_WARN(logger, "sheet file not specified.");
        return false;
    }

    FILE* fp = fopen(m_sheet_file, "r");

    if(!fp)
    {
        LOG4CPLUS_THIS_WARN(logger, "open sheet file '" << m_sheet_file << "' failed.");        
        return false;
    }

    char buffer[MAX_LINE_WIDTH];
    m_rows_data.clear();
    while(true) 
    {
        char* ret_ptr = fgets(buffer, MAX_LINE_WIDTH, fp);
        if(!ret_ptr)
        {
            break;
        }

        // trimming tailer '\n'
        int nchars = strlen(buffer);
        if(nchars > 0 && buffer[nchars - 1] == '\n')
            buffer[nchars - 1] = '\0';
        
        append_line(buffer,ignore_flag);
    }
    fclose(fp);
    return true;    
}

bool TextSheetLoader::append_line(char * line, int ignore_flag)
{
    if(line == NULL)
        return false;

    if((ignore_flag & IGNORE_COMMENT_LINE) && line[0] == '#')
    {
        // this is comment line, ignore
        return false;
    }
    if((ignore_flag & IGNORE_BLANK_LINE) && strlen(line) == 0)
    {
        // this is comment line, ignore
        return false;
    }

    m_rows_data.push_back((string)line);
    return true;
}

bool TextSheetLoader::get_row_columns(int row_index, vector < string > & columns)
{
    if(row_index < 0 || row_index >= (int)m_rows_data.size())
    {
        return false;
    }

    string& str = m_rows_data[row_index];
    return Utility::split(str, m_col_delimitor, columns) > 0;
}

