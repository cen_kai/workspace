#ifndef CLIENT_SOCKET__H
#define CLIENT_SOCKET__H

/*
* xiaojing@xulei.com
* 2012-11-8
* 用于服务器内部通讯的socket，阻塞带超时检查
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
using namespace std;

#include "common/SDLogger.h"

const int SOCK_SEND_TIMEOUT_SEC = 5; 
const int SOCK_READ_TIMEOUT_SEC = 1;

class ClientSocket{
public:
    ClientSocket();
    virtual ~ClientSocket();
    
    int Connect(string ip, int port);
    int Reconnect();
    int Read(char *buf, int len); 
    int Send(const char *buf, int len); 
    void Close(); 

private:
    int _sock;
    string _ip;
    int _port;

private:
    DECL_LOGGER(logger);
};

#endif
