#include "ClientSocket.h"
#include <errno.h>
#include <fcntl.h>
#include "common/Utility.h"

IMPL_LOGGER(ClientSocket, logger);

ClientSocket::ClientSocket()
{
    _sock = -1;
}

ClientSocket::~ClientSocket()
{
    Close();
}

void ClientSocket::Close()
{
    if(_sock != -1)
        close(_sock);
    _sock = -1;
}

int ClientSocket::Reconnect()
{
    Close();

    _sock = socket(AF_INET, SOCK_STREAM, 0);
   
    Utility::set_socket_send_timeout(_sock, SOCK_SEND_TIMEOUT_SEC);
    Utility::set_socket_recv_timeout(_sock, SOCK_READ_TIMEOUT_SEC);
    Utility::set_fd_block(_sock, true); 

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(_ip.c_str());
    addr.sin_port = htons(_port);
    int ret = connect(_sock, (const sockaddr*)&addr, sizeof(struct sockaddr_in));
    const int err = errno;
    LOG4CPLUS_DEBUG(logger, "Reconnect: connect ret=" << ret << ", errno=" << err);
    if(ret == -1){
        Close();
        return -1;       
    }
    return 0;
}

int ClientSocket::Connect(string ip, int port)
{
    Close(); 
    
    _ip = ip;
    _port = port; 
    
    LOG4CPLUS_DEBUG(logger, "Connect: ip=" << ip << ", port=" << port);

    return Reconnect();
}

int ClientSocket::Read(char *buf, int len)
{
    int total = 0;
    while(true)
    {
        int n = recv(_sock, buf + total, len - total, 0);
        const int err = errno;
        LOG4CPLUS_DEBUG(logger, "recv: total=" << total << ", ret=" << n << ", errno=" << err);
        if(n < 0){
            if(err == EINTR)
                continue;
            if(err == EAGAIN) //阻塞带超时检查的读，若返回EAGAIN意味着超时
                return total;
            return -1;
        }else if(n == 0){
            return -1;
        }

        total += n;

        if(total >= len)
            break;
    }

    return total;
}

int ClientSocket::Send(const char *buf, int len)
{
    int total = 0;
    while(true)
    {
        int n = send(_sock, buf + total, len - total, 0);
        int err = errno;
        LOG4CPLUS_DEBUG(logger, "Send: total=" << total << ", ret=" << n << ", errno=" << err);
        if(n<0){
            if(err==EAGAIN || err==EINTR)
                continue;
            return -1;
        }

        total += n;

        if(total >= len)
            break;
    }

    return total;
}
