#ifndef __CLIENT_SESSION_H_201007231457
#define  __CLIENT_SESSION_H_201007231457
#include "server/Session.h"

#include "server/CdnMgrDatagram.h"
#include "Command.h"


class ClientSession : public Session
{
public:
    static const _u32 THREAD_TYPE_DBQUERY = 0x01;
private:
    static const _u32 DEFAULT_READ_BUF_LEN = 1024 * 20;
    static const _u32 DEFAULT_SEND_BUF_LEN = 1024 * 16 * 3;

    uint64_t start_ms;

public:
    ~ClientSession();
    ClientSession()
    {
        m_read_buf_len = DEFAULT_READ_BUF_LEN;
        m_read_buf = new uint8_t[m_read_buf_len];
        m_send_buf_len = DEFAULT_SEND_BUF_LEN;
        m_send_buf = new uint8_t[m_send_buf_len];
        m_cur_read_pos = 0;
        start_ms = Utility::current_time_ms();
        m_wait_send_http_cmd=NULL;
    }
public:
    _u32 on_socket_readable(int fd, EventData* attach_info);
    _u32 on_socket_writable(int fd, EventData* attach_info);
    _u32 on_socket_error(int fd, EventData* attach_info);
    _u32 on_socket_timeout(int fd, EventData* attach_info);
    _u32 handle_pipe_event(PipeEvent &event);
    
    int make_response(PipeEvent &event, stringstream &out);
    int send_http_response(const string &msg);
    HttpCmd *m_wait_send_http_cmd ;
    
private:
    list<Datagram *>m_recv_cmd_list;
    uint8_t *m_read_buf;
    int m_read_buf_len; 
    int m_cur_read_pos;
    int proc_data(uint8_t *data, int data_len);
    int send_http_cmd(HttpCmd *cmd);
private:
    DECL_LOGGER(logger);
};
#endif

