#include <assert.h>



#include "common/SDAes.h"
#include "common/SeqNOGenerator.h"
#include "server/RegParaManager.h"
#include "ClientSession.h" 
#include "common/ClientSocket.h" 
#include <openssl/md5.h>
#include "Command.h"
#include "server/CdnMgrDatagram.h"
#include "server/ServerApp.h"
#include "common/SDDBConnectionPool.h"
#include "inter/TaskProcessThreads.h"



//#include "play_record/SessionCache.h"
#include "client/http_parser.h"

#include <string>

extern ServerApp g_app;
extern TaskProcessThreads *g_task_service;


IMPL_LOGGER(ClientSession, logger);

ClientSession::~ClientSession()
{
    if(m_read_buf)
        delete []m_read_buf;
    list<Datagram *>::iterator iter = m_recv_cmd_list.begin();
    list<Datagram *>::iterator iter_end = m_recv_cmd_list.end();
    for(;iter!=iter_end;iter++)
    {
        delete *iter;
    }
    if(m_wait_send_http_cmd){
        delete  m_wait_send_http_cmd;
    }
}

int ClientSession::send_http_cmd(HttpCmd *cmd)
{
    int cmd_len = cmd->EncodeLen();
    int ret=0;
    if(cmd_len  > (m_send_buf_len-m_cur_send_transfered))
    {
         LOG4CPLUS_ERROR(logger, "message too long to send_http_cmd: m_send_buf_len = " << m_send_buf_len 
            << ", m_cur_send_transfered = " << m_cur_send_transfered
        );
        return HANDLE_ERROR;
    }
    ret = cmd->dump2buf((char*)m_send_buf+m_cur_send_transfered,m_send_buf_len);
    LOG4CPLUS_DEBUG(logger, "handle send_http_cmd  m_send_buf "<<m_send_buf);
    if(ret<0){
        LOG4CPLUS_DEBUG(logger, "handle send_http_cmd  dump2buf wrong");
    }else{
        m_need_send_len +=cmd_len ;
        return send_remain_buf();
    }
    return ret;
}

int ClientSession::proc_data(uint8_t *data, int data_len)
{
    PipeEvent event;
    event.m_fd = m_socket_fd;
    event.timestamp = time(NULL);
    event.m_data = data;
    event.m_data_len = data_len;

    if (g_task_service->add_task(event))
    {
        LOG4CPLUS_DEBUG(logger, "return Handle ok");
        return HANDLE_OK;
    }
    else
    {
        LOG4CPLUS_DEBUG(logger, "return handle error");
        return HANDLE_ERROR;
    }

}


int ClientSession::make_response(PipeEvent &event, stringstream &out)
{
    
    if(event.m_type == PipeEvent::PIPE_TYPE_CLIENT_RESP){
        LOG4CPLUS_DEBUG(logger, "PipeEvent:"<<event.dump()<<" make response: " << (*event.resp)  );
        out<<(*event.resp);
        return 0;
    }else{    
         LOG4CPLUS_DEBUG(logger, "make response: resp = NULL");
    }
        
    if(event.status == PipeEvent::STATUS_TIMEOUT){
        out << "<result><code>-1</code></result>";
        return 0;
    }

    out << "<result><code>-2</code></result>";
    return 0;
}

_u32 ClientSession::on_socket_writable(int fd, EventData* attach_info)
{
    LOG4CPLUS_DEBUG(logger, "admin client fd " << fd << " is writable now. peerip:" << Utility::get_peer_ip(fd));
    int ret = HANDLE_ERROR;
    if(m_connect_stat != CONNECT_STATU_ESTABLISHED)
    {
        LOG4CPLUS_DEBUG(logger, "m_connect_stat[" << m_connect_stat << "] is change to CONNECT_STATU_ESTABLISHED.");
        if( m_wait_send_http_cmd!=NULL )
        {
            ret = send_http_cmd( m_wait_send_http_cmd );
        }
        else
        {
            LOG4CPLUS_WARN(logger, "m_connect_stat=" << m_connect_stat << ", but m_wait_send_cmd is NULL !!!");
        }
        m_connect_stat = CONNECT_STATU_ESTABLISHED;
        return ret;
    }
    else
    {
        return send_remain_buf();
    }
}

_u32 ClientSession::on_socket_readable(int fd, EventData* attach_info)
{
    LOG4CPLUS_TRACE(logger, "on_socket_readable: fd[" << fd << "] ip[" << Utility::get_peer_ip(fd) << "]");
    assert(fd == m_socket_fd);

    int recv_len = m_read_buf_len;
    //read until EAGAIN
    int ret = Utility::recv_nonblock_data(m_socket_fd, (char*)m_read_buf, recv_len);
    //doing my self
    LOG4CPLUS_DEBUG(logger,"fd:"<< m_socket_fd<<" Utility::recv_nonblock_data ret:" << ret << "recv_len:" << recv_len );
    LOG4CPLUS_DEBUG(logger,"data:"<< (char*)m_read_buf);
    if(ret == Utility::NONBLOCK_RECV_ERROR || ret == 0){ //读出错或者eof
        return HANDLE_ERROR;
    }
    ret = proc_data(m_read_buf, recv_len);

    if(ret == (int)HANDLE_ERROR){
        return HANDLE_ERROR;
    }
    LOG4CPLUS_DEBUG(logger, "client session return ret=" << ret);   
    return ret;
}



_u32 ClientSession::on_socket_error(int fd, EventData* attach_info)
{
    LOG4CPLUS_WARN(logger, "on_socket_error, fd[" << fd << "] ip[" << Utility::get_peer_ip(fd) << "]");
    return HANDLE_ERROR;
}

_u32 ClientSession::on_socket_timeout(int fd, EventData* attach_info)
{
    LOG4CPLUS_WARN(logger, "on_socket_timeout, fd[" << fd << "] ip[" << Utility::get_peer_ip(fd) << "]");
    return HANDLE_ERROR;
}

int ClientSession::send_http_response(const string &resp_msg)
{
    stringstream http_out;
    static std::string Pragma_str=g_app.get_conf_string("Pragma","");
    static std::string Cache_control=g_app.get_conf_string("Cache-Control","");
    http_out << "HTTP/1.1 200 OK\r\n"
        << "Connection: close\r\n"
        << "Content-Type: text/plain;charset=UTF-8\r\n"
        << "Content-Length: " << resp_msg.size() << "\r\n";
    if(Pragma_str!="")
    {
        http_out <<"Pragma: "<< Pragma_str
            << "\r\n";
    }
    if(Cache_control!="")
    {
        http_out<<"Cache-Control: "<< Cache_control<<"\r\n";
    }
    http_out<<"\r\n";
    http_out<< resp_msg;
    string cnt;
    cnt = http_out.str();
    
    LOG4CPLUS_DEBUG(logger, "http_out =" << Utility::bytes_to_hex((uint8_t*)http_out.str().c_str(), http_out.str().size()));
    
    if(m_send_buf_len < (int)(m_cur_send_transfered + cnt.size())){
        // 这里最多传送200k内容
        if(cnt.size() >  1024*200 ){
              LOG4CPLUS_ERROR(logger, "message too long to send: m_send_buf_len = " << m_send_buf_len 
                 << ", m_cur_send_transfered = " << m_cur_send_transfered
                 << ", cnt.size = " << cnt.size()
                );
              return HANDLE_ERROR;
        }
        uint8_t *tmp_send_buf = new uint8_t[m_cur_send_transfered + cnt.size()+1024] ;
        m_send_buf_len = m_cur_send_transfered + cnt.size()+1024 ;
        if(m_cur_send_transfered){
            memcpy(tmp_send_buf,m_send_buf, m_cur_send_transfered );
        }
        delete [] m_send_buf ;
        m_send_buf = tmp_send_buf;
    }
    memcpy(m_send_buf + m_cur_send_transfered, cnt.c_str(), cnt.size());
    m_need_send_len += cnt.size();
    return send_remain_buf();
}

_u32 ClientSession::handle_pipe_event(PipeEvent &event)
{
    assert(m_socket_fd == event.m_fd);
    if(event.m_type != PipeEvent::PIPE_TYPE_CLIENT_RESP)
    {
        LOG4CPLUS_WARN(logger, "unkown event.m_type:" << event.m_type);
        return HANDLE_ERROR;
    }
    
    stringstream out;
    make_response(event, out);
    LOG4CPLUS_DEBUG(logger, "make_response: out=" << out.str());
    
    string resp_msg;
    resp_msg = out.str();
    if(event.m_data != NULL)
    {
        delete ((HttpCmd*)event.m_data);
        event.m_data = NULL;
    }
    if(event.resp != NULL)
    {
        delete event.resp;
        event.resp = NULL;
    }

    return send_http_response(resp_msg);
}


