#ifndef STS_COMMAND_H
#define STS_COMMAND_H

#include <set>
#include "common/SDLogger.h"
#include "common/parse_url_parameters.h"
#include "server/ServerApp.h"
#include "common/SDAes.h"
#include "common/Utility.h"
#include <cstdlib>
#include "client/http_parser.h"
#include <string>
extern ServerApp g_app;

class ClientSession;

class HttpCmd
{
public:
    enum REQUEST_TYPE{TYPE_UNKNOWN, TYPE_GET ,TYPE_POST};
    static const int  NOT_STR_INDEX=-1;

    HttpCmd() :session(NULL),settings(NULL){}
    HttpCmd(ClientSession *sess,http_parser_settings_imp *cb_settings):session(sess),parser(),settings(cb_settings)
    {
        http_parser_init(&parser, HTTP_REQUEST);
    }
public:

    void settype(REQUEST_TYPE type){
        m_type = type ;
    }
    bool get_cmd_ulvalue(const char *key,_u64 & number)
    {
        map<string,string>::iterator iter = normal_paras.find(key);
        if(iter==normal_paras.end())
        {
            return false;      
        }
        
        string tmp = iter->second;
        if(Utility::is_digit(tmp))
        {
            long long num = atoll(tmp.c_str());
            if(num >= 0)
            {
                number = (unsigned long long) num;
                return true;
            }
        }
        else if(strncmp(tmp.c_str(),"0x",2)==0 || strncmp(tmp.c_str(),"0X",2)==0)
        {
            unsigned long long num = strtoull(tmp.c_str(),NULL,16);
            if(num >= 0)
            {
                number = num;
                return true;
            }
        }
        return false;
    }

    int EncodeLen()
    {
        if(!content_buf.str().empty()){
            return content_buf.str().length();
        }
        if(m_type==TYPE_GET){
            content_buf<<"GET ";
            string quest_uri("/");
            if(header_paras["Path"]!=""){
                quest_uri = string("/")+header_paras["Path"];
            }
            content_buf<<quest_uri;
            if(!normal_paras.empty()){
                content_buf<<"?";
            }
            map<std::string, std::string>::iterator iter=normal_paras.begin();
            for(;iter!=normal_paras.end();iter++){
                if(iter!=normal_paras.begin()){
                    content_buf<<"&";
                }
                content_buf<<iter->first<<"="<<iter->second;
            }
            content_buf<<" HTTP/1.1\r\n";
            if(header_paras["Host"].empty()){
                LOG4CPLUS_DEBUG(logger,"You Should Set Host field");
                return 0;
            }
            map<std::string, std::string>::iterator iter1=header_paras.begin();
            for(;iter1!=header_paras.end();iter1++){
                if(iter1->first == "Path"){
                    continue;
                }
                content_buf<<iter1->first<<":"<<iter1->second<<"\r\n";
            }
            content_buf<<"\r\n";
            LOG4CPLUS_DEBUG(logger,"content_buf:"<<content_buf.str());
            return content_buf.str().length();
        }
        return  0 ;
    }

    bool get_cmd_string(const char *key,string & value)
    {
        map<string,string>::iterator iter = normal_paras.find(key);
        if(iter==normal_paras.end())
        {
            return false;      
        }
        value = iter->second;
        return true ;
    }

    static REQUEST_TYPE RequestType(char *buf)
    {
        const char * GET = "GET /";
        const char * POST = "POST /";
        if(strstr(buf, GET))
            return TYPE_GET;
        if(strstr(buf, POST))
            return TYPE_POST;
        return TYPE_UNKNOWN;
    }

    static HttpCmd* TryCreate(char *buf, int buf_len, int &result,ClientSession *sess)
    {
        REQUEST_TYPE type = RequestType(buf);

        if(IsComplete(buf,buf_len,type))
        {
            http_parser_settings_imp  cb;
            HttpCmd *cmd = new HttpCmd(sess,&cb);
            int nparsed = http_parser_execute(&(cmd->parser), cmd->settings, buf, buf_len);
            if (nparsed == buf_len && cb.IsCorrect()){
                result = nparsed ;
                cmd->parser_params() ;
                return cmd;
            }
            else{
                delete cmd;
                cmd = NULL;
            }
        }
        else{
           result = -1 ;
        }
        return NULL;
    }

    // 数据有可能切分为两个数据包 故需要判断是否接收完整
    static bool IsComplete(char *buf,int buf_len , REQUEST_TYPE type )
        {
            const char *END = "\r\n\r\n";
            char *pos = buf;
            char *http_end = strstr(pos, END);
            char *body=NULL ;
            if(!http_end){
                END = "\n\n";
                http_end = strstr(pos, END);
            }
            if(!http_end){
                return false;
            }
            //GET方法只有报头即可
            if(type==TYPE_GET)
            {
                return true;
            }
            //效验POST方法包完整性
            body = http_end + strlen(END);
            const char *cnt_len_header = strstr(pos, "Content-Length");
            if(!cnt_len_header){
                return false;
            }
            int content_length = atoi(cnt_len_header+sizeof("Content-Length"));
            if(body-buf+content_length==buf_len)
                return true;
            return false ;
        }

    bool parser_params(){
        list<std::string>::iterator iter = settings->field.begin();
        for(;iter!=settings->field.end();++iter){
            string value = settings->value.front();
            settings->value.pop_front();
            LOG4CPLUS_DEBUG(logger,"field:"<<*iter<<" value:"<<value);
            if( *iter=="Url" )
            {
                int index=NOT_STR_INDEX;
                if((index=get_cmd_name(value.c_str(),cmd_name))==NOT_STR_INDEX)
                       index=0;
                LOG4CPLUS_DEBUG(logger,"paras:"<<value.c_str()+index+1);
                read_cgi_input(normal_paras,value.c_str()+index+1,APPLICATION_WWW_FORM_URLENCODED);
                if(index==0)
                    index=NOT_STR_INDEX;
                string tmp(value,0,index);
                header_paras.insert(make_pair("Path",tmp));         
            }
            else if( *iter=="Body")
            {
                const int flag=0;
                if(flag&&parser.method==HTTP_POST)
                {
                    const char *parse_body = static_cast<const char *>(value.c_str());
                    static int enabled_encrypt = g_app.get_conf_int("server.aes.decrypt.enabled", 0);
                    char *des_body = NULL;
                    if(enabled_encrypt){ //有加密，需先解密
                        int content_length=parser.content_length;
                        des_body = new char[content_length+1];
                        des_body[content_length] = 0;
                        AES aes;
                        aes.setDecryptKey((const uint8_t *)g_app.get_conf_string("server.aes.decrypt.key").c_str());
                        int des_len = content_length;
                        aes.decrypt((const uint8_t*)value.c_str(), content_length, (uint8_t*)des_body, &des_len);
                        parse_body = des_body;
                    }
                    else
                        parse_body = value.c_str();
                    int index=NOT_STR_INDEX;
                    if((index=get_cmd_name(parse_body,cmd_name))==-1)
                            index=0;
                    cgi_param_type type = GetCgiType() ;
                    read_cgi_input(normal_paras, parse_body+index,type); 
                    if(des_body)
                        delete []des_body;
                }
            }
            else{
                    header_paras.insert(make_pair(*iter,value));
            }
        }
        return true ;
    }

    cgi_param_type GetCgiType()
        {
            static string type[4] = {string("application/x-www-form-urlencoded"),string("multipart/form-data"),string("application/json"),string("text/xml")};
            map<std::string, std::string>::iterator iter = header_paras.find(string("Content-type"));
            for(int i=0; i<4; ++i)
            {
                if(iter->second==type[i])
                    return (cgi_param_type)i;
            }
            return     CGI_PARAM_NOTYPE;
        }

    void dump()
        {
            LOG4CPLUS_DEBUG(logger,"message header");
            map<std::string, std::string>::iterator iter=header_paras.begin();
            for(;iter!=header_paras.end();++iter)
            {
                LOG4CPLUS_DEBUG(logger,"field:"<<iter->first<<" value:"<<iter->second);
            }
            LOG4CPLUS_DEBUG(logger,"message paras");
            iter=normal_paras.begin();
            for(;iter!=normal_paras.end();++iter)
            {
                if(iter->first=="movietiming" && iter->second=="2147483647"){
                    LOG4CPLUS_DEBUG(logger,"viewed_time very large");
                }
                LOG4CPLUS_DEBUG(logger,"field:"<<iter->first<<" value:"<<iter->second);
            }
        }
    int dump2buf(char *buf,int size)
        {
            EncodeLen();
            snprintf(buf,size,content_buf.str().c_str(),content_buf.str().length());
            LOG4CPLUS_DEBUG(logger,"dump2buf:"<<buf);
            return 0;
        }

public:
    ClientSession *session;
    http_parser parser;
    //一般settings 是临时对象，不必要释放
    http_parser_settings_imp *settings;
    string cmd_name;
    map<std::string, std::string> header_paras;
    map<std::string, std::string> normal_paras;
private:
    REQUEST_TYPE m_type;
    stringstream  content_buf;
    int get_cmd_name(const char *str , string &cmd)
        {
            const char *p = str ;
            const char *old_p = p;
            int end = 0;
            while(*(p+end)!='\0' && *(p+end)!='?') 
                end++;
            if(*(p+end)=='\0')
            {
                end = -1 ;
                return end ;
            }
            const char *end_p = p + end; 
            while(p<end_p)
            {
                if(*p=='/')
                {
                    old_p=p+1;
                }
                p++;
            }
            cmd.assign(old_p,end_p-old_p);
            return end ;
        }
private:
    DECL_LOGGER(logger);
};


class HttpCmdResp{
public:
    HttpCmdResp()
    {
    }
    string output;

private:
    DECL_LOGGER(logger);
};

#endif
