#include "HttpCmdHandler.h"
#include "client/ClientSession.h"
#include "ServerApp.h"
#include "common/SDDBConnectionPool.h"

extern ServerApp g_app;
extern SDDBConnectionPool g_db_pool;
IMPL_LOGGER(HttpCmdHandler, logger);

HttpCmdHandler::HttpCmdHandler(size_t queue_size, size_t thread_count, const string& script_path):SDTwinThreadPool(thread_count, 1)
{
    _queue = new SDQueue<HttpCmd*>(queue_size);
    _script_path = script_path;
}

HttpCmdHandler::~HttpCmdHandler()
{
}

int HttpCmdHandler::AddCmd(HttpCmd *cmd)
{ 
    bool ret = _queue->push(cmd);
    if(!ret){
        LOG4CPLUS_ERROR(logger, "push cmd failed.");
        return -1;
    }
    return 0; 
}

void HttpCmdHandler::doIt()
{
    HttpCmd *cmd;
    while(1)
    {
        if(!_queue->pop(cmd, 1000) || !cmd){
            continue;
        }    
        LOG4CPLUS_DEBUG(logger, "Proc cmd: cmd_name=" << cmd->cmd_name);
        delete cmd;
    }
}

void HttpCmdHandler::doIt2()
{
    while(1)
    {
        sleep(10*60);
    }
}
