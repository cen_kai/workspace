#include "ServerApp.h"
#include "RegParaManager.h"

RegParaManager *RegParaManager::__instance = NULL;
IMPL_LOGGER(RegParaManager, logger);

extern ServerApp g_app;

RegParaManager::RegParaManager()
{
    InitPara();
}


bool RegParaManager::InitPara()
{
    m_str_para_map.clear();
    m_int_para_map.clear();
       
    int para_num = (int)g_app.get_conf_int("str_para_num",0);

    char buf[128] = {0};
    string str_name ;
    string str_value;   
    for(int i=1;i<=para_num;i++)
    {
        snprintf(buf, 128, "str_para_%d_name", i);
        str_name = g_app.get_conf_string(buf);
        snprintf(buf, 128, "str_para_%d_value", i);
        str_value = g_app.get_conf_string(buf);
           
        m_str_para_map.insert(make_pair(str_name,str_value));
    }

    para_num = (int)g_app.get_conf_int("int_para_num",0);

    string int_name ;
    int int_value;   
    for(int i=1;i<=para_num;i++)
    {
        snprintf(buf, 128, "int_para_%d_name", i);
        int_name = g_app.get_conf_string(buf);
        snprintf(buf, 128, "int_para_%d_value", i);
        int_value = g_app.get_conf_int(buf);
                    
        m_int_para_map.insert(make_pair(int_name,int_value));
    }  

    m_iReportRandomRatioBase = g_app.get_conf_int("sts.peer.report.ratio.base");
    m_iReportRandomRatioNeed = g_app.get_conf_int("sts.peer.report.ratio.need");
    m_iKrdsDeployStart = g_app.get_conf_int("weekend_krds_deploy_start_time", 3);
    m_iKrdsDeployEnd = g_app.get_conf_int("weekend_krds_deploy_end_time", 11);
    
    return true;
}


