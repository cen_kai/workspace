//============================================================
// SessionThreadPool.h 
//                          
// Created: 2009-11-14   tianming
//============================================================

#ifndef __SESSION_THREADPOLL_H
#define  __SESSION_THREADPOLL_H

#include <set>
#include <vector>
#include <string>

#include "common/SDQueue.h"
#include "common/FixedBuffer.h"
#include "common/IOEventServer.h"
#include "common/SDThread.h"
#include "Session.h"
#include "SessionManager.h"
#include "EpollEventServer.h"
#include "inter/SessionThreadData.h"
#include "client/Command.h"

using std::vector;
#include "common/mem_leak.h"

template <typename SessionType > 
class SessionThread : public SDThread, public DefaultEventCallBack
{
public:
    const static _u32 ATTR_LONG_CONNECTION = 0x01; 
public:
    SessionThread(int session_timeout_ms, int new_socket_event_type=IOEventServer::EVENT_READABLE, _u32 session_attr = ATTR_LONG_CONNECTION)
        : m_session_attr(session_attr)
        , m_new_socket_event_type(new_socket_event_type) 
        , m_session_timeout_ms(session_timeout_ms)
    {
        m_ev_server = new EpollEventServer();
        
        if(pipe(m_pipe_fds) == -1)
        {
            LOG4CPLUS_FATAL(logger, "pipe error:" << errno);
            exit(123);
        }
        int flag=fcntl(m_pipe_fds[0],F_GETFL,0);
        flag |= O_NONBLOCK;
        if(fcntl(m_pipe_fds[0],F_SETFL,flag) < 0){
           perror("fcntl");
           exit(1);
        }

        flag=fcntl(m_pipe_fds[1],F_GETFL,0);
        flag |= O_NONBLOCK;
        if(fcntl(m_pipe_fds[1],F_SETFL,flag) < 0){
           perror("fcntl");
           exit(1);
        }
    }
    ~SessionThread()
    {
        if(m_ev_server != NULL)
        {
            delete m_ev_server;
        }

        close(m_pipe_fds[0]);
        close(m_pipe_fds[1]);
    }
    bool add_new_socket(int fd ,HttpCmd *wait_cmd = NULL, _u32 app_type = Session::COMMAND_APP_UNKNOW)
    {
        AddSession *session = new AddSession();
        session->m_fd = fd;
        session->m_event_type = m_new_socket_event_type;
        session->m_app_type = app_type;
        session->m_wait_deal_cmd = wait_cmd ;
        if(wait_cmd!=NULL){
            session->m_event_type |= IOEventServer::EVENT_WRITABLE;
        }
        PipeEvent event;
        event.m_type = PipeEvent::PIPE_TYPE_ADD_SESSION;
        event.m_data = (void *) session;
        return add_pipe_event(event);
    }

    bool add_pipe_event(const PipeEvent &event)
    {
        return write(m_pipe_fds[1], &event, sizeof(event));
    }
    
    void run()
    {
        EventInfo ev_info(this, 0);
        m_ev_server->add_event(m_pipe_fds[0], IOEventServer::EVENT_READABLE, &ev_info);
        m_ev_server->run_event_loop();
    }
    
    void on_fd_readable(int fd, EventData* attach_info)
    {
        if(fd == m_pipe_fds[0])
        {
            on_pipe_readable(fd, attach_info);
            EventInfo ev_info(this, 0);
            get_event_server()->add_event(m_pipe_fds[0], IOEventServer::EVENT_READABLE, &ev_info);
        }
        else
        {
            SessionType *session = NULL;
            if((session = (SessionType *)m_session_manager.find_session_by_fd(fd)) == NULL)
            {
                LOG4CPLUS_ERROR(logger, "in on_fd_readable m_session_manager.find_session_by_fd error! fd:" << fd);
                m_ev_server->notify_fd_closed(fd);
            }
            else
            {
                _u32 session_ret = session->on_socket_readable(fd, attach_info);
                LOG4CPLUS_DEBUG(logger,"fd:"<<fd << "session_ret:" << session_ret);
                if(session_ret == HANDLE_ERROR)
                {
                    m_ev_server->notify_fd_closed(fd);
                    LOG4CPLUS_INFO(logger, "session->on_fd_readable error! fd:" << fd);
                    session->m_session_unlink_flag = true;
                }
                if(session->m_session_unlink_flag)
                {
                    if(session->m_use_session_other_thread == 0)
                    {
                        if(m_session_manager.remove_session(fd))
                        {
                            close(fd);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(logger, "remove_session failed! fd:" << fd);
                        }
                        LOG4CPLUS_INFO(logger, "in on_fd_readable remove_session! fd:" << fd);
                    }
                     return;
                }

                if((session_ret & HANDLE_WRITE_PENDING) == HANDLE_WRITE_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_WRITABLE, &ev_info);
                }

                if(((ATTR_LONG_CONNECTION & m_session_attr) != 0) || (session_ret & HANDLE_READ_PENDING) == HANDLE_READ_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info);
                }
            }
        }
    }
    
    void on_fd_writable(int fd, EventData* attach_info)
    {
        if(fd == m_pipe_fds[1])
        {
            on_pipe_writable(fd, attach_info);
        }
        else
        {
            SessionType *session = NULL;
            if((session = (SessionType *)m_session_manager.find_session_by_fd(fd)) == NULL)
            {
                LOG4CPLUS_ERROR(logger, "in on_fd_writable m_session_manager.find_session_by_fd error! fd:" << fd);
                m_ev_server->notify_fd_closed(fd);
            }
            else
            {
                _u32 session_ret = session->on_socket_writable(fd, attach_info);
                if(session_ret == HANDLE_ERROR)
                {
                    m_ev_server->notify_fd_closed(fd);
                    LOG4CPLUS_WARN(logger, "session->on_socket_writable error! fd:" << fd);
                    session->m_session_unlink_flag = true;
                }
                if(session->m_session_unlink_flag)
                {
                    if(session->m_use_session_other_thread == 0)
                    {
                        if(m_session_manager.remove_session(fd))
                        {
                            close(fd);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(logger, "remove_session failed! fd:" << fd);
                        }
                        LOG4CPLUS_INFO(logger, "in on_fd_writable remove_session! fd:" << fd);
                    }
                    return;
                }

                if((session_ret & HANDLE_WRITE_PENDING) == HANDLE_WRITE_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_WRITABLE, &ev_info);
                }
                if((session_ret & HANDLE_READ_PENDING) == HANDLE_READ_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info);
                }
            }
        }
    }
    
    void on_fd_error(int fd, EventData* attach_info)
    {
        if(fd == m_pipe_fds[0])
        {
            on_pipe_error(fd, attach_info);
        }
        else
        {
            SessionType *session = NULL;
            if((session = (SessionType *)m_session_manager.find_session_by_fd(fd)) == NULL)
            {
                LOG4CPLUS_ERROR(logger, "in on_fd_error m_session_manager.find_session_by_fd error! fd:" << fd);
                m_ev_server->notify_fd_closed(fd);
            }
            else
            {
                _u32 session_ret = session->on_socket_error(fd, attach_info);
                if(session_ret == HANDLE_ERROR)
                {
                    m_ev_server->notify_fd_closed(fd);
                    LOG4CPLUS_WARN(logger, "session->on_socket_error error! fd:" << fd);
                    session->m_session_unlink_flag = true;
                }
                if(session->m_session_unlink_flag)
                {
                    if(session->m_use_session_other_thread == 0)
                    {
                        if(m_session_manager.remove_session(fd))
                        {
                            close(fd);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(logger, "remove_session failed! fd:" << fd);
                        }
                        LOG4CPLUS_INFO(logger, "in on_fd_error remove_session! fd:" << fd);
                    }
                    return;
                }

                if((session_ret & HANDLE_WRITE_PENDING) == HANDLE_WRITE_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_WRITABLE, &ev_info);
                }
                if((session_ret & HANDLE_READ_PENDING) == HANDLE_READ_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info);
                }
            }
        }
    }
    
    void on_fd_timeout(int fd, EventData* attach_info)
    {
        if(fd == m_pipe_fds[0])
        {
            on_pipe_timeout(fd, attach_info);
        }
        else
        {
            SessionType *session = NULL;
            if((session = (SessionType *)m_session_manager.find_session_by_fd(fd)) == NULL)
            {
                LOG4CPLUS_ERROR(logger, "in on_fd_timeout m_session_manager.find_session_by_fd error! fd:" << fd);
                m_ev_server->notify_fd_closed(fd);
            }
            else
            {
                _u32 session_ret = session->on_socket_timeout(fd, attach_info);
                if(session_ret == HANDLE_ERROR)
                {
                    m_ev_server->notify_fd_closed(fd);
                    LOG4CPLUS_WARN(logger, "session->on_socket_timeout error! fd:" << fd);
                    session->m_session_unlink_flag = true;
                }
                if(session->m_session_unlink_flag)
                {
                    if(session->m_use_session_other_thread == 0)
                    {
                        if(m_session_manager.remove_session(fd))
                        {
                            close(fd);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(logger, "remove_session failed! fd:" << fd);
                        }
                        LOG4CPLUS_INFO(logger, "in on_fd_timeout remove_session! fd:" << fd);
                    }
                    return;
                }

                if((session_ret & HANDLE_WRITE_PENDING) == HANDLE_WRITE_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_WRITABLE, &ev_info);
                }
                if((session_ret & HANDLE_READ_PENDING) == HANDLE_READ_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info);
                }
            }
        }
    }

    void on_pipe_readable(int fd, EventData* attach_info)
    {    
        PipeEvent event;
        int ret = read(fd, &event, sizeof(event));
        if(ret != sizeof(event))
        {
            LOG4CPLUS_ERROR(logger, "read pipe error!");
            return;
        }

        if(event.m_type == PipeEvent::PIPE_TYPE_ADD_SESSION)
        {
            AddSession *add_session = (AddSession *)event.m_data;
            SessionType *session = new SessionType();
            session->m_socket_fd = add_session->m_fd;
            if(add_session->m_wait_deal_cmd != NULL)
            {
                session->m_connect_stat = Session::CONNECT_STATU_UNESTABLISHED;
                session->m_app_type = add_session->m_app_type;
                session->m_wait_send_http_cmd = add_session->m_wait_deal_cmd;
            }
            if(!m_session_manager.add_session(session))
            {
                LOG4CPLUS_ERROR(logger, "m_session_manager.add_session failed! fd:" << session->m_socket_fd);
                delete session;
            }
            else
            {
                EventInfo ev_info(this, m_session_timeout_ms);
                if(!m_ev_server->add_event(add_session->m_fd, add_session->m_event_type, &ev_info))
                {
                    LOG4CPLUS_ERROR(logger, "m_ev_server->add_event return error! fd:" << add_session->m_fd);
                }
            }
            delete add_session;
        }
        else
        {
            SessionType *session = NULL;
            if((session = (SessionType *)m_session_manager.find_session_by_fd(event.m_fd)) == NULL)
            {
                LOG4CPLUS_ERROR(logger, "m_session_manager.find_session_by_fd error! fd:" << event.m_fd);
            }
            else
            {
                _u32 session_ret = session->handle_pipe_event(event);
                if(session_ret == HANDLE_ERROR)
                {
                    LOG4CPLUS_WARN(logger, "session->handle_pipe_event error! fd:" << event.m_fd);
                    session->m_session_unlink_flag = true;
                }

                if(session->m_session_unlink_flag )
                {
                    if(session->m_use_session_other_thread == 0)
                    {
                        if(m_session_manager.remove_session(event.m_fd))
                        {
                            close(event.m_fd);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(logger, "remove_session failed! fd:" << event.m_fd);
                        }
                        LOG4CPLUS_INFO(logger, "in on_pipe_readable remove_session! fd:" << event.m_fd);
                    }
                    return;
                }
                
                if((session_ret & HANDLE_WRITE_PENDING) == HANDLE_WRITE_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(event.m_fd, IOEventServer::EVENT_WRITABLE, &ev_info);
                }
                if((session_ret & HANDLE_READ_PENDING) == HANDLE_READ_PENDING)
                {
                    EventInfo ev_info(this, m_session_timeout_ms);
                    get_event_server()->add_event(event.m_fd, IOEventServer::EVENT_READABLE, &ev_info);
                }
            }
        }
    }
    
    void on_pipe_writable(int fd, EventData* attach_info)
    {
        LOG4CPLUS_FATAL(logger, "unexpect on_pipe_writable!!");
        exit(33);
    }
    void on_pipe_error(int fd, EventData* attach_info)
    {
        LOG4CPLUS_FATAL(logger, "unexpect on_pipe_error!!");
        exit(33);
    }
    void on_pipe_timeout(int fd, EventData* attach_info)
    {
        LOG4CPLUS_FATAL(logger, "unexpect on_pipe_timeout!!");
        exit(33);
    }
    
private:
    SessionThread(SessionThread &rhs)
    {
        LOG4CPLUS_ERROR(logger, "can`t SessionThread(SessionThread &src)!!");
    }
    SessionThread &operator = (const SessionThread& rhs)
    {
        LOG4CPLUS_ERROR(logger, "can`t operator = (const SessionThread& rhs)!!");
        return *this;
    }
private:
    _u32 m_session_attr;
    int m_new_socket_event_type;
    //0 read, 1 write
    int m_pipe_fds[2];
    EpollEventServer* m_ev_server;
    SessionManager m_session_manager;
    int m_session_timeout_ms;
private:
    DECL_LOGGER(logger);
};
template <typename SessionType > 
IMPL_LOGGER(SessionThread<SessionType>, logger);


template <typename SessionType > 
class SessionThreadPool
{
public:
    SessionThreadPool(int thread_num, int session_timeout_ms, int new_socket_event_type=IOEventServer::EVENT_READABLE):m_thread_num(thread_num)
    {
        m_thread_handlers.resize(m_thread_num);
        for(int i=0; i<m_thread_num; i++)
        {
            SessionThread<SessionType> *thread = new SessionThread<SessionType>(session_timeout_ms, new_socket_event_type);
            m_thread_handlers[i] = thread;
        }
    }
    
    ~SessionThreadPool()
    {
        for(int i=0; i<m_thread_num; i++)
        {
            delete m_thread_handlers[i];
            m_thread_handlers[i] = NULL;
        }
    }
    
public:
    bool add_new_socket(int fd ,HttpCmd *wait_cmd = NULL, _u32 app_type = Session::COMMAND_APP_UNKNOW)
    {
        return m_thread_handlers[fd%m_thread_num]->add_new_socket(fd,wait_cmd,app_type);
    }
    bool add_pipe_event(const PipeEvent &event)
    {
        return m_thread_handlers[event.m_fd%m_thread_num]->add_pipe_event(event);
    }
    void start_threads()
    {
        for(int i=0; i<m_thread_num; i++)
        {
            m_thread_handlers[i]->start();
        }
    }
private:
    int m_thread_num;
    
    vector< SessionThread<SessionType>* > m_thread_handlers;
private:
    DECL_LOGGER(logger);
};
template <typename SessionType > 
IMPL_LOGGER(SessionThreadPool<SessionType>, logger);

#endif // #ifndef __QUERYDB_THREADPOLL_H_20091119_1419

