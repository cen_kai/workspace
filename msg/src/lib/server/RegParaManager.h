#ifndef __REG_PARA_MANAGER_H_201008271510__
#define __REG_PARA_MANAGER_H_201008271510__
#include "common/SDLogger.h"
#include "common/common.h"
#include "common/Utility.h"

class RegParaManager
{
public:
    static RegParaManager* get_instance()
    {
        if(__instance ==  NULL)
        {
            __instance = new RegParaManager();
        }

        return __instance;
    }

    RegParaManager();
    bool InitPara();

public:
    map<string, string> m_str_para_map;
    map<string, int> m_int_para_map;

    _u32 m_iReportRandomRatioBase;
    _u32 m_iReportRandomRatioNeed;
    _u32 m_iKrdsDeployStart;
    _u32 m_iKrdsDeployEnd;
private:
    static RegParaManager* __instance;

    DECL_LOGGER(logger);    
};

#endif


