#ifndef __CLIENT_THREAD_H_20100719_1609
#define  __CLIENT_THREAD_H_20100719_1609

#include "SessionThreadPool.h"
#include "ClientThreadData.h"


//适用于短链接客户端，长连接客户端建议开线程池，用同步阻塞方式
template <typename SessionType >
class ClientThread
{
public:
    ClientThread(int thread_num, int session_timeout_ms, int new_socket_event_type=IOEventServer::EVENT_WRITABLE)
    {        
        m_session_thread_pool = new SessionThreadPool<SessionType>(thread_num, session_timeout_ms, new_socket_event_type);
        
        return;
    }

    //保持和ServiceThread调用形式一致
    bool start(bool force_restart = false)
    {
        m_session_thread_pool->start_threads();
        return true;
    }

    bool add_pipe_event(const PipeEvent &event)
    {
        return m_session_thread_pool->add_pipe_event(event);
    }
private:
private:
    SessionThreadPool<SessionType> *m_session_thread_pool;
    SDQueue<AddClientData> m_new_client_data;
private:
    DECL_LOGGER(logger);
};

template <typename SessionType > 
IMPL_LOGGER(ClientThread<SessionType>, logger);

#endif


