//==============================================================
// EventServerFactory.h : interface of class EventServerFactory 
//      (this class encapsulates how to create IOEventServer implementation classes)
//
// Author: JeffLuo
// Created: 2007-01-29
//===============================================================

#include "common/common.h"
#include "EventServerFactory.h"
#include "ServerApp.h"
#include "PollEventServer.h"
#include "EpollEventServer.h"
#include <sys/epoll.h>
#include <errno.h>

extern ServerApp g_app;

EventServerFactory* EventServerFactory::__instance;
int EventServerFactory::_epoll_available = -1;

IMPL_LOGGER(EventServerFactory, logger);

EventServerFactory::EventServerFactory()
{
    read_conf();
}

void EventServerFactory::read_conf()
{
    m_cur_event_mechanism = g_app.get_conf_string("server.event.mechanism");
}

EventServerFactory* EventServerFactory::get_instance()
{
    if(__instance == NULL)
    {
        __instance = new EventServerFactory();
    }

    return __instance;
}

IOEventServer* EventServerFactory::create_event_server()
{
    IOEventServer* ev_server = NULL;

    if(m_cur_event_mechanism.empty() || m_cur_event_mechanism == "epoll")
    {
        LOG4CPLUS_DEBUG(logger, "try to create event_server using epoll.");
        if(is_epoll_available())
        {
            LOG4CPLUS_WARN(logger, "current event mechanism: epoll");
        
            ev_server = new EpollEventServer();
            return ev_server;
        }
    }

    // resort to poll
    LOG4CPLUS_WARN(logger, "current event mechanism: poll");    
    ev_server = new PollEventServer();
    return ev_server;
}

bool EventServerFactory::is_epoll_available()
{
    const static int EPOLL_EVENT_SIZE = 64;

    if(_epoll_available != -1)
        return _epoll_available != 0;

    int epoll_fd = epoll_create(EPOLL_EVENT_SIZE);
    if(epoll_fd == -1)
    {
        LOG4CPLUS_WARN(logger, "epoll_create() failed, errno=" << errno << "(" << strerror(errno) << ").");
        _epoll_available = 0;
        return false;
    }

    close(epoll_fd);
    _epoll_available = 1;
    
    return true;
}


