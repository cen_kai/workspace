//============================================================
// ServerApp.h : implementation of class ServerApp
//                           (distribution server application context)
// Author: JeffLuo
// Created: 2006-09-04
//============================================================

#include "ServerApp.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include "common/Utility.h"
//#include "statcommand/StatCmdServerTraffic.h"
//#include "SendFileThreadPool.h"

//extern ServerApp g_app;

IMPL_LOGGER(ServerApp, logger);

P2PGlobalStat::P2PGlobalStat()
{
    m_cur_sessions = 0;
    m_max_sessions = 0;
    m_total_sessions = 0;
    m_cut_by_slowspeeds = 0;

    m_cur_band = 0;

    m_total_files = 0;
    m_active_files = 0;

    m_local_files = 0;
    m_send_storagefiles= 0;
    m_recv_storagefiles = 0;
    
    m_old_p2p_uploaded = 0;
    m_cdn_protocol_uploaded = 0;

    m_unfinish_cur_sessions = 0;
    m_unfinish_max_sessions = 0;
    m_unfinish_total_sessions = 0;

    m_unfinish_old_p2p_uploaded = 0;
    m_unfinish_cdn_protocol_uploaded = 0;
    m_unfinish_files = 0;

    bzero(m_partfile_request_num, sizeof(m_partfile_request_num));

    //jidong.yu 20071226
    m_read_times = 0;
    m_readv_times = 0;
    m_scatter_times = 0;
    m_fail_times = 0;
    m_readss_times = 0;


    m_write_times = 0;
    m_writev_times = 0;
    m_gather_times = 0;
    m_fail_write_times = 0;
    m_combin_reqs = 0;
    //for scatter stat
    //end
}

void P2PGlobalStat::reset()
{
    m_max_sessions = m_cur_sessions;
    m_total_sessions = 0;

    m_active_files = 0;
    
    m_old_p2p_uploaded = 0;
    m_cdn_protocol_uploaded = 0;

    m_cut_by_slowspeeds = 0;

}

void P2PGlobalStat::unfinish_reset()
{
    m_unfinish_max_sessions = m_unfinish_cur_sessions;
    m_unfinish_total_sessions = 0;

    m_unfinish_old_p2p_uploaded = 0;
    m_unfinish_cdn_protocol_uploaded = 0;
}

string P2PGlobalStat::get_stat_details()
{
    char details[1024];
#if 0
    char* buffer = details;
    // sessions 
    int written = sprintf(buffer, "(sessions,%d,%d,%d)", m_cur_sessions, m_max_sessions, m_total_sessions);
    if(written < 0)
        return "";
    buffer += written;

    // # of files
    written = sprintf(buffer, ", (files,%d,%d,%d,%d,%d)", 
        m_total_files, m_local_files, m_send_storagefiles, m_recv_storagefiles, m_active_files);
    if(written < 0)
        return "";
    buffer += written;    

    // # 
    written = sprintf(buffer, ", (cut_no_slow,%d)", m_cut_by_slowspeeds);
    if(written < 0)
        return "";
    buffer += written;    

    // uploaded
    string old_p2p_up = Utility::filesize_readable_text(m_old_p2p_uploaded);
    string cdn_protocol_up = Utility::filesize_readable_text(m_cdn_protocol_uploaded);    
    written = sprintf(buffer, ", (upload,%s,%s)", old_p2p_up.c_str(), cdn_protocol_up.c_str());
    if(written < 0)
        return "";
    buffer += written;

    _u64 now = Utility::current_time_ms();

    m_cur_band = (int)m_speed_stat.get_avg_speed(5, now);
    written = sprintf(buffer, ", (band=%d, level=%d)", m_cur_band, g_app.get_bandwidth_control_level());    
    buffer += written;
#endif

    return (string)details;
}


string P2PGlobalStat::get_partfile_request_details()
{
    char details[10240] = {0};

    char* buffer = details;

    int written = snprintf(buffer, 10239, "partfile request details: ");
    if(written < 0)
        return "";
    buffer += written;

    for(int i = 0; i < MAX_PART_NUM; i++)
    {
        written = snprintf(buffer, 10239, "part%d=%u  ", i, m_partfile_request_num[i]);
        if(written < 0)
            return "";
        buffer += written;
    }

    return (string)details;
}



string P2PGlobalStat::get_unfinish_stat_details()
{
    char details[1024];
#if 0
    char* buffer = details;
    // sessions 
    int written = sprintf(buffer, "(sessions,%d,%d,%d)", m_unfinish_cur_sessions, m_unfinish_max_sessions, m_unfinish_total_sessions);
    if(written < 0)
        return "";
    buffer += written;

    // uploaded
    string old_p2p_up = Utility::filesize_readable_text(m_unfinish_old_p2p_uploaded);
    string cdn_protocol_up = Utility::filesize_readable_text(m_unfinish_cdn_protocol_uploaded);    
    written = sprintf(buffer, ", (upload,%s,%s)", old_p2p_up.c_str(), cdn_protocol_up.c_str());
    if(written < 0)
        return "";
    buffer += written;

    _u64 now = Utility::current_time_ms();
    written = sprintf(buffer, ", (band=%d, level=%d)", (int)m_unfinish_speed_stat.get_avg_speed(5, now), g_app.get_bandwidth_control_level());    
    buffer += written;
#endif
    return (string)details;
}

#if 0
StatCommand* P2PGlobalStat::get_stat_as_cmd() const
{
    StatCmdServerTraffic* server_traffic = new StatCmdServerTraffic();

    Utility::ip_ntoa(g_app.get_external_ip(), server_traffic->m_server_ip, 20);
    server_traffic->m_stat_time = (int)time(NULL);
    server_traffic->m_time_span = g_app.get_conf_int("server.p2pstat.dump.interval.seconds");
    server_traffic->m_cur_user_count = m_cur_sessions;
    server_traffic->m_max_user_count = m_max_sessions;
    server_traffic->m_file_count = m_total_files;
    server_traffic->m_active_file_count = m_active_files;
    server_traffic->m_uploaded_bytes = m_old_p2p_uploaded + m_cdn_protocol_uploaded;

    return server_traffic;
}
#endif

ServerApp::ServerApp()
{
    m_admin_server_socket = -1;
    m_admin_server_port = 0;
    m_server_bind_addr = 0;

    m_transfer_svr_listen_port = 0;
    
    m_product_major_ver = 0;
    m_product_build_num = 0;    
    m_product_flag = 0;

    m_max_refused_times = -1;

    m_system_page_size = 0;

    m_max_allowed_conns = -1;
    m_total_speed_max = -1;
    m_speed_over_limit_sleep_ms = -1;    
    m_bandwidth_control_level = NO_BANDWIDTH_LIMIT;
    m_bandwidth_chg_time = 0;

    m_p2s_cmd_next_seq_no = 10;

    m_cur_time = time(NULL);
    m_dyna_config = NULL;
    m_dyna_cfg = NULL;

    m_low_upload_ver = 0;
    m_ver_policy = 0;
    m_web_low_upload_ver = 0;
    m_web_ver_policy = 0;

    m_upload_unfinished_files = -1;

    m_file_stat = -1;

    m_ignore_nocid_session = 1;
    m_report_tracker_comlpete_file = 1;
    m_default_speed_threshold_kb = -1;

    m_upload_slow_speed = 0;
    m_peer_speed_diff_kb = 60;

    m_divide_dir = -1;
    m_dir_num = 0;

    m_storage_port = -1;

    m_named_bycid = -1;

    m_cut_min_time = -1;

    m_loadseedfile_fromfile= -1;

    m_is_pub_hotcid = -1;

    m_check_file_before_send = -1;
    
    int ret = pipe(m_notify_pipe_fds);
    if(ret == -1)
    {
        // LOG4C is not ready, so dump msg to stderr
        fprintf(stderr, "create pipe failed, errno=%d\n", errno);
    }

    // 2007-07-20
    // add by Kang
    ret = pipe(m_notify_sendfile_result_pipe_fds);
    if(ret == -1)
    {
        // LOG4C is not ready, so dump msg to stderr
        fprintf(stderr, "create notify_sendfile_result_pipe failed, errno=%d\n", errno);
    }
    // end

    m_fadvise_support_status = STATUS_UNKNOWN;

    m_upload_revise_level = 0;

    m_transfer_client_port = -1;

    m_delete_time_out_file =  false;
}

int ServerApp::get_distserver_type()
{
    return m_distserver_type;
}

// 2007-07-09
// add by Kang
bool ServerApp::control_peer_speed()
{
    return m_control_peer_speed;
}
// end

string ServerApp::get_transfer_client_ip()
{
    if(m_transfer_client_ip.empty())
    {
        m_transfer_client_ip = get_conf_string("server.transfer_client_ip", "127.0.0.1");
    }
    return m_transfer_client_ip;
}

int ServerApp::get_transfer_client_port()
{
    if(m_transfer_client_port == -1)
    {
        m_transfer_client_port = get_conf_int("server.transfer_client_port", 19010);
    }
    return m_transfer_client_port;
}


bool ServerApp::load_conf()
{
    if(m_cfg_file.empty())
    {
        LOG4CPLUS_WARN(logger, "Application config file not specified!!");
        return false;
    }

    bool succ = m_cfg_reader.load(m_cfg_file.c_str());
    if(succ)
    {
        string dyna_conf_file = m_cfg_reader.get_string("server.dynamic.conf.file");
        m_dyna_config = new DynamicConfiguration(dyna_conf_file.c_str());
    }

    m_dyna_cfg = new ConfigWriter(m_dyna_cfg_file.c_str());
    
    m_distserver_type = get_conf_int("server.type", 1);
    if(get_conf_int("server.queryreslevel.enabled", 0) == 1)
    {
        m_distserver_type = 0;
    }

    m_distserver_reportrc_enabled = (get_conf_int("server.reportrc.enabled", 1) != 0);
    
    m_upload_allowed_reslevel = get_conf_int("server.upload.policy.allowed.reslevel", 2);

    m_max_filesize_can_dist = get_conf_int("server.max_filesize_can_dist.mb", 300) * 1024 * 1024;

    m_control_peer_speed = (get_conf_int("server.upload.policy.control.peer.speed", 0) == 1);

    m_low_upload_ver = get_conf_int("server.thunderclient.gui.version", 0);
    m_ver_policy = get_conf_int("server.thunderclient.gui.version.policy", 0);
    m_web_low_upload_ver = get_conf_int("server.thunderclient.web.version", 0);
    m_web_ver_policy = get_conf_int("server.thunderclient.web.version.policy", 0);

    m_finishing_total_upload_file = get_conf_string("server.finishing.total.upload.file");
    m_ignore_nocid_session = get_conf_int("server.p2psession.log.ignore.nocid.sessions",1);
    m_report_tracker_comlpete_file = get_conf_int("server.report.tracker.complete.file",1);
    m_upload_slow_speed = get_conf_int("server.upload.policy.only.upload.slow.peers", 0);
    m_peer_speed_diff_kb = get_conf_int("server.upload.policy.slow.peers.speed.diff.kb", 60);

    int mirror_num = get_conf_int("server.mirrorsvr.number", 0);
    char ipkey[64];
    char portkey[64];
    char transferportkey[64];
    char pubdirkey[64];
    char urldirkey[64];
    for(int i = 0; i < mirror_num; i ++)
    {
        memset(ipkey, 0, 64);
        memset(portkey, 0, 64);
        memset(transferportkey, 0, 64);
        memset(pubdirkey, 0, 64);
        memset(urldirkey, 0, 64);
        snprintf(ipkey, 63, "server.mirrorsvr.%d.ip", i);
        snprintf(portkey, 63, "server.mirrorsvr.%d.port", i);
        snprintf(transferportkey, 63, "server.mirror.transfersvr.%d.port", i);
        snprintf(pubdirkey, 63, "server.mirrorsvr.%d.pubdir", i);
        snprintf(urldirkey, 63, "server.mirrorsvr.%d.urldir", i);
        MirrorSvr tMirrorSvr;
        tMirrorSvr.ip = get_conf_string(ipkey);
        tMirrorSvr.port = get_conf_int(portkey, 3087);
        tMirrorSvr.transferport = get_conf_int(transferportkey, 19008);
        tMirrorSvr.pubdir = get_conf_string(pubdirkey);
        tMirrorSvr.urldir = get_conf_string(urldirkey);
        m_mirror_svr_list.push_back(tMirrorSvr);
    }

    char conf_key[256];
    int pubsvr_num = get_conf_int("server.pub.server.number", 0);
    m_is_storage_distsvr = (pubsvr_num > 0);
    if(m_is_storage_distsvr)
    {
        m_distserver_reportrc_enabled = false;
    }
    
    LOG4CPLUS_DEBUG(logger, " pubsvr_num " << pubsvr_num);
    for(int i = 0; i < pubsvr_num; i++)
    {
        ServerAddr addr;
        snprintf(conf_key, 255, "server.pub.server.%d.ip",  i);
        addr.m_ip = get_conf_string(conf_key);

        snprintf(conf_key, 255, "server.pub.server.%d.port",  i);
        addr.m_port = get_conf_int(conf_key);
        LOG4CPLUS_DEBUG(logger, " addr.m_ip (" << i << ") = " << addr.m_ip);
        LOG4CPLUS_DEBUG(logger, " addr.m_port (" << i << ") = " << addr.m_port);
        m_pubsvr_list.push_back(addr);
    }

    m_storage_ip = get_conf_string("server.storagesvr.ip", "");

    //added by tianming,20071224
    //0 means deafult, 1 means use scatter read
    int tempvalue = get_conf_int("server.read.mechanism", 0);
    m_isScatterRead =  (tempvalue==1?true:false);
    LOG4CPLUS_INFO(logger, " read model is "<<m_isScatterRead);
    m_is_gather_write = (1== get_conf_int("server.write.mechanism", 0));

    m_delete_time_out_file = (get_conf_int("server.delete.timeout.file", 0) == 1);
     m_cdn_type = get_conf_int("server.cdn.type", 0);
     m_max_allow_size = get_conf_int("server.cdn.max.allow.size", 0);
     
     m_max_allowed_conns = get_conf_int("server.upload.policy.max.connections");


     LOG4CPLUS_DEBUG(logger, " m_cdn_type is "<<m_cdn_type<<", m_max_allow_size is  "<<m_max_allow_size<<", m_max_allowed_conns is "<< m_max_allowed_conns);


     m_cdnmgr_ip = get_conf_string("transfer.cdnmgr.ip", "cl.kankan.xunlei.com");
     m_cdnmgr_port = get_conf_int("transfer.cdnmgr.port", 8880);
    
    return succ;
}

bool ServerApp::load_upload_server_port_conf()
{
    string port_source = get_conf_string("server.upload.server.port");
    const char *delimitor = ":";
    vector <string> port_array;
    if(Utility::split(port_source, delimitor, port_array) == 0)
    {
        LOG4CPLUS_WARN(logger, "upload server port not specifed in configuration file!!");
        return false;
    }

    int port;
    for(_u32 i=0; i<port_array.size(); i++)
    {
        port = atoi(port_array[i].c_str());
        if(port <= 0)
        {
            LOG4CPLUS_WARN(logger, "upload server port " << i << " invalid: " << port_array[i]);
            return false;
        }

        m_upload_server_port.push_back(port);
    }
    return true;
}

int ServerApp::get_upload_server_listen_port_num()
{
    return m_upload_server_port.size();
}

int ServerApp::get_upload_server_socket(int index)
{ 
    if(index < 0 || index >= (int)m_upload_server_socket.size())
    {
        LOG4CPLUS_WARN(logger, "get upload server socket use invalid index: " << index);
        return -1;
    }

    return m_upload_server_socket[index]; 
}

int ServerApp::get_upload_server_listen_port(int index)
{ 
    if(index < 0 || index >= (int)m_upload_server_port.size())
    {
        LOG4CPLUS_WARN(logger, "get upload server port use invalid index: " << index);
        return -1;
    }
    return m_upload_server_port[index]; 
}

int ServerApp::get_pubsvr_num()
{
    return m_pubsvr_list.size();
}

bool ServerApp::get_pubsvr_by_index(string &ip, int &port, int index)
{
    if(index < 0 || index >= (int)m_pubsvr_list.size())
    {
        return false;
    }
    ip = m_pubsvr_list[index].m_ip;
    port = m_pubsvr_list[index].m_port;
    return true;
}


int ServerApp::get_pubsvr_index_by_ip_port(const string &ip, const int &port)
{
    for(int i=0, num=(int)m_pubsvr_list.size(); i < num; i++)
    {
        if(m_pubsvr_list[i].m_ip==ip && m_pubsvr_list[i].m_port==port)
            return i;
    }
    return -1;
}


int16_t ServerApp::get_product_major_ver() 
{
    if(m_product_major_ver <= 0)
    {
        m_product_major_ver = (int16_t)get_conf_int("server.product.major.version");
    }

    return m_product_major_ver;
}


int ServerApp::is_upload_unfinished_files()
{
    if(m_upload_unfinished_files == -1)
    {
        m_upload_unfinished_files = get_conf_int("server.upload.unfinished", 1);
    }
    return m_upload_unfinished_files;
}

bool ServerApp::upload_ver4()
{
    if(m_low_upload_ver  == 0 || m_low_upload_ver == 0)
    {
        return true;
    }
    return false;
}

bool ServerApp::is_upload_ver(_u32 ver)
{
// policy
// 0: 支持全部版本(缺省值) 1: 只支持指定的版本(=) 
// 2: 只支持指定版本或以上版本(>=)  3: 只支持指定版本以下版本( < )
// 4: 只支持指定版本或下版本( <=)
    if(ver < 40000)    //web
    {
        switch(m_web_ver_policy)
        {
            case 0:
            {
                return true;
            }
            case 1:
            {
                if(ver == m_web_low_upload_ver)
                {
                    return true;
                }
                break;
            }
            case 2:
            {
                if(ver >= m_web_low_upload_ver)
                {
                    return true;
                }
                break;
            }
            case 3:
            {
                if(ver < m_web_low_upload_ver)
                {
                    return true;
                }
                break;
            }
            case 4:
            {
                if(ver <= m_web_low_upload_ver)
                {
                    return true;
                }
                break;
            }
        }
    }
    else if(ver >= 50000)
    {
        switch(m_ver_policy)
        {
            case 0:
            {
                return true;
            }
            case 1:
            {
                if(ver == m_low_upload_ver)
                {
                    return true;
                }
                break;
            }
            case 2:
            {
                if(ver >= m_low_upload_ver)
                {
                    return true;
                }
                break;
            }
            case 3:
            {
                if(ver < m_low_upload_ver)
                {
                    return true;
                }
                break;
            }
            case 4:
            {
                if(ver <= m_low_upload_ver)
                {
                    return true;
                }
                break;
            }
        }
    }
    return false;
}

string ServerApp::get_urldir()
{
    string urldir = get_conf_string("server.seedfile.collect.pub.dir.list");
    if(urldir == "")
    {
        string seedfile_dirs = get_conf_string("server.seedfile.dir.list");
        if(seedfile_dirs == "")
        {
            LOG4CPLUS_WARN(logger, "no pubdir !!!");
        }
        vector < std :: string > strlist;
        Utility::split(seedfile_dirs, ":", strlist);
        urldir = strlist[0];
    }
            
    char target_part[512] = {0};
    strcpy(target_part, urldir.c_str());
    if(target_part[strlen(target_part) - 1] != '/')
    {
        strcat(target_part, "/");
    }
    return target_part;
}

string ServerApp::dir_name(const string &filepath)
{
    if(filepath.empty())
    {
        return "";
    }
    char srcstr[512] = {0};
    strcpy(srcstr, filepath.c_str());
    char *p = srcstr;
    int len = strlen(srcstr);
    char *q = p + (len -1);
    while(*q != '/')
    {
        q --;
    }
    q ++;
    *q = 0;
    return p;
}

int ServerApp::get_amin_port()
{
    return m_admin_server_port;
}

string ServerApp::get_tmp_dir()
{
    if(m_storage_tmp_dir.empty())
    {
        m_storage_tmp_dir = get_conf_string("server.storage.seedfile.tmp.dir", get_urldir().c_str());
    }
    return m_storage_tmp_dir;
}

bool ServerApp::get_mirror_svr_by_index(int index, const string &filename, string &ip, int &port, string &pubdir, int &transferport)
{
    if(index >= 0 && index < (int)m_mirror_svr_list.size())
    {
        ip = m_mirror_svr_list[index].ip;
        port = m_mirror_svr_list[index].port;
        transferport = m_mirror_svr_list[index].transferport;
        if(filename != "")
        {
            if(get_urldir() != dir_name(filename))
            {
                pubdir = m_mirror_svr_list[index].pubdir;
            }
            else
            {
                pubdir = m_mirror_svr_list[index].urldir;
            }
        }
        LOG4CPLUS_WARN(logger, "get_urldir()=" << get_urldir() << " dir_name(filename)=" << dir_name(filename));
        return true;
    }
    else
    {
        LOG4CPLUS_WARN(logger, "get_mirror_svr_by_index fail " << filename);
        return false;
    }
}

int16_t ServerApp::get_product_build_num()
{
    if(m_product_build_num <= 0)
    {
        m_product_build_num = (int16_t)get_conf_int("server.product.build.number");
    }

    return m_product_build_num;
}

int ServerApp::get_file_stat_policy()
{
    if(m_file_stat < 0)
    {
        m_file_stat = get_conf_int("server.record.file.stat.policy",0);
    }
    return m_file_stat;
}

int ServerApp::get_dir_num()
{
    if(m_dir_num <= 0)
    {
        m_dir_num = get_conf_int("server.speed.filedir.num",50);
    }
    return m_dir_num;
}

int ServerApp::use_divide_dir()
{
    if(m_divide_dir < 0)
    {
        m_divide_dir = get_conf_int("server.usedividedir.flag",2); //0 表示不建子目录 1 表示按文件大小分子目录2 表示按cid分子目录
    }
    return m_divide_dir;
}

bool ServerApp::coll_by_storage()
{
    return !m_storage_ip.empty();
}

string ServerApp::get_storage_svr_ip()
{
    return m_storage_ip;
}

int ServerApp::get_storage_svr_port()
{
    if(m_storage_port < 0)
    {
        m_storage_port = get_conf_int("server.storagesvr.port", 3078);
    }
    return m_storage_port;
}

bool ServerApp::is_named_by_cid()
{
    if(m_named_bycid < 0)
    {
        m_named_bycid = get_conf_int("server.named.bycid",0);
    }
    return m_named_bycid==1?true:false;
}

int ServerApp::get_cut_mintime()
{
    if(m_cut_min_time < 0)
    {
        m_cut_min_time = get_conf_int("server.peer.cut.min.time.sec",30);
    }
    return m_cut_min_time;
}

bool ServerApp::loadseedfile_fromfile()
{
    if(m_loadseedfile_fromfile < 0)
    {
        m_loadseedfile_fromfile = get_conf_int("server.loadseedfile.fromfile",0);
    }
    return m_loadseedfile_fromfile != 0;
}

bool ServerApp::pub_hotcid()
{
    if(m_is_pub_hotcid < 0)
    {
        m_is_pub_hotcid = get_conf_int("server.pub.hotcid",0);
    }
    return m_is_pub_hotcid != 0;
}

string ServerApp::get_repeatfile_dir()
{
    if(m_repeatfile_dir.empty())
    {
        m_repeatfile_dir = get_conf_string("server.repeat.file.dir", "/data/repeat_file/");
    }
    return m_repeatfile_dir;
}

bool ServerApp::check_file_before_send()
{
    if(m_check_file_before_send < 0)
    {
        m_check_file_before_send = get_conf_int("server.checkfile.beforesend", 0);
    }
    return m_check_file_before_send != 0? true:false;
}

string ServerApp::get_timeout_file_dir()
{
    if(m_timeout_file_dir.empty())
    {
        m_timeout_file_dir = get_conf_string("server.timeout.file.dir", "");
    }
    return m_timeout_file_dir;
}

bool  ServerApp::get_delete_timeout_file()
{
       return m_delete_time_out_file;
}

bool ServerApp::log_finishing_total_upload(string filename, _u64 filesize, _u64 total_upload)
{
    if(m_finishing_total_upload_file.empty())
    {
        LOG4CPLUS_WARN(logger, "finishing stat filename is empty!");    
        return false;
    }
    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", m_finishing_total_upload_file.c_str(), Utility::get_date_string().c_str());

    int fd = open(full_path, O_WRONLY | O_CREAT | O_APPEND, COMMON_FILE_MODE);
    if(fd < 0)
    {
        LOG4CPLUS_WARN(logger, "failed to open file '" << full_path << "', errno=" << errno);
        return false;
    }        

    char time_desc[32];
    int len = snprintf(time_desc, sizeof(time_desc), "[%s] - ", Utility::get_time_string().c_str());
    ssize_t written = write(fd, time_desc,  len);
    if(written < 0)
    {
        LOG4CPLUS_WARN(logger, "write() returns " << written << " when try to write " << len << " bytes, errno=" << errno);
        return false;
    }
    char buf[1024];
    len = snprintf(buf, sizeof(buf), "[%s,%lu] - ", filename.c_str(), filesize);
    written = write(fd, buf, len);
    if(written < 0)
    {
        LOG4CPLUS_WARN(logger, "write() returns " << written << " when try to write " << len << " bytes, errno=" << errno);
        return false;
    }

    len = snprintf(buf, sizeof(buf), "[%lu] ", total_upload);
    written = write(fd, buf, len);
    if(written < 0)
    {
        LOG4CPLUS_WARN(logger, "write() returns " << written << " when try to write " << len << " bytes, errno=" << errno);
        close(fd);
        return false;
    }
    
    written = write(fd, "\n", 1);
    close(fd);

    return true;
}

int ServerApp::get_product_flag()
{
    if(m_product_flag <= 0)
    {
        m_product_flag = get_conf_int("server.client.product.flag");
    }

    return m_product_flag;
}

_u16 ServerApp::get_max_refused_times()
{
    if(m_max_refused_times == -1)
    {
        m_max_refused_times = get_conf_int("server.upload.policy.max.refused.times");
    }

    if(m_max_refused_times <= 0)
        return DEFAULT_REFUSED_TIMES;
    else
        return (_u16)m_max_refused_times;
}


int ServerApp::max_allowed_connections()
{
    if(m_max_allowed_conns == -1)
    {
        m_max_allowed_conns = get_conf_int("server.upload.policy.max.connections");
    }

    return m_max_allowed_conns;
}

bool ServerApp::send_by_sendfile()
{
    if(m_send_by_sendfile == -1)
    {
        m_send_by_sendfile = get_conf_int("server.upload.policy.send.by.sendfile", 1);
    }

    return m_send_by_sendfile == 1;
}


int ServerApp::p2p_total_speed_max()
{
    if(-1 == m_total_speed_max)
    {
        m_total_speed_max = get_conf_int("server.upload.policy.total.speed.max.kb");
    }

    return m_total_speed_max;
}

int ServerApp::speed_over_limit_sleep_ms()
{
    if(-1 == m_speed_over_limit_sleep_ms)
    {
        m_speed_over_limit_sleep_ms = get_conf_int("server.upload.policy.speed.overmax.sleep.ms");
    }

    return m_speed_over_limit_sleep_ms;
}

string ServerApp::get_dynamic_conf_file()
{
    if(m_dynamic_conf_file.empty())
    {
        m_dynamic_conf_file = get_conf_string("server.dynamic.conf.file");
    }

    return m_dynamic_conf_file;
}

string ServerApp::get_hub_url_prefix()
{
    if(m_hub_url_prefix.empty())
    {
        string conf_file = get_dynamic_conf_file();
        if(!conf_file.empty())
        {
            ConfigReader conf_reader(conf_file.c_str());
            if(conf_reader.load())
            {
                m_hub_url_prefix = conf_reader.get_string("server.report.hub.url.prefix");
            }                
        }
    }

    return m_hub_url_prefix;
}

void ServerApp::report_external_ip(_u32 local_external_ip)
{
    if(local_external_ip == 0 || local_external_ip == 0xFFFFFFFF)
        return;

    bool exists = false;
    for(int i = 0, size = m_server_external_ips.size(); i < size; i++)
    {
        if(local_external_ip == m_server_external_ips[i])
        {
            exists = true;
            break;
        }
    }

    if(!exists)
        m_server_external_ips.push_back(local_external_ip);
}

_u32 ServerApp::get_external_ip(int index)
{
    int cur_size = (int)m_server_external_ips.size();
    if(index < 0 || index > cur_size || (index > 0 && index == cur_size))
        return 0;

    if(index < cur_size)
        return m_server_external_ips[index];
    if(index == 0 && cur_size == 0)
    {
        string bind_ip = get_conf_string("server.upload.server.bind.ip", NULL, false);
        if(!bind_ip.empty() && bind_ip != "0")
        {
            struct in_addr addr;
            memset(&addr, 0, sizeof(addr));
            inet_aton(bind_ip.c_str(), &addr);
            if(addr.s_addr != 0 && addr.s_addr != 0xFFFFFFFF)
                m_server_external_ips.push_back(addr.s_addr);
            
            return addr.s_addr;
        }        
    
        // try to fetch external ip from configuration
        int ip_num = get_conf_int("server.external.ip.number");
        if(ip_num > 0)
        {
            string ip_addr = get_conf_string("server.external.ip.0");
            if(!ip_addr.empty())
            {
                struct in_addr addr;
                memset(&addr, 0, sizeof(addr));
                inet_aton(ip_addr.c_str(), &addr);
                return addr.s_addr;
            }
        }
    }

    return 0;
}

int ServerApp::get_admin_server_listen_port() const
{
    return m_admin_server_port; 
}

bool ServerApp::distserver_reportrc_enabled()
{ 
    return m_distserver_reportrc_enabled; 
}

bool ServerApp::is_storage_distsvr()
{
    return m_is_storage_distsvr;
}

_u64 ServerApp::get_max_filesize_can_dist()
{
    return m_max_filesize_can_dist;
}

string ServerApp::get_big_file_dir()
{
    if(m_big_file_dir.empty())
    {
        m_big_file_dir = get_conf_string("server.big.file.dir", "/data/delfiles/");
    }
    return m_big_file_dir;
}

int ServerApp::get_upload_allowed_reslevel()
{
    return m_upload_allowed_reslevel;
}

void ServerApp::set_upload_revise_level(int level)
{
    m_upload_revise_level = level;
}

int ServerApp::get_upload_revise_level()
{
    return m_upload_revise_level;
}


int ServerApp::get_transfer_svr_listen_port()
{
    if(m_transfer_svr_listen_port == 0)
    {
        m_transfer_svr_listen_port = get_conf_int("transfersvr.listen.port", 19008);
    }
    return m_transfer_svr_listen_port;
}


bool ServerApp::prepare_upload_server_socket(bool socket_block_mode)
{
    if(!load_upload_server_port_conf())
    {
        LOG4CPLUS_WARN(logger, "load upload server port failure!!");
        return false;
    }

    int sock_fd;
    for(_u32 i=0; i<m_upload_server_port.size(); i++)
    {
        sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(sock_fd == -1)
        {
            LOG4CPLUS_ERROR(logger, "create upload server socket failed.");
            return false;
        }
        
        if(!socket_block_mode && !Utility::set_fd_block(sock_fd, socket_block_mode))
        {
            LOG4CPLUS_ERROR(logger, "set upload server socket " << sock_fd << " nonblock failed");
            return false;
        }
    
        // set reuse
        int reuse = 1;
        if(setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1)
        {
            LOG4CPLUS_ERROR(logger, "set socket " << sock_fd << " SO_REUSEADDR option failed, errno is "<<errno);
            return false;
        }
    
        struct sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        addr.sin_port = htons(m_upload_server_port[i]);
    
        string bind_ip = get_conf_string("server.upload.server.bind.ip");
        if(!bind_ip.empty() && bind_ip != "0")
        {
            addr.sin_addr.s_addr = inet_addr(bind_ip.c_str());    
            m_server_bind_addr = addr.sin_addr.s_addr;
        }
    
        if(bind(sock_fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
        {
            LOG4CPLUS_ERROR(logger, "Cannot bind to port "<<m_upload_server_port[i]<<" and errno is " <<errno);
            return false;
        }
        LOG4CPLUS_INFO(logger, "succeed to bind port " << m_upload_server_port[i]);
    
        if(listen(sock_fd, SOMAXCONN) == -1)
        {
            LOG4CPLUS_ERROR(logger, "Cannot listen on port "<<m_upload_server_port[i]<<" and errno is " <<errno);
            return false;
        }
        LOG4CPLUS_INFO(logger, "upload server is listening on port "<<m_upload_server_port[i]);

        m_upload_server_socket.push_back(sock_fd);
    }
        
    return true;        
}
    
bool ServerApp::prepare_admin_server_socket(bool socket_block_mode)
{
    if(m_admin_server_port <= 0)
        m_admin_server_port = get_conf_int("game.event.admin.server.port", 18996);
    if(m_admin_server_port <= 0)    
    {
        LOG4CPLUS_WARN(logger, "admin server port not specifed in configuration file!!");
        return false;
    }

    m_admin_server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(m_admin_server_socket == -1)
    {
        LOG4CPLUS_ERROR(logger, "create admin server socket failed.");
        return false;
    }

    if(!socket_block_mode && !Utility::set_fd_block(m_admin_server_socket, socket_block_mode))
    {
        LOG4CPLUS_ERROR(logger, "set admin server socket nonblock failed");
        return false;
    }

    // set reuse
    int reuse = 1;
    if(setsockopt(m_admin_server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1)
    {
        LOG4CPLUS_ERROR(logger, "set socket SO_REUSEADDR option failed, errno is "<<errno);
        return false;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(m_admin_server_port);

    string bind_ip = get_conf_string("server.upload.server.bind.ip");
    if(!bind_ip.empty() && bind_ip != "0")
    {
        addr.sin_addr.s_addr = inet_addr(bind_ip.c_str());    
    }    

    if(bind(m_admin_server_socket, (struct sockaddr*)&addr, sizeof(addr)) == -1)
    {
        LOG4CPLUS_ERROR(logger, "Cannot bind to port "<<m_admin_server_port<<" and errno is " <<errno);
        return false;
    }
    LOG4CPLUS_INFO(logger, "succeed to bind port "<<m_admin_server_port);

    if(listen(m_admin_server_socket, SOMAXCONN) == -1)
    {
        LOG4CPLUS_ERROR(logger, "Cannot listen on port "<<m_admin_server_port<<" and errno is " <<errno);
        return false;
    }
    LOG4CPLUS_INFO(logger, "admin server is listening on port "<<m_admin_server_port);
    
    return true;
    
}

bool ServerApp::prepare_admin_udp_server_socket(bool socket_block_mode)
{
    if(m_admin_server_port <= 0)
        m_admin_server_port = get_conf_int("transfer.admin.server.port", 18996);
    if(m_admin_server_port <= 0)    
    {
        LOG4CPLUS_WARN(logger, "admin server port not specifed in configuration file!!");
        return false;
    }

    m_admin_udp_server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(m_admin_udp_server_socket == -1)
    {
        LOG4CPLUS_ERROR(logger, "create udp admin server socket failed.");
        return false;
    }

    if(!socket_block_mode && !Utility::set_fd_block(m_admin_udp_server_socket, socket_block_mode))
    {
        LOG4CPLUS_ERROR(logger, "set udp admin server socket nonblock failed");
        return false;
    }

    // set reuse
    int reuse = 1;
    if(setsockopt(m_admin_udp_server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1)
    {
        LOG4CPLUS_ERROR(logger, "set udp socket SO_REUSEADDR option failed, errno is "<<errno);
        return false;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(m_admin_server_port);

    string bind_ip = get_conf_string("server.upload.server.bind.ip");
    if(!bind_ip.empty() && bind_ip != "0")
    {
        addr.sin_addr.s_addr = inet_addr(bind_ip.c_str());    
    }    

    if(bind(m_admin_udp_server_socket, (struct sockaddr*)&addr, sizeof(addr)) == -1)
    {
        LOG4CPLUS_ERROR(logger, "Cannot bind to port "<<m_admin_server_port<<" and errno is " <<errno);
        return false;
    }
    LOG4CPLUS_INFO(logger, "succeed to bind udp port "<<m_admin_server_port);
    
    return true;
    
}

string ServerApp::dump_server_status()
{
    char buffer[4096];

    #if 0
    string dump_output;

    string time = Utility::get_time_string();

    SendFileThreadPool* sendfile_pool = SendFileThreadPool::get_instance();
    string sendfile_status = sendfile_pool->report_status();

    sprintf(buffer, "[%s] %s", time.c_str(), sendfile_status.c_str());
    #endif

    return (string)buffer;
}


