#ifndef _SESSION_CAHCE_H_
#define _SESSION_CAHCE_H_

#include <pthread.h>


#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

#include <ext/hash_map>
using namespace __gnu_cxx;

namespace __gnu_cxx
{
template<> struct hash<string>
{
    size_t operator()(const string& s) const
    {
        return __stl_hash_string(s.c_str());
    }
};
}

#include "common/SDLogger.h"
#include "common/SDThread.h"
#include "common/cmysql.h"
#include "common/ThreadSafeHashMap.h"
#include "client/Command.h"
#include "VerifyService.h"
#include "inter/SessionThreadData.h"


struct SessionCacheImp
{
    enum ClientType
    {
        WEB = 1,
        CLIENT = 2
    };

    enum VerifyState
    {
        VERIFYING = 0,
        PASS = 1,
        ILLEGLE = 2
    };

    static void init_app_name()
    {
        // ��д
    }   
    
    SessionCacheImp()
    {
        m_timestamp = time(NULL);
    }
    SessionCacheImp(_u64 uid,const string& sessionid,ClientType ctype,_u64 businessid):
        m_userid(uid),m_sessionid(sessionid),m_client_type(ctype),m_businessid(businessid)
    {
        m_timestamp = time(NULL);
    }

    void set(_u64 uid,const string& sessionid,ClientType ctype,_u64 businessid)
        
    {
        m_userid = uid;
        m_sessionid = sessionid;
        m_client_type = ctype;
        m_businessid = businessid;
    }
        
    void getplatform(string &platform)const

    {
        if(m_client_type==WEB)
        {
            platform="WEB";
        }
        else if(m_client_type==CLIENT)
            platform="CLIENT";
        else
            platform="UNKOWN";
    }

    void getappname(string &appname)const

    {
        NameIter iter=m_appname.find(m_businessid);
        if(iter != m_appname.end() )
        {
            appname=iter->second;
        }
        appname="UNKOWN";
    }

    bool timeout()
    {
        static const int32_t SESSION_FLUSH_SEC = g_app.get_conf_int("session.timeout.min", 120)*60;
        return (m_timestamp + SESSION_FLUSH_SEC)< time(NULL);
    }
    
    _u64 m_userid;
    string m_sessionid;
    
    time_t m_timestamp;
    
    VerifyState m_state;

    ClientType m_client_type;

    _u64 m_businessid;

    static map<_u64,string> m_appname ;
    typedef map<_u64,string>::iterator NameIter ;
};

struct SessionCheck
{
    SessionCheck():m_userid(0){};
    SessionCheck(_u64 userid,const string& sessionid):
        m_userid(userid),m_sessionid(sessionid){};
    SessionCheck& operator=(const SessionCheck& sc)
    {
        m_userid = sc.m_userid;
        m_sessionid = sc.m_sessionid;

        return *this;
    }
    void set(_u64 userid,const string& sessionid)
    {
        m_userid = userid;
        m_sessionid = sessionid;
    }

    _u64 m_userid;
    string m_sessionid;

    size_t hash_value() const
    {
        return (m_userid & 0x7FFFFFFF);
    }

    bool operator< (const SessionCheck & sc) const
    {
        if(this->m_userid < sc.m_userid)
        {
            return true;
        }
        else if(this->m_userid == sc.m_userid)
        {
            return this->m_sessionid < sc.m_sessionid;
        }
        else 
        {
            return false;
        }
    }
    
};



class SessionCache : public SDThread
{
public:
    static SessionCache * Instance();
    bool check_session(const SessionCacheImp& ssch_imp);
    bool get_checked_session(const SessionCheck& ssck,SessionCacheImp& ssch_imp);
    bool add_cache(const SessionCheck& ssck,const SessionCacheImp& ssch_imp);
    int get_cache_size()
    {
        return m_sessions->size();
    }
    
private:
    ThreadSafeHashMap<SessionCheck, SessionCacheImp> *m_sessions;
    VerifyService* m_verify_service;
    static SessionCache * _inst ;
    void run();
    SessionCache()
    {
        //m_verify_service = new VerifyService(3);
        m_sessions = new ThreadSafeHashMap<SessionCheck, SessionCacheImp>(10000,false,true,1000*10000);
    };

private:
    DECL_LOGGER(logger);
};

#endif
