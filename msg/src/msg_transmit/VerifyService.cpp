#include "common/Utility.h"
#include "common/SDMutexLock.h"

#include <list>
using namespace std;

#include "common/mem_leak.h"
#include "vipverify/check.h"
#include "VerifyService.h"
#include "SessionCache.h"

IMPL_LOGGER(VerifyThread, logger);


extern ServerApp g_app;

void VerifyThread::run()
{
    while(true)
    {
        SessionCacheImp* ssch_imp = NULL;
        if(m_check_queue.pop(ssch_imp,100))
        {
            int ret = 0;
            if(ssch_imp->m_client_type == SessionCacheImp::WEB)
            {
                static int web_port = g_app.get_conf_int("vip.web.port", 5687);
                
                //static string web_ip = g_app.get_conf_string("vip.web.ip", "127.0.0.1");
                static string vip_domain = g_app.get_conf_string("vip.web.domain", "websessionbin.reg");
                string  web_ip = Utility::get_first_ip_from_domainname(vip_domain.c_str());
                
                ret = Validate_web_session(static_cast<const string&>(web_ip),web_port,static_cast<const string&>(ssch_imp->m_sessionid));
                
            }
            else if(ssch_imp->m_client_type == SessionCacheImp::CLIENT)
            {
                static int client_port = g_app.get_conf_int("vip.client.port", 5687);
                //static string client_ip = g_app.get_conf_string("vip.client.ip", "127.0.0.1");
                static string vip_domain = g_app.get_conf_string("vip.client.domain", "sessiongateway.reg");
                string  client_ip = Utility::get_first_ip_from_domainname(vip_domain.c_str());
                
                ret = Validate_cli_session(static_cast<const string&>(client_ip),client_port,
                            static_cast<const string&>(ssch_imp->m_sessionid),ssch_imp->m_userid,ssch_imp->m_businessid);
            }
            else
            {
                LOG4CPLUS_ERROR(logger,"UNKnown Client Type");
                continue;
            }

            if(ret == 0)
            {
                ssch_imp->m_state = SessionCacheImp::PASS;
            }
            else if(ret == -1)
            {
                LOG4CPLUS_ERROR(logger,"ILLEGLE USER");
                ssch_imp->m_state = SessionCacheImp::ILLEGLE;
            }
            else if(ret == ERROR_CODE)
            {
                LOG4CPLUS_ERROR(logger,"VIPSERVER Connection ERROR");
                ssch_imp->m_state = SessionCacheImp::PASS;
            }
            else
            {
                LOG4CPLUS_ERROR(logger,"Check Unknown ERROR");
                ssch_imp->m_state = SessionCacheImp::PASS;
            }

            //SessionCheck ssck(ssch_imp->m_userid,ssch_imp->m_sessionid);
            //static RequestMgr *request_mgr = RequestMgr::Instance();
            //request_mgr->Reportresult(ssck,ssch_imp->m_state);
                
        }
    }
}

