#ifndef PLAY_RECORD_COMMON__H
#define PLAY_RECORD_COMMON__H

#include <string>
#include <map>
using namespace std;


#include "common/SDLogger.h"
#include "client/Command.h"

enum PlayRecordType { UNKOWNTYPE=0,VIEW_TYPE_LONG=1,VIEW_TYPE_LOCAL=2,VIEW_TYPE_CLOUD=3,VIEW_TYPE_SHORT = 4,VIEW_TYPE_MUSIC = 5};
extern map<string,int> record_type_map;
void init_record_type_map();


enum ChargeType{ VIDEO_FREE=0,VIDEO_CHARGE=1};

struct VodMovieInfo{
    uint64_t movieid;
    uint64_t submovieid;
    string gcid;
    string title;
    string channel; //频道名，如 电影，综艺，连续剧
    string play_url;
    string bitrate;//分辨率
    int chapter; //第几集
    string chapter_name;
    int    chapter_type:8; //视频类型
    string bigposter;    //横版海报地址
    string verticalposter; //竖版海报地址
    int64_t productid ;
    string version;    // 更新字段
    int updateinfo; //判断是否有更新
    string date;
    int partindex;
    int64_t charge:8 ;
    uint32_t movie_length; //影片时长，秒
    time_t load_timestamp; //从数据库加载的时间点，如果太久，应该重新加载
    int size(){
         int size=0;
         size += sizeof(uint64_t);    // movieid
         size += sizeof(uint64_t);  //submovieid
         size += gcid.size()+sizeof(string); 
         size += title.size()+sizeof(string);
         size += channel.size()+sizeof(string);
         size += play_url.size()+sizeof(string);
         size += bitrate.size()+sizeof(string);
         size += sizeof(int);    // chapter
         size += chapter_name.size()+sizeof(string);
         size += sizeof(int);  //chapter_type
         size += bigposter.size()+sizeof(string);
         size += verticalposter.size()+sizeof(string);
         size += sizeof(int64_t);  // productid
         size += sizeof(int64_t);   //charge
         size += sizeof(uint32_t);  // movie_length
         size += sizeof(time_t);  // load_timestamp 
         return size;
    }
};

struct MovieUniqueKey{
    uint64_t charge;
    uint64_t submovieid;
    MovieUniqueKey()
    {
        charge = VIDEO_FREE;
        submovieid = 0;
    }

    MovieUniqueKey(uint64_t nCharge,uint64_t nSubmovieid)
    {
        charge=nCharge;
        submovieid=nSubmovieid;
    }
    
    MovieUniqueKey(const MovieUniqueKey &key)
    {
        charge = key.charge;
        submovieid = key.submovieid;
    }

    bool operator < (const MovieUniqueKey & key) const
    {
        if( charge!=key.charge )
        {
            return charge < key.charge;
        }
        if( submovieid!=key.submovieid )
        {
            return submovieid < key.submovieid;
        }
        return false;
    }

    size_t hash_value() const
    {
        return (submovieid & 0x7FFFFFFF);
    }
    
};


struct ShortMovieInfo{
    uint64_t video_id;
    string  title;
    string  vodurl;
    string  kankanvodurl;
    uint64_t  length;
    string imgshot;   //竖版海报
    string img;   //横板海报
    int  charge;
    time_t load_timestamp; 
};

typedef MovieUniqueKey ShortMovieKey;


struct MusicInfo{
    uint64_t albumid ;
    int charge;
    string title;
    uint64_t length;
    string mv_vod_url;
    string img;
    void CalMvPlayLength(){
        uint64_t filesize=0;
        int real_bitrate=0;
        int index=0;
        const int filesize_index=7;
        const int bitrate_index=10;
        const char *p=mv_vod_url.c_str();
        const char *key_begin=p;
        const char *key_end=NULL;
        for(; *p!='\0' ; ++p){
            if('/'== *p ){
                key_end=p;
                ++index;
                if(filesize_index==index ){
                    string fsize(key_begin,key_end);
                    filesize = strtoull(fsize.c_str(),NULL,16);
                }
                if(bitrate_index==index){
                    string bitrate(key_begin,key_end);
                    real_bitrate = strtoull(bitrate.c_str(),NULL,16);
                }
                key_begin=p+1;
            }
        }
        if(real_bitrate){
            length = filesize*8/real_bitrate;
        }else{
            length = 0;
        }
    }
    time_t load_timestamp; 
};

typedef MovieUniqueKey MusicKey;


struct PlayRecordKey{
    PlayRecordType  type;
    ChargeType  charge;
    uint64_t movieid;
    string gcid;
    string url_hash;

    PlayRecordKey()
    {
        type =  UNKOWNTYPE;
        movieid = 0;
        charge = VIDEO_FREE;
        gcid = "";
        url_hash = "";
    }

    PlayRecordKey(const PlayRecordKey & key)
    {
        type = key.type;
        movieid = key.movieid;
        charge = key.charge;
        gcid = key.gcid;
        url_hash = key.url_hash;
    }

    bool operator < (const PlayRecordKey & key) const
    {
        if(type != key.type){
            return type < key.type;
        }
        if(charge != key.charge){
            return charge < key.charge;
        }
        if(movieid != key.movieid){
            return movieid < key.movieid;
        }
        if(url_hash != key.url_hash){
            return url_hash < key.url_hash;
        }
        return gcid < key.gcid;
    }
};

struct StoreKey{
    uint64_t  userid;
    struct PlayRecordKey   moviekey;
    StoreKey(){
        userid=0;
    }
    StoreKey(uint64_t user , PlayRecordKey key){
        userid = user ;
        moviekey = key ;
    }
    StoreKey(const StoreKey &key){
        userid = key.userid;
        moviekey = key.moviekey ;
    }
    bool operator < (const StoreKey &key) const 
    {
        if( userid!=key.userid){
            return userid<key.userid;
        }
        return moviekey < key.moviekey;
    }
    size_t hash_value() const
    {
        return (userid & 0x7FFFFFFF);
    }
};

struct StoreInfo{
    int  change:4;    //有新观看，是否已经通知过
    StoreInfo(){
        change = 1 ;
    }
    StoreInfo(int notified){
        change = notified ;
    }
    bool isChange(){
        return (change==1);
    }
    bool Clear(){
        change = 0 ;
        return 1;
    }
};

class PlayRecord{
public:
    PlayRecord() 
    {
        view_type = UNKOWNTYPE;
        _changed_time = 0;
        _save_time = 0;
    }

    virtual ~PlayRecord()
    {
    }
    //判断是否应该存数据库
    bool NeedUpdate() const
    {
        time_t  now=time(NULL);
        if( _save_time + 60*30 < now )//已经有30分钟没保存了
            return true;
        if(_changed_time <= _save_time)
            return false;
        if(_changed_time+60*10<now)   //10分钟不再更新
            return true;
        return false;
    }
    //判断在2小时内是否有过更新
    bool IsOld()const
    {
        static const int32_t RECORD_FLUSH_SEC = g_app.get_conf_int("record.update.sec", 60*60*2);
        time_t now = time(NULL);
        if(_changed_time == 0)
        {
           return false;
        }
        if(_changed_time + RECORD_FLUSH_SEC < now)
        {
            return true;
        }
        return false;
    }

    void SetUpdateFlag()
    {
        _changed_time = time(NULL);
    }

    void ClearUpdateFlag()
    {
        _save_time = time(NULL);
    }

    bool operator < (const PlayRecord &rec) const
    {
        return play_time < rec.play_time;
    }

public:
    PlayRecordType view_type;
    uint64_t userid;
    uint32_t viewed_time; //已经看了多少秒
    uint32_t movie_length; 
    time_t play_time; //观看的时间点(unixtime) 

    string device_type; //pc, iphone, ipad, nexus等设备
    string device_version; //
    string product_name; //Xmp, web, ipadkankan 等
    string product_version; //

    virtual void toJson(stringstream &sstream) const;
    virtual uint64_t KeyID() = 0 ;
    virtual int size();
    
public:
    time_t _changed_time;
    time_t _save_time;
    DECL_LOGGER(logger);
};


bool less_record_pointer(const PlayRecord* rl,const PlayRecord* rr );
bool greater_record_pointer(const PlayRecord* rl,const PlayRecord* rr );


class VodPlayRecord : public PlayRecord{
public:
    VodPlayRecord(){ view_type = VIEW_TYPE_LONG; }
    VodPlayRecord(VodPlayRecord *p)
    {
        view_type = p->view_type ;
        userid = p->userid ;
        viewed_time = p->viewed_time ;
        movie_length =  p->movie_length ; 
        play_time =  p->play_time ; 

        device_type = p->device_type ; 
        device_version = p->device_version ; 
        product_name =  p->product_name ; 
        product_version =  p->product_version ; 
    
        _changed_time = p->_changed_time ;
        _save_time =  p->_save_time;
    
        movie = p->movie ;
        litimageaddr = p->litimageaddr ;
    }
    VodMovieInfo movie;
    int   is_stored;   //是否收藏
    string litimageaddr; //缩略图地址
    
    static VodPlayRecord* TryCreate(HttpCmd* cmd, string &errinfo)
    {
        _u64  timepoint ;
        if(!cmd->get_cmd_ulvalue("movietiming",timepoint))
        {
            errinfo = "err movietiming";
            return NULL;
        }

        _u64 playtime ;
        if(!cmd->get_cmd_ulvalue("playtime",playtime))
        {
            errinfo = "err playtime";
            return NULL;
        }
        time_t now = time(NULL);
        if((long int)playtime > now)
        {
            playtime = now;
        }
        

        _u64 subid;
        if(!cmd->get_cmd_ulvalue("subid",subid))
        {
            errinfo = "err subid";
            return NULL;
        }

        string  litimageaddr("");
        if(!cmd->get_cmd_string("litimageaddr",litimageaddr))
        {
            LOG4CPLUS_INFO(logger,"no litimageaddr");
        }
        

        string device_type;
        if(!cmd->get_cmd_string("devicetype",device_type))
        {
            errinfo = "err devicetype";
            return NULL;
        }

        string device_version;
        if(!cmd->get_cmd_string("deviceversion",device_version))
        {
            errinfo = "err deviceversion";
            return NULL;
        }
        string product_name;
        if(!cmd->get_cmd_string("product",product_name))
        {
            errinfo = "err productname";
            return NULL;
        }
        string product_version;
        if(!cmd->get_cmd_string("productversion",product_version))
        {
            errinfo = "err productversion";
            return NULL;
        }
        
        uint64_t  charge;
        if(!cmd->get_cmd_ulvalue("charge",charge))
        {
            errinfo = "no charge type";
            return NULL;
        }
        
        VodPlayRecord* p_record = new VodPlayRecord();
        
        p_record->movie.charge=charge;
        p_record->movie.submovieid=subid;
    
        p_record->litimageaddr = litimageaddr;
        p_record->play_time = playtime;
        p_record->viewed_time = timepoint;
        
        p_record->device_type = device_type;
        p_record->device_version = device_version;
        p_record->product_name = product_name;
        p_record->product_version = product_version;

        return p_record;
        
    }
    virtual void toJson(stringstream &sstream)const;

    virtual uint64_t KeyID()
        {
            return movie.movieid ;
        }

    virtual  int size();
    
    void CalScreenShot()
        {
            stringstream str;
            str << movie.movieid/1000 << "/" << movie.movieid << "/" << movie.chapter
                << "_"<<movie.chapter_type <<"_";
            litimageaddr = str.str();
        }
    
};



class ShortPlayRecord : public PlayRecord{
public:
    ShortPlayRecord(){ view_type = VIEW_TYPE_SHORT; }
    ShortPlayRecord(ShortPlayRecord *p)
    {
        view_type = p->view_type ;
        userid = p->userid ;
        viewed_time = p->viewed_time ;
        movie_length =  p->movie_length ; 
        play_time =  p->play_time ; 

        device_type = p->device_type ; 
        device_version = p->device_version ; 
        product_name =  p->product_name ; 
        product_version =  p->product_version ; 
    
        _changed_time = p->_changed_time ;
        _save_time =  p->_save_time;

        shortmovie = p->shortmovie ;
    }
    ShortMovieInfo shortmovie;
    
    static ShortPlayRecord* TryCreate(HttpCmd* cmd, string &errinfo)
    {
        _u64  timepoint ;
        if(!cmd->get_cmd_ulvalue("movietiming",timepoint))
        {
            errinfo = "err movietiming";
            return NULL;
        }

        _u64 playtime ;
        if(!cmd->get_cmd_ulvalue("playtime",playtime))
        {
            errinfo = "err playtime";
            return NULL;
        }
        time_t now = time(NULL);
        if((long int)playtime > now)
        {
            playtime = now;
        }
        
        _u64 videoid;
        if(!cmd->get_cmd_ulvalue("videoid",videoid))
        {
            errinfo = "err videoid";
            return NULL;
        }

        string device_type;
        if(!cmd->get_cmd_string("devicetype",device_type))
        {
            errinfo = "err devicetype";
            return NULL;
        }

        string device_version;
        if(!cmd->get_cmd_string("deviceversion",device_version))
        {
            errinfo = "err deviceversion";
            return NULL;
        }
        string product_name;
        if(!cmd->get_cmd_string("product",product_name))
        {
            errinfo = "err productname";
            return NULL;
        }
        string product_version;
        if(!cmd->get_cmd_string("productversion",product_version))
        {
            errinfo = "err productversion";
            return NULL;
        }
        
        uint64_t  charge;
        if(!cmd->get_cmd_ulvalue("charge",charge))
        {
            errinfo = "no charge type";
            return NULL;
        }
        
        ShortPlayRecord* p_record = new ShortPlayRecord();
        
        p_record->shortmovie.charge=charge;
        p_record->shortmovie.video_id=videoid;
        p_record->play_time = playtime;
        p_record->viewed_time = timepoint;
        
        p_record->device_type = device_type;
        p_record->device_version = device_version;
        p_record->product_name = product_name;
        p_record->product_version = product_version;

        return p_record;
    }
    virtual void toJson(stringstream &sstream)const;

    virtual uint64_t KeyID()
    {
        return shortmovie.video_id ;
    }
};


class MusicPlayRecord : public PlayRecord{
    public:
        MusicInfo music;
        MusicPlayRecord(){ view_type = VIEW_TYPE_MUSIC; };
        MusicPlayRecord(MusicPlayRecord *p)
        {
            view_type = p->view_type ;
            userid = p->userid ;
            viewed_time = p->viewed_time ;
            movie_length =  p->movie_length ; 
            play_time =  p->play_time ; 

            device_type = p->device_type ; 
            device_version = p->device_version ; 
            product_name =  p->product_name ; 
            product_version =  p->product_version ; 
        
            _changed_time = p->_changed_time ;
            _save_time =  p->_save_time;

            music = p->music ;
        }
        static MusicPlayRecord* TryCreate(HttpCmd *cmd, string &errinfo)
            {
                  _u64  timepoint ;
                if(!cmd->get_cmd_ulvalue("movietiming",timepoint))
                {
                    errinfo = "err movietiming";
                    return NULL;
                }

                _u64 playtime ;
                if(!cmd->get_cmd_ulvalue("playtime",playtime))
                {
                    errinfo = "err playtime";
                    return NULL;
                }
                time_t now = time(NULL);
                if((long int)playtime > now)
                {
                    playtime = now;
                }
        
                
                _u64 albumid;
                if(!cmd->get_cmd_ulvalue("albumid",albumid))
                {
                    errinfo = "err albumid";
                    return NULL;
                }

                string device_type;
                if(!cmd->get_cmd_string("devicetype",device_type))
                {
                    errinfo = "err devicetype";
                    return NULL;
                }

                string device_version;
                if(!cmd->get_cmd_string("deviceversion",device_version))
                {
                    errinfo = "err deviceversion";
                    return NULL;
                }
                string product_name;
                if(!cmd->get_cmd_string("product",product_name))
                {
                    errinfo = "err productname";
                    return NULL;
                }
                string product_version;
                if(!cmd->get_cmd_string("productversion",product_version))
                {
                    errinfo = "err productversion";
                    return NULL;
                }
                
                uint64_t  charge;
                if(!cmd->get_cmd_ulvalue("charge",charge))
                {
                    errinfo = "no charge type";
                    return NULL;
                }
                
                MusicPlayRecord* p_record = new MusicPlayRecord();
                
                p_record->music.charge=charge;
                p_record->music.albumid=albumid;
                p_record->play_time = playtime;
                p_record->viewed_time = timepoint;
                
                p_record->device_type = device_type;
                p_record->device_version = device_version;
                p_record->product_name = product_name;
                p_record->product_version = product_version;

                return p_record;
            }
        virtual void toJson(stringstream &sstream)const;

        virtual uint64_t KeyID()
        {
            return music.albumid ;
        }
};


class LocalPlayRecord : public PlayRecord{ 
public:
    LocalPlayRecord() { view_type = VIEW_TYPE_LOCAL; }
    static LocalPlayRecord* TryCreate(HttpCmd* cmd, string &errinfo)
    {
        string gcid;
        if(!cmd->get_cmd_string("moviegcid",gcid))
        {
            errinfo = "err moviegcid";
            return NULL;
        }

        string title;
        if(!cmd->get_cmd_string("movietitle",title))
        {
            errinfo = "err movietitle";
        }
        
        _u64  timepoint ;
        if(!cmd->get_cmd_ulvalue("movietiming",timepoint))
        {
            errinfo = "err movietiming";
            return NULL;
        }

        _u64 playtime ;
        if(!cmd->get_cmd_ulvalue("playtime",playtime))
        {
            errinfo = "err playtime";
            return NULL;
        }
        time_t now = time(NULL);
        if((long int)playtime > now)
        {
            playtime = now;
        }
        
        
        _u64 movie_length;
        if(!cmd->get_cmd_ulvalue("movielength", movie_length))
        {
            errinfo = "err movie_length";
            return NULL;
        }

        string device_type;
        if(!cmd->get_cmd_string("devicetype",device_type))
        {
            errinfo = "err devicetype";
            return NULL;
        }

        string device_version;
        if(!cmd->get_cmd_string("deviceversion",device_version))
        {
            errinfo = "err deviceversion";
            return NULL;
        }
        string product_name;
        if(!cmd->get_cmd_string("product",product_name))
        {
            errinfo = "err productname";
            return NULL;
        }
        string product_version;
        if(!cmd->get_cmd_string("productversion",product_version))
        {
            errinfo = "err productversion";
            return NULL;
        }
        

        LocalPlayRecord* p_record = new LocalPlayRecord();

        p_record->gcid = gcid;                
        p_record->viewed_time = timepoint;
        p_record->play_time = playtime;
        p_record->title = title;
        p_record->movie_length = movie_length;
        
        p_record->device_type = device_type;
        p_record->device_version = device_version;
        p_record->product_name = product_name;
        p_record->product_version = product_version;

        return p_record;
        
    }

    virtual void toJson(stringstream &sstream)const;
    virtual uint64_t KeyID()
    {
        return 0 ;
    }
    string cid;
    string gcid;
    uint64_t filesize;
    string title; 
};

class CloudPlayRecord : public PlayRecord{
public:    
    CloudPlayRecord() { view_type = VIEW_TYPE_CLOUD; }
    static CloudPlayRecord* TryCreate(HttpCmd* cmd, string &errinfo){
        string gcid;
        if(!cmd->get_cmd_string("gcid", gcid)){
            errinfo = "err moviegcid";
            return NULL;
        }

        string url_hash;
        if(!cmd->get_cmd_string("urlhash", url_hash)){
            errinfo = "err urlhash";
            return NULL;
        }

        string cid;
        if(!cmd->get_cmd_string("cid", cid)){
            errinfo = "err cid";
            return NULL;
        }

        string url;
        if(!cmd->get_cmd_string("url", url)){
            errinfo = "err url";
            return NULL;
        }

        string file_name;
        if(!cmd->get_cmd_string("filename", file_name)){
            errinfo = "err filename";
            return NULL;
        }

        uint64_t task_type ;
        if(!cmd->get_cmd_ulvalue("tasktype", task_type)){
            errinfo = "err tasktype";
            return NULL;
        }

        string src_url;
        if(!cmd->get_cmd_string("srcurl", src_url)){
            errinfo = "err srcurl";
            return NULL;
        }

        uint64_t filesize ;
        if(!cmd->get_cmd_ulvalue("filesize", filesize)){
            errinfo = "err filesize";
            return NULL;
        }

        uint64_t duration ;
        if(!cmd->get_cmd_ulvalue("duration", duration)){
            errinfo = "err duration";
            return NULL;
        }

        uint64_t playtime ;
        if(!cmd->get_cmd_ulvalue("playtime", playtime)){
            errinfo = "err playtime";
            return NULL;
        }
        time_t now = time(NULL);
        if((long int)playtime > now)
        {
            playtime = now;
        }
        

        uint64_t playflag ;
        if(!cmd->get_cmd_ulvalue("playflag", playflag)){
            errinfo = "err playflag";
            return NULL;
        }

        uint64_t createtime ;
        if(!cmd->get_cmd_ulvalue("createtime", createtime)){
            errinfo = "err createtime";
            return NULL;
        }

        CloudPlayRecord* p_record = new CloudPlayRecord();
        if(NULL == p_record){
            errinfo = "err failed to create CloudPlayRecord";
            return NULL;
        }

        p_record->gcid = gcid;
        p_record->url_hash = url_hash;
        p_record->cid = cid;
        p_record->url = url;
        p_record->file_name = file_name;
        p_record->task_type = task_type;
        p_record->src_url = src_url;
        p_record->filesize = filesize;
        p_record->movie_length = duration;
        p_record->play_time = playtime;
        p_record->playflag = playflag;
        p_record->createtime = createtime;

        return p_record;
    }

    string gcid;
    string url_hash;
    string cid;
    string url;
    string file_name;
    uint32_t task_type;
    string src_url;
    uint64_t filesize;

    uint32_t playflag;
    time_t createtime;
    
    virtual void toJson(stringstream &sstream)const;
    virtual uint64_t KeyID()
    {
        return 0 ;
    }
        
};


#endif
