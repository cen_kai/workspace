#ifndef RECORD_CACHE__H
#define RECORD_CACHE__H

#include <boost/smart_ptr/shared_ptr.hpp>
//using namespace boost; 不能直接使用这个命名空间，和threadsafehashmap中的hash_value特化冲突

#include <string>
#include <vector>

using namespace std;

#include "common/ThreadSafeHashMap.h"
#include "common/SDDBConnectionPool.h"
#include "msg_transmit/common.h"
#include "common/SDThread.h"
#include "common/SDLogger.h"
#include "common/SDMutexLock.h"
#include "msg_transmit/StoreCache.h"

class RecordCache : public SDThread{
public:
    typedef map<PlayRecordKey, boost::shared_ptr<PlayRecord> > UserRecordSetType;

    static RecordCache * GetInstance();

    void UpdateOnlinePlayRecord(PlayRecord *record);  //更新在线视频long short music    
    void UpdateLocalPlayRecord(LocalPlayRecord &record);
    void UpdateCloudPlayRecord(CloudPlayRecord &record);
    void DeleteOneVodRecord(uint64_t userid, PlayRecordKey video);
    void Clear(uint64_t userid);
    void NotifiedOneVodRecord(uint64_t userid, PlayRecordKey video, string version);
    int Query(uint64_t userid, const string & platform,uint64_t flag ,const string & app_name, /*OUT*/vector<PlayRecord*> & records);

    int NotifiedRecord(uint64_t userid,uint64_t movieid,PlayRecordType flag , ChargeType charge,StoreCache::INST_TYPE type);

private:
    string ToVersion(string version,string type);
    bool FillMovieInfo(PlayRecord *record);
    bool FillMovieInfo(VodMovieInfo & info); //根据info里面的subid和movieid填充其他字段
    bool FillMovieInfo(ShortMovieInfo & info);
    bool FillMovieInfo(MusicInfo & info);

    void run();
    void SaveToDB(boost::shared_ptr<PlayRecord> & record);
private:
    RecordCache();
    static RecordCache * _instance;
    static SDMutexLock _instance_lock;
    
    ThreadSafeHashMap<ShortMovieKey, ShortMovieInfo> * _shortmovie_info;
    ThreadSafeHashMap<MusicKey,MusicInfo> * _music_info ;
    ThreadSafeHashMap<MovieUniqueKey, VodMovieInfo> * _movie_info;

    SDDBConnectionPool _movie_info_db_pool; // movie shortmovie music共用一个连接池
    
    ThreadSafeHashMap<uint64_t, UserRecordSetType >* _play_info;

    SDDBConnectionPool _play_info_db_pool;
    
    DECL_LOGGER(logger);
};

#endif
