#ifndef JSON_UTIL_CPP
#define JSON_UTIL_CPP


#include "json_util.hpp"
#include "msg_transmit/common.h"
#include <list>
#include <vector>

using namespace std;
/**********************实现代码*****************/

template <>
inline void ToJson(const int& nValue, stringstream& outSS)
{
    outSS<<nValue;
}

template<>
inline void ToJson(const unsigned int &nValue , stringstream &outSS)
{
    outSS<<nValue ;
}

template <>
inline void ToJson(const float& fValue, stringstream& outSS)
{
    outSS<<fValue;
}


template <>
inline void ToJson(const double& dValue, stringstream& outSS)
{
    outSS<<dValue;
}

template <>
inline void ToJson(const long unsigned int& ullValue, stringstream& outSS)
{
    outSS<<ullValue;
}

template <>
inline void ToJson(const long int& llValue, stringstream& outSS)
{
    outSS<<llValue;
}



template <>
inline void ToJson(const char *pszValue, stringstream& outSS)
{
    outSS << "\"";
    int nLength = strlen(pszValue);
    for(int i=0; i<nLength; ++i)
    {
        switch(pszValue[i])
        {
            case '\'':
                outSS << "\\'";
                break;
            case '\"':
                outSS << "\\\"";
                break;            
            case '\\':
                outSS << "\\\\";
                break;
            default:
                outSS << pszValue[i];
        }
    }
    outSS << "\"";
}

template <>
inline void ToJson(const string &sValue, stringstream& outSS)
{
    ToJson(sValue.c_str(),outSS);
}

template <typename ObjType>
inline void ToJson(const ObjType &obj, stringstream& outSS)
{
    obj.toJson(outSS);
}

template <typename ObjType>
inline void ToJson(ObjType *pObj, stringstream& outSS)
{
    pObj->toJson(outSS);
}

template <typename ObjType>
inline void ToJsonPair(const ObjType &obj, stringstream& outSS, const char *pszKey=NULL)
{
    if(NULL == pszKey)
        ToJson(obj,outSS);
        
    if(pszKey)
    {
        outSS << "\"" << pszKey << "\":";
        ToJson(obj,outSS);
    }
    else
    {
        ToJson(obj,outSS);
    }
}


template<typename ObjType, template<typename T> class CollType>
inline void ToJson(const CollType<ObjType> &vObjs, stringstream& outSS)
{
    outSS << "[";
    
    typename CollType<ObjType>::const_iterator it; //typename 是必须的
    it = vObjs.begin();
    if(it != vObjs.end())
    {
        ToJson(*it,outSS);    
        for(++it; it!=vObjs.end(); ++it)
        {
            outSS << ", ";
            ToJson(*it,outSS);
        }
    }    
    
    outSS << "]";
}

/*
template<>
inline void ToJson(const list<PlayRecord> &vObjs, stringstream& outSS)
{
        outSS << "[";

        list<PlayRecord>::const_iterator it; //typename ê±??
        it = vObjs.begin();
        if(it != vObjs.end())
        {
                ToJson(*it,outSS);
                for(++it; it!=vObjs.end(); ++it)
                {
                        outSS << ", ";
                        ToJson(*it,outSS);
                }
        }

        outSS << "]";
}

template<>
inline void ToJson(const vector<PlayRecord> &vObjs, stringstream& outSS)
{
        outSS << "[";

        vector<PlayRecord>::const_iterator it; //typename ê±??
        it = vObjs.begin();
        if(it != vObjs.end())
        {
                ToJson(*it,outSS);
                for(++it; it!=vObjs.end(); ++it)
                {
                        outSS << ", ";
                        ToJson(*it,outSS);
                }
        }

        outSS << "]";
}
*/

template<>
inline void ToJson(const vector<PlayRecord*> &vObjs, stringstream& outSS)
{
        outSS << "[";

        vector<PlayRecord*>::const_iterator it; //typename ê±??
        it = vObjs.begin();
        if(it != vObjs.end())
        {
                ToJson(*it,outSS);
                for(++it; it!=vObjs.end(); ++it)
                {
                        outSS << ", ";
                        ToJson(*it,outSS);
                }
        }

        outSS << "]";
}

#endif

