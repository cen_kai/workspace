#ifndef _VERIFY_SERVICE_H_
#define _VERIFY_SERVICE_H_

#include <pthread.h>


#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

#include <ext/hash_map>
using namespace __gnu_cxx;


#include "common/SDLogger.h"
#include "common/SDThread.h"
#include "common/cmysql.h"
#include "common/ThreadSafeHashMap.h"
#include "common/SDQueue.h"
#include "vipverify/check.h"
class SessionCacheImp;


class VerifyThread : public SDThread
{
public:

    VerifyThread(SDQueue<SessionCacheImp*>& check_queue):m_check_queue(check_queue){};
    

private:
    void run();
    SDQueue<SessionCacheImp*>& m_check_queue;


    
private:
    DECL_LOGGER(logger);
};




class VerifyService
{
public:
    VerifyService(int thread_num):m_thread_num(thread_num)
    {
        m_check_queue = new SDQueue<SessionCacheImp*>(10000);
        m_result_queue = new SDQueue<SessionCacheImp*>(10000);
        
        m_thread_handlers.resize(m_thread_num);
        for(int i=0; i<m_thread_num; i++)
        {
            VerifyThread *thread = new VerifyThread(*m_check_queue);
            m_thread_handlers[i] = thread;
        }
    }
    bool addSessionToCheck(SessionCacheImp* ssch_imp)
    {
        return m_check_queue->push(ssch_imp,100);
    }

    void start_threads()
    {
        for(int i=0; i<m_thread_num; i++)
        {
            m_thread_handlers[i]->start();
        }
    }
    
private:
    int m_thread_num;
    vector<VerifyThread*> m_thread_handlers;
    SDQueue<SessionCacheImp*>* m_check_queue;
    SDQueue<SessionCacheImp*>* m_result_queue;
};

#endif
