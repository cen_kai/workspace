#ifndef __STORE_CACHE_H__
#define __STORE_CACHE_H__
//将收藏和订阅记录整合播放记录

#include <string>
#include <vector>

using namespace std;

#include "common/ThreadSafeHashMap.h"
#include "common/SDDBConnectionPool.h"
#include "msg_transmit/common.h"
#include "common/SDThread.h"
#include "common/SDLogger.h"
#include "common/SDMutexLock.h"


//StoreCache有两个示例，一个是收藏，一个订阅的
class StoreCache : public SDThread {
public:
    typedef ThreadSafeHashMap<StoreKey, StoreInfo >::DataMapIter  Iter ;
    enum INST_TYPE {
        STORE_INST,
        SUBSCRIBE_INST
    };
    StoreCache(INST_TYPE type = STORE_INST);
    static StoreCache * GetInstance(INST_TYPE type = STORE_INST );

    void AddStoreRecord(StoreKey &key);
    void DelStoreRecord(StoreKey &key);
    bool NotifiedStoreServer(VodPlayRecord  *record);
    bool IsStore(StoreKey key);

    void run();
    
    void SaveToDB(pair<StoreKey,StoreInfo> v);
    static void NotifiedAllRecord( INST_TYPE type );
    static void LoadDB( INST_TYPE type );
    static bool initiative_send_cmd(string server_domainname, short port, HttpCmd*cmd);
    int size();
public:
        ThreadSafeHashMap<StoreKey, StoreInfo >* _store_info;
        SDDBConnectionPool _store_info_db_pool;
        string  _db_table_name;
        string m_domain;
        int  m_port;
        int _is_notifiedall;  //现在是否正在notifiedall，某时刻只能一个
        SDMutexLock _db_lock;   //dump到数据库，和数据库load的锁
        INST_TYPE m_inst_type;
private:
    static StoreCache * _instance_store;
    static StoreCache * _instance_subscibe;
    static SDMutexLock _instance_lock;
    void ClearCache();
    static void *_loaddb(void *args);
    static void * _NotifiedAllRecord(void *args);
    DECL_LOGGER(logger);
};

#endif
