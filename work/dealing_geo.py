#!/usr/bin/python
# coding: utf-8

if __name__ == "__main__":
    #add middle point
    fd = open("data/all_area.txt")
    ofile = open("data/all_out.txt","w")
    area_data = fd.readlines()
    for line in area_data:
        line_data = line.split(":")
        area_name = line_data[0]
        area_round = line_data[1].split("-")
        ofile.write(area_name)
        ofile.write(":")
        for boundary in area_round:
            points = boundary.strip().split(";")
            count = len(points)
            middle1 = 0.0
            middle2 = 0.0
            for point in points:
                #print point
                middle1 += float(point.split(",")[0])
                middle2 += float(point.split(",")[1])
            ofile.write("%f,%f"%(middle1/count,middle2/count))
            ofile.write(";")
            ofile.write(boundary)
            ofile.write("-")
        ofile.write("\n")
else:
    pass
