#!/usr/bin/python


import sys
import os
import json
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(os.path.join(BASE_DIR,'..\\common'))

from common.Codec import *
from common.Logger import * 
from time import ctime 

import gl

#to deal with tcp data
class tcp_cmd_handle:
	def __init__(self, socket_handle, logger):
		self._socke_handle = socket_handle
		self._logger = logger
		#self._protocol_fields = [('total_len','IT'),('ver','I'),('cmd','I'),('data','s')]
		self._protocol_fields = [('ver','I'),('cmd','I'),('data','s')]
		self._proto_handle = ProtocolHandle('!', logger)        
		self._proto_handle.set_field(self._protocol_fields)
		#self._data_hanlde = data_mgr(logger)
		self._data_hanlde = gl.g_data_mgr
		self._data = r''
		self._data_list = None
		
	def __del__(self):
		pass

	def process_data(self, recv_data):
		try:
			print recv_data
			self._data_list = self._proto_handle.decode(recv_data)
			print self._data_list
			if not self._data_list:
				if self._logger:
					self._logger.info("failed to decode...")
				return 0
			else:
				print type(self._data_list[2])
				for item in self._data_list:
					if type(item) is str:
						print item.decode('utf8')
					else:
						print item
				result = self._data_hanlde.process_cmd(self._data_list, self._socke_handle)
				#result = True
				if result:
					return self._data_list[0]
				else:
					return 0
		except Exception,e:
			print "%s:%s\n%s" %(Exception,e, traceback.format_exc())
			return 0
		
	
