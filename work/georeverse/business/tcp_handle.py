#!/usr/bin/python

import sys
import os
import traceback
import errno
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(os.path.join(BASE_DIR,'..\\common'))
from common.Epoll import *
import select

from tcp_cmd_handle import *

import gl

#to deal with new connection
class Tcp_socket_handle(Event_handle_obj):
	def __init__(self, socket_obj, epoll_obj, logger):
		Event_handle_obj.__init__(self, socket_obj, epoll_obj)
		self._logger = logger
		self._recv_data = b""
		self._send_data = b""
		self._handle = tcp_cmd_handle(self, logger)

	def __del__(self):
		Event_handle_obj.__del__(self)
	
	def on_readable(self, fd):
		self._logger.info("fd:%d..."%fd)
		self._logger.info("Tcp_socket_handle on_readable...")
		while True:
			try:
				self._logger.info("in read")
				temp_data = self._socket_obj.recv(1024)
				self._logger.info("recv %d"%len(temp_data))
				if not temp_data and not self._recv_data == 0:
					self._logger.info("read done")
					self._socket_obj.close()
					self._epoll_obj.del_event(fd, Epoll_svr.READ_EVENT|Epoll_svr.WRITE_EVENT)
					break
				else:
					self._recv_data += temp_data
			except socket.error, msg:
				if msg.errno == errno.EAGAIN:
					self._epoll_obj.modify_event(fd, Epoll_svr.READ_EVENT|Epoll_svr.WRITE_EVENT)
					break
				else:
					self._socket_obj.close()
					self._epoll_obj.del_event(fd, select.EPOLLET|Epoll_svr.WRITE_EVENT)
					self._logger.error(msg)
					break	
			if len(self._recv_data) > 0:
				self._logger.error("received len = %d" %len(self._recv_data))
				dealed_len = self._handle.process_data(self._recv_data)
				self._recv_data = self._recv_data[dealed_len:]

	def on_writable(self, fd):
		self._logger.info("Tcp_socket_handle on_writable...")
		try:
			if len(self._send_data) > 0:
				send_len = self._socket_obj.send(self._send_data)
				self._send_data = self._send_data[send_len:]
		except Exception,e:
			self._logger( "fd=%d,%s:%s\n%s" %(fd,Exception,e, traceback.format_exc()))

	def on_error(self, fd):
		self._logger.info("on_error fd=%d..." %fd)
		self._socket_obj.close()
		self._epoll_obj.del_event(fd, Epoll_svr.READ_EVENT|Epoll_svr.WRITE_EVENT)

	def on_timer(self, fd):
		pass

	def send_data(self, data):
		self._logger.info("send_data...")
		self._send_data += data
		self.on_writable(-1)
