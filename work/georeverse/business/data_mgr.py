#!/usr/bin/python
# coding: utf-8

import traceback

from time import ctime 
import sys
import os
import json
import time
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(os.path.join(BASE_DIR,'..\\common'))

from common.Codec import *
from common.Geography import *
from common.Bou import *

import gl

class data_mgr:
	def __init__(self, logger):
		self._logger = logger
		self._protocol_fields = [('ver','I'),('cmd','I'),('data','s')]
		self._protocol_fields = [('total_len','IT'),('ver','I'),('cmd','I'),('data','s')]
		self._proto_handle = ProtocolHandle('!', logger)
		self._proto_handle.set_field(self._protocol_fields)
		
	def process_cmd(self, cmd_data, socket_handle):
		self._logger.info("process_cmd for cmd=%d,type(cmd_data[2])=%s" %(cmd_data[1],type(cmd_data[2])))
		try:
			self.georeverse(cmd_data,socket_handle)
			return True
		except Exception, e:
			self._logger.warn("%s:%s\n%s" %(Exception,e, traceback.format_exc()))
			return False
		
	def georeverse(self, cmd_data,socket_handle):
		try:
			self._logger.info("dealing..")
			point = Point(cmd_data[2])
			city_name = ""
			nearest_five_city = []
			nearest_five_city = gl.g_geofile.find_location(point)
			for city in nearest_five_city:
				print city.decode("gb18030").encode("utf8")
				city_boundaries = gl.g_geofile.adm3_geo_infos[city].boundaries
				print len(city_boundaries)
				if isPointsInPolygons(point, city_boundaries):
					city_name = city
					break
			if city_name == "":
				city_name = " ".join(nearest_five_city[0].split(" ")[:2])
			text = '{"result":"%d","servertime":"%d","position":"%s"}' %(1,1,city_name.decode("gb18030").encode("utf8"))
			#text = '{"result":"%d","servertime":"%d","token":"%s"}' %(1,1,"ck")
			protocl_values = [0, 1]
			protocl_values.append(text)
			socket_handle.send_data(self._proto_handle.encode(protocl_values))
			return True
		except Exception, e:
			text = '{"result":"%d","servertime":"%d","position":"%s"}' %(1,1,"city_name")
			protocl_values = [0, 1]
			protocl_values.append(text)
			socket_handle.send_data(self._proto_handle.encode(protocl_values))
			print "%s:%s\n%s" %(Exception,e, traceback.format_exc())
			self._logger.warn("%s:%s\n%s" %(Exception,e, traceback.format_exc()))
			return False
	def find_nearest_location():
		pass
