#!/usr/bin/python


import Logger
import socket, select
import traceback

class Event_handle_obj:
	def __init__(self, socket_obj, epoll_obj):
		self._socket_obj = socket_obj
		self._epoll_obj = epoll_obj
		
	def __del__(self):
		self._socket_obj.close()
		
	def on_readable(self, fd):
		raise "this is an abstract function"
		
	def on_writable(self, fd):
		raise "this is an abstract function"
		
	def on_error(self, fd):
		raise "this is an abstract function"
		
	def on_timer(self, fd):
		pass
		
'''
    EPOLLERR = 8
    EPOLLET = -2147483648
    EPOLLHUP = 16
    EPOLLIN = 1
    EPOLLMSG = 1024
    EPOLLONESHOT = 1073741824
    EPOLLOUT = 4
    EPOLLPRI = 2
    EPOLLRDBAND = 128
    EPOLLRDNORM = 64
    EPOLLWRBAND = 512
    EPOLLWRNORM = 256
'''

class Epoll_svr:
	def __init__(self, wait_timeout_secs = 1, logger = None):
		self._epoll = select.epoll()
		self._handles = {}
		self._time_out_secs = wait_timeout_secs
		self._logger = logger
		self._should_stop = False

	def __del__(self):
		self._epoll.close()
		self._handles.clear()
		self._should_stop = True

	def add_event(self, fd, event, event_obj):
		self._logger.info("epool add_event...")
		if fd > 0 and event&(select.EPOLLIN|select.EPOLLOUT) and event_obj:
			try:
				self._epoll.register(fd, event|select.EPOLLET)  #register with ET mode
			except Exception, e:
				print Exception,":",e
				self._logger.warn("%s:%s\n%s" %(Exception,e, traceback.format_exc()))
			self._handles[fd] = event_obj
		else:
			self._logger.error("invalid parameter for add_event")

	def del_event(self, fd, event):
		self._logger.info("del_event...")
		self._epoll.unregister(fd)
		del self._handles[fd]

	def modify_event(self, fd, event):
		if fd > 0 and event&(select.EPOLLIN|select.EPOLLOUT):
			if self._handles[fd]:
				self._epoll.modify(fd, event)
			else:
				if self._logger:
					self._logger.error("_handles[%d] not existed!!!" %fd)
		else:
				if self._logger:
					self._logger.error("invalid parameter for modify_event!!!")

	def run_loop(self):
		while not self._should_stop:
			event_list = self._epoll.poll(self._time_out_secs)
			if event_list:
				self._logger.info("epoll return:%s"%event_list)
			for fd, event in event_list:
				if event & select.EPOLLIN:
					if self._handles[fd]:
						self._handles[fd].on_readable(fd)
					else:
						if self._logger:
							self._logger.error("event obj not existed when %d readable!!!" %fd)
				if event & select.EPOLLOUT:
					if fd in self._handles.keys() and self._handles[fd]:
						self._handles[fd].on_writable(fd)
					else:
						if self._logger:
							self._logger.error("event obj not existed when %d writable!!!" %fd)
				if event & (select.EPOLLHUP|select.EPOLLERR):
					if fd in self._handles.keys() and self._handles[fd]:
						self._handles[fd].on_error(fd)
					else:
						if self._logger:
							self._logger.error("event obj not existed when %d error!!!" %fd)			

	def stop_loop(self):
		self._should_stop = True

	#class property
	READ_EVENT = select.EPOLLIN
	WRITE_EVENT = select.EPOLLOUT
