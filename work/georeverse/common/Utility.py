#!/usr/bin/python
# coding: utf-8
import socket
import os
import re
import traceback
import select
from Codec import *
import Logger

class Utility:
	@staticmethod
	def get_pid():
		return os.getpid()

	@staticmethod
	def set_working_dir(path):
		if path.startswith("/"):
			os.chdir(path)
			return True
		elif path.startswith("./") or path.startswith("../") :
			os.chdir(os.path.dirname(os.path.abspath(path)))
			return True
		else:
			return False

	@staticmethod
	def setenv(key, value):
		 os.environ[key] = value

	@staticmethod
	def unsetenv(key):
		 del os.environ[key]

	@staticmethod
	def recv_all(sock):
		data = ""
		while True:
			temp_data = sock.recv(1024)
			if len(temp_data) == 0:
				break
			else:
				print "received %d data" %len(temp_data)
				data += temp_data
				if len(temp_data) < 1024:
					break
		if len(data) > 0:
			return data
		else:
			return None
	
	@staticmethod
	def send_and_recv(ip, port, req_fields, req_value, resp_fields, code_mode = '!', logger = None):
		decode_list = None
		try:
			temp_socket = None
			try:
				#encode protocol data
				handle = ProtocolHandle(code_mode, logger)
				handle.set_field(req_fields)
				data = handle.encode(req_value)
				
				temp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				
				#connect
				if logger:
					logger.debug("ready to connect to %s:%d" %(ip,port))
				temp_socket.connect((ip, port))
				temp_socket.settimeout(120)
				
				#send
				if logger:
					logger.debug("ready to send %d data" %len(data))
				temp_socket.sendall(data)
				if logger:
					logger.debug("finished to send %d data" %len(data))
				
				#recv
				if logger:
					logger.debug("ready to recv data")
				data = Utility.recv_all(temp_socket)
				if logger:
					logger.debug("received %d data" %len(data))
				
				#decode protocol data
				if data:
					handle.set_field(resp_fields)
					decode_list = handle.decode(data)
					logger.debug("%s" %decode_list)
					if decode_list:
						return decode_list
					else:
						if logger:
							logger.warn("failed to decode")
				else:
					if logger:
						logger.warn("failed to received data")
				
			finally:
				if temp_socket:
					temp_socket.close()
			
			return decode_list
			
		except Exception,e:
			#print Exception,":",e
			if logger:
				logger.warn("%s:%s\n %s" %(Exception, e, traceback.format_exc()))
			return decode_list
			
	@staticmethod
	def send_and_recv_no_decode(ip, port, req_fields, req_value, code_mode = '!', logger = None):
		try:
			temp_socket = None
			try:
				#encode protocol data
				handle = ProtocolHandle(code_mode, logger)
				handle.set_field(req_fields)
				data = handle.encode(req_value)
				
				logger.info("ready to send hex_data=%s" %(binascii.b2a_hex(data)))
				
				temp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				
				#connect
				if logger:
					logger.debug("ready to connect to %s:%d" %(ip,port))
				temp_socket.connect((ip, port))
				
				#send
				if logger:
					logger.debug("ready to send %d data" %len(data))
				temp_socket.sendall(data)
				if logger:
					logger.debug("finished to send %d data" %len(data))
				
				#recv
				if logger:
					logger.debug("ready to recv data")
				data = Utility.recv_all(temp_socket)
				if logger:
					logger.debug("received %d data" %len(data))
				
				#decode protocol data
				if data:
					return data
				else:
					if logger:
						logger.warn("failed to received data")
					return None
					
			finally:
				if temp_socket:
					temp_socket.close()
			
		except Exception,e:
			if logger:
				logger.warn("%s:%s\n %s" %(Exception, e, traceback.format_exc()))
			return None

		
	# 编码转换
	@staticmethod
	def convert_codec(data, from_codec, to_codec):
		try:
			return data.decode(from_codec).encode(to_codec)
		except:
			return None
#for debug and example
if __name__ == "__main__":
	pass
else:
	pass






