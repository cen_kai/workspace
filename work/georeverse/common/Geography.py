#!/usr/bin/python
#coding=utf-8
import os
import sys
import csv
import math


def singleton(cls):
	instances = {}
	def getinstance(*args, **kw):
		if cls not in instances:
			instances[cls] = cls(*args, **kw)
		return instances[cls]
	return getinstance

class Point:
	longitude = 0.0
	latitude = 0.0
	def __init__(self, coordinate):
		self.longitude , self.latitude = map(float,coordinate.split(","))
	def get_distance(self, coordinate):
			lon1, lat1, lon2, lat2 = map(math.radians, [coordinate.longitude, coordinate.latitude, self.longitude, self.latitude])  
			# haversine formula
			dlon = lon2 - lon1
			dlat = lat2 - lat1
			a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
			c = 2 * math.asin(math.sqrt(a))
			r = 6371
			return c * r
		
class CityGeo:
	coordinate = []
	boundaries = []
	def __init__(self, coordinate, boundaries):
		#use str init
		self.coordinate = Point(coordinate)
		#use str init
		self.boundaries = map(Point, boundaries.split(";"))
		
@singleton
class Geo:
	def __init__(self, file_path):
		self._file_path = file_path
		self.geo_infos = {}
		self.adm3_geo_infos = {}
		self.__load_geo_file()
		
	def __load_geo_file(self):
		try:
			datap = open(self._file_path)
			geo_datas = datap.readlines()
			for city_data in geo_datas:
				city_name, city_coordinate, city_boundaries = city_data.split(":")
				self.geo_infos[city_name] = CityGeo(city_coordinate, city_boundaries)
				if len(city_name.split(" ")) == 3:#adm3
					self.adm3_geo_infos[city_name] = CityGeo(city_coordinate, city_boundaries)
		except Exception,e:
			print Exception,":",e,	
	def find_nearest_five_location(self, coordinate):
		five_nearest_location = []
		for i in range(5):
			nearest_distance = 30000#init to a big distance
			nearest_city = ""
			for city in self.adm3_geo_infos:
				distance = coordinate.get_distance(self.adm3_geo_infos[city].coordinate)
				if distance <= nearest_distance and city not in five_nearest_location:
					nearest_distance = distance
					nearest_city = city
			five_nearest_location.append(nearest_city)
		return five_nearest_location
if __name__ == "__main__":
	geo_china = Geo("../../data/geo.dat")
	#print geo_china.geo_infos["安徽省".decode("utf-8").encode("gb18030")]
	print geo_china.geo_infos
	#for location in geo_china.geo_infos:
	#print geo_china.find_rearest_location([31.2585327, 121.426890350924])	
