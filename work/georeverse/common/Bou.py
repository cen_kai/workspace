#!/usr/bin/python
#coding=utf-8
import sys,os,time
import Geography

def getPolygonBounds(points):
	length = len(points)
	top = down = left = right = points[0]
	for i in range(1,length):
		if points[i].longitude > top.longitude:
			top = points[i]
		elif points[i].longitude < down.longitude:
			down = points[i]
		else:
			pass
		if points[i].latitude > right.latitude:
			right = points[i]
		elif points[i].latitude < left.latitude:
			left = points[i]
		else:
			pass

	point0 = Geography.Point("%f,%f"%(top.longitude,left.latitude))
	point1 = Geography.Point("%f,%f"%(top.longitude,right.latitude))
	point2 = Geography.Point("%f,%f"%(down.longitude,right.latitude))
	point3 = Geography.Point("%f,%f"%(down.longitude,left.latitude))
	polygonBounds = [point0,point1,point2,point3]
	return polygonBounds

def isPointInRect(point,polygonBounds):
	if point.longitude >= polygonBounds[3].longitude and point.longitude <= polygonBounds[0].longitude and point.latitude >= polygonBounds[3].latitude and point.latitude <= polygonBounds[2].latitude:
		return True
	else:
		return False
def isPointsInPolygons(point,polygonset):
	polygonBounds = getPolygonBounds(polygonset)
	if not isPointInRect(point,polygonBounds):
		return False
	length = len(polygonset)
	p1 = polygonset[0]
	flag = False
	for i in range(1,length):
		p2 = polygonset[i]
		#point is the same 
		if (point.longitude == p1.longitude and point.latitude == p1.latitude) or (point.longitude == p2.longitude and point.latitude == p2.latitude):
			return True
		if (p2.latitude < point.latitude and p1.latitude >= point.latitude) or (p2.latitude >= point.latitude and p1.latitude < point.latitude):
			if (p2.latitude == p1.latitude):
				x = (p1.longitude + p2.longitude)/2
			else:
				x = p2.longitude - (p2.latitude - point.latitude)*(p2.longitude - p1.longitude)/(p2.latitude - p1.latitude)
			#point is on the edge of polygonset
			if (x == point.longitude):
				return True
			if (x > point.longitude):
				flag = not flag
			else:
				pass
		else:
			pass
		p1 = p2
	return flag

if __name__ == "__main__":
	print "test"
	pass

		

