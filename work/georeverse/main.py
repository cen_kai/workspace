#!/usr/bin/python
# coding: utf-8

from common.Logger import * 
from common.Config import *
from common.Utility import *
from common.Geography import *
from common.Epoll import *
from server_handle import *
from business.data_mgr import *
import gl

import os
import time

g_svr_ip = ""
g_svr_port = 0

#g_logger = None
#g_config = None

if __name__ == "__main__":
	#set working dir
	flag = Utility.set_working_dir(os.path.dirname(os.path.abspath(sys.argv[0])))
	if not flag:
		print "not supported file path..."
		exit

	#set default charset
	reload(sys)
	sys.setdefaultencoding('utf8')

	#init
	gl.g_logger = Logger.Logger("../conf/log.conf")
	gl.g_config = Config("../conf/server.conf")
	gl.g_geofile = Geo("../data/geo.dat")

	g_svr_ip = gl.g_config.get_string("server_ip", "0.0.0.0")
	g_svr_port = gl.g_config.get_int("server_port", 12345)
	
	#init data mgr
	gl.g_data_mgr = data_mgr(gl.g_logger)

	#tcp svr
	tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	tcp_socket.bind((g_svr_ip, g_svr_port))
	tcp_socket.listen(1)
	tcp_socket.setblocking(0)

	reverse_geocoder_epoll_svr = Epoll_svr(1, gl.g_logger)
	reverse_geocoder_epoll_svr.add_event(tcp_socket.fileno(), Epoll_svr.READ_EVENT, Tcp_Server_handle(tcp_socket, reverse_geocoder_epoll_svr, gl.g_logger))
	reverse_geocoder_epoll_svr.run_loop()

else:
	pass
