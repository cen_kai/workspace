#!/usr/bin/python  
# coding: utf-8

import traceback

from common.Logger import * 
from common.Config import *
from common.Codec import *
from common.Utility import *
from socket import *  
import time

import json

if __name__ == "__main__":
	#set default charset
	reload(sys)
	sys.setdefaultencoding('utf8')
	
	ip = 'localhost'  
	port = 12345  
	
	g_logger = Logger.Logger("../conf/test_log.conf")
	g_config = Config("../conf/server.conf")
	
	result = 0
	servertime = 0
	my_token = ''
	decode_list = None
	url = None
	cur_protocol_len = 0
	data1 = b''
	data = b''
	
	protocol_fields = [('total_len','IT'),('ver','I'),('cmd','I'),('data','s')]
	handle = ProtocolHandle("!", g_logger)
	handle.set_field(protocol_fields)
	
	temp_socket = socket(AF_INET, SOCK_STREAM)
	temp_socket.connect((ip, port))
	temp_socket.settimeout(5)
	
	#1. handshake
	print "\n-------------------ready to handshake------------------------"
	protocol_values = [-1,1,1]
	location = '123.45,12.345'
	protocol_values.append(location)
	data = handle.encode(protocol_values)
	
	temp_socket.sendall(data)
	#recv handshake resp
	data = Utility.recv_all(temp_socket)
	print "len(data)=%d" %len(data)
	if data:
		handle.set_field(protocol_fields)
		decode_list = handle.decode(data)
	if decode_list:
		print decode_list