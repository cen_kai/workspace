#!/usr/bin/python

import traceback

from common.Epoll import *
from business.tcp_handle import *

from time import ctime 

		
class Tcp_Server_handle(Event_handle_obj):
	def __init__(self, socket_obj, epoll_obj, logger):
		Event_handle_obj.__init__(self, socket_obj, epoll_obj)
		self._logger = logger
		
	def __del__(self):
		Event_handle_obj.__del__(self)
		
	def on_readable(self, fd):
		self._logger.info("server fd:%d"%fd)
		self._logger.info("on_readable")
		try:
			socket_obj,address_info = self._socket_obj.accept()
			if socket_obj:
				socket_obj.setblocking(0)
				self._epoll_obj.add_event(socket_obj.fileno(), Epoll_svr.READ_EVENT|Epoll_svr.WRITE_EVENT, Tcp_socket_handle(socket_obj, self._epoll_obj, self._logger) )
				self._logger.info("succeed to put new socket in epoll")
			else:
				self._logger.warn("faild to accept")
		except Exception, e:
			self._logger.warn("%s:%s\n%s" %(Exception,e, traceback.format_exc()))
		
	def on_writable(self, fd):
		pass
		
	def on_error(self, fd):
		self._socket_obj.close()
		self._epoll_obj.del_event(fd, Epoll_svr.READ_EVENT|Epoll_svr.WRITE_EVENT)
		
	def on_timer(self, fd):
		pass
