#!/usr/bin/python  
# coding: utf-8

import traceback

from common.Logger import * 
from common.Config import *
from common.Codec import *
from common.Utility import *
from socket import *  
import time
import struct

import json

if __name__ == "__main__":
	#set default charset
	reload(sys)
	sys.setdefaultencoding('utf8')
	
	ip = '10.20.22.11'  
	port = 12345  
	
	g_logger = Logger.Logger("../conf/test_log.conf")
	g_config = Config("../conf/server.conf")
	
	result = 0
	servertime = 0
	my_token = ''
	decode_list = None
	url = None
	cur_protocol_len = 0
	data1 = b''
	data = b''
	
	protocol_fields = [('ver','I'),('cmd','I'),('location','s')]
	#protocol_fields = [('total_len','IT'),('ver','I'),('cmd','I'),('location','s')]
	handle = ProtocolHandle("!", g_logger)
	handle.set_field(protocol_fields)
	
	temp_socket = socket(AF_INET, SOCK_STREAM)
	temp_socket.connect((ip, port))
	temp_socket.settimeout(150)
	
	#1. handshake
	print "\n-------------------ready to test------------------------"
	protocol_values = [1,1,"101.990447,26.932392"]
	data = handle.encode(protocol_values)
	data = struct.pack("!3I20s", 1,1,20,"101.990447,26.932392");
	print data
	print data2
	print len(data),len(data2)
	
	temp_socket.sendall(data)
	data = Utility.recv_all(temp_socket)
	print "len(data)=%d" %len(data)
	if data:
		handle.set_field(protocol_fields)
		decode_list = handle.decode(data)
	if decode_list:
		print decode_list
	temp_socket.close()
	print "done"
