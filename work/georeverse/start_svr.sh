#!/bin/sh

#to kill old  process
ps -eaf|grep main.py|grep -v grep|awk '{print $2}'|xargs kill -9
sleep 2

dir=`pwd`
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$dir/../lib/
export PATH=$PATH:$dir/

echo $LD_LIBRARY_PATH
echo $PATH

ulimit -c unlimited
#ulimit -SHn 1048576

echo "ready to start main svr now..."
python ./main.py >out.log 2>err.log &
sleep 2

ps -ef|grep main.py
