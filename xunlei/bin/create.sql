create database play_record;

use play_record;

CREATE TABLE `vod_record` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `movieid` int(11) NOT NULL default '0',
  `subid` int(11) NOT NULL default '0',
  `viewtype` int(3) NOT NULL default 1 comment '视频类型',
  `charge` int(11) NOT NULL default '0' comment '视频是否收费', 
  `flag`  int(3)  NOT NULL default 1 comment '记录是否有效',
  `play_time` datetime NOT NULL default '0000-00-00 00:00:00' comment '播放操作的时间点',
  `viewed_time` int not null default 0 comment '视频已经观赏的进度，单位秒',
  `device_type` varchar(32) NOT NULL default '' comment'设备类型 pc, pad, phone',
  `device_version` varchar(64) NOT NULL default '' comment'设备型号',
  `product_name` varchar(32) NOT NULL default '' comment '产品名称 Xmp, web, ipadkankan等',
  `product_version` varchar(64) NOT NULL default '' comment'产品版本',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userid` (`userid`, `movieid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `store_record` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `movieid` int(11) NOT NULL default '0',
  `notifited`  int(3)  NOT NULL default 1 comment '记录是否通知store svr',
  `flag`  int(3)  NOT NULL default 1 comment '记录是否有效',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userid` (`userid`, `movieid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `local_record` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `gcid` char(40) NOT NULL default '',
  `cid` char(40) not null default '',
  `filesize` int not null default 0,
  `flag`  int(3)  NOT NULL default 1 comment '记录是否有效',
  `title` varchar(255) not null default '' comment '视频文件名',
  `movie_length` int(11) NOT NULL default '0' comment '电影长度，单位秒',
  `play_time` datetime NOT NULL default '0000-00-00 00:00:00' comment '播放操作的时间点',
  `viewed_time` int not null default 0 comment '视频已经观赏的进度，单位秒',
  `device_type` varchar(32) NOT NULL default '' comment'设备类型 pc, pad, phone',
  `device_version` varchar(64) NOT NULL default '' comment'设备型号',
  `product_name` varchar(32) NOT NULL default '' comment '产品名称 Xmp, web, ipadkankan等',
  `product_version` varchar(64) NOT NULL default '' comment'产品版本',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userid` (`userid`, `gcid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `cloud_record` (
  `id` int NOT NULL auto_increment,
  `userid` int NOT NULL default '0',
  `gcid` char(40),
  `url_hash` char(20) not null default '',
  `cid` char(40),
  `url` text NOT NULL,
  `file_name` varchar(255) not null default '',
  `task_type` int NOT NULL default '0',
  `src_url` text NOT NULL,
  `filesize` int,
  `duration` int comment '影片时长',
  `flag`  int(3)  NOT NULL default 1 comment '记录是否有效',
  `playtime` datetime NOT NULL default '0000-00-00 00:00:00' comment '播放操作的时间点',
  `playflag` int comment '播放标识（待看、已看等）',
  `createtime` datetime NOT NULL default '0000-00-00 00:00:00' comment '创建的时间点',
  `device_type` varchar(32) NOT NULL default '' comment'设备类型 pc, pad, phone',
  `device_version` varchar(64) NOT NULL default '' comment'设备型号',
  `product_name` varchar(32) NOT NULL default '' comment '产品名称 Xmp, web, ipadkankan等',
  `product_version` varchar(64) NOT NULL default '' comment'产品版本',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userid` (`userid`, `url_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


