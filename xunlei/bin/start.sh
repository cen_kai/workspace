#!/bin/sh

path=`pwd`
cd $(cd "$(dirname "$0")"; pwd)/
echo "kill play_record_svr..."
killall -9 play_record_svr
sleep 1

ulimit -c unlimited
ulimit -n 65535

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../lib:/usr/local/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../lib/mysql
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/xl_lib/lib/:/usr/local/mysql/lib/mysql

echo "startup server, waiting..."
#./play_record_svr >server.out 2>server.err &
./play_record_svr  &
#valgrind --tool=memcheck --leak-check=yes ./play_record_svr >server.out 2>server.err &

sleep 2
ps -ef | grep play_record_svr
tail -f ../log/play_record_svr.log
cd $path

