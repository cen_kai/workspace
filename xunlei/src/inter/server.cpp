#include <signal.h>

#include <sched.h>

#include <locale.h>
#include <time.h>

#ifdef LOGGER
#include <log4cplus/configurator.h>
using namespace log4cplus;
#endif

#include <string>
#include <sstream>
using namespace std;

#include "server/ServerApp.h"

#include "server/ServiceThread.h"
#include "client/ClientSession.h"
#include "play_record/StoreCache.h"

#include "TaskProcessThreads.h"

ServerApp g_app;

ServiceThread< ClientSession > * g_client_service;
TaskProcessThreads *g_task_service;

string crossdomain_cnt;


static log4cplus::Logger _logger = log4cplus::Logger::getInstance("server");

void sign_t(int sign)
{
	printf("error!! in thread %u\n", (unsigned int) pthread_self());
	abort();
//	kill(0, SIGKILL);
}



int main(int argc, char* argv[])
{
	fprintf(stderr, "start.....\n");
#ifdef LOGGER
	PropertyConfigurator::doConfigure("../conf/log4cpluscplus.properties");
#endif

	srand(time(NULL));
	setlocale(LC_ALL, "");

	//for debug 
	signal(SIGSEGV, sign_t);
		
	g_app.set_config_file("../conf/play_record_svr.conf");
	g_app.set_dyna_config_file("../conf/server_dyna.conf");
	if (!g_app.load_conf())
	{
		fprintf(stderr, "failed to load configuration.\n");
		return -1;
	}

	
	if(FILE *fp = fopen("../conf/crossdomain.xml", "r")){
		char buf[1024]; 
		while(fgets(buf, sizeof(buf), fp)){
			crossdomain_cnt += buf;
		}
		fclose(fp);
	}else{
		stringstream out;
		out << "<?xml version=\"1.0\"?>"
			<< "<!DOCTYPE cross-domain-policy SYSTEM \"http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd\">"
			<< "<cross-domain-policy><allow-access-from domain=\"*\" secure=\"true\" /></cross-domain-policy>";
		crossdomain_cnt = out.str();
	}

    TaskProcessThreads::init_task_type();
	init_record_type_map();

	SessionCacheImp::init_app_name();

	RecordCache::GetInstance()->start();
	SessionCache::Instance()->start();
	StoreCache::LoadDB(StoreCache::STORE_INST);
	StoreCache::LoadDB(StoreCache::SUBSCRIBE_INST);
	StoreCache::GetInstance(StoreCache::SUBSCRIBE_INST)->start();
	StoreCache::GetInstance(StoreCache::STORE_INST)->start();
	//dealing cmd
    g_task_service = new TaskProcessThreads(g_app.get_conf_int("task.process.threads.count", 8));
    g_task_service->startThreads();

    sleep(2);
	
	string server_addr = g_app.get_conf_string("cloud.bind.ip", "0.0.0.0");
	int thread_num = g_app.get_conf_int("cloud.process.num", 8);	
	int session_timeout_ms = g_app.get_conf_int("cloud.socket.overdue.sec", 0) * 1000;	

	int client_port = g_app.get_conf_int("cloud.bind.port", 8080);	
	g_client_service = new ServiceThread< ClientSession >(server_addr, client_port, thread_num, session_timeout_ms);
	g_client_service->start();

	
	g_client_service->wait_terminate();

	return 0;
}

