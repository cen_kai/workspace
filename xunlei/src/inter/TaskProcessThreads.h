#ifndef TASK_PROCESS_THREADS
#define TASK_PROCESS_THREADS

#include "common/SDQueue.h"
#include "inter/SessionThreadData.h"
#include "common/SDThreadPool.h"
#include "common/SDLogger.h"
/*
 xiaojing@xunlei.com 2013-7-15
 用于在后台处理查询等任务
 避免IO线程受到阻塞
*/
#include "play_record/RecordCache.h"
#include "play_record/SessionCache.h"

#define TYPE_CHAR 1
#define TYPE_INT 2


class TaskProcessThreads : public SDThreadPool{
public:
    TaskProcessThreads(size_t thread_count);
    ~TaskProcessThreads();
    bool add_task(PipeEvent &event);

	static void init_task_type();
	static int get_task_type(const string& cmd);
private:
    void doIt();
	
	bool check_session(PipeEvent& task, SessionCacheImp& ssch_imp);
	bool get_ssch_imp(HttpCmd* cmd,SessionCacheImp& ssch_imp);
	void add_verify(PipeEvent& task,SessionCacheImp& ssch_imp);
	void get_conf(PipeEvent& task);
	void query_record(PipeEvent& task,const SessionCacheImp& ssch_imp);
	void get_page(PipeEvent& task,const SessionCacheImp& ssch_imp);
	void report_record(PipeEvent& task,const SessionCacheImp& ssch_imp);
	void delete_record(PipeEvent& task,const SessionCacheImp& ssch_imp);
	void clear_record(PipeEvent& task,const SessionCacheImp& ssch_imp);
	void store_record(PipeEvent& task);

	void make_call_back(PipeEvent& task);
	void return_result(PipeEvent& task);
	void return_error(PipeEvent& task,const string& err);
    struct json_pair
	{
        json_pair(const char *vkey,const char *vvalue):key(vkey),cvalue(vvalue){type=TYPE_CHAR;}
		json_pair(const char *vkey,_u64 vvalue):key(vkey),ivalue(vvalue){type=TYPE_INT;}
        const char *key,*cvalue ;
		_u64 ivalue;
		int type;
    };
    string err_json(const char *pinfo=NULL);
    void make_json(stringstream &ss,vector < PlayRecord*> *records,_u64 num,vector<struct json_pair> &vect);
    SDQueue<PipeEvent> *_tasks;

	static map<string,int> m_task_type;
    DECL_LOGGER(logger); 
};

#endif
