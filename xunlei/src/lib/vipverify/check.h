#ifndef __CHECK_H__
#define __CHECK_H__

// web
#define STATUS_OK  0
#define ERROR_PARA -1
#define ERROR_SHAREMEM -2
#define ERROR_TIMEOUT -3
#define ERROR_SERVER_W -4


// cli
#define ERROR_PARSE_PROCOL  -1
#define ERROR_SESSION_KICK -2
#define ERROR_TIMEOUT -3
#define ERROR_NOPRESENT -4
#define ERROR_SERVER_C -5
#define ERROR_DB -6
#define ERROR_PRIVILEGE -7


#define  ERROR_CODE  -16



int Validate_web_session(const string &ip , int port ,  const string &sessionid );
int Validate_cli_session(const string &ip , int port , const string &sessionid , int userid , int businessid );

#endif
