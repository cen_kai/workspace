#include<iostream>
#include "lib/common/cbinmsg.h"
#include<sys/types.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<errno.h>
#include<netinet/in.h>
#include<string.h>

#include "lib/common/SDLogger.h"
#include "lib/common/Utility.h"
#include "check.h"

#ifdef LOGGER
#include <log4cplus/configurator.h>
using namespace log4cplus;
#endif

static log4cplus::Logger _logger = log4cplus::Logger::getInstance("server");

// web/client  sessionid验证错误代码的含义参见会员接口wiki(http://wiki.i.mm首页账号登录服务)
int web_error_code( const string &errstr  )
{
   const int web_err_num = 6 ;
   static string  err[web_err_num] = {"200","401","400","402","403","500"};
   int i=0 ; 
   for (; i<web_err_num; ++i)
   {
     if ( err[i] == errstr )
     {
        break ;
     } 
   }
   return -i ;
}
 
int cli_error_code( const string &errstr )
{
   const int cli_err_num = 8 ;
   static string  err[cli_err_num] = {"200","401","402","403","404","500","501","505"};
   int i=0 ; 
   for (; i<cli_err_num; ++i)
   {
     if ( err[i] == errstr )
     {
        break ;
     } 
   }
   return -i ;
}

void set_socket_timeout( int socketfd ,int sends , int rcvs)
{
    struct timeval timeout={sends,0};
    setsockopt(socketfd,SOL_SOCKET,SO_SNDTIMEO,(char*)&timeout,sizeof(struct timeval));
    timeout.tv_sec =rcvs ;
    setsockopt(socketfd,SOL_SOCKET,SO_RCVTIMEO,(char*)&timeout,sizeof(struct timeval));
}

int Validate_web_session(const string &ip , int port ,  const string &sessionid )
{
    const int BUF_SIZE = 4096 ;
	
    int socketfd = Utility::connect_by_ip_port_tm(ip.c_str(),port,3) ;
    if ( socketfd == -1 )
    {
        return ERROR_CODE ;
    }
    set_socket_timeout(socketfd,3,15);
    CBinMsgRecord  res ;
    CBinMsgRow para ; 
    para.AddField("request","getusrinfor");
    para.AddField("sessionid",sessionid);
    res.AddRow(para);
    char szbuf[BUF_SIZE];
    int sendlen = sizeof(szbuf) ;
    res.Encode(szbuf,sendlen);
    int  iret = Utility::send_data(socketfd,szbuf,sendlen);
    if ( iret == -1 )
    {
        return ERROR_CODE ;
    }
	LOG4CPLUS_DEBUG(_logger,"send data");
    int recvlen = sizeof(szbuf) ;
    iret = read(socketfd,szbuf,recvlen);
	LOG4CPLUS_DEBUG(_logger,"recv data");
    close(socketfd);
    if ( iret == -1 || iret == 0 )
    {
		LOG4CPLUS_DEBUG(_logger,"read ret value: "<<iret);		
        return ERROR_CODE ;
    }
    CBinMsgRecord res_recv ; 
    res_recv.Decode(szbuf,iret);
    CBinMsgRow *ptr = res_recv.GetNextRow();
	if(ptr){
    	CBinMsgField *fptr = ptr->GetNextField();
		if(fptr){
		    if ( fptr->m_strName == "result" )
		    {
				 LOG4CPLUS_DEBUG(_logger,"server return code: "<<fptr->m_strValue.c_str());
		         return web_error_code( static_cast<const string &>(fptr->m_strValue) );
		    }
		}
	}
    return ERROR_CODE ;
}

int Validate_cli_session(const string &ip , int port , const string &sessionid , int userid , int businessid )
{
    const int BUF_SIZE = 4096 ;
    int socketfd = Utility::connect_by_ip_port_tm(ip.c_str(),port,1) ;
    if ( socketfd == -1 )
    {
        return socketfd ;
    }
    set_socket_timeout(socketfd,10,10);
    CBinMsgRecord  res ;
    CBinMsgRow para ; 
    para.AddField("request","checksessionid");
    para.AddField("userid",userid);
    para.AddField("clientoperationid",businessid);
    para.AddField("sessionid",sessionid);
    res.AddRow(para);
    char szbuf[BUF_SIZE];
    int sendlen = sizeof(szbuf) ;
    res.Encode(szbuf,sendlen);
    int  iret = Utility::send_data(socketfd,szbuf,sendlen);
    if ( iret == -1 )
    {
        return ERROR_CODE ;
    }
	LOG4CPLUS_DEBUG(_logger,"send data");
    int recvlen = sizeof(szbuf) ;
    iret = read(socketfd,szbuf,recvlen);
    close(socketfd);
    if ( iret == -1 || iret == 0 )
    {
        return ERROR_CODE ;
    }
	LOG4CPLUS_DEBUG(_logger,"recv data");
    CBinMsgRecord res_recv ; 
    res_recv.Decode(szbuf,iret);
    CBinMsgRow *ptr = res_recv.GetNextRow() ;
	if(ptr){
		CBinMsgField *fptr = ptr->GetNextField();
		if(fptr){
		    if ( fptr->m_strName == "result" )
		    {
		       LOG4CPLUS_DEBUG(_logger,"server return code: "<<fptr->m_strValue.c_str());
		       return cli_error_code( static_cast<const string &>(fptr->m_strValue)); 
		    }	
		}
	}
    return  ERROR_CODE ;
}

