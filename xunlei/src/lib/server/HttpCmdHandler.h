#ifndef TASK_HANDLER__H
#define TASK_HANDLER__H

#include <string>
#include <sstream>
using namespace std;

#include "common/SDThreadPool.h"
#include "common/SDLogger.h"
#include "common/SDQueue.h"
#include "client/Command.h"

class HttpCmdHandler:public SDTwinThreadPool
{
public:
	HttpCmdHandler(size_t queue_size, size_t thread_count, const string& script_path="");
	virtual ~HttpCmdHandler();
	int AddCmd(HttpCmd *cmd);
	virtual void doIt();
	virtual void doIt2();
protected:
	SDQueue<HttpCmd*> *_queue;
	string _script_path;
private:
	DECL_LOGGER(logger);
};

#endif
