// ClientSessionManager.h 
//
// Author: tianming
// Created: 2009-11-18
//===============================================================

#ifndef __CLIENT_SESSION_MANAGER_H_20091118
#define  __CLIENT_SESSION_MANAGER_H_20091118

#include <ext/hash_map>
#include "Session.h"
#include "common/SDLogger.h"

using namespace __gnu_cxx;


class SessionManager
{
	typedef hash_map<int, Session* > Hashtable;
	typedef hash_map<int, Session* >::iterator ht_iterator;
	const static int INIT_HASHTABLE_BUCKET_NUM = 16;
public:
	SessionManager(): m_session_maps(INIT_HASHTABLE_BUCKET_NUM)
	{

	}

	Session* find_session_by_fd(int socket_fd)
	{
		ht_iterator it = m_session_maps.find(socket_fd);
		if(it == m_session_maps.end())
			return NULL;

		return it->second;
	}

	bool remove_session(int socket_fd)
	{
		ht_iterator it = m_session_maps.find(socket_fd);
		if(it == m_session_maps.end())
			return false;

		Session* session = it->second;
		m_session_maps.erase(it);
		delete session;

		LOG4CPLUS_INFO(logger, "admin client session identified by fd " << socket_fd << " removed now.");	
		
		return true;
	}
	
	bool add_session(Session* session)
	{
		if(session == NULL)
			return false;
		
		pair<ht_iterator, bool> insert_result = m_session_maps.insert(make_pair(session->m_socket_fd, session));
		return insert_result.second;
	}

	int active_sesssion_num() const { return m_session_maps.size(); }
	
private:
	Hashtable m_session_maps;

private:
	DECL_LOGGER(logger);
};

#endif // #ifndef __CLIENT_SESSION_MANAGER_H_20091118


