
#ifndef __SESSION_H_201007091654
#define  __SESSION_H_201007091654
#include <list>
#include "openssl/md5.h"

#include "common/SDAes.h"
#include "common/SDLogger.h"
#include "common/common.h"
#include "common/Utility.h"
#include "common/buffer_encoder_noswap.h"
#include "common/IOEventServer.h"
#include "Datagram.h"
#include "inter/SessionThreadData.h"

#define HANDLE_OK 0x0
#define HANDLE_READ_PENDING 0x01
#define HANDLE_WRITE_PENDING 0x02
#define HANDLE_ERROR 0xFFFFFFFF

using namespace std;

#include "common/mem_leak.h"

class Session
{
public:
	enum COMMAND_APP
	{
		COMMAND_APP_UNKNOW = 0x0,
		COMMAND_APP_HTTP = 0x01,
		COMMAND_APP_BIN = 0x02,
		COMMAND_APP_HTTP_GET = 0x03,
		COMMAND_APP_HTTP_POST = 0x04
	};
	enum CONNECT_STATUS{
		CONNECT_STATU_ESTABLISHED = 0,
		CONNECT_STATU_UNESTABLISHED
	};
	enum CONNECT_TYPE{
		CONNECT_TYPE_CLIENT = 0,
		CONNECT_TYPE_USER_CENT,
		CONNECT_TYPE_GROUP_MANAGER,
		CONNECT_TYPE_GROUP_SERVER
	};
public:
	Session()
	{
		m_socket_fd = 0;
		m_session_unlink_flag = 0;
		//设计成标记位而不是引用计数是为了方便定位
		//不过引用计数更灵活，如果有同类其他线程并行处理的需求可改为应用计数
		m_use_session_other_thread = 0;
		m_send_buf = NULL;
		m_send_buf_len = 0;
		m_need_send_len = 0;
		m_cur_send_transfered = 0;
		m_app_type = COMMAND_APP_UNKNOW;
		m_connect_stat  = CONNECT_STATU_ESTABLISHED;
	}
	virtual ~Session()
	{
		if(m_send_buf)
			delete []m_send_buf;
		list<Datagram *>::iterator iter = m_send_command_list.begin();
		list<Datagram *>::iterator iter_end = m_send_command_list.end();
		for(;iter!=iter_end;iter++)
		{
			delete *iter;
		}
        for(list<string*>::iterator it=m_send_buf_list.begin(); it!=m_send_buf_list.end(); ++it)
        {
            delete *it;
        }
	}
public:
	virtual _u32 on_socket_readable(int fd, EventData* attach_info) = 0;
	virtual _u32 on_socket_writable(int fd, EventData* attach_info)
	{
		return send_remain_buf();
	}
	virtual _u32 on_socket_error(int fd, EventData* attach_info) = 0;
	virtual _u32 on_socket_timeout(int fd, EventData* attach_info) = 0;

	//需要在该函数中释放PipeEvent.m_app_info 指针
	virtual _u32 handle_pipe_event(PipeEvent &event) = 0;

	_u32 encoder_command(Datagram *cmd)
	{
		int cmd_len = cmd->EncodeLen();
		if(cmd_len + 1024 > m_send_buf_len)
		{
			if(m_send_buf)
			{
				delete []m_send_buf;
			}
			m_send_buf = new uint8_t[cmd_len + 1024];
			m_send_buf_len = cmd_len + 1024;
		}

		char *encoder_pos = (char *)m_send_buf;
		if(m_app_type == COMMAND_APP_HTTP)
		{
			//预留http头
			encoder_pos = (char *)m_send_buf + 512;
		}

		EncoderNoSwap encoder(encoder_pos, cmd_len);
		encoder & *cmd;
		m_need_send_len = encoder._pos;
		LOG4CPLUS_DEBUG(logger,"try send buf[" << m_need_send_len << "], buf:" << m_send_buf);
		return HANDLE_OK;
	}

	//cmd 不能是临时数据(栈数据)，因为可能延时发送
	_u32 send_command(Datagram *cmd)
	{
		_u32 ret = HANDLE_OK;
		//need wait writable
		if(m_need_send_len > 0)
		{
			m_send_command_list.push_back(cmd);
			return HANDLE_WRITE_PENDING;
		}
		else
		{
			ret = encoder_command(cmd);
			delete cmd;
			cmd=NULL;
			if(ret == HANDLE_OK)
			{
				return send_remain_buf();
			}
			else
			{
				return ret;
			}
		}
	}

	
	_u32 send_remain_buf()
	{
		_u32 ret = HANDLE_OK;
		while(m_need_send_len > 0)
		{
			int send_ret = Utility::send_nonblock_data(m_socket_fd, (char*)(m_send_buf + m_cur_send_transfered), m_need_send_len);
			if(send_ret == -1)
			{
				return HANDLE_ERROR;
			}
			else
			{
				m_cur_send_transfered += send_ret;
				m_need_send_len -= send_ret;
				if(m_need_send_len > 0)
				{
					return HANDLE_WRITE_PENDING;
				}
				else
				{
					m_cur_send_transfered = 0;
					m_need_send_len = 0;
					if(!m_send_command_list.empty())
					{
						Datagram *cmd = m_send_command_list.back();
						m_send_command_list.pop_back();
						ret = encoder_command(cmd);
						delete cmd;
						if(ret != HANDLE_OK)
						{
							return ret;
						}
					}
                    else if(!m_send_buf_list.empty())  //用于兼容protobuff产生的string类型消息
                    {
                        string *data = m_send_buf_list.front();
                        m_send_buf_list.pop_front();
                        ret = write_send_buffer(data);
                        delete data;
						if(ret != HANDLE_OK)
						{
							return ret;
						}
                    }
					else
					{
						return HANDLE_OK;
					}
				}
			}
		}

		return HANDLE_OK;
	}
    void add_ref()
    {
        m_use_session_other_thread++;
    } 
    int release_ref()
    {
        return --m_use_session_other_thread;
    }
	bool new_other_thread_data(_u32 thread_type, PipeEvent &data)
	{
		if((m_use_session_other_thread & thread_type) != 0)
		{
			return false;
		}
		else
		{
			data.m_fd = m_socket_fd;
			data.m_thread_type = thread_type;
			m_use_session_other_thread = (m_use_session_other_thread | thread_type);
			return true;
		}
	}
	bool free_other_thread_data(PipeEvent &data)
	{
		if((m_use_session_other_thread & data.m_thread_type) == 0)
		{
			return false; 
		}
		else
		{
			m_use_session_other_thread = (m_use_session_other_thread & (~data.m_thread_type));
			return true;
		}
	}
	
public:
    //为了兼容protobuff产生的string类型消息  xiaojing@xunlei.com 2012-8-15
    list<string*> m_send_buf_list;
    _u32 send_message(string *data)
    {
        _u32 ret = HANDLE_OK;
        if(m_need_send_len > 0)
        {
            m_send_buf_list.push_back(data);
            return HANDLE_WRITE_PENDING;
        }
        else
        {
            ret = write_send_buffer(data);
            delete data;
            if(ret == HANDLE_OK)
            {
                return send_remain_buf();
            }
            else
            {
                return ret;
            }
        }
    }
    _u32 write_send_buffer(string *data)
    {
        int cmd_len = data->size() + sizeof(uint32_t);
        if(cmd_len + 1024 > m_send_buf_len)
        {
            if(m_send_buf)
            {
                delete []m_send_buf;
            }
            m_send_buf = new uint8_t[cmd_len + 1024];
            m_send_buf_len = cmd_len + 1024;
        }
        //由于protobuf的消息无法自解析，需要提供消息长度作为前缀
        uint32_t length = htonl(data->size());
        memcpy(m_send_buf, (char*)&length, sizeof(uint32_t));
        memcpy(m_send_buf + sizeof(uint32_t), data->c_str(), data->size());
        m_need_send_len = data->size() + sizeof(uint32_t);
        return HANDLE_OK;
    }
   
    ///////////


	int m_socket_fd;
	bool m_session_unlink_flag;
	//设计成标记位而不是引用计数是为了方便定位
	//不过引用计数更灵活，如果有同类其他线程并行处理的需求可改为应用计数
	_u32 m_use_session_other_thread;
	list< Datagram * > m_send_command_list;
	uint8_t *m_send_buf;
	int m_send_buf_len;
	int m_need_send_len;
	int m_cur_send_transfered;
	_u32 m_app_type;

	_u32 m_connect_stat;
	CONNECT_TYPE m_connect_type;
	Datagram * m_wait_deal_cmd;
private:
	DECL_LOGGER(logger);
};

#endif


