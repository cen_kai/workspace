
#ifndef __CDN_MGR_DATAGRAM__H
#define __CDN_MGR_DATAGRAM__H

#include <string>
#include "common/common.h"
#include "common/buffer_encoder_noswap.h"
#include "common/FileUtil.h"

using std::string;

class CdnManagerDatagram {
public:
	static const int QUERY_CDN_PEER_REQ = 1003;
	static const int QUERY_CDN_PEER_RESP  = 1004;

public:
	static const int PROCOTOL_MAGIC_NUM = 0x12345678;
	//static const int PROCOTOL_MAGIC_NUM = 0x11112222;
	static const int CURRENT_PROTOCOL_VERSION = 2013;
	static const int HEADER_MAGIC_POSITION = 0;
	static const int HEADER_TYPE_POSITION = 26;
	static const int HEADER_VERSION_POSITION = 8;
	static const int HEADER_SEQUENCE_POSITION = 12;
	static const int HEADER_LENGTH_POSITION = 16;

	static const int HEADER_LENGTH = 28;
	static const int MAX_DATAGRRAM_LEN = 4 * 1024 * 1024;

	CdnManagerDatagram() {
	}


	CdnManagerDatagram(uint16_t type) :
		_magic(PROCOTOL_MAGIC_NUM),_client_ip(0) ,_version(CURRENT_PROTOCOL_VERSION)
		, _thunder_ver(0)	
		, _compress_flag(0)
		,  _command_id(type){
	}

	//基类，析构函数必须为virtual类型的
	virtual ~CdnManagerDatagram(){

	}

	uint32_t _magic;
	uint32_t _client_ip;
	uint32_t _version;
	uint32_t _sequence;
	uint32_t _length;
	uint32_t _thunder_ver;
	uint16_t _compress_flag;
	uint16_t _command_id;


	inline uint8_t get_datagram_type() {
		return _command_id;
	}
	inline uint32_t get_sequence() {
		return _sequence;
	}
	inline void set_sequence(uint32_t seq) {
		_sequence = seq;
	}

	template<class Archive>
	void serialize(Archive& ar) {
		ar & _magic;
		ar & _client_ip;
		ar & _version;
		ar & _sequence;
		ar & _length;
		ar & _thunder_ver;
		ar & _compress_flag;
		ar & _command_id;
	}

	size_t GetHeaderLen() const
	{
		return ( sizeof(_magic)
			+ sizeof(_client_ip)
			+ sizeof(_version)
			+ sizeof(_sequence)
			+ sizeof(_thunder_ver)
			+ sizeof(_compress_flag)
			+ sizeof(_command_id) );
	}	
};


class QueryCdnmanagerRequest: public CdnManagerDatagram
{
public:
	struct _cdnmanager_taskinfo 
	{
		_cdnmanager_taskinfo() 
		{
			filesize = 0;
		}
		template<class Archive>
		void serialize(Archive& ar) {
			uint32_t size = 4 + MAX_CID_LEN + 4 + MAX_CID_LEN + 8;
			ar & size;
			ar & gcid;
			ar & cid;
			ar & filesize;
		}

		size_t EncodeLen() const
		{	
			size_t size = 0;
			
			size += sizeof(int); //自身结构大小
			//size += sizeof(int) + gcid.cid.size();
			//size += sizeof(int) + cid.cid.size();
			size += sizeof(int) + MAX_CID_LEN; 
			size += sizeof(int) + MAX_CID_LEN; 
			size += sizeof(filesize);
			return size;
		}

		std::string to_string()const{
			ostringstream ss;
			ss << "gcid: " << gcid.dump()
				<< " cid: " << cid.dump()
				<< " filesize: " << filesize;
			return ss.str();
		}

		//std::string gcid;
		//std::string cid;
		CIDWrapper gcid;
		CIDWrapper cid;
		uint64_t filesize;
	};
	
	QueryCdnmanagerRequest() :CdnManagerDatagram(CdnManagerDatagram::QUERY_CDN_PEER_REQ) 
	{
		m_need_peer = 1; //0 代表只要状态，1代表还要具体信息
	}
	uint64_t m_userid;
	string   m_peerid;
	string   m_loginkey;
	std::vector<_cdnmanager_taskinfo> m_tasks;
	uint32_t m_need_peer;//0 代表只要状态，1代表还要具体信息
		 

	std::string to_string()
	{
		return "";
	}

	template<class Archive>
	void serialize(Archive& ar) {
		_length = EncodeLen() - GetHeaderLen() + 4;  //好吧，我承认这里+4很magic，但确实这么算是正确的...
		CdnManagerDatagram::serialize<Archive>(ar);

		ar & m_userid;
		ar & m_peerid;
		ar & m_loginkey;

		uint32_t size = m_tasks.size();
		ar & size;
		for(uint32_t i=0 ; i<size ;i++)
		{
			ar & m_tasks[i];
		}
		ar & m_need_peer;
	}	
	size_t EncodeLen() const
	{
		size_t size = 0;
		size = sizeof(int); //自身大小
		size += GetHeaderLen(); //报文头部大小
		size += sizeof(m_userid);
		size += sizeof(int) + m_peerid.size();
		size += sizeof(int) + m_loginkey.size();
		size_t count = m_tasks.size();
		size += sizeof(int); //tasks number
		for(size_t i=0; i<count; ++i)
		{
			size += m_tasks[i].EncodeLen();
		}
		size += sizeof(m_need_peer);
		return size;
	}
};

class QueryCdnmanagerResponse: public CdnManagerDatagram
{
public:
	struct _peer_info 
	{
		_peer_info() 
		{

		}
		template<class Archive>
		void serialize(Archive& ar) {
			uint32_t size = 4 + peerid.size() + 8 + 2 + 2 + 4+ 4 + 4 + 4;
			ar & size;
			
			ar & peerid;
			ar & ip  ;
			ar & port;
			ar & udp_port;
			ar & res_level;
			ar & use_level;
			ar & capability;
			ar & cdn_type;
		}

		std::string to_string()const{
			ostringstream ss;
			ss << "peerid: "<<peerid
				<<" port: "<<port
				<<" udpport:" << udp_port
				<< " reslevel:" << res_level
				<<" uselevel:" << use_level
				<<" capability: "<<capability
				<<" cdntype:" << cdn_type;

			return ss.str();
		}

		string peerid;
		uint64_t ip;
		uint16_t port;
		uint16_t udp_port;
		uint32_t res_level;
		uint32_t use_level;
		uint32_t capability;
		uint32_t cdn_type;
	};	

	struct _server_info 
	{
		_server_info() 
		{
		}
		template<class Archive>
		void serialize(Archive& ar) {
			uint32_t size = 4 + url.size() + 4 + cookie.size();
			ar & size;
			ar & url;
			ar & cookie;
		}

		std::string to_string()const
		{
			ostringstream ss;
			ss << "url: "<< url
				<<" cookie: "<<cookie;

			return ss.str();
		}
		string url;
		string cookie;
	};	

	struct _cdn_info 
	{
		_cdn_info() 
		{

		}
		template<class Archive>
		void serialize(Archive& ar) 
		{
			uint32_t size = 0;
			ar & size;

			ar & status;
			
			ar & size;
			if( size > 100 )
			{
				throw "error peercont";
			}

			peer_vec.resize(size);
			for(uint32_t i = 0; i < size; i++)
			{
				_peer_info& peer = peer_vec[i];
				peer.serialize(ar);
			}
			
			ar & size;
			if( size > 100 )
			{
				throw "error server count";
			}

			server_vec.resize(size);
			for(uint32_t i = 0; i < size; i++)
			{
				_server_info& server = server_vec[i];
				server.serialize(ar);
			}
			
		}

		std::string to_string()const
		{
			ostringstream ss;
			ss << "cdn.status: "<< status;

			for(uint32_t i = 0 ; i < peer_vec.size(); i++)
			{
				ss << peer_vec[i].to_string();
			}

		    for(uint32_t i = 0 ; i < server_vec.size(); i++)
			{
				ss << server_vec[i].to_string();
			}

			return ss.str();
		}

		uint32_t status;
		std::vector<_peer_info> peer_vec;
		std::vector<_server_info> server_vec;
	};	

public:
	QueryCdnmanagerResponse() :CdnManagerDatagram(CdnManagerDatagram::QUERY_CDN_PEER_RESP) 
	{
		_version = 2012;
	}

	uint16_t status;

	std::vector<_cdn_info> cdn_vec;

	std::string to_string()
	{
		ostringstream oss;
		oss << "response.status:" << status;
		for (uint32_t i = 0; i < cdn_vec.size(); i++)
			{
			  oss << cdn_vec[i].to_string() ;
			}
		return oss.str();
	}
	template<class Archive>
	void serialize(Archive& ar) 
	{
		CdnManagerDatagram::serialize<Archive>(ar);

		ar & status;

		uint32_t size = 0;
		ar & size;

		if(size > 3000)throw "too much cdn infosss.";

		cdn_vec.resize(size);

		for(uint32_t i = 0; i < size; i ++)
		{
			_cdn_info& info = cdn_vec[i];
			info.serialize(ar);
		}
	}	
};

#endif 



