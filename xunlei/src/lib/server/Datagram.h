#ifndef DATA_GRAM__H 
#define DATA_GRAM__H 

#include <string>
#include "common/common.h"
#include "common/buffer_encoder_noswap.h"

using std::string;

class Datagram
{
public:
	virtual ~Datagram(){}
    Datagram(uint8_t type)
            :_version(CURRENT_PROTOCOL_VERSION),
            _seq(0),
            _type(type)
    {
    }
public:
    //datagram types
	enum
	{	
		COMMAND_UNKNOW = -1,
	        
		COMMAND_HTTP_CMD = 1,
	    COMMAND_HTTP_CMD_RESP = 2,
	};
	static const uint32_t CMD_ATTRIBUTE_HTTP = 0x01;
	
	static const uint32_t MAX_CMD_BUF_LEN = 2048;
	static const uint32_t MAX_CMD_LEN = 10 * 1024 * 1024;
	
    static const uint32_t CURRENT_PROTOCOL_VERSION = 1;
    static const uint32_t PROCOTOL_MAGIC_NUM = 0x786C7365;

	
    //errcode define
   
    static const uint32_t ERR_GET_FPID_TIMEOUT = 302;
    static const uint32_t ERR_GET_FP_TIMEOUT=303;

    static const uint32_t ERR_UNKNOWN = 501;
    static const uint32_t ERR_INVALID_PACKET = 502;

    static const uint32_t ERR_NO_SPATIAL_FINGER_REQUEST = 102;	
	
	//= ��Աƫ����Ϣ
	static const uint32_t HEADER_VERSION_POSITION = 0;
	static const uint32_t HEADER_SEQUENCE_POSITION = 4;
	static const uint32_t HEADER_LENGTH_POSITION = 8;
	static const uint32_t HEADER_TYPE_POSITION = 12;
	static const uint32_t HEADER_BODY_POSITION = 12;
	static const int HEADER_LENGTH = 13;

    uint32_t       _version;
    uint32_t       _seq;
    uint32_t       _length;
	uint8_t		   _type;
	
    virtual void serialize(EncoderNoSwap& ar){
		serializeHead<EncoderNoSwap>(ar);
		serialize_body(ar);
	}
    virtual void serialize(DecoderNoSwap& ar){
		serializeHead<DecoderNoSwap>(ar);
		serialize_body(ar);
	}
    virtual void serialize_body(EncoderNoSwap& ar){}
    virtual void serialize_body(DecoderNoSwap& ar){}

	virtual int EncodeParaLen(){return MAX_CMD_BUF_LEN;}

    template<class Archive>
    void serializeHead(Archive& ar)
    {
		_length = EncodeLen() - 12;
        ar&_version;
        ar&_seq;
        ar&_length;
		ar&_type;
    }
	
	virtual int EncodeLen()
	{
		return EncodeHeadLen() + EncodeParaLen();
	}

	int EncodeHeadLen()
	{
		static int ret =  sizeof(_version) 
			+ sizeof(_seq)
			+ sizeof(_length)
			+ sizeof(_type);

		return ret;
	}
	virtual string GetContent() const 
	{ 
		return "";
	}
};

#endif 



