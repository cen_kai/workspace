#ifndef __TIME_DEL__
#define __TIME_DEL__

#include <stdlib.h>
#include <sys/time.h>
#include <ctime>

class Timedelt {
public:
        Timedelt( unsigned long  &delt ):diff(delt){
        clock_gettime(CLOCK_MONOTONIC,&start);
}

        ~Timedelt(){
        clock_gettime(CLOCK_MONOTONIC,&end);
        diff = 1000000000 * (end.tv_sec-start.tv_sec)+ end.tv_nsec-start.tv_nsec;
}
private:
        struct timespec start,end;
        unsigned long   &diff ;
};




#endif

