//============================================================
// FileUtil.h : interface of utility class for thunder file hash functions
//
// Author: JeffLuo
// Created: 2006-08-28
//============================================================

#ifndef __FILEHASH_UTIL_H_20060828_11
#define  __FILEHASH_UTIL_H_20060828_11

#include "common/common.h"
#include "common/SDLogger.h"
#include "common/Utility.h"

class ThunderFileID;

class FileUtil
{
private:
	FileUtil() { }  // private ctor to prevent instantiate

public:
	const static int CID_PART_LENGTH = (20<<10); // 20K
	const static int CID_SIZE = 20;	
	const static int GCID_PART_SIZE=(256<<10); // 256K
	const static int MD5_RET_SIZE = 16;

	static int calc_file_cid(const char* filepath, uint8_t* cid_buf, int& buffer_len);
	static int calc_file_gcid(const char* filepath, uint8_t* gcid_buf, int& buffer_len, int& bcid_num, uint8_t** bcid_buf_ret = NULL);

	static int create_xcid_file(const char* xcid_path, uint8_t* cid, uint8_t* gcid, uint8_t* bcid, int bcid_block_num, _u64 filesize);
	static int read_bcid_block_num(const char* xcid_path);
	static bool read_bcid(const char* xcid_path, uint8_t* bcid_buf, int buffer_len);
	static bool read_filesize_from_xcid(const char* xcid_path, _u64& filesize);
	static bool read_cid_gcid_filesize(const char* xcid_path, uint8_t* cid_buf, int cid_buf_len, uint8_t* gcid_buf, int gcid_buf_len, _u64& filesize);
	
	static int calc_gcid_partsize(_u64 filesize);

	static bool calc_file_md5(const char* filepath, uint8_t* md5_ret, int& buffer_len);
	static bool read_md5sum(const char* md5file, char* buffer, int buffer_len, bool log_warning = true);
	static bool is_true_head(const HeadFile &head);
private:
	DECL_LOGGER(logger);
};

class ThunderFileID
{
public:
	ThunderFileID(uint8_t cid[], _u64 filesize)
	{
		memcpy(m_cid, cid, FileUtil::CID_SIZE);
		m_filesize = filesize;
	}

	
	
public:
	uint8_t m_cid[FileUtil::CID_SIZE];
	_u64 m_filesize;
};

struct CIDWrapper
{
	uint8_t cid[MAX_CID_LEN];

	CIDWrapper()
	{
		memset(cid, 0, MAX_CID_LEN);
	}

	CIDWrapper(uint8_t* src_cid)
	{
		memcpy(cid, src_cid, MAX_CID_LEN);
	}

	CIDWrapper(uint8_t* src_cid, int cid_len)
	{
		memcpy(cid, src_cid, MAX_CID_LEN);
	}

	bool operator < (const CIDWrapper& rhs) const
	{
		int ret = memcmp(cid, rhs.cid, MAX_CID_LEN);
		return (ret < 0);
	}
	
	bool operator == (const CIDWrapper& rhs) const
	{
		return 0 == memcmp(cid, rhs.cid, MAX_CID_LEN);
	}

	_u32 hash_value() const
	{
		int result = 1;
		
	 	for (int i = 0; i < MAX_CID_LEN; i++)
			result = 31 * result + cid[i];
	 
		return abs(result);
	}

	string dump() const
	{
		return Utility::bytes_to_hex(cid, MAX_CID_LEN);
	}
};


struct CIDFilesizeWrapper
{
	CIDWrapper m_cid;
	_u64 m_filesize;

	CIDFilesizeWrapper()
	{
	}

	CIDFilesizeWrapper(uint8_t* src_cid, _u64 filesize)
		:m_cid(src_cid), m_filesize(filesize)
	{
	}

	bool operator < (const CIDFilesizeWrapper& rhs) const
	{
		if(m_filesize == rhs.m_filesize)
		{
			return m_cid < rhs.m_cid;
		}
		return m_filesize < rhs.m_filesize;
	}
	
	bool operator == (const CIDFilesizeWrapper& rhs) const
	{
		return m_filesize==rhs.m_filesize && m_cid==rhs.m_cid;
	}
};


class PartInfo
{
public:
	PartInfo()
	{
		//m_fd = -1ll;
		//m_aio_fd = -1ll;
		m_bcid_num = 0;
		m_total_head_len = 0;
		m_path[0] = 0;
		memset(m_cid, 0, MAX_CID_LEN);
		memset(m_gcid, 0, MAX_CID_LEN);
	}

	~PartInfo()
	{
		
	}

	//初始化分片信息
	bool init(const char *path)
	{
		FILE *fp = fopen(path, "r");
		if(fp == NULL)
		{
			LOG4CPLUS_THIS_WARN(logger, "can not open file " << path << "!!!!!!!!!!!!");
			return false;
		}
		strncpy(m_path, path, MAX_PATH_LEN);
		HeadFile head;
		if(1 != fread(&head.m_magic_num, sizeof(head.m_magic_num), 1, fp))
		{
			LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
			fclose(fp);

			return false;
		}
		if(1 != fread(&head.m_ver, sizeof(head.m_ver), 1, fp))
		{
			LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
			fclose(fp);

			return false;
		}
		if(1 != fread(&head.m_type, sizeof(head.m_type), 1, fp))
		{
			LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
			fclose(fp);

			return false;
		}
		if(1 != fread(&head.m_headlength, sizeof(head.m_headlength), 1, fp))
		{
			LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
			fclose(fp);

			return false;
		}

		LOG4CPLUS_THIS_DEBUG(logger, " head.m_ver is " << head.m_ver << "head.m_magic_num" << head.m_magic_num << "head.m_type" << head.m_type);

		// 种子文件不包括分片信息，单独一个文件
		if(FileUtil::is_true_head(head) == false)	//normal file 
		{
			LOG4CPLUS_THIS_DEBUG(logger, "is normal file ");
			m_total_filesize = Utility::retrieve_filesize(path);
			if(m_total_filesize == 0)
			{
				fclose(fp);
				return false;
			}
			m_type = NORMALFILE;
			m_total_head_len = 0;
			m_part_num = 0;
			m_begin_pos = 0;
			m_cur_filesize = m_total_filesize;
			m_bcid_num = 0;
		}
		else // 种子文件包括多个分片信息
		{
			LOG4CPLUS_THIS_DEBUG(logger, "head.m_type is " << head.m_type << "!!!!!!!!!!!!");
			m_total_head_len = 0;
			m_type = head.m_type; // type 的取值可能为FILE_INDEX 、FILE_PARTS
			m_total_head_len += sizeof(HeadFile);
			switch(head.m_ver)
			{
				case HEAD_VER_10001:
				{
					PartFile_V10001 tpart;
					if(1 != fread(&tpart.m_part_num, sizeof(tpart.m_part_num), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_cid, sizeof(tpart.m_cid), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_gcid, sizeof(tpart.m_gcid), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_total_filesize, sizeof(tpart.m_total_filesize), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_begin_pos, sizeof(tpart.m_begin_pos), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_cur_filesize, sizeof(tpart.m_cur_filesize), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_bcid_num, sizeof(tpart.m_bcid_num), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					
					m_total_head_len += sizeof(PartFile_V10001);
					m_part_num = tpart.m_part_num;
					m_total_filesize = tpart.m_total_filesize;
					m_begin_pos = tpart.m_begin_pos;
					m_cur_filesize = tpart.m_cur_filesize;
					m_bcid_num = tpart.m_bcid_num;
					if(m_total_filesize < 0 || m_begin_pos < 0 || m_cur_filesize < 0 || m_bcid_num < 0)
					{
						LOG4CPLUS_THIS_WARN(logger, "part info error 1. m_part_num:" << tpart.m_part_num
								<< " m_total_filesize:" << tpart.m_total_filesize
								<< " m_begin_pos:" << tpart.m_begin_pos
								<< " m_cur_filesize:" << tpart.m_cur_filesize
								<< " m_bcid_num:" << tpart.m_bcid_num);							
						return false;
					}
					if(m_begin_pos + m_cur_filesize > m_total_filesize)
					{
						LOG4CPLUS_THIS_WARN(logger, "part info error 2. m_begin_pos" << tpart.m_begin_pos
								<< " m_cur_filesize:" << tpart.m_cur_filesize
								<< " m_total_filesize:" << tpart.m_total_filesize);							
						return false;
					}
					memcpy(m_cid, tpart.m_cid, MAX_CID_LEN);
					memcpy(m_gcid, tpart.m_gcid, MAX_CID_LEN);
					LOG4CPLUS_THIS_DEBUG(logger, "m_gcid is :" << Utility::bytes_to_hex(tpart.m_gcid, MAX_CID_LEN) << " and file path is :" << path);
					LOG4CPLUS_THIS_DEBUG(logger, "");
					break;
				}
				case HEAD_VER_10002:
				{
					PartFile_V10002 tpart;
					if(1 != fread(&tpart.m_part_num, sizeof(tpart.m_part_num), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_cid, sizeof(tpart.m_cid), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_gcid, sizeof(tpart.m_gcid), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_total_filesize, sizeof(tpart.m_total_filesize), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_begin_pos, sizeof(tpart.m_begin_pos), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_cur_filesize, sizeof(tpart.m_cur_filesize), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					if(1 != fread(&tpart.m_bcid_num, sizeof(tpart.m_bcid_num), 1, fp))
					{
						LOG4CPLUS_THIS_WARN(logger, "read errno:" << errno);							
						fclose(fp);

						return false;
					}
					
					m_total_head_len += sizeof(PartFile_V10001);
					m_part_num = tpart.m_part_num;
					m_total_filesize = tpart.m_total_filesize;
					m_begin_pos = tpart.m_begin_pos;
					m_cur_filesize = tpart.m_cur_filesize;
					m_bcid_num = tpart.m_bcid_num;
					if(m_total_filesize < 0 || m_begin_pos < 0 || m_cur_filesize < 0 || m_bcid_num < 0)
					{
						LOG4CPLUS_THIS_WARN(logger, "part info error 3. m_part_num:" << tpart.m_part_num
								<< " m_total_filesize:" << tpart.m_total_filesize
								<< " m_begin_pos:" << tpart.m_begin_pos
								<< " m_cur_filesize:" << tpart.m_cur_filesize
								<< " m_bcid_num:" << tpart.m_bcid_num);							
						return false;
					}
					if(m_begin_pos + m_cur_filesize > m_total_filesize)
					{
						LOG4CPLUS_THIS_WARN(logger, "part info error 4. m_begin_pos" << tpart.m_begin_pos
								<< " m_cur_filesize:" << tpart.m_cur_filesize
								<< " m_total_filesize:" << tpart.m_total_filesize);							
						return false;
					}
					memcpy(m_cid, tpart.m_cid, MAX_CID_LEN);
					memcpy(m_gcid, tpart.m_gcid, MAX_CID_LEN);
					LOG4CPLUS_THIS_DEBUG(logger, "m_gcid is :" << Utility::bytes_to_hex(tpart.m_gcid, MAX_CID_LEN) << " and file path is :" << path);
					LOG4CPLUS_THIS_DEBUG(logger, "");
					break;
				}
				default:
				{
					LOG4CPLUS_THIS_WARN(logger, "unkwon version.");							
					fclose(fp);
					return false;
				}
			}
		}

		fclose(fp);
		return true;
	}

	char *get_bcid() const
	{
		if(strlen(m_path) < 1)
		{
			LOG4CPLUS_THIS_WARN(logger, "not init PartInfo !!!");
			return false;
		}
		if(m_bcid_num == 0 || m_total_head_len < (int)sizeof(HeadFile))
		{
			LOG4CPLUS_THIS_WARN(logger, "not get the bcid len!!!");
			return false;
		}
		FILE *fp = fopen(m_path, "r");
		if(fp == NULL)
		{
			LOG4CPLUS_THIS_WARN(logger, "can not open file " << m_path << "!!!!!!!!!!!!");
			return false;
		}
		int pos = (int)fseek(fp,m_total_head_len,SEEK_SET);
		if(pos != 0)
		{
			LOG4CPLUS_THIS_WARN(logger, "fseek file " << m_path << " to " << m_total_head_len << " fail!!!!!!!!!!!! and pos is" << pos);
			return false;
		}
		char *bcid_buf = new char[m_bcid_num*MAX_CID_LEN];
		int ret = fread(bcid_buf, m_bcid_num*MAX_CID_LEN, 1, fp);
		if(ret != 1)
		{
			delete [] bcid_buf;
			return NULL;
		}
		fclose(fp);
		return bcid_buf;
	}

	bool operator < (const PartInfo& rhs)
	{
		if(m_begin_pos < rhs.m_begin_pos)
		{
			return true;
		}
		return false;
	}

	bool operator == (const PartInfo& rhs)
	{
		return (m_begin_pos == rhs.m_begin_pos && m_cur_filesize == rhs.m_cur_filesize);
	}

public:
	int m_part_num;  // start from 0
	int m_type; // 定义种子文件的种类，可能的取值包括NORMALFILE、FILE_INDEX 、FILE_PARTS
	char m_path[MAX_PATH_LEN];
	char m_gcid[MAX_CID_LEN];
	char m_cid[MAX_CID_LEN];
    _u64 m_total_filesize;
	_u64 m_begin_pos;
	_u64 m_cur_filesize;
	int m_total_head_len;
	int m_bcid_num;
	//SInt64 m_fd;
	//SInt64 m_aio_fd;
private:
	DECL_LOGGER(logger);
};

#endif // #ifndef __FILEHASH_UTIL_H_20060828_11
