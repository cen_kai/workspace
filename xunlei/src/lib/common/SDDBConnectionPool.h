// SDDBConnectionPool.h: interface for the SDDBConnectionPool class.

//

//////////////////////////////////////////////////////////////////////



#if !defined(AFX_SDDBCONNECTIONPOOL_H__D127E721_0B62_4474_B615_FF5DDFA9E93B__INCLUDED_)

#define AFX_SDDBCONNECTIONPOOL_H__D127E721_0B62_4474_B615_FF5DDFA9E93B__INCLUDED_



#include <string>

#include <vector>

#include <mysql.h>

#include <common/SDException.h>

#include "common/SDLogger.h"

#define RETRYTIMES 3



using namespace std;



class SDMYSQL

{

public:    

   SDMYSQL(unsigned port,const std::string& host,const std::string& user,const std::string& passwd,const std::string& dbname);

   ~SDMYSQL();



   bool Ensure_Connect();



   bool Init();





   //query buffer and get return,must use count(*),otherwise it get 0

   unsigned get_num_return(char* buffer);



   bool get_num_return(char* buffer,unsigned& num);



   bool get_num_return(char* buffer,int& num);



   //query a sql like select * from sd_tables;

   bool query(const char* buffer,unsigned repeattime = RETRYTIMES);

   long get_affect_rows() { return mysql_affected_rows(&m_sql); }


   //after use query, use this function to get return data, this data's length is return

   int  get_value_data(const MYSQL_ROW& row,unsigned long *lengths,unsigned fields_pos,char* data);



   //after use query, use this function to get return string

   std::string get_value_string(const MYSQL_ROW& row,unsigned long *lengths,unsigned fields_pos);



   //after use query, use this function to get return integer

   int get_value_int(const MYSQL_ROW& row,unsigned long *lengths,unsigned fields_pos);



   MYSQL*  get_mysql() {return &m_sql;}



   std::string getHost() {return m_host;}



   std::string     get_errorinfo();

private:

   //reconnect this server if ping failed

   bool reconnect();

private:

    void    _init();
   unsigned    m_port;

   std::string m_host;

   std::string m_user;

   std::string m_passwd;

   std::string m_dbname;

public:

   //this connect is using by some thread ???

   bool        m_isuse;

   bool        m_isinit;

   MYSQL   m_sql;

    DECL_LOGGER(logger);

};



#include "common/SDLogger.h"

class SDDBConnectionPool

{

public:

   SDDBConnectionPool(){pthread_mutex_init(&m_sqlpool_lock,NULL);}

   SDDBConnectionPool(unsigned poolsize,unsigned port,const std::string& host, \

       const std::string& user,const std::string& passwd,const std::string& dbname);

   SDDBConnectionPool(const SDDBConnectionPool& dbc_pool);



   virtual ~SDDBConnectionPool();



   //init this connect pool

   bool    Init();

   void set_size(uint32_t size);



   //get sql's connection from connnect pool

   SDMYSQL*    get_sql(bool autocreate = true);



   //return back sql's connection to connnect pool

   void    release_sql(SDMYSQL* sql);



   std::string gethost() const {return m_host;}

   std::string getuser() const {return m_user;}

   std::string getpwd() const {return m_passwd;}

   std::string getdbname() const {return m_dbname;}



   unsigned    getport() const {return m_port;}

//private:

   unsigned    m_port;

   std::string m_host;

   std::string m_user;

   std::string m_passwd;

   std::string m_dbname;



   std::vector<SDMYSQL*> m_sqlpool_list;

   pthread_mutex_t m_sqlpool_lock;



   DECL_LOGGER(logger);

};



class Safe_MYSQL

{

public:

   Safe_MYSQL(SDDBConnectionPool* dbpool) : _haveset(false)

   {

       _dbpool = dbpool;

       _safe_sql = NULL;

   }



   ~Safe_MYSQL()

   {

       if(_safe_sql != NULL && _dbpool != NULL)

       {

           _dbpool->release_sql(_safe_sql);

           _safe_sql = NULL;

       }

   }



   SDMYSQL*    get_safesql(bool autocreate = true)

   {

       if(!_haveset)

       {

           if(_dbpool != NULL)

           {

               _safe_sql = _dbpool->get_sql(autocreate);

               _haveset = (_safe_sql != NULL);

           }

           else

           {

               _safe_sql = NULL;

           }

           return _safe_sql;

       }

       else

       {

           return NULL;

       }

   }

private:

   SDMYSQL*                    _safe_sql;

   SDDBConnectionPool*         _dbpool;

   bool                        _haveset;

};





//Notice:  user of this class must catch exception!! 

class SDRES_RESULT

{

public:

   SDRES_RESULT(MYSQL* mysql);

   SDRES_RESULT(SDMYSQL* sdmysql);

   SDRES_RESULT(SDMYSQL* sdmysql,const char* buffer);

   ~SDRES_RESULT();



   bool next();

   void release();



   bool is_Null();



   int get_value_int();

   int64_t get_value_long();



   std::string get_value_string();



   int     get_value_data(char* data,unsigned &date_len);

private:



   MYSQL_RES*  m_result;

   MYSQL_ROW   m_row;

   unsigned        m_fields_pos;

   unsigned        m_max_num_fields;

   unsigned long* m_lengths;

};



class        DBSTMT

{

public:        

   DBSTMT(const char* query,SDMYSQL* mysql);

   ~DBSTMT()

   {

       if(NULL != m_stmt)

       {

           mysql_stmt_close(m_stmt);

           m_stmt = NULL;

       }

   }



public:

   void    execute(MYSQL_BIND* bind);

   

   void    execute()

   {

       if(mysql_stmt_execute(m_stmt))

       {

           throw SDException("mysql_stmt_execute in DBSTMT::execute error=" + std::string(mysql_stmt_error(m_stmt)) );

       }

   }

   unsigned long affected_rows() 

   {

       return mysql_stmt_affected_rows(m_stmt);

   }

   

   inline void bind(MYSQL_BIND* bind)

   {

       if(NULL == bind)

       {

           throw   SDException("bind is null in DBSTMT::execute");

       }

       

       if(mysql_stmt_bind_param(m_stmt,bind) )

       {

           throw SDException("mysql_stmt_bind_param errorl in DBSTMT::execute error=" + std::string(mysql_stmt_error(m_stmt)) );

       }

   }

   int fetch()

   {

       return        mysql_stmt_fetch(m_stmt)==0;

   }



   static int EscapeString (string &str);

private:

   DBSTMT(const DBSTMT&);

   DBSTMT&        operator=(const DBSTMT&);



private:

   MYSQL_STMT*        m_stmt;

};



#endif // !defined(AFX_SDDBCONNECTIONPOOL_H__D127E721_0B62_4474_B615_FF5DDFA9E93B__INCLUDED_)

