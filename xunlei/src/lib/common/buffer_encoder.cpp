#include <arpa/inet.h>
#include "buffer_encoder.h"
#include "Utility.h"

Encoder::Encoder(char* buffer, size_t size)
		: _buffer(buffer)
		, _size(size)
		, _pos(0)
{

}
Encoder::~Encoder()
{

}

void Encoder::save(int n)
{
	n=htonl(n);
	*this<<n;
}
void Encoder::save(int16_t n)
{
	n=htons(n);
	*this<<n;
}
void Encoder::save(char n)
{
	*this<<n;
}
void Encoder::save(uint8_t n)
{
	*this<<n;
}
void Encoder::save(uint16_t n)
{
	n=htons(n);
	*this<<n;
}
void Encoder::save(uint32_t n)
{
	n=htonl(n);
	*this<<n;
}
void Encoder::save(uint64_t n)
{
	n=htonll(n);
	*this<<n;
}
void Encoder::save(int64_t n)
{
	n=htonll(n);
	*this<<n;
}
void Encoder::save(const string& str)
{
	int n=str.length();
	n=htonl(n);
	*this<<n;
	*this<<str;
}

void Encoder::save(char *str)
{
	int n=0;
	if(str)
	{
		n=strlen(str);
	}
	else
	{
		n=0;
	}
	int nn=htonl(n);
	*this<<nn;
	if(str)
	{
		memcpy(_buffer+_pos, str, n);
	}
	else
	{
		//donothing
	}
	_pos+=n;
}

void Encoder::save(double n)
{
	n=htond(n);
	*this<<n;
}


Decoder::Decoder(char* buffer, size_t size)
		: _buffer(buffer)
		, _size(size)
		, _pos(0)
{
}
Decoder::~Decoder()
{

}
	
void Decoder::load(int &n)
{
	*this>>n;
	n=ntohl(n);
}
void Decoder::load(int16_t &n)
{
	*this>>n;
	n=ntohs(n);
}
void Decoder::load(char &n)
{
	*this>>n;
}
void Decoder::load(uint8_t &n)
{
	*this>>n;
}
void Decoder::load(uint16_t &n)
{
	*this>>n;
	n=ntohs(n);
}
void Decoder::load(uint32_t &n)
{
	*this>>n;
	n=ntohl(n);
}
void Decoder::load(uint64_t &n)
{
	*this>>n;
	n=ntohll(n);
}
void Decoder::load(int64_t &n)
{
	*this>>n;
	n=ntohll(n);
}
void Decoder::load(string& str)
{
	int len;
	*this>>len;
	len=ntohl(len);
			
	str.resize(len);
	memcpy((char*)str.c_str(), _buffer+_pos, len);
	_pos+=len;
}

void Decoder::load(char *str)
{
	int n;
	*this>>n;
	n=htonl(n);
	strncpy(str, _buffer+_pos, n);
	str[n] = '\0';
	_pos+=n;
}

void Decoder::load(double &n)
{
	*this>>n;
	n=htond(n);
}


