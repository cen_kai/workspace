#include "mem_leak.h"
#include <iostream>
using namespace std;

MemChecker g_checker;	

MemChecker::MemChecker(const char* stat_file_path, int interval_seconds)
{
    pthread_mutex_init(&_lock, NULL);
    if(!stat_file_path){
        stat_file_path = "../stat/mem_check";
    }
    _file_path = stat_file_path;
}

MemChecker::~MemChecker()
{
    pthread_mutex_destroy(&_lock);
}
void MemChecker::AddOperation(MemOperation &op)
{
    SDAutoLock lock(&_lock);
    if(op.OperationType == 0){
        nodes[op.pBuffer] = op;
    }else if(op.OperationType == 1){
        nodes.erase(op.pBuffer);
    } 
}

void MemChecker::run()
{
    while(true)
    {
        sleep(_interval);
        string dump = Dump();
        if(FILE *file = fopen(_file_path.c_str(), "w+")){
            fputs(dump.c_str(), file);
            fclose(file);
        } 
    }
}

string MemChecker::Dump()
{
    SDAutoLock lock(&_lock);
    map<string, int> leak;
    for(map<void *, MemOperation>::iterator it=nodes.begin(); it!=nodes.end(); ++it)
    {
        stringstream key;
        key << it->second.Filename << ":" << it->second.LineNum;
        map<string, int>::iterator leak_it = leak.find(key.str());
        if(leak_it == leak.end()){
            leak[key.str()] = 0;
        }
        leak[key.str()] += 1;
    }
    stringstream out;
    for(map<string, int>::iterator it=leak.begin(); it!=leak.end(); ++it)
    {
        out << it->first << " : count=" << it->second << endl;
    }
    return out.str();
}

#if defined( MEM_DEBUG )
void* operator new( size_t nSize, char* pszFileName, int nLineNum )
{
	MemOperation record;
	void *pResult;
	pResult = ::operator new( nSize );
	if ( pResult )
	{
		strncpy( record.Filename, pszFileName, FILENAME_LENGTH - 1 );
		record.Filename[ FILENAME_LENGTH - 1 ] = '\0';
		record.LineNum = nLineNum;
		record.AllocSize = nSize;
		record.OperationType = 0;
		record.errCode = 0;
		record.pBuffer = pResult;

	    g_checker.AddOperation(record);	
	}
	
	return pResult;
}

void* operator new[]( size_t nSize, char* pszFileName, int nLineNum )
{
	MemOperation record;
	void *pResult;
	
	pResult = ::operator new[]( nSize );
	
	if ( pResult )
	{
		// record the alloc memory
		strncpy( record.Filename, pszFileName, FILENAME_LENGTH - 1 );
		record.Filename[ FILENAME_LENGTH - 1 ] = '\0';
		record.LineNum = nLineNum;
		record.AllocSize = nSize;
		record.OperationType = 0;
		record.errCode = 0;
		record.pBuffer = pResult;

	    g_checker.AddOperation(record);	
	}

	return pResult;
}


void operator delete( void *ptr )
{
	MemOperation record;
	
	record.AllocSize = 0; // Unknown, but compiler known
	record.OperationType = 1;
	record.errCode = 0;
	record.pBuffer = ptr;
	g_checker.AddOperation(record);	
	
	free( ptr );
}


void
operator delete[]( void *ptr )
{
	MemOperation record;

	record.AllocSize = 0; // Unknown, but compiler known
	record.OperationType = 1;
	record.errCode = 0;
	record.pBuffer = ptr;
	g_checker.AddOperation(record);	

	free( ptr );
}


#endif


