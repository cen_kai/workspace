//============================================================
// RecentlyCounter.h : interface of class RecentlyCounter
//                           
// Author: jeffluo
// Created: 2006-11-04
//============================================================

#ifndef __RECENTLY_COUNTER_H_20061104_23
#define __RECENTLY_COUNTER_H_20061104_23

#include "common/common.h"
#include "common/SDLogger.h"

template <_u32 _MAX_TIME>
class RecentlyCounter
{
	struct TimeSliceItem
	{
		TimeSliceItem() : m_last_time_ms(0), m_counter(0) { }
	
		_u64 m_last_time_ms;
		_u32 m_counter;
	};

public:
	const static int PER_SECOND = 0;
	const static int PER_MINUTE = 1;

public:
	RecentlyCounter(int time_precision = PER_SECOND)
	{
		memset(&m_timed_items[0], 0, sizeof(TimeSliceItem) * _MAX_TIME);
	
		set_time_precision(time_precision);
		m_timeslice_index = 0;
	}
	void set_time_precision(int time_precision)
	{
		if(time_precision == PER_SECOND || time_precision == PER_MINUTE)
			m_time_precision = time_precision;
		else
			m_time_precision = PER_SECOND;
	}

	void increase(_u64 time_ms, int count = 1);
	_u32 sum_recent_counter(_u32 timeslice_num, _u64 time_ms);

private:
	_u32 get_timeslice_ms() const 
	{
		return (m_time_precision == PER_MINUTE ? 60000 : 1000);
	}

private:
	TimeSliceItem m_timed_items[_MAX_TIME];
	_u32 m_timeslice_index;
	int m_time_precision;

private:
	static log4cplus::Logger logger;
};


template <_u32 _MAX_TIME>
log4cplus::Logger RecentlyCounter<_MAX_TIME>::logger = log4cplus::Logger::getInstance("RecentlyCounter");



template <_u32 _MAX_TIME>
void RecentlyCounter<_MAX_TIME>::increase(_u64 time_ms, int count)
{
	if (m_timed_items[m_timeslice_index].m_last_time_ms == 0)
	{
		m_timed_items[m_timeslice_index].m_counter = count;
		m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;
		return;
	}

	_u32 timeslice_ms = get_timeslice_ms();		
	if (time_ms - m_timed_items[m_timeslice_index].m_last_time_ms >= _MAX_TIME * timeslice_ms)
	{
		memset(&m_timed_items[0], 0, sizeof(TimeSliceItem) * _MAX_TIME);
		
		m_timed_items[0].m_counter = count;
		m_timed_items[0].m_last_time_ms = time_ms;
		m_timeslice_index = 0;

		return;
	}	

	_u32 cur_time_index = (_u32)(time_ms / timeslice_ms);
	_u32 last_time_index = (_u32)(m_timed_items[m_timeslice_index].m_last_time_ms / timeslice_ms);		
	if(last_time_index > cur_time_index || cur_time_index - last_time_index > _MAX_TIME)
	{
		LOG4CPLUS_THIS_WARN(logger, "increase(): cur_time_index=" << cur_time_index << ", last_time_index=" << last_time_index);
	}
	if(cur_time_index == last_time_index)
	{
		m_timed_items[m_timeslice_index].m_counter += count;
		m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;
	}
	else if(cur_time_index > last_time_index)
	{
		for(_u32 i = 0; i < cur_time_index - last_time_index - 1; i++)
		{
			m_timeslice_index++;
			if(m_timeslice_index >= _MAX_TIME)
				m_timeslice_index = 0;
			
			m_timed_items[m_timeslice_index].m_counter = 0;			
			m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;			
		}

		m_timeslice_index++;
		if(m_timeslice_index >= _MAX_TIME)
			m_timeslice_index = 0;
		m_timed_items[m_timeslice_index].m_counter = count;			
		m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;			
	}	
}

template <_u32 _MAX_TIME>
_u32 RecentlyCounter<_MAX_TIME>::sum_recent_counter(_u32 timeslice_num, _u64 time_ms)
{
	if (m_timed_items[m_timeslice_index].m_last_time_ms == 0)
		return 0;

	if(timeslice_num > _MAX_TIME)
		timeslice_num = _MAX_TIME;

	_u32 timeslice_ms = get_timeslice_ms();	
	if (time_ms - m_timed_items[m_timeslice_index].m_last_time_ms > timeslice_num * timeslice_ms)
	{		
		return 0;
	}	


	_u32 cur_time_index = (_u32)(time_ms / timeslice_ms);
	_u32 last_time_index = (_u32)(m_timed_items[m_timeslice_index].m_last_time_ms / timeslice_ms);	
	if(last_time_index > cur_time_index || cur_time_index - last_time_index > timeslice_num)
	{
		LOG4CPLUS_THIS_WARN(logger, "sum_recent_counter(): cur_time_index=" << cur_time_index << ", last_time_index=" << last_time_index);
	}
	if(cur_time_index > last_time_index)
	{
		timeslice_num -= (cur_time_index - last_time_index);
		if(timeslice_num <= 0 || timeslice_num > _MAX_TIME)
			return 0;
	}

	int cur_index = m_timeslice_index;
	_u32 counter_sum = 0;
	for(_u32 i = 0; i < timeslice_num; i++)
	{
		int counter = m_timed_items[cur_index].m_counter;
		if(counter < 0)
		{
			LOG4CPLUS_THIS_WARN(logger, "sum_recent_counter(): counter=" << counter << " at index #" << cur_index);
		}
		counter_sum += counter;

		cur_index--;
		if(cur_index < 0)
			cur_index = _MAX_TIME - 1;
	}

	return counter_sum;
}

#endif // __RECENTLY_COUNTER_H_20061104_23



