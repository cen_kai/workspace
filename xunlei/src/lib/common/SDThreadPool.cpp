#include "exception"
#include <common/SDThreadPool.h>

using namespace std;

IMPL_LOGGER(SDThreadPool, logger)

SDThreadPool::SDThreadPool(int pool_size) : m_pool_size(pool_size)
{
	m_thread_ids = NULL;
	m_active_count = 0;
	m_dead_count = 0;//
	pthread_mutex_init(&m_count_lock, NULL);
}

SDThreadPool::~SDThreadPool()
{
	if(m_thread_ids)
	{
		delete[] m_thread_ids;
		m_thread_ids = NULL;
	}
	pthread_mutex_destroy(&m_count_lock);
}

bool SDThreadPool::startThreads()
{
	bool ret = true;
	if(m_pool_size > 0)
	{		
		pthread_attr_t attr;
		pthread_attr_init(&attr);

		//set stack size
		pthread_attr_setstacksize(&attr, STACK_SIZE);

		m_thread_ids = new pthread_t[m_pool_size];
		for(int i = 0; i < m_pool_size; i++)
		{
			if(pthread_create((m_thread_ids + i), &attr, threadProc, (void*)this))
			{
				ret = false;
			}
		}

		pthread_attr_destroy(&attr);
	}
	return ret;
}

void SDThreadPool::waitThreadsTermination()
{
	if(m_thread_ids)
	{
		for(int i = 0; i < m_pool_size; i++)
		{
			pthread_join(m_thread_ids[i], NULL);
		}

		delete[] m_thread_ids;
		m_thread_ids = NULL;
	}
}

void* SDThreadPool::threadProc(void* para)
{
	SDThreadPool* me = (SDThreadPool*) para;
	LOG4CPLUS_ERROR(logger, "*******************Thread start,m_pool_size=" << me->m_pool_size);
	try
	{
		me->doIt();
	}
	catch (std::exception& e) 
	{
		LOG4CPLUS_ERROR(logger, "*******************Thread Exception: "<<e.what());
		throw;		
	}
	catch(...)
	{
		LOG4CPLUS_ERROR(logger, "*******************Thread occur unknown error");
		throw;		
	}
	me->setthreaddie();
	return NULL;
}








IMPL_LOGGER(SDTwinThreadPool, logger)

SDTwinThreadPool::SDTwinThreadPool(unsigned poolsize1,unsigned poolsize2)
	: m_pool_size1(poolsize1),m_pool_size2(poolsize2),m_thread_ids1(NULL),m_thread_ids2(NULL),
		m_active_count1(0),m_active_count2(0)
{
	m_dead_count1 = m_dead_count2 = 0;
	
	pthread_mutex_init(&m_count_lock, NULL);
}

SDTwinThreadPool::~SDTwinThreadPool()
{
	waitThreadsTermination();
	pthread_mutex_destroy(&m_count_lock);
}

bool SDTwinThreadPool::startThreads()
{
	bool ret = true;
	if(m_pool_size1 > 0)
	{		
		pthread_attr_t attr;
		pthread_attr_init(&attr);

		//set stack size
		pthread_attr_setstacksize(&attr, STACK_SIZE);

		m_thread_ids1 = new pthread_t[m_pool_size1];
		for(unsigned i = 0; i < m_pool_size1; i++)
		{			
			if(pthread_create(m_thread_ids1+i, &attr, threadProc, (void*)this))
			{
				ret = false;
			}
			LOG4CPLUS_DEBUG(logger, "startThreads  m_thread_ids1[i]=: "<< m_thread_ids1[i]);
		}

		pthread_attr_destroy(&attr);
	}

	if(m_pool_size2 > 0)
	{		
		pthread_attr_t attr;
		pthread_attr_init(&attr);

		//set stack size
		pthread_attr_setstacksize(&attr, STACK_SIZE);

		m_thread_ids2 = new pthread_t[m_pool_size2];
		for(unsigned i = 0; i < m_pool_size2; i++)
		{
			if(pthread_create(m_thread_ids2+i, &attr, threadProc2, (void*)this))
			{
				ret = false;
			}
			LOG4CPLUS_DEBUG(logger, "startThreads  m_thread_ids2[i]=: "<< m_thread_ids2[i]);
		}

		pthread_attr_destroy(&attr);
	}

	return ret;
}

void SDTwinThreadPool::waitThreadsTermination()
{
	if(NULL != m_thread_ids1)
	{
		for(unsigned i = 0; i < m_pool_size1; i++)
		{
			LOG4CPLUS_DEBUG(logger, "waitThreadsTermination  m_thread_ids1[i]=: "<< m_thread_ids1[i]);
			pthread_join(m_thread_ids1[i], NULL);
		}

		delete[] m_thread_ids1;
		m_thread_ids1 = NULL;
	}
	if(NULL != m_thread_ids2)
	{
		for(unsigned i = 0; i < m_pool_size2; i++)
		{
			pthread_join(m_thread_ids2[i], NULL);
		}

		delete[] m_thread_ids2;
		m_thread_ids2 = NULL;
	}
}

void* SDTwinThreadPool::threadProc(void* para)
{
	SDTwinThreadPool* me = (SDTwinThreadPool*) para;
	LOG4CPLUS_ERROR(logger, "*******************Thread start, m_pool_size1=" << me->m_pool_size1);
	try
	{
		me->doIt();
	}
	catch (std::exception& e) 
	{
		LOG4CPLUS_FATAL(logger, "*************Thread Exception: "<<e.what());
		throw;
	}
	catch(...)
	{
		LOG4CPLUS_FATAL(logger, "************Thread2 occur unknown error");
		throw;		
	}
	LOG4CPLUS_DEBUG(logger, " WARN****************thread end******************* ");
	me->setthreaddie();
	return NULL;
}

void* SDTwinThreadPool::threadProc2(void* para)
{
	SDTwinThreadPool* me = (SDTwinThreadPool*) para;
	LOG4CPLUS_ERROR(logger, "*******************Thread2 start,m_pool_size2= " << me->m_pool_size2);
	try
	{
		me->doIt2();
	}
	catch (std::exception& e) 
	{
		LOG4CPLUS_ERROR(logger, "*************Thread Exception: "<<e.what());
		throw;		
	}
	catch(...)
	{
		LOG4CPLUS_FATAL(logger, "***************Thread2 occur unknown error");
		throw;		
	}
	LOG4CPLUS_DEBUG(logger, " WARN****************thread end 2******************* ");
	me->setthreaddie2();	
	return NULL;
}


