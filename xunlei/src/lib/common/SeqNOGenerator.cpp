#include "SeqNOGenerator.h"


SeqNOGenerator *SeqNOGenerator::_instance = NULL;

SeqNOGenerator::SeqNOGenerator()
{
    _seq = 0;
    pthread_mutex_init(&_lock, NULL);    
}

SeqNOGenerator::~SeqNOGenerator()
{
    pthread_mutex_destroy(&_lock);    
}

SeqNOGenerator * SeqNOGenerator::Instance()
{
    if(_instance == NULL){
        _instance = new SeqNOGenerator;
    }
    return _instance;
}
    
uint32_t SeqNOGenerator::GetSeqNO()
{
    pthread_mutex_lock(&_lock);
    uint32_t seq = _seq++;
    pthread_mutex_unlock(&_lock);
    return seq;
}

