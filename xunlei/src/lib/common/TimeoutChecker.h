//==============================================================
// TimeoutChecker.h : interface of class TimeoutChecker
//			   
//
// Author: 
// Created: 2006-09-25
//===============================================================

#ifndef __TIMEOUTCHECKER_H_20060925_20
#define  __TIMEOUTCHECKER_H_20060925_20

#include <ext/hash_map>
#include "common/common.h"
#include <vector>

using namespace __gnu_cxx;

template <class _Key, class _Data>
struct TimeoutFunc
{
	void operator()(_Key key, _Data data)
	{
		
	}
};

template <class _Key, class _Data, class _TimeoutFunc>
class TimeoutChecker
{
public:
	typedef _Key key_type;
	typedef _Data data_type;	

private:
	struct DataExt;

	typedef hash_map<key_type, DataExt> hashtable;
	typedef pair<const key_type, DataExt> node_type;	

	struct DataExt
	{
		data_type m_real_data;
		int m_timeout_ms;
		node_type* m_prev;
		node_type* m_next;
		char m_pos; // -1:middle node's left side, 
		            // 0:just at the middle node, 
		            // 1:middle node's right side.
	};

public:
	TimeoutChecker(_TimeoutFunc timeout_func) : m_timeout_func(timeout_func)
	{
		m_head = NULL;
		m_tail = NULL;
		m_middle = NULL;
		m_middle_timeout_ms = 0;
	}

	bool register_timeout_item(const key_type& key, const data_type& data, int timeout_ms);
	bool unregister_timeout_item(const key_type& key, data_type& data);
	bool get_item(const key_type& key, data_type& data);
	int check_items(int elapsed_ms);
	_u32 size() const { return m_elements_ht.size(); }
	void clear();
	
private:
	hashtable m_elements_ht;
	_TimeoutFunc m_timeout_func;
	node_type* m_head;
	node_type* m_tail;
	node_type* m_middle;
	int m_middle_timeout_ms;
}; 

#endif // __TIMEOUTCHECKER_H_20060925_20

///////////////////////////////////////////////////////////////////////////////
template<class _Key, class _Data, class _TimeoutFunc>
bool TimeoutChecker<_Key, _Data, _TimeoutFunc>::
register_timeout_item(const key_type& key, const data_type& data, int timeout_ms)
{
	typename hashtable::iterator it = m_elements_ht.find(key);
	if (it != m_elements_ht.end()) // already exist
		return false;

	DataExt data_ext;
	data_ext.m_real_data = data;
	data_ext.m_timeout_ms = timeout_ms;
	data_ext.m_prev = NULL;
	data_ext.m_next = NULL;
	data_ext.m_pos = 0;

	// insert
	pair<const typename hashtable::iterator, bool> ins_result 
			= m_elements_ht.insert(node_type(key, data_ext));

	// update the link
	if (m_head == NULL) // empty list
	{
		m_head = &(*(ins_result.first));
		m_tail = m_head;

		m_middle = m_head;
		m_middle_timeout_ms = m_middle->second.m_timeout_ms;
	}
	else if (timeout_ms <= m_head->second.m_timeout_ms) // add as head
	{
		m_head->second.m_timeout_ms -= data_ext.m_timeout_ms; // update next node's tiemout_ms

		m_head->second.m_prev = &(*(ins_result.first));
		ins_result.first->second.m_next = m_head;
		m_head = &(*(ins_result.first));

		ins_result.first->second.m_pos = -1; // inserted node at left side of the middle node

		if (m_elements_ht.size()%2 != 0)
		{
			// middle node move left a step
			m_middle->second.m_pos = 1;
			m_middle_timeout_ms -= m_middle->second.m_timeout_ms;
			m_middle = m_middle->second.m_prev;
			m_middle->second.m_pos = 0;
		}
	}
	else // add as nonhead
	{	
		node_type* ptr = m_head;
		int sum = 0;

		if (timeout_ms >= m_middle_timeout_ms) // optimization, make max length be list size's half
		{
			ptr = m_middle->second.m_next;
			sum = m_middle_timeout_ms;
		}

		while (ptr) // find a proper position for inserting
		{
			sum += ptr->second.m_timeout_ms;
			if (sum >= timeout_ms)
				break;
			ptr = ptr->second.m_next;
		}

		if (!ptr) // add as tail
		{
			ins_result.first->second.m_timeout_ms -= sum;
			
			m_tail->second.m_next = &(*(ins_result.first));
			ins_result.first->second.m_prev = m_tail;
			m_tail = &(*(ins_result.first));
			
			ins_result.first->second.m_pos = 1; // inserted node at right side of the middle node

			if (m_elements_ht.size()%2 == 0)
			{
				// middle node move right a step
				m_middle->second.m_pos = -1;
				m_middle = m_middle->second.m_next;
				m_middle_timeout_ms += m_middle->second.m_timeout_ms;
				m_middle->second.m_pos = 0;
			}					
		}
		else // add as neither tail nor head
		{
			ins_result.first->second.m_timeout_ms -= sum - ptr->second.m_timeout_ms;

			node_type* ptr_prev = ptr->second.m_prev;
			node_type* ptr_next = ptr;

			// update next node's timeout_ms
			ptr->second.m_timeout_ms -= ins_result.first->second.m_timeout_ms;

			ptr_prev->second.m_next = &(*(ins_result.first));
			ins_result.first->second.m_prev = ptr_prev;
			ins_result.first->second.m_next = ptr_next;
			ptr_next->second.m_prev = &(*(ins_result.first));
			
			if (m_elements_ht.size()%2 == 0)
			{
				if (timeout_ms >= m_middle_timeout_ms)
				{
					ins_result.first->second.m_pos = 1; // inserted node at right side of the middle node
					// middle node move right a step
					m_middle->second.m_pos = -1;
					m_middle = m_middle->second.m_next;
					m_middle_timeout_ms += m_middle->second.m_timeout_ms;
					m_middle->second.m_pos = 0;
				}
				else
				{
					ins_result.first->second.m_pos = -1; // inserted node at left side of the middle node
				}
			}
			else
			{
				if (timeout_ms < m_middle_timeout_ms)
				{
					ins_result.first->second.m_pos = -1; // inserted node at left side of the middle node
					// middle node move left a step
					m_middle->second.m_pos = 1;
					m_middle_timeout_ms -= m_middle->second.m_timeout_ms;
					m_middle = m_middle->second.m_prev;
					m_middle->second.m_pos = 0;
				}
				else
				{
					ins_result.first->second.m_pos = 1; // inserted node at right side of the middle node
				}				
			}					
		}
	}

	return true;
}

template<class _Key, class _Data, class _TimeoutFunc>
bool TimeoutChecker<_Key, _Data, _TimeoutFunc>::
unregister_timeout_item(const key_type& key, data_type& data)
{
	typename hashtable::iterator it = m_elements_ht.find(key);
	if (it == m_elements_ht.end()) // not found
		return false;

	data = it->second.m_real_data;

	node_type* ptr_prev = it->second.m_prev;
	node_type* ptr_next = it->second.m_next;

	if (!ptr_prev) // remove head node
	{
		m_head = ptr_next;
		if (m_head)
		{
			m_head->second.m_prev = NULL;
			m_head->second.m_timeout_ms += it->second.m_timeout_ms;

			if (m_elements_ht.size()%2 != 0)
			{
				// middle node move right a step
				m_middle->second.m_pos = -1;
				m_middle = m_middle->second.m_next;
				m_middle_timeout_ms += m_middle->second.m_timeout_ms;
				m_middle->second.m_pos = 0;
			}
		}
		else
		{
			m_tail = m_head;
			m_middle = m_head;
		}
	}
	else if (!ptr_next) // remove tail node
	{
		m_tail = ptr_prev;
		if (m_tail)
		{
			m_tail->second.m_next = NULL;
			if (m_elements_ht.size()%2 == 0)
			{
				// move middle node left a step
				m_middle->second.m_pos = 1;
				m_middle_timeout_ms -= m_middle->second.m_timeout_ms;
				m_middle = m_middle->second.m_prev;
				m_middle->second.m_pos = 0;
			}
		}
		else
		{
			m_head = m_tail;
			m_middle = m_tail;
		}
	}
	else // remove neither tail nor head node
	{			
		ptr_prev->second.m_next = ptr_next;
		ptr_next->second.m_prev = ptr_prev;
		
		if ((int)it->second.m_pos == -1) // remove the node left of middle node
		{
			if (m_elements_ht.size()%2 != 0)
			{
				// middle node move right a step
				m_middle->second.m_pos = -1;
				m_middle = m_middle->second.m_next;
				m_middle_timeout_ms += m_middle->second.m_timeout_ms;
				m_middle->second.m_pos = 0;
			}
		}
		else if ((int)it->second.m_pos == 0) // remove the middle node
		{
			if (m_elements_ht.size()%2 == 0)
			{
				m_middle_timeout_ms -= m_middle->second.m_timeout_ms;
				m_middle = m_middle->second.m_prev;
			}
			else
			{
				m_middle_timeout_ms += m_middle->second.m_timeout_ms;
				m_middle = m_middle->second.m_next;
			}
			m_middle->second.m_pos = 0; // set net middle node
		}
		else // remove the node right of the middle node
		{
			if (m_elements_ht.size()%2 == 0) 			
			{
				// middle node move left a step
				m_middle->second.m_pos = 1;
				m_middle_timeout_ms -= m_middle->second.m_timeout_ms;
				m_middle = m_middle->second.m_prev;
				m_middle->second.m_pos = 0;
			}
		}

		ptr_next->second.m_timeout_ms += it->second.m_timeout_ms; //update next node's timeout_ms

	}	

	m_elements_ht.erase(it); // remove the node

	return true;	
}

template<class _Key, class _Data, class _TimeoutFunc>
bool TimeoutChecker<_Key, _Data, _TimeoutFunc>::
get_item(const key_type& key, data_type& data)
{
	typename hashtable::iterator it = m_elements_ht.find(key);
	if (it == m_elements_ht.end()) // not found
		return false;	

	data = it->second.m_real_data;

	return true;
}

template<class _Key, class _Data, class _TimeoutFunc>
int TimeoutChecker<_Key, _Data, _TimeoutFunc>::
check_items(int elapsed_ms)
{
	if (!m_head) // empty list
		return 0;

	int timeout_ms = elapsed_ms;
	
	int num = 0;
	int sum = 0;
	node_type* ptr = m_head;
	while (ptr)
	{
		sum += ptr->second.m_timeout_ms;
		if (timeout_ms >= sum) // is timeout
		{
			node_type* ptr_prev = ptr->second.m_prev;
			node_type* ptr_next = ptr->second.m_next;

			if (ptr_prev)
				ptr_prev->second.m_next = ptr_next;
			else
			{
				m_head = ptr_next;
				if (m_head)
					m_head->second.m_prev = NULL;
			}
			if (ptr_next)
				ptr_next->second.m_prev = ptr_prev;
			else
			{
				m_tail = ptr_prev;
				if (m_tail)
					m_tail->second.m_next = NULL;
			}

			m_timeout_func(ptr->first, ptr->second.m_real_data); // call the timeout_func

			m_elements_ht.erase(ptr->first); // remove the node
			num += 1;

			ptr = ptr_next; // move next 

			if (m_elements_ht.size()%2 == 0)
			{
				if (ptr)
				{
					// middle node move right a step
					m_middle->second.m_pos = -1;
					m_middle = m_middle->second.m_next;
					m_middle_timeout_ms += m_middle->second.m_timeout_ms;
					m_middle->second.m_pos = 0;
				}
				else
					m_middle = NULL;
			}
		}
		else // not timeout
			break;
	}

	if (ptr) 
	{
		ptr->second.m_timeout_ms = sum - timeout_ms; // update next node's timeout_ms
		m_middle_timeout_ms -= timeout_ms;
	}
	
	return num;	
}

template<class _Key, class _Data, class _TimeoutFunc>
void TimeoutChecker<_Key, _Data, _TimeoutFunc>::clear()
{
	m_elements_ht.clear();
	m_head = NULL;	
	m_tail = NULL;
	m_middle = NULL;
}
