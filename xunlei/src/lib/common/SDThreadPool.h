#ifndef SANDAI_C_THREADPOOL_H
#define SANDAI_C_THREADPOOL_H

#include <pthread.h>
#include "SDLogger.h"

class SDThreadPool
{
	const static int STACK_SIZE = (256 * 1024);

public:
	SDThreadPool(int pool_size);
	virtual ~SDThreadPool();

	bool startThreads();
	void waitThreadsTermination();

	inline int get_pool_size()
	{
		return m_pool_size;
	}

	inline int get_active_thread_count()
	{
		pthread_mutex_lock(&m_count_lock);
		int active_count = m_active_count;
		pthread_mutex_unlock(&m_count_lock);
		return active_count;
	}

	inline int get_dead_thread_count()
	{
		pthread_mutex_lock(&m_count_lock);
		int dead_count = m_dead_count;
		pthread_mutex_unlock(&m_count_lock);
		return dead_count;
	}
protected:
	virtual void doIt() = 0;

	inline void setActive()
	{
		pthread_mutex_lock(&m_count_lock);
		m_active_count++;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setInactive()
	{
		pthread_mutex_lock(&m_count_lock);
		m_active_count--;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setthreaddie()
	{
		pthread_mutex_lock(&m_count_lock);
		++m_dead_count;
		pthread_mutex_unlock(&m_count_lock);
	}
private:
	DECL_LOGGER(logger);

	static void* threadProc(void* para);

	int m_pool_size;
	pthread_t *m_thread_ids;
	pthread_mutex_t m_count_lock;

	int m_active_count;
	int m_dead_count;
};

class SDTwinThreadPool
{
	const static int STACK_SIZE = (256 * 1024);

public:
	SDTwinThreadPool(unsigned poolsize1 = 1,unsigned poolsize2 = 1);
	virtual ~SDTwinThreadPool();

	bool startThreads();
	void waitThreadsTermination();

	inline int get_pool_size()
	{
		return m_pool_size1;
	}

	inline int get_pool_size2()
	{
		return m_pool_size2;
	}

	inline int get_active_thread_count()
	{		
		pthread_mutex_lock(&m_count_lock);
		int native =  m_active_count1;
		pthread_mutex_unlock(&m_count_lock);
		return native;
	}

	inline int get_active_thread_count2()
	{		
		pthread_mutex_lock(&m_count_lock);
		int native =  m_active_count2;
		pthread_mutex_unlock(&m_count_lock);
		return native;
	}

	inline int get_dead_thread_count1()
	{
		pthread_mutex_lock(&m_count_lock);
		int dead_count = m_dead_count1;
		pthread_mutex_unlock(&m_count_lock);
		return dead_count;
	}

	inline int get_dead_thread_count2()
	{
		pthread_mutex_lock(&m_count_lock);
		int dead_count = m_dead_count2;
		pthread_mutex_unlock(&m_count_lock);
		return dead_count;
	}	

	
protected:
	virtual void doIt() = 0;

	virtual void doIt2() = 0;

	inline void setActive()
	{
		pthread_mutex_lock(&m_count_lock);
		m_active_count1++;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setInactive()
	{
		pthread_mutex_lock(&m_count_lock);
		m_active_count1--;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setActive2()
	{
		pthread_mutex_lock(&m_count_lock);
		m_active_count2++;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setInactive2()
	{
		pthread_mutex_lock(&m_count_lock);
		m_active_count2--;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setthreaddie()
	{
		pthread_mutex_lock(&m_count_lock);
		++m_dead_count1;
		pthread_mutex_unlock(&m_count_lock);
	}

	inline void setthreaddie2()
	{
		pthread_mutex_lock(&m_count_lock);
		++m_dead_count2;
		pthread_mutex_unlock(&m_count_lock);
	}
private:
	DECL_LOGGER(logger);

	static void* threadProc(void* para);
	
	static void* threadProc2(void* para);

	unsigned		m_pool_size1;

	unsigned		m_pool_size2;
	
	pthread_t*	m_thread_ids1;
	pthread_t*	m_thread_ids2;
	
	pthread_mutex_t m_count_lock;

	int m_active_count1;
	int m_active_count2;

	int m_dead_count1;
	int m_dead_count2;
};

#endif
