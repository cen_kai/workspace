#ifndef _MEM_LEAK_H_
#define _MEM_LEAK_H_

#include "SDThread.h"
#include "SDMutexLock.h"
#include <map>
#include <list>
#include <stack>
#include <pthread.h>
#include <sys/wait.h>
#include <iostream>
#include <assert.h>
#include <sys/time.h>
#include <sys/types.h>
#include <math.h>  
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <pwd.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>


/*Define Boolean Value*/
#define TRUE		1		//布尔值“真”
#define FALSE		0		//布尔值“假”

/*Define RtnCode To User*/
#define	SUCCESS		0x00		//操作成功

typedef unsigned char		U8 ;
typedef unsigned short		U16 ;
typedef unsigned int		UINT ;
typedef bool			    BOOL ;
typedef unsigned long		DWORD;
typedef unsigned long		timeout_t;


using namespace std;

#define SINGLE_NEW		0x00		// indicate alloc memory with new type
#define ARRAY_NEW		0x01		// indicate alloc memory with new[] type
#define SINGLE_DELETE		0x02		// indicate free memory with delete type
#define ARRAY_DELETE		0x03		// indicate free memory with delete[] type
#define FILENAME_LENGTH		32		// the filename length
#define MEMORY_INFO		0X12345678	// indicate the message type on the message queue

typedef struct
{
	char		Filename[ FILENAME_LENGTH ];	// new所在的源程序文件名
	unsigned long	LineNum;	// new在源文件中的行号
	size_t 		AllocSize;	// 分配的内存大小
	int		OperationType;
	void *		pBuffer;	// 分配后得到的内存指针
	short		errCode;	// 0 - 没有释放, 1 - delete了new[]分配的内存

}MemOperation;

class MemChecker:public SDThread{
public:
    MemChecker(const char* stat_file_path=NULL, int interval_seconds=10);
    void AddOperation(MemOperation &op);
    ~MemChecker();
private:
    pthread_mutex_t _lock;
    void run();
    string Dump();
    string _file_path;
    int _interval;
    map<void *, MemOperation> nodes;
};

#define THIS_FILE          __FILE__
#if defined( MEM_DEBUG )

void* operator new( size_t nSize, char* pszFileName, int nLineNum );
void* operator new[]( size_t nSize, char* pszFileName, int nLineNum );

void operator delete( void *ptr );
void operator delete[]( void *ptr );

#define DEBUG_NEW new( THIS_FILE, __LINE__ )
#define DEBUG_DELETE 	delete

#endif	// end defined( MEM_DEBUG )


#endif

