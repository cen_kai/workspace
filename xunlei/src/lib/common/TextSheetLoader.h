//============================================================
// TextSheetLoader.h: interface of class TextSheetLoader
//		(Text-sheet represents rowset of text data)
//
// Author: JeffLuo
// Created: 2006-09-27
//============================================================

#ifndef __TEXT_SHEET_LOADER_H_20060927_12
#define  __TEXT_SHEET_LOADER_H_20060927_12

#include <string>
#include <vector>
#include "common/SDLogger.h"

using std::vector;
using std::string;

class TextSheetLoader
{
public:
	const static int MAX_FILE_PATH = 256;
	const static int MAX_DELIMITOR_LEN = 16;
	const static int MAX_LINE_WIDTH = 256;
	
	enum IGNORE_FLAGS
	{
		IGNORE_BLANK_LINE = 1,
		IGNORE_COMMENT_LINE = 2
	};
	
	TextSheetLoader(const char* sheet_file, const char* col_delimitor = "\t");

	bool load(int ignore_flag = IGNORE_BLANK_LINE | IGNORE_COMMENT_LINE);

	int get_row_count() const { return m_rows_data.size(); }
	bool get_row_columns(int row_index, vector<string>& columns);

private:
	bool append_line(char* line, int ignore_flag);

private:
	char m_sheet_file[MAX_FILE_PATH];
	char m_col_delimitor[MAX_DELIMITOR_LEN];
	vector<string> m_rows_data;

private:
	DECL_LOGGER(logger);
	
	
};

#endif // #ifndef __TEXT_SHEET_LOADER_H_20060927_12


