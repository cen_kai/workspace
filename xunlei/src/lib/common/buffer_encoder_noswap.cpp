#include <arpa/inet.h>
#include "buffer_encoder_noswap.h"
#include "Utility.h"
#include <common/SDException.h>

EncoderNoSwap::EncoderNoSwap(char* buffer, size_t size)
		: _buffer(buffer)
		, _size(size)
		, _pos(0)
{

}
EncoderNoSwap::~EncoderNoSwap()
{

}

void EncoderNoSwap::save(int n)
{
	*this<<n;
}
void EncoderNoSwap::save(int16_t n)
{
	*this<<n;
}
void EncoderNoSwap::save(int64_t n)
{	
	*this<<n;
}
void EncoderNoSwap::save(char n)
{
	*this<<n;
}
void EncoderNoSwap::save(uint8_t n)
{
	*this<<n;
}
void EncoderNoSwap::save(uint16_t n)
{
	*this<<n;
}
void EncoderNoSwap::save(uint32_t n)
{
	*this<<n;
}
void EncoderNoSwap::save(uint64_t n)
{
	*this<<n;
}
void EncoderNoSwap::save(const string& str)
{
	int n=str.length();
	*this<<n;
	*this<<str;
}

void EncoderNoSwap::save(string& str)
{
	int n=str.length();
	*this<<n;
	*this<<str;
}


void EncoderNoSwap::save(const char *str)
{
	int n=strlen(str);
	*this<<n;
	memcpy(_buffer+_pos, str, n);
	_pos+=n;
}

void EncoderNoSwap::save(double n)
{
	*this<<n;
}

void EncoderNoSwap::save(const CIDWrapper &cid)
{
	int n=MAX_CID_LEN;
	*this<<n;
	memcpy(_buffer+_pos, cid.cid, n);
	_pos+=n;
}

void EncoderNoSwap::save(CIDWrapper &cid)
{
	int n=MAX_CID_LEN;
	*this<<n;
	memcpy(_buffer+_pos, cid.cid, n);
	_pos+=n;
}

void EncoderNoSwap::save(const LightString &data)
{
	*this<<data.m_data_len;
	memcpy(_buffer+_pos, data.m_data_buf, data.m_data_len);
	_pos+=data.m_data_len;
}

void EncoderNoSwap::save(LightString &data)
{
	*this<<data.m_data_len;
	memcpy(_buffer+_pos, data.m_data_buf, data.m_data_len);
	_pos+=data.m_data_len;
}


DecoderNoSwap::DecoderNoSwap(char* buffer, size_t size)
		: _buffer(buffer)
		, _size(size)
		, _pos(0)
{
}
DecoderNoSwap::~DecoderNoSwap()
{

}
	
void DecoderNoSwap::load(int &n)
{
	if(_pos+sizeof(int) > _size)
	{
		throw SDException("DecoderNoSwap::load(int &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(int16_t &n)
{
	if(_pos+sizeof(int16_t) > _size)
	{
		throw SDException("DecoderNoSwap::load(int16_t &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(int64_t &n)
{
	if(_pos+sizeof(int64_t) > _size)
	{
		throw SDException("DecoderNoSwap::load(int64_t &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(char &n)
{
	if(_pos+sizeof(char) > _size)
	{
		throw SDException("DecoderNoSwap::load(char &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(uint8_t &n)
{
	if(_pos+sizeof(uint8_t) > _size)
	{
		throw SDException("DecoderNoSwap::load(uint8_t &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(uint16_t &n)
{
	if(_pos+sizeof(uint16_t) > _size)
	{
		throw SDException("DecoderNoSwap::load(uint16_t &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(uint32_t &n)
{
	if(_pos+sizeof(uint32_t) > _size)
	{
		throw SDException("DecoderNoSwap::load(uint32_t &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(uint64_t &n)
{
	if(_pos+sizeof(uint64_t) > _size)
	{
		throw SDException("DecoderNoSwap::load(uint64_t &n) failed!");
	}
	*this>>n;
}
void DecoderNoSwap::load(string& str)
{
	int len;
	if(_pos+sizeof(int) > _size)
	{
		throw SDException("DecoderNoSwap::load(string &n) failed!");
	}
	*this>>len;
	if(_pos+len > _size)
	{
		throw SDException("DecoderNoSwap::load(string &n) failed!");
	}
			
	str.resize(len);
	memcpy((char*)str.c_str(), _buffer+_pos, len);
	_pos+=len;
}

void DecoderNoSwap::load(char *str)
{
	int n;
	if(_pos+sizeof(int) > _size)
	{
		throw SDException("DecoderNoSwap::load(char *str) failed!");
	}
	*this>>n;
	if(_pos+n > _size)
	{
		throw SDException("DecoderNoSwap::load(char *str) failed!");
	}
	memcpy(str, _buffer+_pos, n);
	str[n] = '\0';
	_pos+=n;
}

void DecoderNoSwap::load(double &n)
{
	if(_pos+sizeof(double) > _size)
	{
		throw SDException("DecoderNoSwap::load(double &n) failed!");
	}
	*this>>n;
}

void DecoderNoSwap::load(CIDWrapper &cid)
{
	int n;
	if(_pos+sizeof(int) > _size)
	{
		throw SDException("DecoderNoSwap::load(CIDWrapper &n) failed!");
	}
	*this>>n;
	if(_pos+n > _size)
	{
		throw SDException("DecoderNoSwap::load(CIDWrapper &n) failed!");
	}
	memcpy(cid.cid, _buffer+_pos, n);
	_pos+=n;
}

void DecoderNoSwap::load(LightString &data)
{
	if(_pos+sizeof(int) > _size)
	{
		throw SDException("DecoderNoSwap::load(LightString &data) failed!");
	}
	*this>>data.m_data_len;
	if(_pos+data.m_data_len > _size)
	{
		throw SDException("DecoderNoSwap::load(LightString &data) failed!");
	}
	memcpy(data.m_data_buf, _buffer+_pos, data.m_data_len);
	_pos+=data.m_data_len;
}


