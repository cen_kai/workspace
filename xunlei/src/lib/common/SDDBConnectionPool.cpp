// SDDBConnectionPool.cpp: implementation of the SDDBConnectionPool class.

//

//////////////////////////////////////////////////////////////////////



#include "SDDBConnectionPool.h"

#include <errmsg.h>

#include <sys/time.h>

#include "server/timedelt.h"

//////////////////////////////////////////////////////////////////////

// Construction/Destruction

//////////////////////////////////////////////////////////////////////



using namespace std;

IMPL_LOGGER(SDMYSQL,logger);

SDMYSQL::SDMYSQL(unsigned port,const std::string& host,const std::string& user,const std::string& passwd,const std::string& dbname)

       : m_port(port),m_host(host),m_user(user),m_passwd(passwd),m_dbname(dbname),m_isuse(false),m_isinit(false)

{

   _init();
    LOG4CPLUS_DEBUG(logger,"SDMYSQL: passwd "<<m_passwd);

}


void SDMYSQL::_init()
{
    mysql_init(&m_sql);

   uint32_t timeout = 3; 

   mysql_options(&m_sql, MYSQL_OPT_CONNECT_TIMEOUT,(char *)&timeout);

    uint32_t  protype=MYSQL_PROTOCOL_TCP; //设置tcp连接方式

    mysql_options(&m_sql,MYSQL_OPT_PROTOCOL,(char *)&protype);
}

SDMYSQL::~SDMYSQL()

{

   mysql_close(&m_sql);

   m_isuse = false;

   m_isinit = false;

}



bool SDMYSQL::reconnect()

{

   mysql_close(&m_sql);

    _init();
    
   if(NULL == mysql_real_connect(&m_sql, m_host.c_str(), m_user.c_str(), m_passwd.c_str(),

       m_dbname.c_str(),m_port,NULL,CLIENT_MULTI_QUERIES))

   {

           return false;

   }

   return true;

}



string SDMYSQL::get_errorinfo()

{

   return mysql_error(&m_sql);

}



bool SDMYSQL::Init()

{    

    m_isinit = true;       
     LOG4CPLUS_DEBUG(logger,"SDMYSQL::Init before");
    if(NULL == mysql_real_connect(&m_sql, m_host.c_str(), m_user.c_str(), m_passwd.c_str(),m_dbname.c_str(),m_port,NULL,CLIENT_MULTI_QUERIES))     

    {
        m_isinit = false ;
        LOG4CPLUS_DEBUG(logger,"mysql_real_connect err ret: "<<get_errorinfo());
        return false;

    }
     LOG4CPLUS_DEBUG(logger,"SDMYSQL::Init after");
     
    return true;

}



bool SDMYSQL::Ensure_Connect()

{
    LOG4CPLUS_DEBUG(logger,"SDMYSQL::Ensure_Connect");

   if(!m_isinit)

   {
        
        LOG4CPLUS_DEBUG(logger,"before m_isinit: "<<m_isinit);
        bool flag=false;
        unsigned long  taketime=0;
        {
            Timedelt checktime(taketime);
            flag = Init();
        }
        LOG4CPLUS_DEBUG(logger,"after m_isinit: "<<m_isinit << "take time:"<<taketime);
   }



   for(unsigned i = 0; i < 1; i++)

   {

       int ret = mysql_ping(&m_sql);

        LOG4CPLUS_DEBUG(logger,"mysql_ping: "<<ret);

       if(ret == 0)

       {

           return true;

       }
        else
        {
            LOG4CPLUS_DEBUG(logger,"mysql ping err ret: "<<get_errorinfo());
        }

       reconnect();

   }

   return (mysql_ping(&m_sql) == 0);

}



int SDMYSQL::get_value_int(const MYSQL_ROW& row,unsigned long *lengths,unsigned fields_pos)

{

   if(lengths != NULL && lengths[fields_pos] > 0 && (char*)row[fields_pos] != NULL)

   {

       return atoi((char*)row[fields_pos]);

   }

   return 0;

}



std::string SDMYSQL::get_value_string(const MYSQL_ROW& row,unsigned long *lengths,unsigned fields_pos)

{

   string str;

   if(lengths != NULL && lengths[fields_pos] > 0 && (char*)row[fields_pos] != NULL)

   {

       return string((char*)row[fields_pos]);

   }

   return str;

}



int SDMYSQL::get_value_data(const MYSQL_ROW& row,unsigned long *lengths,unsigned fields_pos,char* data)

{

   if(lengths != NULL && lengths[fields_pos] > 0 && (char*)row[fields_pos] != NULL)

   {

       memcpy(data,(char*)row[fields_pos],lengths[fields_pos]);

       return lengths[fields_pos];

   }

   return 0;

}



bool SDMYSQL::query(const char* buffer,unsigned repeattimes)

{

   if(repeattimes < 1)

   {

       repeattimes = RETRYTIMES;

   }

   bool ret_val = false;

   for(unsigned i = 0; i < repeattimes; i++)

   {

       int val = mysql_real_query(&m_sql, buffer, strlen(buffer));

       if( val == 0)

       {

           ret_val = true;

           break;

       }
        else
            LOG4CPLUS_DEBUG(logger,"query: "<<get_errorinfo());

       switch(mysql_errno(&m_sql))

       {

           case CR_SERVER_GONE_ERROR:

           case CR_SERVER_LOST :

               {

                   reconnect();

                   break;

               }

           case CR_UNKNOWN_ERROR:

           default:

           case CR_COMMANDS_OUT_OF_SYNC:

               {

                   return false;

               }

               break;

       }

   }



   return ret_val;

}





unsigned SDMYSQL::get_num_return(char* buffer)

{

   if(!query(buffer))

   {

       return 0;

   }



   unsigned num = 0;



   MYSQL_RES* result = NULL;

   do

   {

       result = mysql_use_result(&m_sql);

       if(result == NULL)

       {

           break;

       }



       MYSQL_ROW row;



       unsigned    num_fields = mysql_num_fields(result);

       row = mysql_fetch_row(result);

       if(row == NULL ||num_fields < 1 || *row == NULL)

       {

           break;

       }



       num = atoi (*row);



       if(!mysql_eof(result))  // mysql_fetch_row() failed due to an error

       {

       //  num = 0;

       }

   }

   while(0);



   mysql_free_result(result);



   return num;

}



bool SDMYSQL::get_num_return(char* buffer,unsigned& num)

{

   if(!query(buffer))

   {

       return false;

   }



   num = 0;



   MYSQL_RES* result = NULL;

   do

   {

       result = mysql_use_result(&m_sql);

       if(result == NULL)

       {

           break;

       }



       MYSQL_ROW row;



       unsigned    num_fields = mysql_num_fields(result);

       row = mysql_fetch_row(result);

       if(row == NULL ||num_fields < 1 || *row == NULL)

       {

           break;

       }



       num = atoi (*row);



       if(!mysql_eof(result))  // mysql_fetch_row() failed due to an error

       {

       //  num = 0;

       }

   }

   while(0);



   mysql_free_result(result);



   return true;

}



bool SDMYSQL::get_num_return(char* buffer,int& num)

{

   if(!query(buffer))

   {

       return false;

   }



   num = 0;



   MYSQL_RES* result = NULL;

   do

   {

       result = mysql_use_result(&m_sql);

       if(result == NULL)

       {

           break;

       }



       MYSQL_ROW row;



       unsigned    num_fields = mysql_num_fields(result);

       row = mysql_fetch_row(result);

       if(row == NULL ||num_fields < 1 || *row == NULL)

       {

           break;

       }



       num = atoi (*row);



       if(!mysql_eof(result))  // mysql_fetch_row() failed due to an error

       {

       //  num = 0;

       }

   }

   while(0);



   mysql_free_result(result);



   return true;

}







////////

IMPL_LOGGER(SDDBConnectionPool, logger);

SDDBConnectionPool::SDDBConnectionPool(unsigned poolsize,unsigned port,const std::string& host,

       const std::string& user,const std::string& passwd,const std::string& dbname)

   :m_port(port),m_host(host),m_user(user),m_passwd(passwd),

   m_dbname(dbname)

{

   pthread_mutex_init(&m_sqlpool_lock,NULL);

   for(unsigned i = 0; i < poolsize ; i++)

   {

       SDMYSQL* mysql = new SDMYSQL(port,host,user,passwd,dbname);

       m_sqlpool_list.push_back(mysql);

   }



   }

SDDBConnectionPool::SDDBConnectionPool(const SDDBConnectionPool& dbc_pool)

{

   m_port = dbc_pool.m_port;

   m_host = dbc_pool.m_host;

   m_user = dbc_pool.m_user;

   m_passwd = dbc_pool.m_passwd;

   m_dbname = dbc_pool.m_dbname;

   pthread_mutex_init(&m_sqlpool_lock,NULL);

   for(unsigned i=0,size=dbc_pool.m_sqlpool_list.size(); i<size; i++)

   {

       SDMYSQL* mysql = new SDMYSQL(dbc_pool.getport(),dbc_pool.gethost(),dbc_pool.getuser(),dbc_pool.getpwd(),dbc_pool.getdbname());

       m_sqlpool_list.push_back(mysql);

   }

}

SDDBConnectionPool::~SDDBConnectionPool()

{

   int nsize = m_sqlpool_list.size();

   for(int i = 0 ; i < nsize; i++)

   {

       if(m_sqlpool_list[i] != NULL)

       {

           delete m_sqlpool_list[i];

           m_sqlpool_list[i] = NULL;

       }

   }

   m_sqlpool_list.clear();



   pthread_mutex_destroy(&m_sqlpool_lock);

}



void SDDBConnectionPool::set_size(uint32_t size)

{

   for(unsigned i = 0; i < size ; i++)

   {

       m_sqlpool_list.push_back(new SDMYSQL(m_port,m_host,m_user,m_passwd,m_dbname));

   }

}



bool SDDBConnectionPool::Init()

{

   bool can_connect = false;

   for(unsigned i = 0; i < m_sqlpool_list.size(); i++)

   {

       if(m_sqlpool_list[i] != NULL && m_sqlpool_list[i]->Init() )

       {

           can_connect = true;

       }

   }

   return can_connect;

}



SDMYSQL* SDDBConnectionPool::get_sql(bool autocreate)

{

   SDMYSQL* my_sql = NULL;

   pthread_mutex_lock(&m_sqlpool_lock);

   bool is_connect_fail = false;

   try

   {

       for(unsigned i = 0; i < m_sqlpool_list.size(); i++)

       {

           if(m_sqlpool_list[i] != NULL && !m_sqlpool_list[i]->m_isuse )

           {

               if (m_sqlpool_list[i]->Ensure_Connect() )

               {

                   m_sqlpool_list[i]->m_isuse = true;

                   my_sql =  m_sqlpool_list[i];

                   break;

               }

               else

               {

                   LOG4CPLUS_THIS_WARN(logger, "m_sqlpool_list["<<i<<"]" << m_sqlpool_list[i] << " failed to Ensure_Connect");

                   is_connect_fail = true;

               }

           }

       }

   }

   catch( ... )

   {

       my_sql = NULL;

   }



   // if not found, then re-establish a new connection

   //by tianming ,if connected fail before,need not connect again here

   if(my_sql == NULL && autocreate && !is_connect_fail)

   {

       my_sql = new SDMYSQL(m_port,m_host,m_user,m_passwd,m_dbname);

       //by tianming 20090924,should judge the return value here

       bool flag = my_sql->Init();

       if (flag)

       {

           my_sql->m_isuse = true;

           m_sqlpool_list.push_back(my_sql);

       }

       else

       {

           delete my_sql;

           my_sql = NULL;

       }

   }



   pthread_mutex_unlock(&m_sqlpool_lock);



   return my_sql;

}



void SDDBConnectionPool::release_sql(SDMYSQL* sql)

{

   pthread_mutex_lock(&m_sqlpool_lock);

   try

   {

       for(unsigned i = 0; i < m_sqlpool_list.size(); i++)

       {

           if(m_sqlpool_list[i] == sql)

           {

               m_sqlpool_list[i]->m_isuse = false;

               break;

           }

       }

   }

   catch( ... )

   {

   }



   pthread_mutex_unlock(&m_sqlpool_lock);

}

























SDRES_RESULT::SDRES_RESULT(MYSQL* mysql) : m_result(NULL) , m_row(NULL),m_fields_pos(0),m_max_num_fields(0),m_lengths(NULL)

{

   m_result = mysql_use_result(mysql);

   if(m_result != NULL)

       m_max_num_fields = mysql_num_fields(m_result);

}



SDRES_RESULT::SDRES_RESULT(SDMYSQL* sdmysql) : m_result(NULL) , m_row(NULL),m_fields_pos(0),m_max_num_fields(0),m_lengths(NULL)

{

   m_result = mysql_use_result(sdmysql->get_mysql());

   if(m_result != NULL)

       m_max_num_fields = mysql_num_fields(m_result);

}



SDRES_RESULT::SDRES_RESULT(SDMYSQL* sdmysql,const char* buffer) : m_result(NULL) , m_row(NULL),m_fields_pos(0),m_max_num_fields(0),m_lengths(NULL)

{

   if(sdmysql->query(buffer))

   {

       m_result = mysql_use_result(sdmysql->get_mysql());

       if(m_result != NULL)

           m_max_num_fields = mysql_num_fields(m_result);

   }

}



SDRES_RESULT::~SDRES_RESULT()

{

   release();

}



void SDRES_RESULT::release()

{

   if(m_result != NULL)

   {

       mysql_free_result(m_result);

       m_result = NULL;

   }

}



bool SDRES_RESULT::next()

{

   m_fields_pos = 0;

   if(m_result == NULL) return false;



   m_row = mysql_fetch_row(m_result);

   m_lengths = mysql_fetch_lengths(m_result);

   if(m_row == NULL || m_lengths == NULL)

   {

       return false;

   }

   return true;

}



bool SDRES_RESULT::is_Null()

{

   if(m_lengths != NULL && m_lengths[m_fields_pos] > 0 && (char*)m_row[m_fields_pos] != NULL)

   {

       return false;

   }



   return true;

}



int SDRES_RESULT::get_value_int()

{

   if(m_lengths != NULL && m_lengths[m_fields_pos] > 0 && (char*)m_row[m_fields_pos] != NULL)

   {

       return atoi((char*)m_row[m_fields_pos++]);

   }



   m_fields_pos ++;

   return 0;

}

int64_t SDRES_RESULT::get_value_long()

{

   if(m_lengths != NULL && m_lengths[m_fields_pos] > 0 && (char*)m_row[m_fields_pos] != NULL)

   {

       return atol((char*)m_row[m_fields_pos++]);

   }



   m_fields_pos ++;

   return 0;

}

string SDRES_RESULT::get_value_string()

{

   string str;

   if(m_lengths != NULL && m_lengths[m_fields_pos] > 0 && (char*)m_row[m_fields_pos] != NULL)

   {

       m_fields_pos ++;

       return string((char*)m_row[m_fields_pos-1],  m_lengths[m_fields_pos-1]);

   }

   m_fields_pos ++;

   return str;

}



int SDRES_RESULT::get_value_data(char* data,unsigned &date_len)

{

   if(m_lengths != NULL && m_lengths[m_fields_pos] > 0 && (char*)m_row[m_fields_pos] != NULL)

   {

       date_len = (date_len > m_lengths[m_fields_pos]) ? m_lengths[m_fields_pos] : date_len;

       memcpy(data,(char*)m_row[m_fields_pos],date_len);

       m_fields_pos ++;

       return date_len;

   }

   m_fields_pos++;

   return 0;

}



DBSTMT::DBSTMT(const char* query,SDMYSQL* mysql)

   :m_stmt(NULL)

{

   m_stmt = mysql_stmt_init(&(mysql->m_sql));

   if(NULL == m_stmt)

   {

       throw   SDException("can't init stmt data string=" + std::string(query));

   }



   if( mysql_stmt_prepare(m_stmt,query,strlen(query)) )

   {

       throw   SDException("can't mysql_stmt_prepare data string=" + std::string(query) + string(mysql_stmt_error(m_stmt)) );

   }

}



void DBSTMT::execute(MYSQL_BIND* bind)

{

   if(NULL == bind)

   {

       throw   SDException("bind is null in DBSTMT::execute");

   }

   

   if(mysql_stmt_execute(m_stmt))

   {

       throw SDException("bind is null in DBSTMT::execute error=" + string(mysql_stmt_error(m_stmt)) );

   //  throw        mysql_stmt_error(stmt_);

   }



   if(mysql_stmt_bind_result(m_stmt,bind))

   {

       throw SDException("mysql_stmt_bind_result error in DBSTMT::execute error=" + string(mysql_stmt_error(m_stmt)) );

   }



   if(mysql_stmt_store_result(m_stmt))

   {

       throw SDException("mysql_stmt_store_result error in DBSTMT::execute error=" + string(mysql_stmt_error(m_stmt)) );

   }

}



int DBSTMT:: EscapeString (string &str)

{

   if (str.size()==0) return 0;



   char *buff= new char[str.size()*2+1];

   mysql_escape_string (buff, str.c_str(), str.size());

   str= buff;

   delete[] buff;

   return 0;

}



