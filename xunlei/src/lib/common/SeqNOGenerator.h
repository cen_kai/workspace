#ifndef SEQNO_GENERATOR__H
#define SEQNO_GENERATOR__H

#include <pthread.h>


typedef uint32_t seq_t;

class SeqNOGenerator{
public:
    virtual ~SeqNOGenerator();
    static SeqNOGenerator * Instance();
    uint32_t GetSeqNO();

private:
    SeqNOGenerator();
    uint32_t _seq;
    pthread_mutex_t _lock; 
    static SeqNOGenerator *_instance;
};


#endif
