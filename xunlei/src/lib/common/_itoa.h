#ifndef _ITOA_H
#define _ITOA_H
#include <string>


extern char *_itoa (uint64_t value, char *buflim);

/* Similar to the _itoa functions, but output starts at buf and pointer
   after the last written character is returned.  */
#endif	/* itoa.h */
