#include <map>
#include <string>
#include <sstream>
#include "common/common.h"
#include "common/parse_url_parameters.h"

#include "json/reader.h"
#include "json/value.h"

using namespace std;
#define _NAME 0
#define _VALUE 1

/* x2c() and unescape_url() stolen from NCSA code */
char x2c(const char *what)
{
  register char digit;

  digit = (what[0] >= 'A' ? ((what[0] & 0xdf) - 'A')+10 : (what[0] - '0'));
  digit *= 16;
  digit += (what[1] >= 'A' ? ((what[1] & 0xdf) - 'A')+10 : (what[1] - '0'));
  return(digit);
}

void unescape_url(string &url)
{
  register int x,y;
  char *p_buf = new char[url.size() + 1];
  const char *p_ulr = url.c_str();

  for (x=0,y=0; y < (int)url.size(); ++x,++y) {
    if((p_buf[x] = p_ulr[y]) == '%') {
      p_buf[x] = x2c(&p_ulr[y+1]);
      y+=2;
    }
  }
  p_buf[x] = '\0';

  url = string(p_buf);
  delete []p_buf;
}



struct 	_application_www_form_urlencoded{};
struct 	_multipart_form_data{};
struct 	_application_json{};
struct 	_text_xml{};

int read_cgi_input(map<string, string> &key_value, const char *input, _application_www_form_urlencoded type);
int read_cgi_input(map < string,string > & key_value,const char * input,_application_json type);



int read_cgi_input(map<string, string> &key_value, const char *input,cgi_param_type type)
{
	switch(type)
	{
		case APPLICATION_WWW_FORM_URLENCODED:
			return read_cgi_input(key_value,input,_application_www_form_urlencoded());
		case APPLICATION_JSON:
			return read_cgi_input(key_value,input,_application_json());
		default:
			break;
	}
	return 0;
}


int read_cgi_input(map<string, string> &key_value, const char *input, _application_www_form_urlencoded type)
{
  int i, j, num, token;
  const char *pos_line_end = strchr(input, '\r');
  if(pos_line_end == NULL)
  {
     pos_line_end = strchr(input, '\n');
	 if(pos_line_end == NULL)
	 {
        pos_line_end = input + strlen(input);
	 }
  }
  const char *pos_space = strchr(input, ' ');
  if(!pos_space)
  {
	pos_space = input + strlen(input);
  }
  if(pos_space < pos_line_end)
  {
	pos_line_end = pos_space;
  }
  
  int len = pos_line_end - input;
  char *lexeme;

  if ((lexeme = (char *)malloc(sizeof(char) * len + 1)) == NULL)
    exit(1);

  string value;
  string name;
  i = 0;
  num = 0;
  token = _NAME;
  while (i < len) {
    j = 0;
    while ( (input[i] != '=') && (input[i] != '&') && (i < len) ) {
      lexeme[j] = (input[i] == '+') ? ' ' : input[i];
      i++;
      j++;
    }
    lexeme[j] = '\0';
    if (token == _NAME) {
      name = string(lexeme);
      unescape_url(name);
      if ( (input[i] != '=') || (i == len - 1) ) {
	value = string("");
	key_value[name] = value;
	if (i == len - 1) /* null value at end of expression */
	  num++;
	else { /* error in expression */
	  free(lexeme);
	  return -1;
	}
      }
      else
	token = _VALUE;
    }
    else {
      value = string(lexeme);
	  //LogFileMsg("cgi get name %s value %s", entry.name, entry.value);
      unescape_url(value);
	  key_value[name] = value;
      token = _NAME;
      num++;
    }
    i++;
    j = 0;
  }
  free(lexeme);
  return num;
}

//这里的解析json暂时不能使用数组
int read_cgi_input(map < string,string > & key_value,const char * input,_application_json type)
{
	Json::Reader reader;
	Json::Value root ;
	if( !reader.parse(input,root,false) ){
		return 0;
	}
	Json::Value::Members keys =  root.getMemberNames();
	Json::Value::Members::iterator iter = keys.begin();
	for(; iter!=keys.end(); ++iter)
	{
		Json::Value tmp = root[*iter];
		if(tmp.isString()){
			key_value.insert(make_pair(*iter,tmp.asString()));
		}
		else if(tmp.isInt()){
			stringstream ss ;
			string num ;
			ss<<tmp.asInt();
			ss>>num;
			key_value.insert(make_pair(*iter,num ) );
		}
	}
	return 0;
}


