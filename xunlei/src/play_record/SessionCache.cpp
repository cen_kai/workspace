#include "common/Utility.h"
#include "common/SDMutexLock.h"

#include <list>
#include <sstream>
using namespace std;

#include "common/mem_leak.h"
#include "SessionCache.h"
#include "client/Command.h"
#include "server/timedelt.h"
#include <fstream>

SessionCache * SessionCache::_inst = NULL;
IMPL_LOGGER(SessionCache, logger);

map<_u64,string> SessionCacheImp::m_appname;

SessionCache * SessionCache::Instance()
{
	if(!_inst)
		_inst = new SessionCache;
	return _inst;
}

bool SessionCache::get_checked_session(const SessionCheck& ssck,SessionCacheImp& ssch_imp)
{
		
	if(!m_sessions->copy_data(ssck,ssch_imp))
	{	
		return false;
	}
	else
	{
		return true;
	}
}

bool SessionCache::check_session(const SessionCacheImp& ssch_imp)
{
	SessionCheck ssck(ssch_imp.m_userid,ssch_imp.m_sessionid);
	SessionCacheImp* imp_ptr = m_sessions->get_data(ssck);	
	if(imp_ptr == NULL)
	{	
		LOG4CPLUS_ERROR(logger,"ERROR Session NO Cache, uid:"<<ssch_imp.m_userid<<" sessionid:"<<ssch_imp.m_sessionid);
		
		return false;
	}
	else
	{
		

		if(imp_ptr->m_state == SessionCacheImp::PASS)
		{
			m_sessions->return_data(ssck);	
			return true;
		}

		static bool need_verify = (g_app.get_conf_int("flag.need_verify", 0) == 1);
		if(!need_verify)
		{
			LOG4CPLUS_WARN(logger,"No Need Verify User");
		
			imp_ptr->m_state = SessionCacheImp::PASS;
			m_sessions->return_data(ssck);	
			return true;
		}

		
		int ret = 0;
		unsigned long  take_time ;
		if(ssch_imp.m_client_type == SessionCacheImp::WEB)
		{
			static int web_port = g_app.get_conf_int("vip.web.port", 5687);
			static string web_vip_domain = g_app.get_conf_string("vip.web.domain", "websessionbin.reg");
			string  web_ip = Utility::get_first_ip_from_domainname(web_vip_domain.c_str());	
			LOG4CPLUS_DEBUG(logger, "web_ip:"<<web_ip);
			{
				Timedelt  check(take_time);
				ret = Validate_web_session(web_ip,web_port,ssch_imp.m_sessionid);
			}
				
		}
		else if(ssch_imp.m_client_type == SessionCacheImp::CLIENT)
		{
			static int client_port = g_app.get_conf_int("vip.client.port", 5687);
			static string client_vip_domain = g_app.get_conf_string("vip.client.domain", "sessiongateway.reg");
			string  client_ip = Utility::get_first_ip_from_domainname(client_vip_domain.c_str());
			LOG4CPLUS_DEBUG(logger, "client_ip:"<<client_ip);
			{
				Timedelt  check(take_time);
				ret = Validate_cli_session(client_ip,client_port,
				ssch_imp.m_sessionid,ssch_imp.m_userid,ssch_imp.m_businessid);
			}
		}
		else
		{
			LOG4CPLUS_ERROR(logger,"UNKnown Client Type");
			m_sessions->return_data(ssck);
			return false;
		}

		int check_time_flag = g_app.get_conf_int("vipverify",0);
		if(check_time_flag){
			stringstream log_name;
			time_t now = time(NULL);
			log_name << "../log/log_vip_verify_"<< Utility::get_date_string() <<".txt";
			std::fstream log_file(log_name.str().c_str(),std::ios::out|std::ios::app);
			if(log_file.is_open()){
				log_file<<std::endl<<"now:"<<Utility::get_time_string(now)<<" userid:"<<ssch_imp.m_userid<<" sessionid:"<<ssch_imp.m_sessionid<<"  businessid:"<<ssch_imp.m_businessid<<" time:"<<take_time <<" ret:"<<ret;
			}
		}
	    
		if(ret == 0)
		{
			imp_ptr->m_state = SessionCacheImp::PASS;
		}
		else if(ret == -1 ||  ret ==-2 || ret ==-4 )
		{
			LOG4CPLUS_ERROR(logger,"SessionCache check_session ILLEGLE USER");
			imp_ptr->m_state = SessionCacheImp::ILLEGLE;
		}
		else if(ret == ERROR_CODE)
		{
			LOG4CPLUS_ERROR(logger,"VIPSERVER Connection ERROR");
			imp_ptr->m_state = SessionCacheImp::PASS;
		}
		else
		{
			LOG4CPLUS_ERROR(logger,"Check Unknown ERROR");
			imp_ptr->m_state = SessionCacheImp::PASS;
		}
		imp_ptr->m_timestamp = time(NULL);
		bool result = (imp_ptr->m_state ==  SessionCacheImp::PASS);
		m_sessions->return_data(ssck);	
		
		return result;
	}
	

	
}



bool SessionCache::add_cache(const SessionCheck& ssck,const SessionCacheImp& ssch_imp)
{
		
	m_sessions->add(ssck,ssch_imp);
	return true;
}

void SessionCache::run()
{
    while(true)
	{
		vector<SessionCheck> need_delete;
        
        for(int bucket_id=0; bucket_id<(int)m_sessions->m_bucket_num; bucket_id++)
		{
            m_sessions->m_bucket_mutex_list[bucket_id].lock();
			
           	ThreadSafeHashMap<SessionCheck, SessionCacheImp>::DataMapIter iter = m_sessions->m_bucket_list[bucket_id].begin();
            ThreadSafeHashMap<SessionCheck, SessionCacheImp>::DataMapIter iter_end = m_sessions->m_bucket_list[bucket_id].end();

			for(; iter!=iter_end; ++iter)
			{
                SessionCacheImp & ssch_imp = iter->second.m_data;
                
                if(ssch_imp.timeout())
                {
                	need_delete.push_back(iter->first);
                }
                    
            } 
        
			m_sessions->m_bucket_mutex_list[bucket_id].unlock();
			
        }

		for(size_t i=0; i < need_delete.size(); i++)
		{
			m_sessions->del(need_delete[i]);
		}

		LOG4CPLUS_INFO(logger,"delete "<<need_delete.size()<<" timeout session");
		
		static const int32_t SESSION_FLUSH_SEC = g_app.get_conf_int("session.flush.min", 5)*60;
        sleep(SESSION_FLUSH_SEC);
		
    }
}



