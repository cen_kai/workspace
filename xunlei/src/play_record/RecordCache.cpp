#include "RecordCache.h"
#include "server/ServerApp.h"
#include "common/AutoSqlExecuter.hpp"
#include "server/timedelt.h"
#include "StoreCache.h"
#include <string.h>
#include <fstream>


extern ServerApp g_app;

IMPL_LOGGER(RecordCache, logger);
IMPL_LOGGER(PlayRecord, logger);

RecordCache * RecordCache::_instance = NULL;

SDMutexLock RecordCache::_instance_lock ;

RecordCache::RecordCache()
{
    _movie_info = new ThreadSafeHashMap<MovieUniqueKey, VodMovieInfo>(10000, false, true/*use LRU*/, 1000000);
	_shortmovie_info = new ThreadSafeHashMap<ShortMovieKey, ShortMovieInfo>(10000, false, true/*use LRU*/, 500000);
	_music_info = new ThreadSafeHashMap<MusicKey, MusicInfo>(10000, false, true/*use LRU*/, 500000);
    _play_info = new ThreadSafeHashMap<uint64_t, UserRecordSetType >(10000, false, true/*use LRU*/, 360000);
	
    _movie_info_db_pool.m_host = g_app.get_conf_string("movie.info.db.host", "localhost");
    _movie_info_db_pool.m_port = g_app.get_conf_int("movie.info.db.port", 3306);
    _movie_info_db_pool.m_dbname = g_app.get_conf_string("movie.info.db.dbname", "movie");
    _movie_info_db_pool.m_user = g_app.get_conf_string("movie.info.db.user", "root");
    _movie_info_db_pool.m_passwd = g_app.get_conf_string("movie.info.db.passwd", "");
	const int _movie_info_connects = g_app.get_conf_int("movie.info.db.connect.num",2);
    _movie_info_db_pool.set_size(_movie_info_connects);

    _play_info_db_pool.m_host = g_app.get_conf_string("play.info.db.host", "localhost");
    _play_info_db_pool.m_port = g_app.get_conf_int("play.info.db.port", 3306);
    _play_info_db_pool.m_dbname = g_app.get_conf_string("play.info.db.dbname", "play_record");
    _play_info_db_pool.m_user = g_app.get_conf_string("play.info.db.user","root");
    _play_info_db_pool.m_passwd = g_app.get_conf_string("play.info.db.passwd", "");
	const int _play_info_connects = g_app.get_conf_int("play.info.db.connect.num",2);
    _play_info_db_pool.set_size(_play_info_connects);
}

RecordCache * RecordCache::GetInstance()
{
    if(_instance == NULL){
		_instance_lock.lock();
		if(_instance == NULL){
	        _instance = new RecordCache;
		}
		_instance_lock.unlock();
    }
    return _instance;
}

void RecordCache::run()
{
    while(true){
        static const int32_t RECORD_FLUSH_SEC = g_app.get_conf_int("record.flush.sec", 30);
		static const int32_t FLUSH_USER_FLAG = g_app.get_conf_int("user.flush.flag",0);
		static const int32_t STAT_TIME_INTERVAL = g_app.get_conf_int("user.stat.time",60*60*2);
		static time_t  program_start_time = time(NULL);
		static const int32_t  record_stat_num = 100 ;
		uint64_t  total_record=0;
		int32_t   record_stat_size=0;
        time_t  now = time(NULL);
		int  record_stat_iter=0;
		int  record_num=0;
        for(int bucket_id=0; bucket_id<(int)_play_info->m_bucket_num; bucket_id++){
            _play_info->m_bucket_mutex_list[bucket_id].lock();
			record_num += record_stat_iter;
			record_stat_iter=0;
            ThreadSafeHashMap<uint64_t, UserRecordSetType >::DataMapIter iter = _play_info->m_bucket_list[bucket_id].begin();
            for(; iter!=_play_info->m_bucket_list[bucket_id].end(); ){
                UserRecordSetType & records = iter->second.m_data;
				int check_flag=FLUSH_USER_FLAG;
				if( program_start_time+STAT_TIME_INTERVAL<now){
		                    total_record += records.size();
				}
                for(UserRecordSetType::iterator it=records.begin(); it!=records.end(); ++it){
                    boost::shared_ptr<PlayRecord> record = it->second;
					if( check_flag && !record->IsOld()){
                          check_flag = 0 ;
					}
					if(record_stat_iter<record_stat_num){
                         record_stat_size += record->size();
						 record_stat_iter++;
					}
                    if(!record->NeedUpdate()){
						FillMovieInfo(record.get());
                        continue;
                    }
                    SaveToDB(record);
                    record->ClearUpdateFlag();
                }
				uint64_t userid=iter->first;
				iter++;
				if(check_flag){
						int flag = g_app.get_conf_int("cloud.debug.recordcache.run", 0);
						if(flag){
							time_t now = time(NULL);
							stringstream  filename;
							filename<<"../log/log_recordcache_"<<Utility::get_date_string()<<".txt";
							std::fstream log_file(filename.str().c_str(),std::ios::out|std::ios::app);
							log_file<<endl<<"now:"<<Utility::get_time_string(now)<<" useid="<<userid;
						}
					    _play_info->del2(userid);
				}
            }
			_play_info->m_bucket_mutex_list[bucket_id].unlock();
        }
		record_num += record_stat_iter;
		if( program_start_time+STAT_TIME_INTERVAL<now){
			 program_start_time = now;
             stringstream filename;
             filename<<"../log/log_record_"<<Utility::get_date_string()<<".txt";
             std::fstream log_file(filename.str().c_str(),std::ios::out|std::ios::app);
             log_file<<endl<<"now:"<<Utility::get_time_string(now);
			 int32_t record_size=0;
			 if(record_num){
				 record_size=record_stat_size/record_num ;
			 }
			 log_file<<" record_stat_size"<<record_stat_size << "  record_num" <<record_num<<std::endl;
             log_file<<" play_info total user num: "<<_play_info->size_plus()<<" user records:"<<total_record <<" size per record "<< record_size <<std::endl;
             log_file<<" movie_info total size: "<<_movie_info->size_plus()<<std::endl;
             log_file<<" shortmovie_info total size: "<<_shortmovie_info->size_plus()<<std::endl;
             log_file<<" music_info total size: "<<_shortmovie_info->size_plus()<<std::endl;
        }
        sleep(RECORD_FLUSH_SEC / 2);
    }
}


//这里的SaveToDB是顺序执行的所以不会对buf有竞争关系
static char sql_buf[4096];

void RecordCache::SaveToDB(boost::shared_ptr<PlayRecord> & record)
{
    if(record->view_type == VIEW_TYPE_LONG){ 
        boost::shared_ptr<VodPlayRecord> vod_ptr = boost::dynamic_pointer_cast<VodPlayRecord>(record);	
		snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO vod_record(userid, movieid, subid,viewtype, charge,notified,version,play_time, viewed_time,"
			"device_type,device_version,product_name,product_version) "
			"VALUES(%"PRIu64",%"PRIu64",%"PRIu64",%d,%lu,%d,\"%s\",FROM_UNIXTIME(%"PRIu64"),%u,\"%s\",\"%s\",\"%s\",\"%s\" ) ON DUPLICATE KEY UPDATE "
			"subid=%"PRIu64",viewtype=%d,charge=%"PRIu64",flag=1,play_time=FROM_UNIXTIME(%"PRIu64"),viewed_time=%u,device_type=\"%s\",device_version=\"%s\",product_name=\"%s\", product_version=\"%s\",notified=\"%d\", version=\"%s\" "
			,vod_ptr->userid,vod_ptr->movie.movieid,vod_ptr->movie.submovieid,vod_ptr->view_type,vod_ptr->movie.charge,vod_ptr->movie.updateinfo,vod_ptr->movie.version.c_str(),vod_ptr->play_time,vod_ptr->viewed_time   \
			,vod_ptr->device_type.c_str(),vod_ptr->device_version.c_str(),vod_ptr->product_name.c_str(),vod_ptr->product_version.c_str() \
			,vod_ptr->movie.submovieid,vod_ptr->view_type,vod_ptr->movie.charge,vod_ptr->play_time,vod_ptr->viewed_time   \
		    ,vod_ptr->device_type.c_str(),vod_ptr->device_version.c_str(),vod_ptr->product_name.c_str(),vod_ptr->product_version.c_str(),vod_ptr->movie.updateinfo,vod_ptr->movie.version.c_str()
		);
		int flag = g_app.get_conf_int("cloud.debug.savetodb", 0);
		if(flag){
			time_t now = time(NULL);
			stringstream  filename;
			filename<<"../log/log_sql_"<<Utility::get_date_string()<<".txt";
			std::fstream log_file(filename.str().c_str(),std::ios::out|std::ios::app);
			log_file<<endl<<"now:"<<Utility::get_time_string(now)<<" sql="<<sql_buf;
		}
    }
	else if(record->view_type==VIEW_TYPE_SHORT){
		boost::shared_ptr<ShortPlayRecord> vod_ptr = boost::dynamic_pointer_cast<ShortPlayRecord>(record);
		snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO vod_record(userid, movieid, subid,viewtype, charge,play_time, viewed_time,"
			"device_type,device_version,product_name,product_version) "   \
			"VALUES(%"PRIu64",%"PRIu64",%d,%d,%d,FROM_UNIXTIME(%"PRIu64"),%u,\"%s\",\"%s\",\"%s\",\"%s\" )"  \
			"ON DUPLICATE KEY UPDATE "   \
			"viewtype=%d,charge=%d,flag=1,play_time=FROM_UNIXTIME(%"PRIu64"),viewed_time=%d,device_type=\"%s\",device_version=\"%s\",product_name=\"%s\", product_version=\"%s\""   \
			,vod_ptr->userid,vod_ptr->shortmovie.video_id,0,vod_ptr->view_type,vod_ptr->shortmovie.charge,vod_ptr->play_time,vod_ptr->viewed_time   \
			,vod_ptr->device_type.c_str(),vod_ptr->device_version.c_str(),vod_ptr->product_name.c_str(),vod_ptr->product_version.c_str() \
			,vod_ptr->view_type,vod_ptr->shortmovie.charge,vod_ptr->play_time,vod_ptr->viewed_time   \
			,vod_ptr->device_type.c_str(),vod_ptr->device_version.c_str(),vod_ptr->product_name.c_str(),vod_ptr->product_version.c_str()
		);
	}
	else if(record->view_type==VIEW_TYPE_MUSIC){
		boost::shared_ptr<MusicPlayRecord> vod_ptr = boost::dynamic_pointer_cast<MusicPlayRecord>(record);
		snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO vod_record(userid, movieid, subid,viewtype, charge,play_time, viewed_time," \
			"device_type,device_version,product_name,product_version) "  \
			"VALUES(%"PRIu64",%"PRIu64",%d,%d,%d,FROM_UNIXTIME(%"PRIu64"),%u,\"%s\",\"%s\",\"%s\",\"%s\" )" \
			"ON DUPLICATE KEY UPDATE "   \
			"viewtype=%d,charge=%d,flag=1,play_time=FROM_UNIXTIME(%"PRIu64"),viewed_time=%d,device_type=\"%s\",device_version=\"%s\",product_name=\"%s\", product_version=\"%s\""	\
			,vod_ptr->userid,vod_ptr->music.albumid,0,vod_ptr->view_type,vod_ptr->music.charge,vod_ptr->play_time,vod_ptr->viewed_time   \
			,vod_ptr->device_type.c_str(),vod_ptr->device_version.c_str(),vod_ptr->product_name.c_str(),vod_ptr->product_version.c_str() \
			,vod_ptr->view_type,vod_ptr->music.charge,vod_ptr->play_time,vod_ptr->viewed_time   \
			,vod_ptr->device_type.c_str(),vod_ptr->device_version.c_str(),vod_ptr->product_name.c_str(),vod_ptr->product_version.c_str() 
		);
	}
	else if(record->view_type == VIEW_TYPE_LOCAL){
		boost::shared_ptr<LocalPlayRecord> local_ptr = boost::dynamic_pointer_cast<LocalPlayRecord>(record);
		snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO local_record(userid, gcid, cid, filesize, title, movie_length,play_time, "     \
				" viewed_time, device_type, device_version, product_name, product_version) "     \
				"VALUES(%"PRIu64",\"%s\",\"%s\",%"PRIu64",\"%s\",%d,FROM_UNIXTIME(%"PRIu64")"    \
				",%u,\"%s\",\"%s\",\"%s\",\"%s\") "    \
				"ON DUPLICATE KEY UPDATE play_time=FROM_UNIXTIME(%"PRIu64"),viewed_time=%u,flag=1,device_type=\"%s\",device_version=\"%s\",product_name=\"%s\",product_version=\"%s\" "   \
				,local_ptr->userid,local_ptr->gcid.c_str(), local_ptr->cid.c_str(),local_ptr->filesize,local_ptr->title.c_str(),local_ptr->movie_length,local_ptr->play_time  \
				,local_ptr->viewed_time,local_ptr->device_type.c_str(),local_ptr->device_version.c_str(),local_ptr->product_name.c_str(),local_ptr->product_version.c_str()  \
				,local_ptr->play_time ,local_ptr->viewed_time,local_ptr->device_type.c_str(),local_ptr->device_version.c_str(),local_ptr->product_name.c_str(),local_ptr->product_version.c_str()
		);
		LOG4CPLUS_DEBUG(logger, "prepare sql=" << sql_buf);
	}
	else if(record->view_type == VIEW_TYPE_CLOUD){
		boost::shared_ptr<CloudPlayRecord> cloud_ptr = boost::dynamic_pointer_cast<CloudPlayRecord>(record);
		snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO cloud_record(userid, gcid, url_hash, cid, url, file_name, task_type, src_url, filesize, "  \
			"duration, playtime, playflag, createtime, device_type, device_version, product_name, product_version) "     \
			" VALUES(%"PRIu64",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",%u,\"%s\",%"PRIu64","  \
			" %d,FROM_UNIXTIME(%"PRIu64"),%d,FROM_UNIXTIME(%"PRIu64"),\"%s\",\"%s\",\"%s\",\"%s\") "  \
			" ON DUPLICATE KEY UPDATE "  \
			"gcid=\"%s\",cid=\"%s\" , url=\"%s\" , file_name=\"%s\", task_type=%d"  \
			",src_url=\"%s\",filesize=%"PRIu64",flag=1,duration=%d,playtime=FROM_UNIXTIME(%"PRIu64"),playflag=%u" \
			",createtime=FROM_UNIXTIME(%"PRIu64"),device_type=\"%s\",device_version=\"%s\",product_name=\"%s\",product_version=\"%s\""  \
			,cloud_ptr->userid,cloud_ptr->gcid.c_str(),cloud_ptr->url_hash.c_str(),cloud_ptr->cid.c_str(),cloud_ptr->url.c_str(), cloud_ptr->file_name.c_str(), cloud_ptr->task_type,cloud_ptr->src_url.c_str(), cloud_ptr->filesize \
			,cloud_ptr->movie_length,cloud_ptr->play_time,cloud_ptr->playflag,cloud_ptr->createtime  \
			,cloud_ptr->device_type.c_str(),cloud_ptr->device_version.c_str(),cloud_ptr->product_name.c_str(),cloud_ptr->product_version.c_str()\
			,cloud_ptr->gcid.c_str(),cloud_ptr->cid.c_str(),cloud_ptr->url.c_str(),cloud_ptr->file_name.c_str()  \
			,cloud_ptr->task_type,cloud_ptr->src_url.c_str(),cloud_ptr->filesize,cloud_ptr->movie_length  \
			,cloud_ptr->play_time,cloud_ptr->playflag,cloud_ptr->createtime \
			, cloud_ptr->device_type.c_str() ,cloud_ptr->device_version.c_str(),cloud_ptr->product_name.c_str(),cloud_ptr->product_version.c_str()
		);
		LOG4CPLUS_DEBUG(logger, "prepare sql=" << sql_buf);
	}
    AutoSqlExecuter query(&_play_info_db_pool, string(sql_buf), false); 
}


void RecordCache::UpdateLocalPlayRecord(LocalPlayRecord & record)
{
	LOG4CPLUS_DEBUG(logger, "update user:"<<record.userid<<" local play record");
	uint64_t key = record.userid;
    UserRecordSetType * records = _play_info->get_data(key);
    if(!records){
		LOG4CPLUS_DEBUG(logger, "Not get user records,create one");
        UserRecordSetType record_set;
        //_play_info->return_data(key);

		vector<PlayRecord*>  tmp; // size为0时 要查一下数据库
		Query(record.userid,string(""),0,string(""),tmp);
        records = _play_info->get_data(key);

        if(!records){
		 	LOG4CPLUS_ERROR(logger, "No User Records");
            return;
        }
    }
	PlayRecordKey record_key;
	record_key.type = VIEW_TYPE_LOCAL;
    record_key.movieid = 0;
    record_key.gcid = record.gcid;
	record_key.url_hash="";
	
	LOG4CPLUS_DEBUG(logger, "before update local Records size:"<<records->size());
	
	boost::shared_ptr<LocalPlayRecord> rec_ptr(new LocalPlayRecord(record));
	rec_ptr->SetUpdateFlag();
	
	(*records)[record_key] = rec_ptr; 
	LOG4CPLUS_DEBUG(logger, "after update local Records size:"<<records->size());
	
	_play_info->return_data(key);
}

void RecordCache::UpdateOnlinePlayRecord(PlayRecord *precord)
{
	
	LOG4CPLUS_DEBUG(logger, "update user:"<<precord->userid<<" vod play record");
    uint64_t key = precord->userid;
    UserRecordSetType * records = _play_info->get_data(key);
    if(!records){
		LOG4CPLUS_DEBUG(logger, "Not get user records,create one");
        UserRecordSetType record_set;

		vector<PlayRecord*>  tmp; // flag为0时 要查一下数据库
		Query(precord->userid,string(""),0,string(""),tmp);

        records = _play_info->get_data(key);
		if(!records){
		 	LOG4CPLUS_ERROR(logger, "No User Records");
            return;
        }
    }

	LOG4CPLUS_DEBUG(logger, "before update vod Records size:"<<records->size());

    PlayRecordKey record_key;
	bool  result = false;
	unsigned long  taketime=0;
	{
			Timedelt checktime(taketime);
			result = FillMovieInfo(precord);
	}
	
	LOG4CPLUS_DEBUG(logger,"search db take "<<taketime<<" ns");
	
	if(result == false || precord->viewed_time <= 0  || precord->viewed_time > precord->movie_length ){
	  stringstream log_name;
	  time_t now = time(NULL);
	  log_name <<"../log/log_bug_" << Utility::get_date_string() <<".txt";
      std::fstream log_file(log_name.str().c_str(),std::ios::out|std::ios::app);
         log_file<<std::endl<<"now:"<<Utility::get_time_string(now)<<" userid:"<<precord->userid<<" "<<" viewed_time:"<<precord->viewed_time;
		 if(precord->view_type==VIEW_TYPE_LONG){
		 	VodPlayRecord *p = (VodPlayRecord*)precord ;
				log_file<<" movieid:"<<p->movie.movieid << " subid:"<<p->movie.submovieid << " title:"<<p->movie.title;
		 }		 
		_play_info->return_data(key);
		return ;
	}
	
    record_key.type = precord->view_type ;
    record_key.movieid = precord->KeyID();
    record_key.gcid="";
	record_key.url_hash="";
	if(precord->view_type==VIEW_TYPE_LONG){
		record_key.charge = (ChargeType)((VodPlayRecord*)precord)->movie.charge;
	}
    UserRecordSetType::iterator it = records->find(record_key);
	if(precord->view_type==VIEW_TYPE_LONG){
		boost::shared_ptr<VodPlayRecord> rec_ptr(new VodPlayRecord(static_cast<VodPlayRecord*>(precord)));
    	rec_ptr->SetUpdateFlag();		
		string version;
		int notified=0;
		if( it != records->end() ){
			boost::shared_ptr<VodPlayRecord> vod_ptr = boost::dynamic_pointer_cast<VodPlayRecord>(it->second);
			version = vod_ptr->movie.version;
			notified  = vod_ptr->movie.updateinfo;
			if(version==rec_ptr->movie.version){
				LOG4CPLUS_DEBUG(logger," old version: "<<version<<"  ? new version " << rec_ptr->movie.version <<"old flag:"<<notified);
				rec_ptr->movie.updateinfo=notified;
			}
		}else{
			rec_ptr->movie.updateinfo=0;
		}
		(*records)[record_key] = rec_ptr; //覆盖即合并在线点播记录
	} else if(precord->view_type==VIEW_TYPE_SHORT){
		boost::shared_ptr<ShortPlayRecord> rec_ptr(new ShortPlayRecord(static_cast<ShortPlayRecord*>(precord)));
		rec_ptr->SetUpdateFlag();
		(*records)[record_key] = rec_ptr; //覆盖即合并在线点播记录
	} else if(precord->view_type==VIEW_TYPE_MUSIC){
		boost::shared_ptr<MusicPlayRecord> rec_ptr(new MusicPlayRecord(static_cast<MusicPlayRecord*>(precord)));
		rec_ptr->SetUpdateFlag();
		(*records)[record_key] = rec_ptr; //覆盖即合并在线点播记录
	}
	LOG4CPLUS_DEBUG(logger, "after update vod Records size:"<<records->size());
	//֪ͨstore_svr
	if(precord->view_type==VIEW_TYPE_LONG){
		static StoreCache   *store_record_cache =  StoreCache::GetInstance(StoreCache::STORE_INST);
		StoreKey  store_key(precord->userid,record_key);
		if(store_record_cache->IsStore(store_key)){
			store_record_cache->NotifiedStoreServer((VodPlayRecord *)(*records)[record_key].get());
		}
		static StoreCache *subscibe_record_cache =  StoreCache::GetInstance(StoreCache::SUBSCRIBE_INST);
		if(subscibe_record_cache->IsStore(store_key)){
			subscibe_record_cache->NotifiedStoreServer((VodPlayRecord *)(*records)[record_key].get());
		}
	}
	_play_info->return_data(key);	
}

void RecordCache::UpdateCloudPlayRecord(CloudPlayRecord &record){
	LOG4CPLUS_DEBUG(logger, "update user:"<<record.userid<<" cloud play record");
	uint64_t key = record.userid;
	UserRecordSetType * records = _play_info->get_data(key);
	if(!records){
		LOG4CPLUS_DEBUG(logger, "Not get user records,create one");
        UserRecordSetType record_set;
        //_play_info->return_data(key);

		vector<PlayRecord*>  tmp;
        Query(record.userid,string(""),0,string(""),tmp);
        records = _play_info->get_data(key);
        if(!records){
		 	LOG4CPLUS_ERROR(logger, "No User Records");
            return;
        }
    }

    PlayRecordKey record_key;
    record_key.type = VIEW_TYPE_CLOUD;
    record_key.url_hash = record.url_hash;

	boost::shared_ptr<CloudPlayRecord> rec_ptr(new CloudPlayRecord(record));
    rec_ptr->SetUpdateFlag();
    (*records)[record_key] = rec_ptr;
    LOG4CPLUS_DEBUG(logger, "after update cloud Records size:"<<records->size());
	
    _play_info->return_data(key);
}

void RecordCache::DeleteOneVodRecord(uint64_t userid, PlayRecordKey movie)
{
	LOG4CPLUS_DEBUG(logger,"DeleteOneVod Record userid:"<<userid);
    UserRecordSetType * user_records = _play_info->get_data(userid);
    if(user_records){
		if(movie.type==UNKOWNTYPE){
			LOG4CPLUS_INFO(logger,"user have records,delete one vodrecord type unkown");
			_play_info->return_data(userid);	
			return ;
		}
		LOG4CPLUS_DEBUG(logger,"delete charge:"<<movie.charge<<" movieid:"<<movie.movieid<<" gcid:"<<movie.gcid<<" urlhash:"<<movie.url_hash);
        user_records->erase(movie);
	    _play_info->return_data(userid);
    }
    stringstream sql;
	if(VIEW_TYPE_LONG==movie.type || VIEW_TYPE_SHORT==movie.type || VIEW_TYPE_MUSIC== movie.type){
	    sql << "UPDATE  vod_record set flag=0 WHERE userid=" << userid << " AND movieid=" << movie.movieid << " AND charge=" << movie.charge << " AND viewtype=" << movie.type;
	}else if(VIEW_TYPE_LOCAL == movie.type){
		sql << "UPDATE  local_record set flag=0 WHERE userid=" << userid << " AND gcid=" << movie.gcid ;		
	}else if(VIEW_TYPE_CLOUD==movie.type){
		sql << "UPDATE cloud_record set flag=0 WHERE userid=" << userid << " AND urlhash=" << movie.url_hash;		
	}
	else{
		LOG4CPLUS_INFO(logger,"delete one vodrecord type unkown");
		return;
	}
    AutoSqlExecuter query(&_play_info_db_pool, sql.str(), false);
}

void RecordCache::Clear(uint64_t userid)
{
    stringstream sql;
    sql << "UPDATE  vod_record set flag=0 WHERE userid=" << userid;
    AutoSqlExecuter query(&_play_info_db_pool, sql.str(), false);
    UserRecordSetType empty;
    _play_info->add(userid, empty);
}

//ʵ��ʹ��ʱ��long����flag�ֶ�Ŀǰû��ʲô��
int RecordCache::NotifiedRecord(uint64_t userid,uint64_t movieid,PlayRecordType flag,ChargeType charge,StoreCache::INST_TYPE type)
{
	static StoreCache *inst_cache = StoreCache::GetInstance(StoreCache::STORE_INST);
	static StoreCache *subscribe_cache = StoreCache::GetInstance(StoreCache::SUBSCRIBE_INST);
	int ret=0;
    UserRecordSetType * user_records = _play_info->get_data(userid);
    if(!user_records){
        const uint64_t JUSE_LOAD_FROM_DB=0;     
        LOG4CPLUS_DEBUG(logger, "record cache query a special movie");
        vector<PlayRecord*>  tmp; 
        Query(userid,string(""),JUSE_LOAD_FROM_DB,string(""),tmp);
        user_records = _play_info->get_data(userid);
    }
    PlayRecordKey record_key;
    record_key.type =  flag;
    record_key.movieid = movieid;
    record_key.gcid="";
    record_key.url_hash="";
	record_key.charge = charge ;
    UserRecordSetType::iterator it = user_records->find(record_key);
    if(it==user_records->end()){
        ret = 0 ;
    }else{
		ret = 1 ;
		if( type==StoreCache::STORE_INST ){
			inst_cache->NotifiedStoreServer((VodPlayRecord *)(*user_records)[record_key].get());
		}else if( type==StoreCache::SUBSCRIBE_INST ) {
			subscribe_cache->NotifiedStoreServer((VodPlayRecord *)(*user_records)[record_key].get());
		}
    }
    _play_info->return_data(userid);
    return ret ;
}


int RecordCache::Query(uint64_t userid, const string & platform,uint64_t flag, const string & app_name, /*OUT*/vector<PlayRecord*> & records)
{
	LOG4CPLUS_DEBUG(logger, "record cache query userid:"<<userid);
	const uint64_t JUSE_LOAD_FROM_DB=0;
    UserRecordSetType * user_records = _play_info->get_data(userid);
    if(user_records){
		LOG4CPLUS_DEBUG(logger, "find user records size:" << user_records->size() << " flag: " << flag);
		if(JUSE_LOAD_FROM_DB==(flag&0x1))
		{
			_play_info->return_data(userid); 
			return user_records->size();
		}
        for(UserRecordSetType::iterator it=user_records->begin(); it!=user_records->end(); ++it){
			if(it->first.type == VIEW_TYPE_LONG && ((flag>>VIEW_TYPE_LONG)&0x1)){
				boost::shared_ptr<VodPlayRecord> vod_ptr = boost::dynamic_pointer_cast<VodPlayRecord>(it->second);
                records.push_back(new VodPlayRecord(*vod_ptr));
			}else if(it->first.type == VIEW_TYPE_SHORT && ((flag>>VIEW_TYPE_SHORT)&0x1) ){
				boost::shared_ptr<ShortPlayRecord> vod_ptr = boost::dynamic_pointer_cast<ShortPlayRecord>(it->second);
                records.push_back(new ShortPlayRecord(*vod_ptr));
			}
			else if(it->first.type == VIEW_TYPE_MUSIC && ((flag>>VIEW_TYPE_MUSIC)&0x1) ){				
				boost::shared_ptr<MusicPlayRecord> vod_ptr = boost::dynamic_pointer_cast<MusicPlayRecord>(it->second);
                records.push_back(new MusicPlayRecord(*vod_ptr));
			}else if(it->first.type == VIEW_TYPE_LOCAL && ((flag>>VIEW_TYPE_LOCAL)&0x1)){
				boost::shared_ptr<LocalPlayRecord> local_ptr = boost::dynamic_pointer_cast<LocalPlayRecord>(it->second);
                records.push_back(new LocalPlayRecord(*local_ptr));
            }else if(it->first.type == VIEW_TYPE_CLOUD && ((flag>>VIEW_TYPE_CLOUD)&0x1)){
				boost::shared_ptr<CloudPlayRecord> cloud_ptr = boost::dynamic_pointer_cast<CloudPlayRecord>(it->second);
                records.push_back(new CloudPlayRecord(*cloud_ptr));
            }
        }
        _play_info->return_data(userid); 
		
		sort(records.begin(), records.end(),greater_record_pointer);
        return records.size();
    }
    
    stringstream sql;
	time_t now = time(NULL);
	const _u64 LIFE_TIME = 3*30*24*3600;
	time_t THREE_TIME_POINT = now - LIFE_TIME ;
    UserRecordSetType play_records;
    //从vod_record表中加载在线点播记录
    sql << "SELECT viewtype, movieid, subid, charge,version,notified,UNIX_TIMESTAMP(play_time), viewed_time,"
		<<"device_type,device_version,product_name,product_version "
		<<"FROM vod_record WHERE userid=" << userid <<" AND "
		<<" UNIX_TIMESTAMP(play_time)>" << THREE_TIME_POINT << " AND flag=1";
	LOG4CPLUS_DEBUG(logger, "query from db use :"<<sql.str());
    AutoSqlExecuter query_vod(&_play_info_db_pool, sql.str());
    if(query_vod.Good()){
		const int IMPLEMENT_TYPE = 0;
		const int NOT_IMPLEMENT_TYPE = -1;
		int flag=IMPLEMENT_TYPE;
        while(query_vod.Records()->next()){
			int view_type = query_vod.Records()->get_value_int();
			flag=0;
			PlayRecord *p_record=NULL;
			uint64_t movieid , submovieid , charge,update_flag;
			string version;
			if(view_type==VIEW_TYPE_LONG){
				p_record = static_cast<PlayRecord*>(new VodPlayRecord);
			}
			else if(view_type==VIEW_TYPE_SHORT){
				p_record = static_cast<PlayRecord*>(new ShortPlayRecord);
			}
			else if(view_type==VIEW_TYPE_MUSIC){
				p_record = static_cast<PlayRecord*>(new MusicPlayRecord);
			}
			else {
				LOG4CPLUS_ERROR(logger,"Type:"<<view_type<<" have not implement");
				flag=NOT_IMPLEMENT_TYPE;
			}
			if(flag==IMPLEMENT_TYPE){
				boost::shared_ptr<PlayRecord> record(p_record);
	            
	            p_record->userid = userid;
				movieid = query_vod.Records()->get_value_long();
				submovieid = query_vod.Records()->get_value_long();
				charge = query_vod.Records()->get_value_long();
				version = query_vod.Records()->get_value_string();
				update_flag =  query_vod.Records()->get_value_long();
				if(view_type==VIEW_TYPE_LONG){
	            	((VodPlayRecord*)p_record)->movie.movieid = movieid;
	            	((VodPlayRecord*)p_record)->movie.submovieid = submovieid ;
					((VodPlayRecord*)p_record)->movie.charge = charge ;
					((VodPlayRecord*)p_record)->movie.version = version;
				    ((VodPlayRecord*)p_record)->movie.updateinfo = update_flag;
				}
				else if(view_type==VIEW_TYPE_SHORT){
					((ShortPlayRecord*)p_record)->shortmovie.video_id = movieid;
					((ShortPlayRecord*)p_record)->shortmovie.charge = charge ;
				}
				else if(view_type==VIEW_TYPE_MUSIC){
					((MusicPlayRecord*)p_record)->music.albumid = movieid ;
					((MusicPlayRecord*)p_record)->music.charge = charge;
				}
	            p_record->play_time = query_vod.Records()->get_value_long();
	            p_record->viewed_time = query_vod.Records()->get_value_long();
				p_record->device_type = query_vod.Records()->get_value_string();
				p_record->device_version = query_vod.Records()->get_value_string();
				p_record->product_name = query_vod.Records()->get_value_string();
				p_record->product_version = query_vod.Records()->get_value_string();
				
	            PlayRecordKey key; 
	            key.type = record->view_type ;
	            key.movieid = movieid; 
				key.gcid = "";
	            bool flag = FillMovieInfo(p_record);
				if(!flag){
					continue;
				}
				// 计算缩略图
				if(record->view_type==VIEW_TYPE_LONG){
					((VodPlayRecord*)p_record)->CalScreenShot();
				}
				
	            play_records[key] = record;
				if(record->view_type == VIEW_TYPE_LONG){
					LOG4CPLUS_DEBUG(logger,
		                "record: userid" << record->userid << " viewed_time" << record->viewed_time << " movieid:" << ((VodPlayRecord*)p_record)->movie.movieid);
				}else if(record->view_type==VIEW_TYPE_SHORT){
					LOG4CPLUS_DEBUG(logger,
		                "record: userid" << record->userid << " viewed_time" << record->viewed_time << " videoid:" << ((ShortPlayRecord*)p_record)->shortmovie.video_id);
				}else if(record->view_type==VIEW_TYPE_MUSIC){
					LOG4CPLUS_DEBUG(logger,
						"record: userid" << record->userid << " viewed_time" << record->viewed_time << " albumid:" << ((MusicPlayRecord*)p_record)->music.albumid);
				}
			}
			else{
	            query_vod.Records()->get_value_long();
	            query_vod.Records()->get_value_long();
				query_vod.Records()->get_value_long();
	            query_vod.Records()->get_value_long();
				query_vod.Records()->get_value_long();
				query_vod.Records()->get_value_string();
	            query_vod.Records()->get_value_long();
				query_vod.Records()->get_value_string();
				query_vod.Records()->get_value_string();
				query_vod.Records()->get_value_string();
				query_vod.Records()->get_value_string();
			}
        }
    }

    //从local_record表中加载本地播放记录
    sql.str("");
    sql << "SELECT gcid, cid, filesize, title, movie_length, play_time, viewed_time," 
        << " device_type, device_version, product_name, product_version"
        << " FROM local_record "
        << " WHERE userid=" << userid << " AND flag=1" ;
    LOG4CPLUS_DEBUG(logger, "query local_record with:" << sql.str());
    AutoSqlExecuter query_local(&_play_info_db_pool, sql.str());
    if(query_local.Good()){
        //UserRecordSetType play_records;
        while(query_local.Records()->next()){
            boost::shared_ptr<LocalPlayRecord> record(new LocalPlayRecord);
            record->userid = userid; 
            record->gcid = query_local.Records()->get_value_string();
            record->cid = query_local.Records()->get_value_string();
            record->filesize = query_local.Records()->get_value_long();
            record->title = query_local.Records()->get_value_string();
            record->movie_length = query_local.Records()->get_value_long();
            record->play_time = query_local.Records()->get_value_long(); //~!数据库里面是datetime字段...
            record->viewed_time = query_local.Records()->get_value_long();
			record->device_type = query_local.Records()->get_value_string();
			record->device_version = query_local.Records()->get_value_string();
			record->product_name = query_local.Records()->get_value_string();
			record->product_version = query_local.Records()->get_value_string();
            
            PlayRecordKey key; 
            key.type = VIEW_TYPE_LOCAL; 
			key.gcid = record->gcid;
            play_records[key] = record;
        }
    }

	//从cloud_record表中加载本地播放记录
    sql.str("");
    sql << "SELECT gcid, url_hash, cid, url, file_name, task_type, src_url, filesize," 
        << " duration, playtime, playflag, createtime, device_type, device_version, product_name, product_version"
        << " FROM cloud_record "
        << " WHERE userid=" << userid << " AND flag=1" ;
    LOG4CPLUS_DEBUG(logger, "query cloud_record with:" << sql.str());
    AutoSqlExecuter query_cloud(&_play_info_db_pool, sql.str());
    if(query_cloud.Good()){
        while(query_cloud.Records()->next()){
            boost::shared_ptr<CloudPlayRecord> record(new CloudPlayRecord);
            record->userid = userid; 
            record->gcid = query_cloud.Records()->get_value_string();
            record->url_hash = query_cloud.Records()->get_value_string();
            record->cid = query_cloud.Records()->get_value_string();
            record->url = query_cloud.Records()->get_value_string();
			record->file_name = query_cloud.Records()->get_value_string();
            record->task_type = query_cloud.Records()->get_value_long();
            record->src_url = query_cloud.Records()->get_value_string();
            record->filesize = query_cloud.Records()->get_value_long();
            record->movie_length = query_cloud.Records()->get_value_long();
            record->play_time = query_cloud.Records()->get_value_long();	//~!数据库里面是datetime字段...
            record->playflag = query_cloud.Records()->get_value_long();
            record->createtime = query_cloud.Records()->get_value_long();	//~!数据库里面是datetime字段...
			record->device_type = query_cloud.Records()->get_value_string();
			record->device_version = query_cloud.Records()->get_value_string();
			record->product_name = query_cloud.Records()->get_value_string();
			record->product_version = query_cloud.Records()->get_value_string();
            
            PlayRecordKey key; 
            key.type = VIEW_TYPE_CLOUD; 
			key.url_hash = record->url_hash;
            play_records[key] = record;
        }
    }

    _play_info->add(userid, play_records);
	if((0x1&flag) != JUSE_LOAD_FROM_DB){
	    return Query(userid, platform, flag, app_name, records);
    }else{
        return 0;
    }
	
    return -1;
}

string RecordCache::ToVersion(string version,string type)
{
	string ret;
	wchar_t *pwcs;
	int size=0;
	if((type!="teleplay") && (type!="tv") && (type!="anime") && (type!="documentary") && (type!="fashion") && (type!="lesson")){
		return ret;
	}
	size = mbstowcs(NULL,version.c_str(),0);
	if(size==0){
         LOG4CPLUS_DEBUG(logger, "ToVersion size is zero ");
		 return "";
	}
	pwcs = new wchar_t[size+1];
	size = mbstowcs(pwcs,version.c_str(),size+1);
	if ( (wcschr(pwcs,L'完') == NULL)  && (wcschr(pwcs,L'全') == NULL) ) {
		if( wcspbrk(pwcs,L"预告片")==NULL ){
			if( type=="tv" || type=="documentary" ){
				ret = "更新至";
				ret += version;
			}else{
				ret = version;
			}
		}else{
			ret = "预告片";
		}
	}else{
		ret = "已完结";
	}
	delete  []pwcs;
	LOG4CPLUS_DEBUG(logger, "ToVersion size is "<<size << " version:" << version << "ret:"<<ret );
	return ret;

}

void RecordCache::NotifiedOneVodRecord(uint64_t userid, PlayRecordKey video, string version)
{
	LOG4CPLUS_DEBUG(logger, "notified  user:"<<userid<<" version:"<<version<<" vod play record");
    UserRecordSetType * records = _play_info->get_data(userid);
    if(!records){
		LOG4CPLUS_DEBUG(logger, "Not get user records,create one");
        UserRecordSetType record_set;
        //_play_info->return_data(userid);

		vector<PlayRecord*>  tmp; // flag为0时 要查一下数据库
		Query(userid,string(""),0,string(""),tmp);

        records = _play_info->get_data(userid);
		if(!records){
		 	LOG4CPLUS_ERROR(logger, "No User Records");
            return;
        }
    }
	UserRecordSetType::iterator it = records->find(video);
	if(it!=records->end()){
		boost::shared_ptr<VodPlayRecord> vod_ptr = boost::dynamic_pointer_cast<VodPlayRecord>(it->second);
		LOG4CPLUS_DEBUG(logger, "notified  user:"<<userid<<" old version:"<<vod_ptr->movie.version<<" vod play record");
		if(vod_ptr->movie.version == version ){
			LOG4CPLUS_DEBUG(logger, "notified  vod_ptr->movie.update_flag:1 ");
			vod_ptr->movie.updateinfo = 0 ;
		}
	}
	_play_info->return_data(userid);
}


//仅仅加载在线视频
bool RecordCache::FillMovieInfo(PlayRecord *record)
{
	if(record->view_type==VIEW_TYPE_LONG){
		VodPlayRecord *p_record = static_cast<VodPlayRecord *> (record);
		bool flag =  FillMovieInfo(p_record->movie);
		p_record->movie_length = p_record->movie.movie_length;
		if(flag)
		  p_record->CalScreenShot();
		return flag ;
	} else if( record->view_type == VIEW_TYPE_SHORT ){
		ShortPlayRecord *p_record = static_cast<ShortPlayRecord *> (record);
		bool flag = FillMovieInfo(p_record->shortmovie);
		p_record->movie_length = p_record->shortmovie.length;
		return flag;
	}else if ( record->view_type == VIEW_TYPE_MUSIC ){
		MusicPlayRecord *p_record = static_cast<MusicPlayRecord *> (record);
		bool flag = FillMovieInfo(p_record->music);
		p_record->movie_length = p_record->music.length;
		return flag;
	}
	return false ;
}

bool RecordCache::FillMovieInfo(VodMovieInfo & info)
{
	LOG4CPLUS_DEBUG(logger, "FillMovieInfo submovieid:"<<info.submovieid);
    int charge = info.charge;
    MovieUniqueKey key(charge,info.submovieid);
    VodMovieInfo * sub_info = _movie_info->get_data(key);
    if(sub_info){
        string version = sub_info->version;
		int flag=info.updateinfo;
		if ( (version !="") && (info.version!=version) ){
					if(info.channel!="movie" ){
						flag = 1 ;
					}else{
						flag = 0 ;
					}
		}
		info = *sub_info;
		info.updateinfo = flag ;
		info.charge = charge;
		
        _movie_info->return_data(key);
        time_t now = time(NULL);
        static int SUBMOVIE_INFO_LIFE_SEC = g_app.get_conf_int("submovie.info.life.sec", 60*60*4);
        
        if(sub_info->load_timestamp + SUBMOVIE_INFO_LIFE_SEC < now){ //超时，删除之
            _movie_info->del(key);
            return FillMovieInfo(info);
        }
        return true;
    }

    //缓存没命中，从媒资库里面去查
    //付影片查v_vip_submovie 免费影片查v_mp4_submovie 
    stringstream sql;
	if( info.charge==0 ){
    sql << " SELECT M.movieid, S.gcid, M.title, M.type, S.vodurl, M.hd_vod_num,M.bitrate, FROM_UNIXTIME(S.dateonshow), S.number, S.chaptertype,S.chaptertitle, S.play_length, M.productid ,M.longvideo_image,M.big_poster,M.version,S.position "
        << " FROM movie.v_mp4_submovie AS S, movie.v_movie AS M "
        << " WHERE S.submovieid=" << info.submovieid << " AND M.movieid = S.movieid ";
	}else{
	sql << " SELECT M.movieid, S.gcid, M.title, M.type, S.vodurl, M.hd_vod_num, M.bitrate, FROM_UNIXTIME(S.dateonshow),S.number, S.chaptertype,S.chaptertitle, S.play_length, M.productid ,M.longvideo_image,M.big_poster,M.version,S.position  "
		<< " FROM movie.v_vip_submovie AS S, movie.v_movie AS M "
		<< " WHERE S.submovieid=" << info.submovieid << " AND M.movieid = S.movieid ";		
	}
    AutoSqlExecuter query(&_movie_info_db_pool, sql.str());


	LOG4CPLUS_DEBUG(logger, "query MovieInfo from db use :"<<sql.str());
    bool has = false;
	int hd_vod_num=0;
    if(query.Good()){
        while(query.Records()->next()){
			hd_vod_num=0;
            info.movieid = query.Records()->get_value_long();
            info.gcid = query.Records()->get_value_string();
            info.title = query.Records()->get_value_string();
            info.channel = query.Records()->get_value_string();
            info.play_url = query.Records()->get_value_string();
			hd_vod_num = query.Records()->get_value_int();
			info.bitrate = query.Records()->get_value_string();
			string tmp = query.Records()->get_value_string();
			info.date = strtok((char*)tmp.c_str()," ");
            info.chapter = query.Records()->get_value_int();
			info.chapter_type = query.Records()->get_value_int();
            info.chapter_name = query.Records()->get_value_string();
            info.movie_length = query.Records()->get_value_long();
			info.productid = query.Records()->get_value_long();
			info.bigposter = query.Records()->get_value_string();
			info.verticalposter = query.Records()->get_value_string();
			string version = query.Records()->get_value_string(); 
			info.partindex=query.Records()->get_value_long();
			LOG4CPLUS_DEBUG(logger, "run new version:"<< version <<" old version:"<<info.version << " updateinfo:"<<info.updateinfo);
			if ( info.version!=version ){
					info.version = version;
					if(version !="" ){
						if(info.channel!="movie"){
							info.updateinfo = 1 ;
						}else{
							info.updateinfo = 0 ;
						}
					}else{
						info.updateinfo=0;
					}
			}
			if(hd_vod_num>0){
				info.bitrate="1080P";
			}
            has = true;
        }
		if(has)
		{
			info.load_timestamp = time(NULL);
	        _movie_info->add(key, info);
		}
    }  
	else
	{
		LOG4CPLUS_ERROR(logger, "query failed");
	}
    //_movie_info->return_data(key);
    
    return has;
}


bool RecordCache::FillMovieInfo(MusicInfo & info ){
	LOG4CPLUS_DEBUG(logger, "FillMovieInfo albumid:"<<info.albumid);
	MusicKey key(VIDEO_FREE,info.albumid);
	MusicInfo* sub_info = _music_info->get_data(key);
    if(sub_info){
        info = *sub_info;
        _music_info->return_data(key);
        time_t now = time(NULL);
        static int SUBMOVIE_INFO_LIFE_SEC = g_app.get_conf_int("submovie.info.life.sec", 60*60*4);
        
        if(sub_info->load_timestamp + SUBMOVIE_INFO_LIFE_SEC < now){ //超时，删除之
            _music_info->del(key);
            return FillMovieInfo(info);
        }
        return true;
    }

	stringstream sql;

	sql << "SELECT A.albumid,T.trackid,T.title,TIME_TO_SEC(T.length),T.mv_vod_url,A.img_shot FROM music.album as A ,music.track AS T "
		<< "WHERE A.albumid = T.albumid AND A.albumid = " << info.albumid ;
	LOG4CPLUS_DEBUG(logger, "query MusicInfo from db use :"<<sql.str());
	AutoSqlExecuter query(&_movie_info_db_pool, sql.str());
	//先music库album和track表找视频时长，若没有则根据播放地址计算
    bool has = false;
    if(query.Good()){
        while(query.Records()->next()){
            info.albumid  = query.Records()->get_value_long();
			uint64_t trackid = query.Records()->get_value_long(); 
			info.title = query.Records()->get_value_string();
			info.length = query.Records()->get_value_long();
			info.mv_vod_url = query.Records()->get_value_string();
			info.img = query.Records()->get_value_string();
			if(0==info.length){
				if(""==info.mv_vod_url){
					stringstream sql_second;				
					sql_second << " SELECT V.play_length,V.vodurl FROM video.v_video_submovie AS V ,music.track AS T   " 
							   << " WHERE V.channel=2 AND V.videoid=T.trackid  AND T.trackid=" << trackid << " limit 1 ";
					AutoSqlExecuter query_second(&_movie_info_db_pool, sql_second.str());
					if(query_second.Good()){
						while(query_second.Records()->next()){
							info.length = query_second.Records()->get_value_long();
							info.mv_vod_url = query_second.Records()->get_value_string();
						}
					}else{
						break;
					}
				}
			}
			if(0==info.length){
				info.CalMvPlayLength();
			}
            has = true;
        }
		if(has)
		{
			info.load_timestamp = time(NULL);
	        _music_info->add(key, info);
		}
    }  
	else
	{
		LOG4CPLUS_ERROR(logger, "query failed");
	} 
    return has;
	
}

bool RecordCache::FillMovieInfo(ShortMovieInfo & info){
	LOG4CPLUS_DEBUG(logger, "FillMovieInfo videoid:"<< info.video_id);
	ShortMovieKey key(VIDEO_FREE,info.video_id);
	ShortMovieInfo *sub_info = _shortmovie_info->get_data(key);
	 if(sub_info){
        info = *sub_info;
        _shortmovie_info->return_data(key);
        time_t now = time(NULL);
        static int SUBMOVIE_INFO_LIFE_SEC = g_app.get_conf_int("submovie.info.life.sec", 60*60*4);
        
        if(sub_info->load_timestamp + SUBMOVIE_INFO_LIFE_SEC < now){ //超时，删除之
            _shortmovie_info->del(key);
            return FillMovieInfo(info);
        }
        return true;
    }
	stringstream sql;
	sql << "SELECT VS.video_id,V.subject,VS.vodurl,V.vodurl,TIME_TO_SEC(VS.playtime),V.img_shot,V.img FROM video.v_section AS VS , video.videos AS V WHERE "
		<< " VS.video_id = V.id AND VS.video_id = " << info.video_id << " limit 1 ;";
	LOG4CPLUS_DEBUG(logger, "query ShortMovieInfo from db use :"<<sql.str());
	AutoSqlExecuter query(&_movie_info_db_pool, sql.str());

	bool has = false;
    if(query.Good()){
        while(query.Records()->next()){
			info.video_id = query.Records()->get_value_long();
			info.title= query.Records()->get_value_string();
			info.vodurl = query.Records()->get_value_string();
			info.kankanvodurl = query.Records()->get_value_string();
			info.length= query.Records()->get_value_long();
			info.imgshot = query.Records()->get_value_string();
			info.img = query.Records()->get_value_string();
            has = true;
        }
		if(has)
		{
			info.load_timestamp = time(NULL);
	        _shortmovie_info->add(key, info);
		}
    }  
	else
	{
		LOG4CPLUS_ERROR(logger, "query failed");
	}
   // _shortmovie_info->return_data(key);
	return has  ;
}

