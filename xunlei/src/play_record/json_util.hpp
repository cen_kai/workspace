/*
json格式输出工具函数
author: xiaojing@xunlei.com
createdate: 2010-4-22
*/

#ifndef JSON_UTIL_H
#define JSON_UTIL_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <sstream>
using namespace std;


/* 用户定义类型 ，将调用obj.toJson()来实现*/
/* 引用 */
template <typename ObjType>
void ToJson(const ObjType &obj, stringstream& outSS);
/* 指针 */
template <typename ObjType>
void ToJson(ObjType *pObj, stringstream& outSS);


/* key:value 形式输出 */
template <typename ObjType>
void ToJsonPair(const ObjType &obj, stringstream& outSS, const char *pszKey=NULL);


/* 集合 [1,2,3,4] 形式*/
template<typename ObjType, template<typename T> class CollType>
void ToJson(const CollType<ObjType> &vObjs, stringstream& outSS);


#endif

