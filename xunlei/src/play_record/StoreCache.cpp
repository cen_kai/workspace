#include "StoreCache.h"
#include "common/AutoSqlExecuter.hpp"
extern ServerApp g_app;
#include "server/ServiceThread.h"
#include "play_record/RecordCache.h"
#include <iostream>

IMPL_LOGGER(StoreCache, logger);
StoreCache* StoreCache::_instance_store=NULL;
StoreCache* StoreCache::_instance_subscibe=NULL;
SDMutexLock StoreCache::_instance_lock ;


extern ServiceThread< ClientSession >  * g_client_service;

StoreCache::StoreCache(INST_TYPE type )
{
	_store_info = new ThreadSafeHashMap<StoreKey, StoreInfo>(10000, false, false, 10000000); 
    int _store_info_connects=0;
	if( type==STORE_INST ){
		_store_info_db_pool.m_host = g_app.get_conf_string("store.info.db.host", "localhost");
	    _store_info_db_pool.m_port = g_app.get_conf_int("store.info.db.port", 3306);
	    _store_info_db_pool.m_dbname = g_app.get_conf_string("store.info.db.dbname", "play_record");
		_db_table_name = g_app.get_conf_string("store.info.db.tablename", "store_record");
	    _store_info_db_pool.m_user = g_app.get_conf_string("store.info.db.user","root");
	    _store_info_db_pool.m_passwd = g_app.get_conf_string("store.info.db.passwd", "");
		_store_info_connects = g_app.get_conf_int("store.info.db.connect.num",4);
		m_domain = g_app.get_conf_string("store.server.domain","history.yun.kankan.com");
        m_port = g_app.get_conf_int("store.server.port",8686);
	}else{
		_store_info_db_pool.m_host = g_app.get_conf_string("subscribe.info.db.host", "localhost");
	    _store_info_db_pool.m_port = g_app.get_conf_int("subscribe.info.db.port", 3306);
	    _store_info_db_pool.m_dbname = g_app.get_conf_string("subscribe.info.db.dbname", "play_record");
		_db_table_name = g_app.get_conf_string("store.info.db.tablename", "subscribe_record");
	    _store_info_db_pool.m_user = g_app.get_conf_string("subscribe.info.db.user","root");
	    _store_info_db_pool.m_passwd = g_app.get_conf_string("subscribe.info.db.passwd", "");
		_store_info_connects = g_app.get_conf_int("subscribe.info.db.connect.num",4);
		m_domain = g_app.get_conf_string("subscribe.server.domain","history.yun.kankan.com");
        m_port = g_app.get_conf_int("subscribe.server.port",8686);
	}
	_is_notifiedall = 0;
	m_inst_type = type;
    _store_info_db_pool.set_size(_store_info_connects);
}

StoreCache * StoreCache::GetInstance( INST_TYPE type )
{
   StoreCache * &_instance = (type==STORE_INST)?_instance_store:_instance_subscibe;
   if(_instance == NULL){
	    _instance_lock.lock();
		if(_instance == NULL){
			_instance = new StoreCache(type);
		}
		_instance_lock.unlock();
    }
    return _instance;
}

bool StoreCache::NotifiedStoreServer(VodPlayRecord  *record)
{
   if(record==NULL){
   		 LOG4CPLUS_DEBUG(logger,"NotifiedStoreServer NULL pointer");
		return true;
   }
   LOG4CPLUS_DEBUG(logger, "NotifiedStoreServer httpcmd  before");
   LOG4CPLUS_DEBUG(logger,"NotifiedStoreServer userid:"<<record->userid);
   HttpCmd *cmd = new HttpCmd();
   stringstream buf;
   cmd->settype(HttpCmd::TYPE_GET);
   cmd->header_paras["Host"]=m_domain;
   cmd->header_paras["Path"]="play";
   buf<<record->userid;
   string userid;
   buf>>userid;
   cmd->normal_paras["userid"]=userid;
   buf.str("");
   buf.clear();
   buf<<record->movie.movieid;
   string movieid;
   buf>>movieid;
   cmd->normal_paras["movieid"]=movieid;
   buf.str("");
   buf.clear();
   string subid;
   buf<<record->movie.submovieid;
   buf>>subid;
   cmd->normal_paras["subid"]=subid;
   buf.str("");
   buf.clear();
   string viewed_time;
   buf<<record->viewed_time;
   buf>>viewed_time;
   cmd->normal_paras["viewedtime"]=viewed_time;
   buf.str("");
   buf.clear();
   string play_time;
   buf<<record->play_time;
   buf>>play_time;
   cmd->normal_paras["playtime"]=play_time;
   buf.str("");
   buf.clear();
   string device_type=record->device_type;
   cmd->normal_paras["devicetype"]=device_type;
   LOG4CPLUS_DEBUG(logger, "NotifiedStoreServer httpcmd  after");
   string  web_ip = Utility::get_first_ip_from_domainname(m_domain.c_str());
   return initiative_send_cmd(web_ip,m_port,cmd);
}

bool  StoreCache::initiative_send_cmd(string server_ip, short port, HttpCmd *cmd)
{
   LOG4CPLUS_DEBUG(logger, "initiative_send_cmd ip:"<<server_ip << " port:"<<port);

   int socketfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if(socketfd == -1)
   {
   	   LOG4CPLUS_DEBUG(logger, "initiative_send_cmd socket function err:"<<errno);
       return -1;
   }


   struct sockaddr_in addr;                                                                                       
   addr.sin_family = AF_INET;                                                                                     
   addr.sin_addr.s_addr = inet_addr(server_ip.c_str()); 
   addr.sin_port = htons(port);      
   int ret_v = connect(socketfd, (struct sockaddr*)&addr, sizeof(addr));                                                                                                         
   if( ret_v  == -1)                                              
   {
       LOG4CPLUS_DEBUG(logger, "initiative_send_cmd connect function err:"<<errno);
       close(socketfd);
       return -1;
   }
   Utility::set_fd_block(socketfd, false);
   Utility::set_socket_linger(socketfd,true,0);
   
   LOG4CPLUS_DEBUG(logger, "connect after");

   bool ret = g_client_service->add_new_socket(socketfd,cmd,Session::COMMAND_APP_HTTP);
   return ret ;
}


void StoreCache::AddStoreRecord(StoreKey &key)
{
	LOG4CPLUS_DEBUG(logger,"AddStoreRecord");
	static RecordCache *record_cache = RecordCache::GetInstance();
	char sql_buf[2048];
	StoreInfo   tmp ;
	_store_info->add(key,tmp);
	snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO %s(userid,movieid,notifited,flag) VALUES(%"PRIu64",%"PRIu64",0,1)  ON DUPLICATE KEY UPDATE  notifited=0,flag=1",_db_table_name.c_str(),key.userid,key.moviekey.movieid);
	LOG4CPLUS_DEBUG(logger,"AddStoreRecord sql"<<sql_buf);	
	AutoSqlExecuter query(&_store_info_db_pool, string(sql_buf), false); 
	//若有播放记录，则发送到指定的svr
	record_cache->NotifiedRecord(key.userid,key.moviekey.movieid,key.moviekey.type,key.moviekey.charge,m_inst_type);
}

void StoreCache::DelStoreRecord(StoreKey &key)
{
	LOG4CPLUS_DEBUG(logger,"DelStoreRecord");
	char sql_buf[2048];
	_store_info->del(key);
	snprintf(sql_buf,sizeof(sql_buf),"UPDATE  %s set flag = 0 WHERE userid=%"PRIu64" AND movieid=%"PRIu64" ",_db_table_name.c_str(),key.userid,key.moviekey.movieid);
	AutoSqlExecuter query(&_store_info_db_pool, string(sql_buf), false); 
}

bool StoreCache::IsStore(StoreKey key)
{
    bool flag = _store_info->find(key);
	LOG4CPLUS_DEBUG(logger,"IsStore movieid:"<<key.moviekey.movieid);
    return flag;
}


void StoreCache::SaveToDB(std::pair<StoreKey,StoreInfo> v)
{
	char sql_buf[2048];
	snprintf(sql_buf,sizeof(sql_buf),"INSERT INTO %s(userid,movieid,notifited,flag) VALUES(%"PRIu64",%"PRIu64",%d,1) ON DUPLICATE KEY UPDATE notifited=%d,flag=1",_db_table_name.c_str(),v.first.userid,v.first.moviekey.movieid,v.second.change,v.second.change);
	AutoSqlExecuter query(&_store_info_db_pool, string(sql_buf), false); 
}

//调用该函数会启动一个线程来dump
void StoreCache::NotifiedAllRecord( INST_TYPE type )
{
   {
   	   StoreCache *store_cache = StoreCache::GetInstance(type);
	   if(store_cache==NULL){
			return ;
	   }
	   SDAutoLock  lock_inst(store_cache->_db_lock.raw_lock_ptr());   	   
	   if(store_cache->_is_notifiedall==0){
	      store_cache->_is_notifiedall=1;
   	   }else{
   	      //已经在notifiedallrecord直接退出
          return ;
	   }
   }
   pthread_t tid;
   pthread_attr_t attr;
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
   pthread_create(&tid, &attr, _NotifiedAllRecord,(void*)type );
   pthread_attr_destroy(&attr);
}


//该调用会启动一个线程来加载数据
void  StoreCache::LoadDB( INST_TYPE type )
{
   pthread_t tid;
   pthread_attr_t attr;
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
   pthread_create(&tid, &attr, _loaddb, (void*)type);
   pthread_attr_destroy(&attr);
}

int  StoreCache::size()
{
  return _store_info->size_plus();
}


void StoreCache::run()
{
	//该线程主要清理没有已经剔除了的记录
	//这样记录就不必要考虑其他类型删除的操作
	const static  int  execute_interval = g_app.get_conf_int("cache.sleep.time",60*60);
	{
		//线程同步，先加载完所有的数据
		SDAutoLock  lock_inst(_db_lock.raw_lock_ptr());
	}
	char sql_buf[2048];
	while(1){
		sleep(execute_interval);
		std::list<StoreKey>  del_list;
		for(int bucket_id=0; bucket_id<(int)(_store_info->m_bucket_num); bucket_id++){
            _store_info->m_bucket_mutex_list[bucket_id].lock();
            ThreadSafeHashMap<StoreKey, StoreInfo >::DataMapIter iter=_store_info->m_bucket_list[bucket_id].begin();
            for(; iter!=_store_info->m_bucket_list[bucket_id].end(); iter++ ){
				snprintf(sql_buf,sizeof(sql_buf),"SELECT userid FROM %s.vod_record WHERE userid=%"PRIu64" AND movieid=%"PRIu64" AND flag=1 ",_db_table_name.c_str(),iter->first.userid,iter->first.moviekey.movieid);
				AutoSqlExecuter query(&_store_info_db_pool, string(sql_buf), true);
				SDRES_RESULT * result = query.Records();
				if(result==NULL){
					LOG4CPLUS_ERROR(logger,"StoreCache::run  "<<sql_buf<<" get result wrong");
					continue;
				}
				if(!result->next()){
					del_list.push_back(iter->first);
				}else{
				    result->get_value_long();
				}
            }
			_store_info->m_bucket_mutex_list[bucket_id].unlock();
		}
		for(std::list<StoreKey>::iterator iter=del_list.begin();iter!=del_list.end();++iter){
				DelStoreRecord(*iter);
		}
		LOG4CPLUS_DEBUG(logger,"cache "<<_db_table_name << " run while(1) size: "<<_store_info->size_plus());
	}
}

void StoreCache::ClearCache()
{
	if(_store_info){
		delete  _store_info ;
	    _store_info = NULL ;
	}
	_store_info = new ThreadSafeHashMap<StoreKey, StoreInfo>(10000, false, false, 10000000); 
}

void *StoreCache::_loaddb(void *args)
{
   INST_TYPE type = (StoreCache::INST_TYPE)(long)args;
   if( type!=STORE_INST && type!=SUBSCRIBE_INST ){
	   std::cout<<"load db type wrong"<<std::endl;
	   return NULL;
   }
   char sql_buf[2048];
   StoreCache *store_cache = StoreCache::GetInstance(type);
   //线程同步，先加载完所有数据，再启动线程执行run
   SDAutoLock  lock_inst(store_cache->_db_lock.raw_lock_ptr());
 
   int load_every_time_begin = 0;
   const int load_every_time = 1000 ; 
   int load_every_time_end = load_every_time ;
   snprintf(sql_buf,sizeof(sql_buf),"SELECT  count(*) FROM %s WHERE flag=1 ",store_cache->_db_table_name.c_str() );
   int total_records_num=0;
   {
   	    AutoSqlExecuter query(&(store_cache->_store_info_db_pool), string(sql_buf), true);
   	    SDRES_RESULT * result = query.Records();
		while(result->next()){
			total_records_num = result->get_value_long();
		}
   }
   for(;total_records_num>0;){
   	    snprintf(sql_buf,sizeof(sql_buf),"SELECT userid,movieid,notifited FROM %s WHERE flag=1 AND id > %d AND id <= %d", store_cache->_db_table_name.c_str(),load_every_time_begin,load_every_time_end );
		load_every_time_begin +=  load_every_time ;
		load_every_time_end += load_every_time ;
		total_records_num -= load_every_time ;
		
		AutoSqlExecuter query(&(store_cache->_store_info_db_pool), string(sql_buf), true);
		if( load_every_time_begin>0x0FFFFFFF ){
			break;
		}
		SDRES_RESULT * result = query.Records();
		while(result->next()){
			StoreKey key;
			key.userid = result->get_value_long();
			key.moviekey.movieid  = result->get_value_long();
			key.moviekey.charge = (ChargeType)0;
			key.moviekey.gcid = "";
			key.moviekey.url_hash = "";
			key.moviekey.type = VIEW_TYPE_LONG;
			long  flag=result->get_value_long();
			StoreInfo   tmp(flag) ;
    	    store_cache->_store_info->add(key,tmp);
		}
   }
   std::cout<<"load "<< store_cache->_db_table_name << "  "<< store_cache->size() <<" records from db "<<std::endl;
   return NULL;
}

void *StoreCache::_NotifiedAllRecord(void *args)
{
	INST_TYPE type = (StoreCache::INST_TYPE)(long)args;
	if( type!=STORE_INST && type!=SUBSCRIBE_INST ){
		std::cout<<"load db type wrong"<<std::endl;
		return NULL;
	}

	StoreCache *store_cache = StoreCache::GetInstance(type);
	static RecordCache*  record_cache = RecordCache::GetInstance();
	for(int bucket_id=0; bucket_id<(int)(store_cache->_store_info->m_bucket_num); bucket_id++){
            store_cache->_store_info->m_bucket_mutex_list[bucket_id].lock();
            ThreadSafeHashMap<StoreKey, StoreInfo >::DataMapIter iter =store_cache->_store_info->m_bucket_list[bucket_id].begin();
            for(; iter!=store_cache->_store_info->m_bucket_list[bucket_id].end(); iter++ ){
				record_cache->NotifiedRecord(iter->first.userid,iter->first.moviekey.movieid,iter->first.moviekey.type,iter->first.moviekey.charge,type);
            }
			store_cache->_store_info->m_bucket_mutex_list[bucket_id].unlock();
	}
	return NULL;
}
