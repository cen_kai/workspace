#include "play_record/common.h"
#include "play_record/json_util.hpp"
#include "play_record/json_util.cpp"

map<string,int> record_type_map;

inline void PlayRecord::toJson(stringstream &sstream) const
{
        sstream << "{";
        ToJsonPair(userid, sstream, "userid"); 
        sstream<<",";
        ToJsonPair(play_time, sstream, "play_time"); 
        sstream << "}";
}

inline int PlayRecord::size()
{
        int size=0;
        size += sizeof(uint64_t);    // userid
        size += sizeof(uint32_t);    //viewed_time
        size += sizeof(uint32_t);    //movie_length
        size += sizeof(time_t);      //play_time
        size += device_type.size()+sizeof(string);
        size += device_version.size()+sizeof(string);
        size += product_name.size()+sizeof(string);
        size += product_version.size()+sizeof(string);
        size += sizeof(time_t);     // changed_time
        size += sizeof(time_t);     //save_time
        return size;
}

void init_record_type_map()
{
        record_type_map["long"]=VIEW_TYPE_LONG;
        record_type_map["online"]=VIEW_TYPE_LONG;
        record_type_map["short"]=VIEW_TYPE_SHORT;
        record_type_map["local"]=VIEW_TYPE_LOCAL;
        record_type_map["cloud"]=VIEW_TYPE_CLOUD;
        record_type_map["music"]=VIEW_TYPE_MUSIC;
}

inline int  VodPlayRecord::size()
{
    int size=0;
    size = PlayRecord::size();
    size +=  litimageaddr.size()+sizeof(string);
    size += movie.size()+sizeof(string);
    return size;
}
inline void VodPlayRecord::toJson(stringstream &sstream)const
{
        sstream << "{";
        ToJsonPair(movie.title,sstream,"movietitle");
        sstream<<",";
        ToJsonPair(movie.movieid,sstream,"movieid");
        sstream<<",";
        ToJsonPair(movie.channel,sstream,"movietype");
        sstream<<",";
        ToJsonPair(movie.submovieid,sstream,"subid");
        sstream<<",";
        ToJsonPair(litimageaddr,sstream,"screenshot");
        sstream<<",";
        ToJsonPair(movie.chapter,sstream,"episodeid");
        sstream<<",";    
        ToJsonPair(movie.chapter_name,sstream,"chaptername");
        sstream<<",";  
        ToJsonPair(movie.play_url,sstream,"vodaddress");
        sstream<<",";     
        ToJsonPair(movie.bitrate,sstream,"resolution");
        sstream<<",";
        ToJsonPair(viewed_time, sstream, "movietiming"); 
        sstream<<",";
        ToJsonPair(movie.movie_length,sstream,"movielength");
        sstream<<",";
        ToJsonPair(movie.productid,sstream,"productid");
        sstream<<",";
        ToJsonPair(movie.bigposter,sstream,"bigposter");
        sstream<<",";
        ToJsonPair(movie.verticalposter,sstream,"verbigposter");
        sstream<<",";   
        ToJsonPair("long",sstream,"viewtype");
        sstream<<",";
        ToJsonPair(movie.partindex,sstream,"partindex");
        sstream<<",";
		ToJsonPair(movie.date,sstream,"dateonshow");
		sstream<<",";
        ToJsonPair(movie.charge,sstream,"charge");
        sstream<<",";
        ToJsonPair(movie.updateinfo,sstream,"notified");
        sstream<<",";
        ToJsonPair(movie.version,sstream,"version");
        sstream<<",";   
        ToJsonPair(play_time,sstream,"playtime");
        sstream<<",";
        ToJsonPair(device_type,sstream,"devicetype");
        sstream<<",";
        ToJsonPair(device_version,sstream,"deviceversion");
        sstream<<",";
        ToJsonPair(product_name,sstream,"productname");
        sstream<<",";
        ToJsonPair(product_version,sstream,"productversion");
        sstream<<",";
        ToJsonPair("yes",sstream,"copyright");
        sstream << "}";
}


inline void LocalPlayRecord::toJson(stringstream &out)const
{
    out << "{";
    ToJsonPair(title, out, "movietitle");
    out<<",";
    ToJsonPair(gcid,out,"gcid");
    out<<",";
    ToJsonPair(cid,out,"cid");
    out << ",";
    ToJsonPair("local",out,"viewtype");
    out<<",";
    ToJsonPair(movie_length, out, "movielength");
    out << ",";
    ToJsonPair(viewed_time,out,"movietiming");
    out<<",";
    ToJsonPair(play_time, out, "playtime");
    out<<",";
    ToJsonPair(device_type,out,"devicetype");
    out<<",";
    ToJsonPair(device_version,out,"deviceversion");
    out<<",";
    ToJsonPair(product_name,out,"productname");
    out<<",";
    ToJsonPair(product_version,out,"productversion");
    out<<",";
    ToJsonPair("yes",out,"copyright");
    out << "}";
}

inline void CloudPlayRecord::toJson(stringstream &sstream)const{
    sstream << "{";
    ToJsonPair(gcid,sstream,"gcid");
    sstream<<",";
    ToJsonPair(cid,sstream,"cid");
    sstream<<",";
    ToJsonPair("cloud",sstream,"viewtype");
    sstream<<",";
    ToJsonPair(url_hash,sstream,"urlhash");
    sstream<<",";
    ToJsonPair(url,sstream,"url");
    sstream<<",";
    ToJsonPair(file_name,sstream,"filename");
    sstream<<",";
    ToJsonPair(task_type,sstream,"tasktype");
    sstream<<",";    
    ToJsonPair(src_url,sstream,"srcurl");
    sstream<<",";  
    ToJsonPair(filesize,sstream,"filesize");
    sstream<<",";     
    ToJsonPair(movie_length,sstream,"movielength");
    sstream<<",";
    ToJsonPair(play_time, sstream, "playtime"); 
    sstream<<",";
    ToJsonPair(viewed_time,sstream,"movietiming");
    sstream<<",";
    ToJsonPair(playflag,sstream,"playflag");
    sstream<<",";
    ToJsonPair(createtime,sstream,"createtime");
    
    sstream<<",";
    ToJsonPair(device_type,sstream,"devicetype");
    sstream<<",";
    ToJsonPair(device_version,sstream,"deviceversion");
    sstream<<",";
    ToJsonPair(product_name,sstream,"productname");
    sstream<<",";
    ToJsonPair(product_version,sstream,"productversion");
    sstream<<",";
    ToJsonPair("yes",sstream,"copyright");
    sstream << "}";
}

inline void ShortPlayRecord::toJson(stringstream &out)const
{
    out << "{";
    ToJsonPair(shortmovie.video_id, out, "videoid");
    out << ",";
    ToJsonPair(shortmovie.title, out, "movietitle");
    out << ",";
    ToJsonPair(shortmovie.vodurl, out, "vodurl");
    out << ",";
    ToJsonPair(shortmovie.kankanvodurl, out, "kankanvodurl");
    out << ",";
    ToJsonPair(play_time, out, "playtime");
    out<<",";
    ToJsonPair(shortmovie.length, out, "videolength");
    out<<",";
    ToJsonPair(viewed_time, out, "movietiming");
    out<<",";
    ToJsonPair("short",out,"viewtype");
    out<<",";
    ToJsonPair(shortmovie.charge,out,"charge");
    out<<",";
    ToJsonPair(shortmovie.imgshot,out,"imgshot");
    out<<",";
    ToJsonPair(shortmovie.img,out,"img");
    out<<",";
    ToJsonPair(device_type,out,"devicetype");
    out<<",";
    ToJsonPair(device_version,out,"deviceversion");
    out<<",";
    ToJsonPair(product_name,out,"productname");
    out<<",";
    ToJsonPair(product_version,out,"productversion");
    out<<",";
    ToJsonPair("yes",out,"copyright");
    out << "}";
}

inline void MusicPlayRecord::toJson(stringstream &out)const
{
    out << "{";
    ToJsonPair(music.albumid, out, "albumid");
    out << ",";
    ToJsonPair(music.title, out, "movietitle");
    out << ",";
    ToJsonPair(music.mv_vod_url, out, "vodurl");
    out<<",";
    ToJsonPair(music.length, out, "movielength");
    out<<",";
    ToJsonPair(play_time, out, "playtime");
    out << ",";
    ToJsonPair(viewed_time, out, "movietiming");
    out << ",";
    ToJsonPair(music.img,out,"imgshot");
    out<<",";
    ToJsonPair("music",out,"viewtype");
    out<<",";
    ToJsonPair(music.charge,out,"charge");
    out<<",";
    ToJsonPair(device_type,out,"devicetype");
    out<<",";
    ToJsonPair(device_version,out,"deviceversion");
    out<<",";
    ToJsonPair(product_name,out,"productname");
    out<<",";
    ToJsonPair(product_version,out,"productversion");
    out<<",";
    ToJsonPair("yes",out,"copyright");
    out << "}";
}


bool less_record_pointer(const PlayRecord* rl,const PlayRecord* rr )
{
    return rl->play_time < rr->play_time ;
}

bool greater_record_pointer(const PlayRecord* rl,const PlayRecord* rr )
{
    return rl->play_time > rr->play_time ;
}


