#!/usr/bin/env python
# -*- coding: utf8 -*-
import struct
import time
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
dns_list = [ 
		#('tel', 'mp4.cl.kankan.com', False),
		('tel', 't2387.sandai.net', False),
		#('tel', 'tel.p2s.cl.kankan.com', False),
]
#query a gcid from which cdn
def get_send_str(gcid):	
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #gcid_len
		'20s', #GCID
	]
	#print struct.calcsize(''.join(fmt))
	x = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		25,
		121,
		20,
		binascii.a2b_hex(gcid)
	)	
	return x

	
def check_one_dns(dns, query_str, is_p2p, port=80):
	ip = socket.gethostbyname(dns)	
	#print ip
	#recv_str = send_query((ip, 80), query_str)	
	recv_str = send_query((ip, 3088), query_str)	
	cid,file_size,cdn_list = parse(recv_str, is_p2p)
	return cid,file_size,cdn_list
	
def send_query(addr, query_str):
	#print 'send', binascii.b2a_hex(query_str)
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(40)
	sock.connect(addr)		
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*80)	
	return recv_str
	
	
def parse_p2p_cdn(recv_str):
	#print binascii.b2a_hex(recv_str)
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		#'i', #filepath size  p2p的cdnmgr协议略微不同，没有文件路径
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))
	cdn_fmt = cdn_fmt_tpl 
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items
	
def parse_cdn(recv_str):
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		'i', #filepath size
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))	
	items = struct.unpack(''.join(cdn_fmt_tpl), recv_str[:cdn_fmt_size])
	cdn_fmt = cdn_fmt_tpl + ['%ds' % (items[-1],)]
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items
	
def parse(recv_str, is_p2p):
	fmt = ['=',
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #CID len
		'20s', #CID
		'Q', #filesize
		'i', #querytimes
		'i', #CDN count
	]
			
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	#print items
	cid = binascii.b2a_hex(items[5]).upper()
	file_size = items[6]
	#print 'filesize', items[6]
	total_cdn_count = items[-1]	
	buf_str = recv_str[fmt_size:]
	cdn_list = []
	while len(buf_str) > 0:
		used_len = 0
		result = None
		if is_p2p:			
			used_len, result = parse_p2p_cdn(buf_str)
		else:
			used_len, result = parse_cdn(buf_str)
		buf_str = buf_str[used_len:]		
		#print socket.inet_ntoa(struct.pack("I", result[1])), result[5], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(result[7]))
		cdn_list.append((socket.inet_ntoa(struct.pack("I", result[1])),result[5],))
	return cid,file_size,cdn_list
		#print 
#tranfer a gcid to another cdn

def parse_transfer_str(recv_str):	 
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #status 20		
	]
	
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	if len(items) <> 6:
		print 'resp format error'
		return
	status = items[5]
	if status == 0:
		print 'transfer success'
	elif status == 1:
		print 'file_exist'
	else:
		print 'other error'

def ip_aton(ip_str):
	return struct.unpack('I', socket.inet_aton(ip_str))[0]
	
def get_transfer_str(gcid, filesize, targ_cdn_ip):
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #gcid-length 20
		'20s', #GCID
		'Q', #filesize
		'I', #target cdn ip
	]
	total_length =  struct.calcsize(''.join(fmt))
	#print total_length 
	x = struct.pack(''.join(fmt), 
		10,  #protocol_ver  10
		0x4c27d9e5, #magic_num  0x4c27d9e5
		int(time.time()), 
		total_length - 16, # total_length - ADMIN_CMD_HEADER_LEN
		79,  #cmd_typeid			
		20,
		binascii.a2b_hex(gcid),
		filesize,
		ip_aton(targ_cdn_ip),
	)	
	return x
	
def send_transfer_query(addr, query_str):
	#print 'send', binascii.b2a_hex(query_str)
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(15)
	sock.connect(addr)		
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*80)
	#print 'recv_str', binascii.b2a_hex(recv_str)
	return recv_str
	
def transfer_gcid_to_cdn(src_cdn_ip,des_cdn_ip,gcid,file_size):
	send_cmd_str = get_transfer_str(gcid, file_size, des_cdn_ip)
	recv_str = send_transfer_query((src_cdn_ip, 3080), send_cmd_str)
	parse_transfer_str(recv_str)

		
if __name__ == '__main__':
    import sys
    #print sys.argv
    gcid = 'DA79D4E29CEF629DCE4275A17859BAB92A921A21'
    if len(sys.argv) == 3:
         gcid = sys.argv[1]
         dest_cdn_ip = sys.argv[2]
    else:
         print '<usage> transfer_gcid_to_cdn.py {gcid} {dest_cdn_ip}'
         sys.exit(0)
    #print 'GCID:', gcid
	#print 'OUTPUT FORMAT:'
	#print '{IP}\t{CDN_TYPE}\t{INSERT_TIME}'

    query_str = get_send_str(gcid)
    for dns in dns_list:
	#	print dns[0].decode('utf8'), 'CDN:'
        cid,file_size,cdn_list = check_one_dns(dns[1], query_str, dns[2])
	#print gcid,cid,file_size,cdn_list
	if len(cdn_list) > 0 :
	    print 'transfer gcid:',' src cdn: ',cdn_list[0][0],'dest cdn: ',dest_cdn_ip,' gcid:',gcid,' cid:',cid,' file_size: ',file_size
	    transfer_gcid_to_cdn(cdn_list[0][0],dest_cdn_ip,gcid,file_size)
	'''
	check_one_dns('cnc-cl1.kankan.xunlei.com', query_str, False, 81)
	check_one_dns('cnc-cl2.kankan.xunlei.com', query_str, False, 81)
	'''

