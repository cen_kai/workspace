# -*- coding: gbk -*-

#######################
######默认配置#########
PINGServerAddress  = 'hub5u.sandai.net'
PINGServerPort     = 3076
MVHUBServerAddress = 'mvhub5pr.sandai.net'
MVHUBServerPort    = 80
########################

from Crypto.Cipher import AES
import 	struct, binascii
from hashlib import md5	
from socket import *
import array
import struct
import binascii
import time
import sys
import optparse
import string


def encrypt_data(plain_text, aes_tool): 
	#字符串长度不是16的整数倍时末尾需要填充，从C++代码来看，是填充的0，可是Python中却不对。
	#通过枚举发现填充chr(2)时可以正常加密解密。。。。。
	#print  binascii.b2a_hex(plain_text + chr(2) * (16 - len(plain_text) % 16)).upper()
	return aes_tool.encrypt(plain_text + chr(2) * (16 - len(plain_text) % 16))

def decrypt_data(text,aes_tool):
    return aes_tool.decrypt(text)
	
def get_query_str(cid):
	mvhub_query_no_encrypt_fmt = [
		'=',
		'B', #cmd type
		'I', #peerid length 16
		'16s', #peerid
		'I', #cid length 20
		'20s', #cid
		'Q', #filesize
		'I', #gcid length 0  这里不需要gcid
		#'20s',#gcid  这里不需要gcid
		'B', #peer capability 106
		'I', #inter ip
		'I', #NAT type 39
		'B', #level resource
		'B', #resource capability 
		'I', #shub res number
		'I', #query times
		'I', #p2p capability
		'I', #UPNP ip
		'H', #UPNP port
		'I', #max res 100
		'I', #start calc time
	]
	cmd_length = struct.calcsize(''.join(mvhub_query_no_encrypt_fmt))	
	#print cmd_length
	cmd_body = struct.pack(''.join(mvhub_query_no_encrypt_fmt), 
		6, #cmd type
		16, #peerid length 16
		'8C89A55C63A11F90', #peerid
		20, #cid length 20
		binascii.a2b_hex(cid), #cid
		0, #filesize
		0, #gcid length 0  这里不需要gcid
		#'20s',#gcid  这里不需要gcid
		106, #peer capability 106
		0, #inter ip
		39, #NAT type 39
		10, #level resource
		4, #resource capability 
		5, #shub res number
		0, #query times
		0, #p2p capability
		0, #UPNP ip
		0, #UPNP port
		100, #max res 100
		0, #start calc time
	)
	
	mvhub_query_fmt = [
		'=', #紧凑拼接
		'I', #protocol_ver
		'I', #seq
		'I', #cmd len
		'%ds' % (len(cmd_body),), #encry_data
	]
	cmd_data = struct.pack(''.join(mvhub_query_fmt), 
		1,
		0,
		len(cmd_body),
		cmd_body
	)
	m = md5()
	#print binascii.b2a_hex(cmd_data[:8]).upper()
	m.update(cmd_data[:8])
	aes_tool = AES.new(m.digest(), AES.MODE_ECB)
	#print 'before', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	cmd_body = encrypt_data(cmd_body, aes_tool)
	#print 'after', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	
	
	mvhub_query_fmt = [
		#'=', #紧凑拼接
		'I', #protocol_ver
		'I', #seq
		'I', #cmd len
		'%ds' % (len(cmd_body),), #encry_data
	]
	cmd_data = struct.pack(''.join(mvhub_query_fmt), 
		1,
		0,
		len(cmd_body),
		cmd_body
	)
	#print len(cmd_data), binascii.b2a_hex(cmd_data).upper()
	return cmd_data
	
def send_mvhub(cmd_data):
	sock =  socket(AF_INET,SOCK_STREAM)
	sock.settimeout(40)
	sock.connect((MVHUBServerAddress, MVHUBServerPort))			
	sock.send(cmd_data)       
	recv_str, address = sock.recvfrom(1024*80)	
	return recv_str
	
def unpack_recv_str(recved_str):

	mvhub_resp_fmt = [
		'=', #紧凑拼接
		'I', #protocol_ver
		'I', #seq
		'I', #cmd len
		'%ds' %(len(recved_str)-12,), #encry_data
	]
	
	protocol_ver,seq,cmd_len,cmd_body = struct.unpack(
	    ''.join(mvhub_resp_fmt),
	    recved_str
	)
	
	m = md5()
	#print binascii.b2a_hex(recved_str[:8]).upper()
	m.update(recved_str[:8])
	aes_tool = AES.new(m.digest(), AES.MODE_ECB)
	
	#print 'before', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	cmd_body = decrypt_data(cmd_body,aes_tool)
	#print 'after', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	return cmd_body
	
def unpack_resource(resource_body):
    ptr = 0
	
    peerid_size = struct.unpack('i',resource_body[ptr:ptr+4])
    peerid_size = peerid_size[0]
    ptr += 4
    peerid = struct.unpack('%ds' %(peerid_size),resource_body[ptr:ptr+peerid_size])
    ptr += peerid_size
    peerid = peerid[0]
    print 'Peerid: ',peerid
	
    filename_size = struct.unpack('i',resource_body[ptr:ptr+4])
    filename_size = filename_size[0]
    ptr += 4
    filename = struct.unpack('%ds' %(filename_size),resource_body[ptr:ptr+filename_size])
    ptr += filename_size
    filename = filename[0]
    print 'filename: ',filename
	
    ip = struct.unpack('I',resource_body[ptr:ptr+4])
    ip_set = struct.unpack('BBBB',resource_body[ptr:ptr+4])
    ptr += 4
    ip = ip[0]
    print 'ip: ',ip
    print 'ipset: ',ip_set
	
    port = struct.unpack('H',resource_body[ptr:ptr+2])
    ptr += 2
    port = port[0]
    print 'port: ',port
    
    flag = struct.unpack('B',resource_body[ptr:ptr+1])
    ptr += 1
    flag = flag[0]
    print 'flag: ',flag
	
def get_resources(cmd_body):
    resource_head_fmt = [
		'=', #紧凑拼接
		'B', #cmd type
		'B', #Result
		'I',
		'20s', #Cid
		'Q' ,#FileSz
		'I',
		'20s',#GCid
		'B',#resource level
		'I',#array size
	]
	
    head_length = struct.calcsize(''.join(resource_head_fmt))	
    type,result,cidsize,cid,filesize,gcidsize,gcid,level,size =  struct.unpack(
	    ''.join(resource_head_fmt),
	    cmd_body[:head_length]
	)
    #print type,result,filesize,size
    print 'check resource:'
    print 'cid',binascii.b2a_hex(cid).upper()
    print 'gcid',binascii.b2a_hex(gcid).upper()
	
	
    ptr = head_length
    for i in range(0,size):
        print "index:",i
        len = struct.unpack('I',cmd_body[ptr:ptr+4])
        len = len[0]
        ptr += 4
        unpack_resource(cmd_body[ptr:ptr+len])
        ptr += len
        print ''
        
    in_cache_time,active_peers,total_peers = struct.unpack('III',cmd_body[ptr:ptr+12])
    #print in_cache_time,active_peers,total_peers
    
    print  cmd_body[ptr+12:]

def getInt(input = ['','']):
    reInt = struct.unpack_from('I', input[0], input[1])[0]
    input[1] += 4
    return reInt

def getByte(input = ['','']):
    reInt = struct.unpack_from('B', input[0], input[1])[0]
    input[1] += 1
    return reInt

def getShort(input = ['','']):
    reInt = struct.unpack_from('H', input[0], input[1])[0]
    input[1] += 2
    return reInt

def getString(input = ['', '']):
    strlen = struct.unpack_from('I', input[0], input[1])[0]
    input[1] += 4
    realstr = struct.unpack_from('%ds'%strlen, input[0], input[1])[0]
    input[1] += strlen
    return realstr
		
def put64(input =['',''], value = 123):
    struct.pack_into('Q', input[0], input[1], value)
    input[1] += 8

def putInt(input =['',''], value = 123):
    struct.pack_into('I', input[0], input[1], value)
    input[1] += 4
    
def putByte(input =['',''], value = 12):
    struct.pack_into('B', input[0], input[1], value)
    input[1] += 1

def putShort(input =['',''], value = 12):
    struct.pack_into('H', input[0], input[1], value)
    input[1] += 2
    
def putString(input =['',''], value = 'qyf'):
    strlen = len(value)
    struct.pack_into('I', input[0], input[1], strlen)
    input[1] += 4
    struct.pack_into('%ds'%strlen, input[0], input[1], value)
    input[1] += strlen		
	
def getSendStr(ar, peerid):
    protocol_ver = 50
    putInt(ar, protocol_ver)
    cmd_type = 100
    putByte(ar, cmd_type)

    #peerid = issureTrackerConfig.peeridA
    putString(ar, peerid)
    
    return ar[0][0:ar[1]].tostring()

def getPingServerIpPort():
	return (PINGServerAddress,PINGServerPort)

def parse_body(body):
    arrbody = array.array('B', body)
    offset = 0
    ar = [arrbody, offset]
    protocol = getInt(ar)
    cmdid = getByte(ar)
    is_online = getByte(ar)
    if not is_online:
        print 'not on line'
        return 0
    else:
        peerid = getString(ar)
        print 'peer', peerid, 'is online'
        m_lastlogintime = getInt(ar)
        m_nated_type = getShort(ar)
        print 'last ping time = ',time.strftime("%Y%m%d%H%M%S", time.localtime(m_lastlogintime))
        print 'nattype = ', m_nated_type
        return 1

def send_ping(cmd):
	addr = getPingServerIpPort()
	sock = socket(AF_INET, SOCK_DGRAM)
	sock.settimeout(15)
	send_len = sock.sendto(cmd, addr)
	
	return sock
	
def query_ping_peer(peerid):
    try:
        arr = array.array('B', 200*'\x00')
        offset = 0
        arr_offset_list = [arr, offset]
		
        sendStr = getSendStr(arr_offset_list, peerid)
        sock = send_ping(sendStr)
        print 'send query ping succ, peerid =', peerid
        #print "send", send_len, "bytes"

        recv_str, address = sock.recvfrom(4096)
        #print recv_str
        if parse_body(recv_str):
            return 1
        else:
            return 0
        
    except Exception, ex:
        print ex
        return 0
 
def get_report_str(peerid,cid):
	mvhub_report_no_encrypt_fmt = [
		'=',
		'B', #cmd type
		'I', #peerid length 16
		'16s', #peerid
		
		'I', #external ip
		'H', #listen port
		'I', #product flag
		
		'I', #resource count
		'I', #element len
		'I', #cid length 20
		'20s', #cid
		'Q', #filesize
		'I', #gcid length 0  这里不需要gcid
		#'20s',#gcid  这里不需要gcid
		
		'I', #speedup distsvr type
		'I', #cdn system id
	]
	cmd_length = struct.calcsize(''.join(mvhub_report_no_encrypt_fmt))	
	#print cmd_length
	cmd_body = struct.pack(''.join(mvhub_report_no_encrypt_fmt), 
		61, #cmd type
		16, #peerid length 16
		peerid, #peerid
		
		0, #external ip
		0, #listen port
		0, #product flag
		
		1, #element num
		36, #element len
		20, #cid length 20
		binascii.a2b_hex(cid), #cid
		0, #filesize
		0, #gcid length 0  这里不需要gcid
		#'20s',#gcid  这里不需要gcid
		
		
		0, #speedup distsvr type
		0, #cdn system id
		
	)
	
	mvhub_report_head_fmt = [
		'=', #紧凑拼接
		'I', #protocol_ver
		'I', #seq
		
	]
	cmd_data = struct.pack(''.join(mvhub_report_head_fmt), 
		53,
		0
	)
	m = md5()
	#print binascii.b2a_hex(cmd_data).upper()
	m.update(cmd_data)
	aes_tool = AES.new(m.digest(), AES.MODE_ECB)
	#print 'before', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	cmd_body = encrypt_data(cmd_body, aes_tool)
	#print 'after', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	
	
	mvhub_query_fmt = [
		#'=', #紧凑拼接
		'I', #protocol_ver
		'I', #seq
		'I', #cmd len
		'%ds' % (len(cmd_body),), #encry_data
	]
	cmd_data = struct.pack(''.join(mvhub_query_fmt), 
		53,
		0,
		len(cmd_body),
		cmd_body
	)
	#print len(cmd_data), binascii.b2a_hex(cmd_data).upper()
	return cmd_data
	
def unpack_recv_str(recved_str):

	mvhub_resp_fmt = [
		'=', #紧凑拼接
		'I', #protocol_ver
		'I', #seq
		'I', #cmd len
		'%ds' %(len(recved_str)-12,), #encry_data
	]
	
	protocol_ver,seq,cmd_len,cmd_body = struct.unpack(
	    ''.join(mvhub_resp_fmt),
	    recved_str
	)
	
	m = md5()
	#print binascii.b2a_hex(recved_str[:8]).upper()
	m.update(recved_str[:8])
	aes_tool = AES.new(m.digest(), AES.MODE_ECB)
	
	#print 'before', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	cmd_body = decrypt_data(cmd_body,aes_tool)
	#print 'after', len(cmd_body), binascii.b2a_hex(cmd_body).upper()
	return cmd_body
	
def get_response(cmd_body):
	resource_head_fmt = [
		'=', #紧凑拼接
		'B', #cmd type
		'B', #Result
	]
	
	head_length = struct.calcsize(''.join(resource_head_fmt))	
	type,result =  struct.unpack(
		''.join(resource_head_fmt),
		cmd_body[:head_length]
	)
	
	if result is 0:
		print 'report success'
	else:
		print result,'report cid failed'
	
def report_cid(peerid,cid):
	cmd_data = get_report_str(peerid,cid)
	recv_data = send_mvhub(cmd_data)
	#print len(recv_data), binascii.b2a_hex(recv_data).upper()
	cmd_body = unpack_recv_str(recv_data)
	get_response(cmd_body)

def get_ping_str(peerid):
	ping_fmt = [
		'=',
		'I', #proto version
		'B', #cmd type
		'I', #peerid length 16
		'16s', #peerid
		

		
		'I', #inter ip
		'I', #submask
		'H', #port
		
		'I', #product flag
		'I', #product ver
		
		'I', #SNArray
	]
	cmd_length = struct.calcsize(''.join(ping_fmt))	
	#print cmd_length
	cmd_data = struct.pack(''.join(ping_fmt), 
		50,#proto version
		12, #cmd type
		16, #peerid length 16
		peerid, #peerid
		
		
		0, #inter ip
		0, #submask
		8080, #port
		
		0, #product flag
		0, #product ver
		
		0, #SNArray
	)
	return cmd_data
		
def ping_ping_svr(peerid):
	cmd = get_ping_str(peerid)
	send_ping(cmd)

def check_mvhub_resource(cid):
	cmd_data = get_query_str(cid)
	recv_data = send_mvhub(cmd_data)
	#print len(recv_data), binascii.b2a_hex(recv_data).upper()
	cmd_body = unpack_recv_str(recv_data)
	get_resources(cmd_body)
	
def make_ip_port(address):
	ip,port = address.split(':')
	ip = gethostbyname(ip)
	port = string.atoi(port)
	return ip,port

def make_parser_option():
	parser = optparse.OptionParser(usage='%prog \n[ -p pingserver domain:port or IP:port ]\n'
									'[ -m mvhub domain:port or IP:port ]\n[ -P peerid ] \n[ -C cid ]\n')
	
	parser.add_option('-p','--pingserver', dest='ping', 
						help='pingserver address default: %s:%d' %tuple([PINGServerAddress,PINGServerPort]),
						metavar='domain:port or IP:port',
						default='%s:%d' %tuple([PINGServerAddress,PINGServerPort]))  
	parser.add_option('-m','--hub5pr',  dest='mvhub',
						help='mvphub address default: %s:%d' %tuple([MVHUBServerAddress,MVHUBServerPort]),
						metavar='domain:port or IP:port',
						default='%s:%d' %tuple([MVHUBServerAddress,MVHUBServerPort]))  
	
	parser.add_option('-P','--peerid',  dest='peerid',
						help='resource peerid',metavar='peerid') 
						
	parser.add_option('-C','--cid',  dest='cid',
						help='resource cid',metavar='cid') 
					
	
	return parser
	
if __name__ == "__main__":	
	
	parser = make_parser_option()
	(options, args) = parser.parse_args()  
	
	
	if options.cid is None and options.peerid is None:  
		print parser.error("need cid or peerid or both")  
		
	PINGServerAddress,PINGServerPort = make_ip_port(options.ping)
	MVHUBServerAddress,MVHUBServerPort = make_ip_port(options.mvhub)
	
	peerid_check = False
	cid_check    = False
	if options.peerid is not None :
		if len(options.peerid) is 16:
			peerid_check = True
			
	if options.cid is not None :
		if len(options.cid) is 40:
			cid_check = True
			
	if not peerid_check and not cid_check:
		print parser.error("need cid(string of len 40)or peerid(string of len 16) or both") 
		
	elif peerid_check and cid_check:
		query_ping_peer(options.peerid)
		report_cid(options.peerid,options.cid)
		ping_ping_svr(options.peerid)
		check_mvhub_resource(options.cid)
	
	elif peerid_check :
		query_ping_peer(options.peerid)
		
	else :
		temp_peerid = 'FFFFFFFFFFFFFFFF'
		report_cid(temp_peerid,options.cid)
		ping_ping_svr(temp_peerid)
		
	