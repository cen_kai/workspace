# -*- coding: gbk -*-
import struct
import time
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

def parse_recv_str(recv_str):	 
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #status 20		
	]
	
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	if len(items) <> 6:
		print 'resp格式有问题'
		return
	status = items[5]
	if status == 0:
		print '传输成功'
	elif status == 1:
		print '已经有这个文件'
	else:
		print '其他错误'

def ip_aton(ip_str):
	return struct.unpack('I', socket.inet_aton(ip_str))[0]
	
def get_send_str(gcid, filesize, targ_cdn_ip):
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #gcid-length 20
		'20s', #GCID
		'Q', #filesize
		'I', #target cdn ip
	]
	total_length =  struct.calcsize(''.join(fmt))
	#print total_length 
	x = struct.pack(''.join(fmt), 
		10,  #protocol_ver  10
		0x4c27d9e5, #magic_num  0x4c27d9e5
		int(time.time()), 
		total_length - 16, # total_length - ADMIN_CMD_HEADER_LEN
		79,  #cmd_typeid			
		20,
		binascii.a2b_hex(gcid),
		filesize,
		ip_aton(targ_cdn_ip),
	)	
	return x
	
def send_query(addr, query_str):
	print 'send', binascii.b2a_hex(query_str)
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(15)
	sock.connect(addr)		
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*80)
	print 'recv_str', binascii.b2a_hex(recv_str)
	return recv_str
	
def main():
	import sys
	if len(sys.argv) <> 5:
		print 'usage: python trans_cdn_file {src_cdn_ip} {des_cdn_ip} {hex_gcid} {filesize}'
		return
	src_cdn_ip = sys.argv[1]	
	des_cdn_ip = sys.argv[2]
	gcid = sys.argv[3]
	filesize = int(sys.argv[4])		
	send_str = get_send_str(gcid, filesize, des_cdn_ip)
	recv_str = send_query((src_cdn_ip, 3080), send_str)
	parse_recv_str(recv_str)
	
if __name__ == '__main__':
	main()
	