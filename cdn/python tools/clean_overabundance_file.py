# -*- coding: gbk -*-
import struct
import time
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

def del_cdn_file(cdn_ip, hex_cid, filesize):
	fmt=['=	', #����ƴ��
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #cid-length 20
		'20s', #CID
		'Q', #filesize
	]
	total_length =  struct.calcsize(''.join(fmt))
	#print total_length 
	del_cmd_str = struct.pack(''.join(fmt), 
		10,  #protocol_ver  10
		0x4c27d9e5, #magic_num  0x4c27d9e5
		int(time.time()), 
		total_length - 16, # total_length - ADMIN_CMD_HEADER_LEN
		501,  #cmd_typeid			
		20,
		binascii.a2b_hex(hex_cid),
		filesize,
	)	
	
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(15)
	sock.connect((cdn_ip, 3080))		
	sock.send(del_cmd_str)       
	recv_str, address = sock.recvfrom(1024*80)
	
	fmt=['=	', #����ƴ��
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #status 20		
	]
	
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	if len(items) <> 6:
		print 'resp format is wrong.'
		return
	status = items[5]
	if status == 0:
		print 'succeed.'
	elif status == 1:
		print 'no such file.'
	else:
		print 'other error', status
	
def parse_cdn(recv_str):
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		'i', #filepath size
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))	
	items = struct.unpack(''.join(cdn_fmt_tpl), recv_str[:cdn_fmt_size])
	cdn_fmt = cdn_fmt_tpl + ['%ds' % (items[-1],)]
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items

def get_cdnlist_from_mgr(mgr_dns, gcid):
	fmt=['=	', #����ƴ��
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #gcid_len
		'20s', #GCID
	]
	query_str = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		25,
		121,
		20,
		binascii.a2b_hex(gcid)
	)	
	ip = socket.gethostbyname(mgr_dns)
	#print ip
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(40)
	sock.connect((ip, 80))		
	sock.send(query_str)       
	recv_str = ""
	try:
		while True:
			#print 'try read ',
			recv_part_str, address = sock.recvfrom(1024)	
			#print len(recv_part_str)
			if len(recv_part_str) > 0:
				recv_str += recv_part_str
				if len(recv_part_str) < 1024:
					break
			else:
				break
	except:
		pass
	#print len(recv_str)
	fmt = ['=',
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #CID len
		'20s', #CID
		'Q', #filesize
		'i', #querytimes
		'i', #CDN count
	]
			
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	cid = binascii.b2a_hex(items[5]).upper()
	filesize = items[6]
	total_cdn_count = items[-1]	
	buf_str = recv_str[fmt_size:]
	normal_cdn_list = []
	while len(buf_str) > 0:
		used_len = 0
		result = None					
		used_len, result = parse_cdn(buf_str)		
		buf_str = buf_str[used_len:]
		cdn_ip = socket.inet_ntoa(struct.pack("I", result[1]))
		cdn_type = result[5]
		if int(cdn_type) == 0:
			normal_cdn_list.append(cdn_ip)		
	return cid, filesize, normal_cdn_list

def remove_overabundance(mgr_dns, hex_gcid, keep_count):
	cid, filesize, cdn_list = get_cdnlist_from_mgr(mgr_dns, hex_gcid)
	count = 0
	for cdn in cdn_list:
		print cdn,
		count += 1
		if count > keep_count:
			print 'try remove ...',
			del_cdn_file(cdn, cid, filesize)			
		else:
			print
			
def main():
	import sys
	if len(sys.argv) not in (3,4):
		print 'usage: python clean_overabundance_file.py {mgr_dns} {hex_gcid} {keep_count(default 2)}'		
		return
	keep_count = 2	
	mgr_dns = sys.argv[1]
	hex_gcid = sys.argv[2]
	if len(sys.argv) == 4:
		keep_count = int(sys.argv[3])		
	remove_overabundance(mgr_dns, hex_gcid, keep_count)
			
if __name__ == '__main__':
	main()
	