import struct
import sys
import time
import binascii
import random
import socket
def get_send_str(file_infos,ip_int):
   cmd_fmt = ['=',#original byte order
              'i',#protocol version :40
              'i',#sequence number
              'i',#body length
              'B',#cmd type id
              'I',#external ip
              'h',#port
              'i',#gcid number
             ]
   param_fmt= [
              'i', #elemnt length
              'i',   #gcid len
              '20s', #gcid binary
              'i',   #cid len
              '20s', #cid binary
              'Q' ,  #file size
             ]
   total_cmd_fmt = cmd_fmt+param_fmt*len(file_infos)
   param_fmt_size = struct.calcsize(''.join(param_fmt))
   print 'param size:%s' %param_fmt_size
   print total_cmd_fmt
   total_cmd_fmt_size = struct.calcsize(''.join(total_cmd_fmt))
   cmd_content=[]
   cmd_content.append(40)
   cmd_content.append(random.randint(0, 10000000))
   cmd_content.append(total_cmd_fmt_size -12)
   cmd_content.append(75)
   cmd_content.append(ip_int)
   cmd_content.append(8080)
   cmd_content.append(len(file_infos))
   for (gcid,cid,filesize) in file_infos:
       cmd_content.append(56)
       cmd_content.append(20)
       cmd_content.append(binascii.a2b_hex(gcid))
       cmd_content.append(20)
       cmd_content.append(binascii.a2b_hex(cid))
       cmd_content.append(int(filesize))
   print cmd_content
   #s = struct.Struct(''.join(total_cmd_fmt))
   cmd_content_tuple = tuple(cmd_content)
   cmd_str = struct.pack(''.join(total_cmd_fmt),*cmd_content_tuple)
   print cmd_str
   return cmd_str
def send_cmd(addr,query_str):
   print 'send:',binascii.b2a_hex(query_str)
   sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
   sock.settimeout(15)
   sock.connect(addr)
   sock.send(query_str)
   recv_str,address = sock.recvfrom(1024*80)
   return recv_str
def parse_recv_str(recv_str):
   recv_fmt = [
               '=', #original byte order
               'i', #protocol number
               'i', #sequence number
               'i', #body length
               'B', #cmd id
               'B', #cmd status
               ]
   recv_fmt_size = struct.calcsize(''.join(recv_fmt))
   print recv_fmt_size
   print 'recv:',recv_str
   items = struct.unpack(''.join(recv_fmt),recv_str[:recv_fmt_size])
   if len(items) != 5:
      print "fomat error"
      return -2
   status = items[-1]
   if status == 0:
      print 'del success from manager'
   else:
      print 'del fail,for unknown reason'
   return status
def get_ip_address():
    try:
        csock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        csock.connect(('8.8.8.8',80))
        (addr,port) = csock.getsockname()
        csock.close()
        return addr
    except socket.error:
        
        return none
def ip2int(ip):
    ip_int =  struct.unpack('I',socket.inet_aton(str(ip)))[0]
    return ip_int
if __name__ == '__main__':
   ip = get_ip_address()
   ip_int = ip2int(ip)
   if not ip:
      sys.exit(-1)
   x,y,z = 'B9BA9FC467CC700B9B9AFFD11311D3F1D746483D','2DAD338CE40EB32E9C80C94C859AE0E6F3C15757',7246459
   list = []
   list.append((x,y,z))
   mgr_ip='10.10.1.205'
   mgr_port=3088 
   send_str=get_send_str(list,ip_int)
   recv_str=send_cmd((mgr_ip,mgr_port),send_str)
   parse_recv_str(recv_str)
   sys.exit(0)
    
  




