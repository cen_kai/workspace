import sys
import re
import time
import urllib2
import datetime
CMCC_MGR = 'p2s.cl.kankan.com'
def query_by_gcid_file(gcid_list_file):
   i = 0
   j = 0
   k = 0
   failed_list = []
   failed_set = set([])
   failed_map = {}
   query_url = "http://%s/getCdnresource_flv?gcid=" %CMCC_MGR
  # query_url = "http://p2s.cl.kankan.xunlei.com/getCdnresource_flv?gcid="
   for line in open(gcid_list_file):
	gcid = line[:40]
	if gcid not in failed_map:     
	     print 'transfer gcid :',gcid
             http_query= query_url+gcid
	     print http_query
	     try:
	         request = urllib2.Request(http_query)
	         data = urllib2.urlopen(request).read()
	         print data
	         m = re.search(r'.*\ncdnlist1:.*\n.*ip:"([\d|\.]*)",port:(\d*),path:"(.*)"\}',data)
	         if m:
		    print m.group(1),m.group(2),m.group(3)
		    k=k+1
	         else:
		   #failed_set.add(gcid)
		    failed_map[gcid]=1
		    j=j+1
	         i=i+1
	     except Exception,e:
                 failed_map[gcid]=1
                 i=i+1
                 j=j+1
	         print e
        else:
	     failed_map[gcid]= failed_map[gcid]+1
        print "success ratio: %f,total request: %d,success request:%d,failed request:%d" %(k*1.0/i,i,k,j)		 
        if i%1000 == 1000-1:
	     print "time for sleep 5s"
	     time.sleep(5)
	     
   now_time = datetime.datetime.now()
   yes_time = now_time+datetime.timedelta(days=-1)   
   filename = '../stat/stat_fail_download_'+yes_time.strftime('%Y%m%d')
   #filename = '../stat/stat_fail_download_'+time.strftime('%Y%m%d',time.localtime(time.time()))
   stat = "success ratio: %f,total request: %d,success request:%d,failed request:%d\n" %(k*1.0/i,i,k,j) 
  # failed_list=list(failed_set)
   fp = open(filename,"w")
   fp.write(stat)
   #for e in failed_list:
   sort_list = sorted(failed_map.iteritems(),key=lambda d:d[1],reverse = True)
   for e in sort_list:
       fp.write(e[0])
       fp.write('\t')
       fp.write('%d'%e[1])
       fp.write('\n')
   fp.close();
    # time.sleep(1)
def query_by_gcid(gcid):
   query_url = "http://%s/getCdnresource_flv?gcid=" %CMCC_MGR
   http_query= query_url+gcid
   request = urllib2.Request(http_query)
   data = urllib2.urlopen(request).read()
   print data
if __name__ == '__main__':
   if len(sys.argv) < 2:
      print 'usage:python query_cmcc_tool gcidfile [arg]'
   if len(sys.argv) == 2:
      gcid_list_file = sys.argv[1]
      query_by_gcid_file(gcid_list_file)
   if len(sys.argv) == 3:
      gcid = sys.argv[1]
      query_by_gcid(gcid)
   sys.exit(0)  
