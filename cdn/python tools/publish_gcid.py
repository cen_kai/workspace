# -*- coding: gbk -*-
import struct
import time
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
random.seed(time.time())

mgr_dns_list = ['tel.p2s.cl.kankan.com', 'tel-cl.kankan.xunlei.com', 'cnc.p2s.cl.kankan.xunlei.com', 'cnc-cl.kankan.xunlei.com']
mgr_belong_sp = {
		'tel.p2s.cl.kankan.com':'tel',
		'tel-cl.kankan.xunlei.com':'tel',
		'cnc.p2s.cl.kankan.xunlei.com':'cnc',
		'cnc-cl.kankan.xunlei.com':'cnc'
	}

#随机选取负载最小cdn    
def get_min_load_cdn(cdn_mgr_ip):
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid   100   unsigned char
	]
	cmd = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		struct.calcsize(''.join(fmt)) - 12,
		100,		
	)
	recv_str = send_query((cdn_mgr_ip, 80), cmd)

	resp_fmt = ['=',
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #CDN count			
	]
	cdn_fmt = [	
		'I', #IP
		'h', #port
	]
	resp_fmt_size = struct.calcsize(''.join(resp_fmt))	
	#print len(recv_str), resp_fmt_size
	items = struct.unpack(''.join(resp_fmt), recv_str[:resp_fmt_size])
	cdn_count = items[4]
	cdn_fmt_total = []
	#print cdn_count
	for i in range(0, cdn_count-1):
		cdn_fmt_total += cdn_fmt
	cdn_fmt_total = ['='] + cdn_fmt_total
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_total))	
	#print cdn_fmt, cdn_fmt_size, binascii.b2a_hex(recv_str[-cdn_fmt_size:])
	items = struct.unpack(''.join(cdn_fmt_total), recv_str[-cdn_fmt_size:])
	cdn_list = []
	for i in range(0, len(items)/2):		
		cdn_list.append(socket.inet_ntoa(struct.pack("I", items[i*2])))
	return random.choice(cdn_list)
	
#解析cdn
def parse_cdn(recv_str):
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP   unsigned int
		'h', #port short
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		'i', #filepath size
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))	
	items = struct.unpack(''.join(cdn_fmt_tpl), recv_str[:cdn_fmt_size])
	cdn_fmt = cdn_fmt_tpl + ['%ds' % (items[-1],)]
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items

#拼接查询字符串
def get_query_gcid_send_str(gcid):	
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver 协议版本号  int
		'i', #packageseq   包的验证码
		'i', #body len     主体长度
		'B', #cmd_typeid   命令类型id  unsigned char
		'i', #gcid_len     gcid长度
		'20s', #GCID       gcid        char[20]
	]
    #字节流组包
	x = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		25,
		121,
		20,
		binascii.a2b_hex(gcid) #将字符串转换为二进制, 并用十六进制表示
	)	
	return x

def parse_recv_str(recv_str):	 
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #status 20		
	]
	
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	if len(items) <> 6:		 
		return 'resp_err'
	status = items[5]
	if status == 0:
		return 'ok'
	elif status == 1:
		return 'duplicate'
	else:
		return 'other_fault'

def ip_aton(ip_str):
	return struct.unpack('I', socket.inet_aton(ip_str))[0]
	
def get_send_str(gcid, filesize, targ_cdn_ip):
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #gcid-length 20
		'20s', #GCID
		'Q', #filesize    unsigned long long
		'i', #target cdn ip
	]
	total_length =  struct.calcsize(''.join(fmt))
	#print total_length 
	x = struct.pack(''.join(fmt), 
		10,  #protocol_ver  10
		0x4c27d9e5, #magic_num  0x4c27d9e5
		int(time.time()), 
		total_length - 16, # total_length - ADMIN_CMD_HEADER_LEN
		79,  #cmd_typeid  CMD_TYPE_PUSH_FILE
		20,
		binascii.a2b_hex(gcid),
		filesize,
		ip_aton(targ_cdn_ip),
	)	
	return x

#发送查询字符串
def send_query(addr, query_str):
	#print 'send', binascii.b2a_hex(query_str)
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(30)
	sock.connect(addr)		
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*80)
	#print 'recv_str', binascii.b2a_hex(recv_str)
	return recv_str

#解析send_query返回的字符串 is_p2p = False
def parse_gcid_query(recv_str, is_p2p):
	fmt = ['=',
		'i', #protocol_ver 协议版本号  int
		'i', #packageseq   包的验证码
		'i', #body len     主体长度
		'B', #cmd_typeid   命令类型id  unsigned char
		'i', #CID len      cid长度
		'20s', #CID        cid         char[20]
		'Q', #filesize     文件大小    unsigned long long
		'i', #querytimes   查询次数
		'i', #CDN count    cdn个数
	]
			
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size]) #字节流解包
	#print items
	#print 'CID', binascii.b2a_hex(items[5]).upper(), 'filesize', items[6]
	filesize = int(items[6])
	cid = binascii.b2a_hex(items[5]).upper()
	total_cdn_count = items[-1]	
	buf_str = recv_str[fmt_size:]
	cdn_list = []
	
	while len(buf_str) > 0:
		used_len = 0
		result = None
		if is_p2p:    #False
			used_len, result = parse_p2p_cdn(buf_str)
		else:
			used_len, result = parse_cdn(buf_str) #解析cdn
		buf_str = buf_str[used_len:]		
		cdn_ip = socket.inet_ntoa(struct.pack("I", result[1]))
		report_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(result[7]))
		cdn_type = result[5]
		#print cdn_ip, cdn_type, report_time
		cdn_list.append((cdn_ip, cdn_type, report_time))
	return cid, filesize, cdn_list

#publish
#edit by liuyinfeng, 2014-04-15
#need_type_0_count 和 cdn_type_0_count 以及后续 if 判断未起作用, 故而删除
def pub_one(res, mgr_dns, gcid):
	print 'pub ', mgr_dns
	#need_type_0_count = 1
	cdn_mgr_ip = socket.gethostbyname(mgr_dns)		
	cid, filesize, cdn_list = res[mgr_dns]
	#print cid, filesize, cdn_list
	#cdn_type_0_count = 0	
	src_cdn_ip = ''
	need_dispatch = True #调度

    #检查本网机器内是否有拥有gcid资源的0号机
	for cdn in cdn_list: #(cdn_ip, cdn_type, report_time)
		if cdn[1] == 0:
			#cdn_type_0_count += 1
			#if cdn_type_0_count >= need_type_0_count:
			print 'nothing need to do.'
			need_dispatch = False
			break
		else:
			src_cdn_ip = cdn[0]

    #选取src_cdn_ip, 优先选取同网的磁盘柜
	if need_dispatch and src_cdn_ip == '':
		for dns in res:                    #(cid, filesize, cdn_list)
			cdn_list = res[dns][2]         #(cdn_ip, cdn_type, report_time)
			if len(cdn_list) > 0:
				src_cdn_ip = cdn_list[0][0]#500 号 cdn
				cid, filesize, cdn_list = res[dns]
				print 'Get a cdn ', src_cdn_ip, ' from ', dns
				if mgr_belong_sp[dns] == mgr_belong_sp[mgr_dns]:#src_cdn_ip 与 manger 同网
					break				

    #选取des_cdn_ip, 随机选取负载最小的cdn
	if need_dispatch and src_cdn_ip:		
		des_cdn_ip = get_min_load_cdn(cdn_mgr_ip)	
		print 'gcid =', gcid, 'src_cdn_ip =', src_cdn_ip, 'des_cdn_ip =', des_cdn_ip, ' cid=', cid, 'filesize=',filesize
		send_str = get_send_str(gcid, filesize, des_cdn_ip) #格式化命令字符串
		recv_str = send_query((src_cdn_ip, 3080), send_str) #推送资源
		print parse_recv_str(recv_str)
	if need_dispatch and src_cdn_ip == "":
		print 'It is a real trouble.'
	return src_cdn_ip

#查询有资源的cdn
def get_all_res(gcid):
	res = {}
	try:
        #电信网通四个manager
		for mgr_dns in mgr_dns_list:
			gcid_send_str = get_query_gcid_send_str(gcid)              #拼接查询字符串
			cdn_mgr_ip = socket.gethostbyname(mgr_dns)                 #获取manager ip
			recv_str = send_query((cdn_mgr_ip, 80), gcid_send_str)     #发送查询字符串
			cid, filesize, cdn_list = parse_gcid_query(recv_str, False)#解析返回字符串
			res[mgr_dns] = (cid, filesize, cdn_list)                   #格式化保存
	except Exception, err:
		print err
	return res
	
def main():
	import sys
	if len(sys.argv) <> 2:
		print 'usage: python publish_gcid {gcid}'
		return	
	gcid = sys.argv[1]
	res = get_all_res(gcid)  #查询有资源的cdn
	#print res
    
    #publish
	for mgr_dns in mgr_dns_list:
		try:				
			pub_one(res, mgr_dns, gcid)				
		except Exception, err:
			print err
	
if __name__ == '__main__':
	main()
	
