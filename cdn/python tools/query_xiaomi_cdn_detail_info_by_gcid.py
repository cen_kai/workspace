# -*- coding: utf8 -*-
import struct
import time
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 


def get_send_str(gcid):	
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #gcid_len
		'20s', #GCID
	]
	#print struct.calcsize(''.join(fmt))
	x = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		25,
		121,
		20,
		binascii.a2b_hex(gcid)
	)	
	return x

	
def check_one_dns(dns, query_str, is_p2p, port=80):
	ip = socket.gethostbyname(dns)	
	print ip
	recv_str = send_query((ip, 3088), query_str)	
	parse(recv_str, is_p2p)
	
def send_query(addr, query_str):
	#print 'send', binascii.b2a_hex(query_str)
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(40)
	sock.connect(addr)		
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*80)	
	return recv_str
	
	
def parse_p2p_cdn(recv_str):
	#print binascii.b2a_hex(recv_str)
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		#'i', #filepath size  p2p的cdnmgr协议略微不同，没有文件路径
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))
	cdn_fmt = cdn_fmt_tpl 
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items
	
def parse_cdn(recv_str):
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		'i', #filepath size
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))	
	items = struct.unpack(''.join(cdn_fmt_tpl), recv_str[:cdn_fmt_size])
	cdn_fmt = cdn_fmt_tpl + ['%ds' % (items[-1],)]
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items
	
def parse(recv_str, is_p2p):
	fmt = ['=',
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #CID len
		'20s', #CID
		'Q', #filesize
		'i', #querytimes
		'i', #CDN count
	]
			
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	#print items
	print 'CID', binascii.b2a_hex(items[5]).upper(), 'filesize', items[6]
	#print 'filesize', items[6]
	total_cdn_count = items[-1]	
	buf_str = recv_str[fmt_size:]
	while len(buf_str) > 0:
		used_len = 0
		result = None
		if is_p2p:			
			used_len, result = parse_p2p_cdn(buf_str)
		else:
			used_len, result = parse_cdn(buf_str)
		buf_str = buf_str[used_len:]		
		print socket.inet_ntoa(struct.pack("I", result[1])), result[5], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(result[7]))
		#print 
		
if __name__ == '__main__':
	import sys
	#print sys.argv
	gcid = 'DA79D4E29CEF629DCE4275A17859BAB92A921A21'
	if len(sys.argv) > 1:
		gcid = sys.argv[1]
	print 'GCID:', gcid
	print 'OUTPUT FORMAT:'
	print '{IP}\t{CDN_TYPE}\t{INSERT_TIME}'
	dns_list = [ 
        #('电信首缓冲', 'tel.web.cl.kankan.xunlei.com', True), 		
        #('电信p2s', 'tel.p2s.cl.kankan.com', False), 
        ('xiaomi cnc p2s', 'cnc1.p2s.xiaomi.kankan.com', False), 
		#('电信p2p', 'tel-cl.kankan.xunlei.com', False), 
        #('网通首缓冲','cnc.web.cl.kankan.xunlei.com', False),
       # ('网通p2s','cnc.p2s.cl.kankan.xunlei.com', False),
        #('网通p2p','cnc-cl.kankan.xunlei.com', False),
		#('电信商城', 'tel.cdnmgr.shop.xunlei.com', False),
		#('网通商城', 'cnc.cdnmgr.shop.xunlei.com', False),
		
    ]
	query_str = get_send_str(gcid)
	for dns in dns_list:
		print dns[0].decode('utf8'), 'CDN:'
		check_one_dns(dns[1], query_str, dns[2])
	'''
	check_one_dns('cnc-cl1.kankan.xunlei.com', query_str, False, 81)
	check_one_dns('cnc-cl2.kankan.xunlei.com', query_str, False, 81)
	'''