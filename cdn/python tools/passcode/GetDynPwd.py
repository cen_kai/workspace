# coding=utf8
# This automatically generated script may need to be
# edited in order to work correctly.
#author liuxiaojie
import os, sys, string
import poplib
import time
from email import parser

# pop3服务器地址
host = "pop3.xunlei.com"
#host = "pop.qq.com"
# 用户名
username_suffix = "@xunlei.com"
#username_suffix = "@qq.com"

def GetPassCodeFromEmail(host, username, password):

    # 创建一个pop3对象，这个时候实际上已经连接上服务器了
    pp = poplib.POP3(host)
    # 设置调试模式，可以看到与服务器的交互信息
    #pp.set_debuglevel(1)
    # 向服务器发送用户名 
    pp.user(username)
    # 向服务器发送密码
    pp.pass_(password)
    # 获取服务器上信件信息，返回是一个列表，第一项是一共有多上封邮件，第二项是共有多少字节
    count,size = pp.stat()
    #print count

    def is_essh_email(message):
        for piece in message:
            if piece.startswith('Subject: Essh Passcode Mail'):  
                return True
        return False

    #邮件ID(从大到小开始接收最近12封邮件)
    ids = range(count-12, count+1)[::-1]
    passcode = ""
    for i in ids:
        message = pp.retr(i)[1]
        print(message)
        if is_essh_email(message):
            # Concat message pieces:
            #print 'is essh email'
            message = "\n".join(message)
            print(message)
            #Parse message intom an email object:
            message = parser.Parser().parsestr(message)
            charset = message.get_content_charset()
            # content format: user: username passcode: 000111
            content =  message.get_payload(decode=True)
            #print(content)
            idx = content.find("passcode: ")
            if idx > 0:
                passcode = content[idx+len("passcode: "):].strip()
                break
    # 退出
    pp.quit()
    return passcode

if __name__ == '__main__':
    import sys,time
    if len(sys.argv) < 4:
        print ("python %s [username] [password] [outfile] [force_flag]" % (sys.argv[0]))
        sys.exit(1)
	
    username = sys.argv[1] + username_suffix
    password = sys.argv[2]
    outfile  = sys.argv[3]
    force_flag=0
    time.sleep(6)
    if len(sys.argv) > 4:
        force_flag=int(sys.argv[4])
    print force_flag
	#若文件存在，比较创建时间和修改，若大于1小时，从邮件中重新获取验证码
    if force_flag>0:
        before_1h_now = time.time()-300
        if os.path.exists(outfile):
        #print 'outfile had '
            if os.path.getctime(outfile)>before_1h_now or os.path.getmtime(outfile)>before_1h_now:
                print 'already had passcode'
                sys.exit(1)
	
    passcode = ""
    passcode = GetPassCodeFromEmail(host, username, password)
    #创建文件
    print passcode
    file = open(outfile,'w')
    file.write(passcode)
    file.close()
