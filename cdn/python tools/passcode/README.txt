﻿1.#用途
登录essh.sandai.net时，自动输入passcode


2.依赖python 2.7.2库

3.配置
步骤1.参考
http://blog.csdn.net/fly542/article/details/7513571
步骤2.
见截图


4.原理
GetDynPwn.py 读取邮件的passcode,更新passcode.txt内容。
passcode.vbs  首先创建进程执行GetDynPwn.py 代码，然后读取passcode.txt里面的passcode,键入passcode到xshell里面

5.常见问题
passcode.vbs文件的编码对该脚本会有影响，测试过utf-8 无bom类型在win 8.1是通过的