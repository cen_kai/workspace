#!/usr/bin/env python
#coding: utf-8
import struct
import time
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
exclude_ips = ['121.9.214.3',]
def parse_recv_str(recv_str):
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #status 20		
	]
	
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	if len(items) <> 6:
		print 'resp格式有问题'
		return
	status = items[5]
	if status == 0:
		print '删除成功'
	elif status == 1:
		print '没有这个文件'
	else:
		print '其他错误'
	
def get_send_str(cid, filesize):
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver  10
		'i', #magic_num  0x4c27d9e5
		'i', #m_timestamp
		'i', #cmd_length
		'h', #cmd_typeid		
		'i', #cid-length 20
		'20s', #CID
		'Q', #filesize
	]
	total_length =  struct.calcsize(''.join(fmt))
	#print total_length 
	x = struct.pack(''.join(fmt), 
		10,  #protocol_ver  10
		0x4c27d9e5, #magic_num  0x4c27d9e5
		int(time.time()), 
		total_length - 16, # total_length - ADMIN_CMD_HEADER_LEN
		501,  #cmd_typeid			
		20,
		binascii.a2b_hex(cid),
		filesize,
	)	
	return x


	
def send_query(addr, query_str):
	#print 'send', binascii.b2a_hex(query_str)
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(15)
	sock.connect(addr)		
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*80)
	#print 'recv_str', binascii.b2a_hex(recv_str)
	return recv_str
''' 
query a gcid from cdn manager
'''
dns_list = [ 
		#('tel', 'tel.p2s.cl.kankan.com', False),
		('tel', '119.147.41.142', False),
]
#query a gcid from which cdn
def get_query_str(gcid):	
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #gcid_len
		'20s', #GCID
	]
	#print struct.calcsize(''.join(fmt))
	x = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		25,
		121,
		20,
		binascii.a2b_hex(gcid)
	)	
	return x

	
def check_one_dns(dns, query_str, is_p2p, port=80):
	ip = socket.gethostbyname(dns)	
	#print ip
	recv_str = send_query((ip, 3088), query_str)	
	#recv_str = send_query((ip, 80), query_str)	
	cid,file_size,cdn_list = parse(recv_str, is_p2p)
	return cid,file_size,cdn_list
	
	
def parse_p2p_cdn(recv_str):
	#print binascii.b2a_hex(recv_str)
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		#'i', #filepath size  p2p的cdnmgr协议略微不同，没有文件路径
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))
	cdn_fmt = cdn_fmt_tpl 
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items
	
def parse_cdn(recv_str):
	cdn_fmt_tpl = ['=',
		'i', #len
		'I', #IP
		'h', #port
		'h', #province
		'h', #city
		'h', #type
		'i', #query_times
		'i', #insert_time
		'i', #report_time
		'i', #filepath size
	]	
	cdn_fmt_size = struct.calcsize(''.join(cdn_fmt_tpl))	
	items = struct.unpack(''.join(cdn_fmt_tpl), recv_str[:cdn_fmt_size])
	cdn_fmt = cdn_fmt_tpl + ['%ds' % (items[-1],)]
	items = struct.unpack(''.join(cdn_fmt), recv_str[: struct.calcsize(''.join(cdn_fmt))])	
	return struct.calcsize(''.join(cdn_fmt)), items
	
def parse(recv_str, is_p2p):
	fmt = ['=',
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #CID len
		'20s', #CID
		'Q', #filesize
		'i', #querytimes
		'i', #CDN count
	]
			
	fmt_size = struct.calcsize(''.join(fmt))
	items = struct.unpack(''.join(fmt), recv_str[:fmt_size])
	#print items
	cid = binascii.b2a_hex(items[5]).upper()
	file_size = items[6]
	#print 'filesize', items[6]
	total_cdn_count = items[-1]	
	buf_str = recv_str[fmt_size:]
	cdn_list = []
	while len(buf_str) > 0:
		used_len = 0
		result = None
		if is_p2p:			
			used_len, result = parse_p2p_cdn(buf_str)
		else:
			used_len, result = parse_cdn(buf_str)
		buf_str = buf_str[used_len:]		
		#print socket.inet_ntoa(struct.pack("I", result[1])), result[5], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(result[7]))
		cdn_list.append((socket.inet_ntoa(struct.pack("I", result[1])),result[5],))
	return cid,file_size,cdn_list


def main():
    import sys
    if len(sys.argv) <> 2:
        print 'usage: python del_cdn_file {gcid} '
        return
    #cdn_ip = sys.argv[1]
    gcid = sys.argv[1]
    cdn_port = 3080
    #exclude_ip = sys.argv[3]
    query_str = get_query_str(gcid)
    for dns in dns_list:
        cid,file_size,cdn_list = check_one_dns(dns[1], query_str, dns[2])
    print cid,file_size,cdn_list
    cdn_500 = []
    cdn_0 = []
    need_del = False
    for cdn in cdn_list:
        if cdn[1] == 500:
		   cdn_500.append(cdn[0])
        if cdn[1] == 0:
		   cdn_0.append(cdn[0])
    print 'CDN of type 500:',cdn_500
    print 'CDN of type 0:',cdn_0
	#exclude_ip是指flv的机器 flv的机器一定要把资源调度过去了才删除
    for exclude_ip in exclude_ips:
        if exclude_ip in cdn_0:
            cdn_0.remove(exclude_ip)
            need_del = True
    print cdn_0
    num = len(cdn_0)
    if need_del and len(cdn_0) > 0:
        for cdn_ip in cdn_0:
            if num > 0:
                print 'delete gcid fromd ',cdn_ip		
                send_str = get_send_str(cid, file_size)
                recv_str = send_query((cdn_ip, cdn_port), send_str)
                parse_recv_str(recv_str)
                num = num - 1
            else:
                break
	
if __name__ == '__main__':
	main()
	
