# -*- coding: gbk -*-
import struct
import binascii
import random
import socket
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

_dns_map = {}

def get_send_str(gcid):	
	fmt=['=	', #紧凑拼接
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #gcid_count 这里只放一个
		'20s', #GCID
	]
	#print struct.calcsize(''.join(fmt))
	x = struct.pack(''.join(fmt), 
		40,
		random.randint(0, 10000000),
		25,
		150,
		1,
		binascii.a2b_hex(gcid)
	)
	
	return x

def check_one_dns(dns, query_str):
	if dns not in _dns_map:
		_dns_map['dns'] = socket.gethostbyname(dns)
	ip = _dns_map['dns']	
	#print dns, ip
	recv_str = send_query((ip, 80), query_str)
	return parse(recv_str)
	
def send_query(addr, query_str):
	#print 'send', binascii.b2a_hex(query_str)
	sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(15)
	sock.connect(addr)	
	#sock.connect(('121.14.82.10', 80))	
	sock.send(query_str)       
	recv_str, address = sock.recvfrom(1024*10)
	#print 'recv', binascii.b2a_hex(recv_str)
	return recv_str
	
def parse(recv_str):
	fmt = ['=',
		'i', #protocol_ver
		'i', #packageseq
		'i', #body len
		'B', #cmd_typeid
		'i', #gcid_count 这里只放一个
		'20s', #GCID
		'i', # CDN count
	]
	fmt_part_size = struct.calcsize(''.join(fmt))
	ver, seq, body_len, cmd_type, gcid_count, gcid, cdn_count = struct.unpack(''.join(fmt), recv_str[:fmt_part_size])
	for i in range(0, cdn_count):
		fmt += ['i', 'h']
	#print struct.calcsize(''.join(fmt)), len(recv_str)	
	items = struct.unpack(''.join(fmt), recv_str[:struct.calcsize(''.join(fmt))])	
	cdn_ip = []
	for i in range(0, cdn_count):
		cdn_ip.append( (socket.inet_ntoa(struct.pack("I",items[7+2*i])), items[7+2*i+1]) )
	return cdn_ip
	
if __name__ == '__main__':
	import sys
	#print sys.argv
	gcid_list = ['55D3183877F9D5755057FAAC033329D94B4625C0']
	if len(sys.argv) > 1:
		gcid_list = open(sys.argv[1], 'r')
	
	dns_list = [ 
        #('电信首缓冲', 'tel.web.cl.kankan.xunlei.com'), 
        ('电信p2s', 'tel.p2s.cl.kankan.xunlei.com'), 
        ('电信p2p', 'tel-cl.kankan.xunlei.com'),
		#('电信商城', 'tel.cdnmgr.shop.xunlei.com'),
        #('网通首缓冲','cnc.web.cl.kankan.xunlei.com'),
        ('网通p2s','cnc.p2s.cl.kankan.xunlei.com'),
        ('网通p2p','cnc-cl.kankan.xunlei.com'),
		#('网通商城', 'cnc.cdnmgr.shop.xunlei.com')
    ]
	for line in gcid_list:
		gcid = line[:40]
		print gcid,
		query_str = get_send_str(gcid)
		for dns in dns_list:
			#print dns[0].decode('gbk'), 'CDN:'
			try:
				result = check_one_dns(dns[1], query_str)
				if len(result) < 1:
					print '缺'+dns[0],
			except:
				pass
		print
	