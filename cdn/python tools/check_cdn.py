# -*- coding: utf8 -*-

import re
import struct
import binascii
import random
import socket
import warnings
import datetime

warnings.filterwarnings("ignore", category=DeprecationWarning) 

g_gcid_to_cdns = {}
g_gcid_miss = {}
g_miss_cdn_stat = {}
g_gcid_to_path = {}
g_now = datetime.datetime.now()
g_now_secs = g_now.hour * 3600 + g_now.minute * 60 + g_now.second
g_ignore_mgr_ip = socket.gethostbyname_ex("cdnmgr.shop.xunlei.com")[2]

def get_send_str(gcid):    
    fmt=['=', #紧凑拼接
        'i', #protocol_ver
        'i', #packageseq
        'i', #body len
        'B', #cmd_typeid
        'i', #gcid_count 这里只放一个
        '20s', #GCID
    ]
    #print struct.calcsize(''.join(fmt))
    x = struct.pack(''.join(fmt), 
        40,
        random.randint(0, 10000000),
        25,
        150,
        1,
        binascii.a2b_hex(gcid)
    )
    
    return x

def check_one_dns(dns, query_str):
    ip = socket.gethostbyname(dns)    
    recv_str = send_query((ip, 80), query_str)
    return parse(recv_str)
    
def send_query(addr, query_str):
    sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(30)
    sock.connect(addr)    
    sock.send(query_str)       
    recv_str, address = sock.recvfrom(1024*10)
    return recv_str
    
def parse(recv_str):
    fmt = ['=',
        'i', #protocol_ver
        'i', #packageseq
        'i', #body len
        'B', #cmd_typeid
        'i', #gcid_count 这里只放一个
        '20s', #GCID
        'i', # CDN count
    ]
    fmt_part_size = struct.calcsize(''.join(fmt))
    ver, seq, body_len, cmd_type, gcid_count, gcid, cdn_count = struct.unpack(''.join(fmt), recv_str[:fmt_part_size])
    for i in range(0, cdn_count):
        fmt += ['I', 'h']
    #print struct.calcsize(''.join(fmt)), len(recv_str)    
    items = struct.unpack(''.join(fmt), recv_str[:struct.calcsize(''.join(fmt))])    
    cdn_ip = []
    for i in range(0, cdn_count):
        cdn_ip.append(socket.inet_ntoa(struct.pack("I",items[7+2*i])))
    
    #解析cach
    cache_cdn_str = recv_str[struct.calcsize(''.join(fmt)):]
    cache_cdn_fmt = ['=', 'i']
    cache_cdn_fmt_part_size = struct.calcsize(''.join(cache_cdn_fmt))
    items = struct.unpack(''.join(cache_cdn_fmt), cache_cdn_str[:cache_cdn_fmt_part_size])
    cache_cdn_count = items[0]
    for i in range(0, cache_cdn_count):
        cache_cdn_fmt += ['I', 'h']
    items = struct.unpack(''.join(cache_cdn_fmt), cache_cdn_str)
    cache_cdn_ip = []
    for i in range(0, cache_cdn_count):
        cache_cdn_ip.append(socket.inet_ntoa(struct.pack("I",items[1+2*i])))

    return cdn_ip + cache_cdn_ip

def get_gcid_cdn_list(log_file_name):
    for line in open(log_file_name):
        m = re.match('\[(\d*)\].*AdminCmdAddPubStorageFile.*m_filepath=(\S*),.*gcid=(.*) m_flag.*', line)
        if m and len(m.groups()) == 3:
            g_gcid_to_path[m.groups()[2]] = m.groups()[1]
  
        m = re.match('\[(\d*)\].*AdminCmdPubFileFromStorage.*m_gcid=(\S*),.*m_cdn_ip=(.*)]], from:(.*)', line)
        if not m:
            continue 
        if len(m.groups())==4:
            t = m.groups()[0]
            sec = int(t[:2])*3600 + int(t[2:4]) * 60 + int(t[4:])
            #print t, sec, g_now_secs
            '''
            if g_now_secs - sec < 30*60: #10分钟前的，先不查
                continue
            '''
            gcid = m.groups()[1]
            cdn = m.groups()[2]
            mgr_ip = m.groups()[3]
            global g_ignore_mgr_ip
            #print mgr_ip, g_ignore_mgr_ip
            if mgr_ip in g_ignore_mgr_ip:
                continue
            if gcid not in g_gcid_to_cdns:
                g_gcid_to_cdns[gcid] = []
        g_gcid_to_cdns[gcid].append(cdn)
    #print g_gcid_to_cdns

      
if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        get_gcid_cdn_list(filename)
    else:
        print "usage check_cdn.py {cdn_major_cmd_stat_file}"
		
    for gcid in g_gcid_to_cdns:
        #print gcid
        query_str = get_send_str(gcid)
        all_cdn = []
        #for dns in ('web.cl.kankan.xunlei.com', 'p2s.cl.kankan.xunlei.com', 'cl.kankan.xunlei.com'): #, 'cdnmgr.shop.xunlei.com'):
        for dns in ('p2s.cl.kankan.xunlei.com', 'cl.kankan.xunlei.com',): # 'cdnmgr.shop.xunlei.com'):
            all_cdn += check_one_dns(dns, query_str)
        #print all_cdn
        for need_cdn in g_gcid_to_cdns[gcid]:
            if need_cdn not in all_cdn:
                #print 'miss one', need_cdn
                if need_cdn not in g_miss_cdn_stat:
                    g_miss_cdn_stat[need_cdn] = 0
                g_miss_cdn_stat[need_cdn] += 1
                if gcid not in g_gcid_miss:
                    g_gcid_miss[gcid] = []
                g_gcid_miss[gcid].append(need_cdn)
    sort_cdn = []
    for cdn in g_miss_cdn_stat:
        #print cdn, g_miss_cdn_stat[cdn]
        sort_cdn.append((g_miss_cdn_stat[cdn], cdn))
    sort_cdn.sort()
    sort_cdn.reverse()
    trouble_str = ''
    for x in sort_cdn:
        trouble_str += str(x[0]) + '\t' + str(x[1]) + '\r\n'
    for gcid in g_gcid_miss:
        if gcid not in g_gcid_to_path:
            g_gcid_to_path[gcid] = 'unknown path...'
        trouble_str += gcid + '\r\n'
        trouble_str += g_gcid_to_path[gcid] + '\r\n'
        for cdn in g_gcid_miss[gcid]:
            trouble_str += cdn + '\t'
        trouble_str += '\r\n\r\n'
    if trouble_str <> '':
        print "We got big trouble:"
        print trouble_str
    else:
        print 'What a nice day!'
