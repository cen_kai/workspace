import sys
import re
import os
import urllib2
import datetime
def check_download_fail_cdn(stat_fail_file):
     query_url = "http://cnc.p2s.cl.kankan.com/getCdnresource_flv?gcid="
     i = 1
     stat_failed_cdn = {}
     gcid_no_cdn = []
     for line in open(stat_fail_file):
        if i == 1:
            i=i+1
            continue
        else:
            gcid = line[:40]
            gcid,num = line.split('\t')
            print gcid,num
            http_query= query_url+gcid
            print http_query
            try:
                 request = urllib2.Request(http_query)
                 data = urllib2.urlopen(request).read()
                 m = re.search(r'.*\ncdnlist1:.*\n.*ip:"([\d|\.]*)",port:(\d*),path:"(.*)"\}',data)
                 if m:
                    print m.group(1),m.group(2),m.group(3)
                    cdn_ip = m.group(1)
                    cdn_path = m.group(3)
                    if cdn_ip not in stat_failed_cdn:
                         stat_failed_cdn[cdn_ip] = []
                    stat_failed_cdn[cdn_ip].append(gcid)
                 else:
                    print "cdn not exisit"
                    gcid_no_cdn.append(gcid)
            except  Exception,e:
                 print e
     now_time = datetime.datetime.now()
     yes_time = now_time+datetime.timedelta(days=-1)
     cdn_filename = '../stat/stat_cnc_cdn_download_error_'+yes_time.strftime('%Y%m%d')
     gcid_filename = '../stat/stat_cnc_gcid_not_exist_'+yes_time.strftime('%Y%m%d')
     print "cdn gcid can not download:"
     #print stat_failed_cdn
     sort_list = sorted(stat_failed_cdn.iteritems(),key=lambda d:len(d[1]),reverse = True)
     print sort_list
     fp1 = open(cdn_filename,'w')
     for item in sort_list:
         if len(item[1]) > 2:
            fp1.write(item[0])
            fp1.write((' : %d\n[\n' %len(item[1])))
            j = 1
            for gcid in item[1]:
                  if j%3 == 0: 
                      fp1.write('\n') 
                      j = 1 
                  fp1.write(gcid)
                  fp1.write(',')
                  j=j+1
            fp1.write('\n]\n')
     fp1.close()
     print "gcid ,cnc cdn not exist:"
    # print gcid_no_cdn
     fp2 = open(gcid_filename,'w')
     for i in gcid_no_cdn:
         fp2.write(i)
         fp2.write('\n')
     fp2.close()
if __name__ == '__main__':
     if len(sys.argv) != 2 :
        print 'usage check_download_ip.py stat_faile_dowload_file'
     stat_fail_file = sys.argv[1]
     check_download_fail_cdn(stat_fail_file)
     sys.exit(0)

