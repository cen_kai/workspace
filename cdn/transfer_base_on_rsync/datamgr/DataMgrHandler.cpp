//==============================================================
// DBHandleThread.cpp : implementation of class DBHandleThread 
//			   (worker thread for DB update tasks)
//
// tianming
// Created: 2009-01-21
//===============================================================

#include "DataMgrHandler.h"
#include "common/Utility.h"
#include "DataTask_define.h"
#include <fstream>
#include <algorithm>
#include "distserver/SendFileThreadPool.h"
#include <iterator>

extern DistServerApp g_app;

IMPL_LOGGER(DataMgrHandler, logger);

DataMgrHandler* DataMgrHandler::instance = NULL;

DataMgrHandler* DataMgrHandler::get_instance()
{
	if (instance == NULL)
	{
		instance = new DataMgrHandler();
	}

	return instance;
}
	
DataMgrHandler::DataMgrHandler() 
{
	init();
}

DataMgrHandler::~DataMgrHandler()
{
	if (m_task_queue != NULL)
	{
		#if 0
		persist_task *task = NULL;
		while(m_task_queue->pop(task, 200))
		{
			delete task;
			task = NULL;
		}
		#endif

		delete m_task_queue;
		m_task_queue = NULL;
	}
}

void DataMgrHandler::init()
{
	read_conf();
}

void DataMgrHandler::read_conf()
{
	LOG4CPLUS_DEBUG(logger, "read_conf entering...");
		
	sdqueue_capacity = g_app.get_conf_int("transfer.sdqueue.capacity", 1024);
	m_task_queue = new SDQueue<persist_task>(sdqueue_capacity);

	unfinished_file = g_app.get_conf_string("transfer.unfinished.file", "../backup/unfinished_tasks.txt");
	finished_file = g_app.get_conf_string("transfer.finished.file", "../backup/finished_tasks.txt");

	unfinished_file_temp = unfinished_file + ".temp";
}

void DataMgrHandler::add_request(persist_task req)
{
	LOG4CPLUS_DEBUG(logger, "add_request entering...");

	if (req.task_status != 0)
	{
		m_task_queue->push(req);
	}
	else
	{
		if (!is_task_existed(req))
		{
			m_task_queue->push(req);
		}
		else
		{
			LOG4CPLUS_WARN(logger, "add_request,received repeart task,cid="<<Utility::bytes_to_hex((const byte*)req.cid, MAX_CID_LEN));
		}
	}
}

bool DataMgrHandler::is_task_existed(persist_task req)
{
	LOG4CPLUS_DEBUG(logger, "is_task_existed entering...");

	int result = false;
	string key = Utility::bytes_to_hex((const byte*)req.cid, MAX_CID_LEN);
	map<string, persist_task>::iterator ite = map_unfinished_task.find(key);
	if ( ite != map_unfinished_task.end())
	{
		result = true;
		LOG4CPLUS_DEBUG(logger, "is_task_existed,found repeat task");
	}

	return result;
}

//true :changed  false:not changed
bool DataMgrHandler::delete_task(persist_task req)
{
	LOG4CPLUS_DEBUG(logger, "delete_task entering...");

	int result = false;
	string key = Utility::bytes_to_hex((const byte*)req.cid, MAX_CID_LEN);
	map<string, persist_task>::iterator ite = map_unfinished_task.find(key);
	if ( ite != map_unfinished_task.end())
	{
		map_unfinished_task.erase(ite);
		result = true;
		LOG4CPLUS_DEBUG(logger, "after delete_task,map_unfinished_task.size="<<map_unfinished_task.size());
	}
	else
	{
		LOG4CPLUS_WARN(logger, "delete_task,can't find task for cid="<<key);
	}

	return result;
}

void DataMgrHandler::run()
{
	LOG4CPLUS_DEBUG(logger, "run entering...");

	//persist_task task = NULL;
	while(true)
	{
		persist_task task;
		if (m_task_queue->pop(task, 200))
		{
			handle_data_task(task);
			//delete task;
			//task = NULL;
		}
	}
}

void DataMgrHandler::handle_data_task(persist_task task)
{
	LOG4CPLUS_DEBUG(logger, "handle_data_task entering...");
	
	int task_status = task.task_status;
	if (task_status == 0)
	{
		handle_unfinished_task(task);
	}
	else
	{
		handle_finished_task(task);
	}
	
	return ;
}

void DataMgrHandler::handle_unfinished_task(persist_task task)
{
	LOG4CPLUS_DEBUG(logger, "handle_unfinished_task entering...");
	
	string key = Utility::bytes_to_hex((const byte*)task.cid, MAX_CID_LEN);
	map_unfinished_task.insert(map<string, persist_task>::value_type(key, task));
	backup_unfinished_tasks();
}

void DataMgrHandler::handle_finished_task(persist_task task)
{
	LOG4CPLUS_DEBUG(logger, "handle_finished_task entering...");

	if ( delete_task(task) )
	{
		backup_unfinished_tasks();
	}

	append_finished_tasks(task);
}

bool DataMgrHandler::load_unfinished_tasks()
{
	LOG4CPLUS_DEBUG(logger, "load_unfinished_tasks entering...");

	bool result = false;

	ifstream fin(unfinished_file.c_str());
	if (!fin)
	{
		int this_errno = errno;
		LOG4CPLUS_THIS_INFO(logger, "Failed to open the data file:"<<unfinished_file<<",errno="<<this_errno );

		//file is not exist,maybe the first time to run
		if (this_errno == 2)
		{
			result = true;
		}

		return result;
	}

	int valid_task = 0;
	string tempStr("");
	while (getline(fin, tempStr, '\n'))
	{
		//persist_task * task = str_2_task(tempStr);
		persist_task task ;
		bool result = task.str_2_task(tempStr);
		if (result)
		{
			string key = Utility::bytes_to_hex((const byte*)task.cid, MAX_CID_LEN);
			map_unfinished_task.insert(map<string, persist_task>::value_type(key, task));
			++valid_task;
		}
		else
		{
			LOG4CPLUS_THIS_INFO(logger, "str is invalid:"<<tempStr );
		}
	} 
	
	fin.close();

	map<string, persist_task>::iterator ite = map_unfinished_task.begin();
	map<string, persist_task>::iterator ite_end = map_unfinished_task.end();
	for (; ite != ite_end; ++ite)
	{
		SendFileThreadPool::get_instance()->add_request(ite->second);
	}

	LOG4CPLUS_THIS_INFO(logger, "load unifinished task num:"<<map_unfinished_task.size() );
	result = true;
	return result;
}

void DataMgrHandler::backup_unfinished_tasks()
{
	LOG4CPLUS_DEBUG(logger, "backup_unfinished_tasks entering...");

	if (map_unfinished_task.empty())
	{
		LOG4CPLUS_DEBUG(logger, "unfinished_file is empty,just clear...");
		ofstream fout1(unfinished_file.c_str(), ios_base::out);
		if (!fout1)
		{
			LOG4CPLUS_THIS_INFO(logger, "Failed to open the data file:"<<unfinished_file );
			return ;
		}
		fout1.close();

		return;
	}

	ofstream fout(unfinished_file_temp.c_str(), ios_base::out);
	if (!fout)
	{
		LOG4CPLUS_THIS_INFO(logger, "Failed to open the data file:"<<unfinished_file_temp );
		return ;
	}

	ostream_iterator<string> os(fout, "\n");  
	vector<string> all_unfinished_tasks_str;
	map<string, persist_task>::iterator ite = map_unfinished_task.begin();
	map<string, persist_task>::iterator ite_end = map_unfinished_task.end();
	for (; ite != ite_end; ++ite)
	{
		persist_task& temp_task = ite->second;
		string temp = temp_task.task_to_str();
		if (!temp.empty())
		{
			all_unfinished_tasks_str.push_back(temp);
		}
	}
	copy(all_unfinished_tasks_str.begin(), all_unfinished_tasks_str.end(), os);

	fout.close();

	rename (unfinished_file_temp.c_str(), unfinished_file.c_str());

	return;
}

void DataMgrHandler::append_finished_tasks(persist_task task)
{
	LOG4CPLUS_DEBUG(logger, "append_finished_tasks entering...");

	ofstream fout(finished_file.c_str(), ios_base::app);
	if (!fout)
	{
		LOG4CPLUS_THIS_INFO(logger, "Failed to open the data file:"<<finished_file );
		return ;
	}

	//string content = task_2_str(task);
	string content = task.task_to_str();
	fout<<content<<endl;

	fout.close();

	return;
}

#if 0
string DataMgrHandler::task_2_str(persist_task&  task)
{
	//LOG4CPLUS_DEBUG(logger, "task_2_str entering...");
	//LOG4CPLUS_DEBUG(logger, "task_2_str 2 entering...");
	
	//char temp[256];
	char* temp = new char[256];
	//LOG4CPLUS_DEBUG(logger, "task_2_str 3 entering...");
	sprintf(temp, "%s %llu %s  %s %s %s %d", Utility::bytes_to_hex((const byte*)task.cid, MAX_CID_LEN).c_str(), 
		task.filesize, task.file_src_ip, task.file_abs_path, task.task_start_time,
		task.task_end_time, task.task_status);

	return (string)temp;
}

bool DataMgrHandler::str_2_task(string& str, persist_task& task)
{
	LOG4CPLUS_DEBUG(logger, "str_2_task entering...");

	bool result = false;
	std::vector<std::string> result_array;
	int size = Utility::split(str, " ", result_array);
	if (size != 7)
	{
		LOG4CPLUS_WARN(logger, str<<" is not valid task format");
		return result;
	}

	//persist_task task ;//= new persist_task();
	string temp_str = result_array[0];
	int len = MAX_CID_LEN;
	result = Utility::hex_text_to_bytes(temp_str, (byte*)task.cid, len);
	if (!result)
	{
		LOG4CPLUS_WARN(logger, "cid is not valid task format,cid=" <<result_array[0]);
		//delete task;
		//task = NULL;
		return result;
	}

	task.filesize = atoll(result_array[1].c_str());
	task.file_src_ip = result_array[2];
	task.file_abs_path = result_array[3];
	task.task_start_time = result_array[4];
	task.task_end_time = result_array[5];
	task.task_status = atoi(result_array[6].c_str());

	task.task_from = 0;

	result = true;
	return result;
}

#endif
