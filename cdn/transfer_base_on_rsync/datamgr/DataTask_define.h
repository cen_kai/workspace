#ifndef __DataTask_H_20090121_10
#define  __DataTask_H_20090121_10

#include <string>
#include "common/Utility.h"
#include "common/common.h"

using std::string;

//template <_u32 _MAX_TIME>
//log4cplus::Logger persist_task<_MAX_TIME>::logger = log4cplus::Logger::getInstance("persist_task");

class persist_task
{

public:
	persist_task()
	{
		task_from = 1;
	}
	
	char cid[MAX_CID_LEN];
	_u64 filesize;
	string file_src_ip;
	string file_abs_path;  //include path and name
	string task_start_time;
	string task_end_time;
	int task_status;    //0 未处理，小于0 失败，大于0 成功 (1 取文件成功，2文件已存在)

	int task_from;  //任务来源: 0 来自备份，1来自外部请求

	string task_to_str()
	{
		char temp[512];
		//LOG4CPLUS_DEBUG(logger, "task_2_str 3 entering...");
		sprintf(temp, "%s#%llu#%s#%s#%s#%s#%d", Utility::bytes_to_hex((const byte*)cid, MAX_CID_LEN).c_str(), 
			filesize, file_src_ip.c_str(), file_abs_path.c_str(), task_start_time.c_str(),
			task_end_time.c_str(), task_status);

		return (string)temp;
	}

	bool str_2_task(string& str)
	{
		//LOG4CPLUS_DEBUG(logger, "str_2_task entering...");

		bool result = false;
		std::vector<std::string> result_array;
		int size = Utility::split(str, "#", result_array);
		if (size != 7)
		{
			//LOG4CPLUS_WARN(logger, str<<" is not valid task format");
			return result;
		}

		//persist_task task ;//= new persist_task();
		string temp_str = result_array[0];
		int len = MAX_CID_LEN;
		result = Utility::hex_text_to_bytes(temp_str, (byte*)cid, len);
		if (!result)
		{
			//LOG4CPLUS_WARN(logger, "cid is not valid task format,cid=" <<result_array[0]);
			//delete task;
			//task = NULL;
			return result;
		}

		filesize = atoll(result_array[1].c_str());
		file_src_ip = result_array[2];
		file_abs_path = result_array[3];
		task_start_time = result_array[4];
		task_end_time = result_array[5];
		task_status = atoi(result_array[6].c_str());

		task_from = 0;

		result = true;
		return result;
	}

private:
	//DECL_LOGGER(logger);
};

//IMPL_LOGGER(persist_task, logger);

#endif  //__DataTask_H_20090121_10
