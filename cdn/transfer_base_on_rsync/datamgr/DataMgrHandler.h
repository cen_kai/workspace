//==============================================================
// DataMgrHandler.h : interface of class DataMgrHandler 
//			  
//
// tianming
// Created: 2009-01-21
// 实现功能:
// 加载未完成的任务列表，如果未完成的任务列表有变动，
// 刷新相应的文件，如果任务完成，追加到已完成任务文件中
//===============================================================

#ifndef __DATAMGR_HANDLE_THREAD_H_20090121_10
#define  __DATAMGR_HANDLE_THREAD_H_20090121_10

#include "common/SDThread.h"
#include "common/SDLogger.h"
#include "common/SDQueue.h"
#include "common/common.h"
#include "common/Utility.h"
#include <vector>
#include <string>
#include "distserver/DistServerApp.h"

class persist_task;

class DataMgrHandler : public SDThread
{
public:	
	static DataMgrHandler* get_instance();
	~DataMgrHandler();	

	void add_request(persist_task req);
	bool load_unfinished_tasks();
	
private:
	DataMgrHandler();

protected:
	virtual void run();	

private:
	void init();
	void read_conf();
	void handle_data_task(persist_task task);
	void handle_unfinished_task(persist_task task);
	void handle_finished_task(persist_task task);

	void backup_unfinished_tasks();
	void append_finished_tasks(persist_task task);

	//string task_2_str(persist_task& task);
	//bool str_2_task(string& str, persist_task& task);

	bool is_task_existed(persist_task req);
	bool delete_task(persist_task req);

private:	
	int sdqueue_capacity;
	SDQueue<persist_task>* m_task_queue;

	string unfinished_file;
	string finished_file;

	string unfinished_file_temp;

	map<string, persist_task> map_unfinished_task;

private:
	static DataMgrHandler* instance;

private:
	DECL_LOGGER(logger);
};

#endif // __DATAMGR_HANDLE_THREAD_H_20090121_10


