//==============================================================
// EventServerFactory.h : interface of class EventServerFactory 
//	  (this class encapsulates how to create IOEventServer implementation classes)
//
// Author: JeffLuo
// Created: 2007-01-29
//===============================================================

#ifndef __EVENTSERVER_FACTORY_H_20070129_13
#define  __EVENTSERVER_FACTORY_H_20070129_13

#include <string>
#include "common/IOEventServer.h"
#include "common/SDLogger.h"

class EventServerFactory
{
private:
	EventServerFactory();

	void read_conf();

	bool is_epoll_available();

public:
	static EventServerFactory* get_instance();

	IOEventServer* create_event_server();

private:
	// configurable params
	string m_cur_event_mechanism;

private:
	static EventServerFactory* __instance;
	static int _epoll_available;

	DECL_LOGGER(logger);	
};

#endif // #ifndef __EVENTSERVER_FACTORY_H_20070129_13

