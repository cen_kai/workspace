//============================================================
// distserver.cpp : entry point for dist server application
//
// Created: 2009-01-20
//============================================================

#include <signal.h>
#include "DistServerApp.h"
#include "common/IOEventServer.h"
#include "EventHandlerFactory.h"

#include "common/FileUtil.h"
#include "common/LRUHashtable.h"

#include "SendFileHandler.h"
#include "SendFileThreadPool.h"
#include "admin/AdminSessionManager.h"
#include "EventServerFactory.h"

#include "datamgr/DataMgrHandler.h"

#ifdef LOGGER
#include <log4cplus/configurator.h>
using namespace log4cplus;
#endif

// forward declaration
void init_seedfile_list(bool &read_filelevel_succ);

DistServerApp g_app;
EventHandlerFactory g_event_handler_mgr;
AdminSessionManager g_admin_session_mgr;
IOEventServer* g_main_ev_server = NULL;

static log4cplus::Logger _logger = log4cplus::Logger::getInstance("transfer");


int main(int argc, char* argv[])
{
#ifdef LOGGER
	PropertyConfigurator::doConfigure("../conf/log4c_transfer.properties");
#endif

	//ignore some signal : SIGPIPE
	signal(SIGPIPE, SIG_IGN);

	g_app.set_config_file("../conf/transfer.conf");
//	g_app.set_dyna_config_file("../conf/transfer_dyna.conf"); 20150421 liuyinfeng ����������
	if(!g_app.load_conf())
	{
		fprintf(stderr, "failed to load configuration.\n");
		return -1;
	}

	if(!g_app.prepare_admin_server_socket())
	{
		fprintf(stderr, "failed to intialize admin server socket.\n");		
		return -1;
	}	

      	g_app.get_external_ip();

  	SendFileThreadPool* sendfile_pool = SendFileThreadPool::get_instance();
	sendfile_pool->start_threads();

	DataMgrHandler::get_instance()->start();
	DataMgrHandler::get_instance()->load_unfinished_tasks();

	g_event_handler_mgr.init();

	IOEventServer* ev_server = EventServerFactory::get_instance()->create_event_server();
	g_main_ev_server = ev_server;

	int event_type;
	// add admin server listening event
	EventInfo ev_admin(g_event_handler_mgr.get_event_handler(EventHandlerFactory::ADMIN_SERVER_EVENT), 0);
	event_type = IOEventServer::EVENT_READABLE|IOEventServer::EVENT_PERSIST;
	ev_server->add_event(g_app.get_admin_server_socket(), event_type, &ev_admin);

	ev_server->run_event_loop();

	g_event_handler_mgr.cleanup();

	return 0;
}

