//============================================================
// EventHandlerFactory.h : interface of class EventHandlerFactory
//                                 Event handler(event callback) manager
// Created: 2009-01-20
//============================================================

#ifndef __EVENT_HANDLER_FACTORY_20060905_11
#define __EVENT_HANDLER_FACTORY_20060905_11

#include <map>

class IEventCallback;

using std::map;

class EventHandlerFactory
{
public:
	// event handler ID constants
	enum HANDLER_ID_CONST{
		UPLOAD_SERVER_EVENT = 1,
		ADMIN_SERVER_EVENT = 10,			
		PEER_SOCKET_EVENT = 2,
		CONNECT_RESULT_EVENT = 3,
		CUR_TIME_UPDATE_EVENT = 4,
		UPDATE_FILE_STATUS_EVENT = 5,
		REPORTRC_TIMEOUT_EVENT = 6,
		DUMP_P2PSTAT_TIMER_EVENT = 7,
		QUERY_DOWNTIMES_TIMER_EVENT = 8,
		ADMIN_CLIENT_EVENT = 11,
		PIPE_NOTIFY_EVENT = 12,
		QUERY_CID_INFO_EVENT = 13,
		CHECK_DATACENTER_RESULT_EVENT = 14
	};

public:
	EventHandlerFactory() { }

	void init();
	void cleanup();
	IEventCallback* get_event_handler(int handlerID);

private:
	map<int, IEventCallback*> m_event_handler_map;		
};

#endif // #ifndef __EVENT_HANDLER_FACTORY_20060905_11

