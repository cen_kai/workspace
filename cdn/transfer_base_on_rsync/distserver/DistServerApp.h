//============================================================
// DistServerApp.h : interface of class DistServerApp
//                           (distribution server application context)
// Author: JeffLuo
// Created: 2006-08-31
//============================================================

#ifndef __DIST_SERVER_APP_H_20060831_11
#define  __DIST_SERVER_APP_H_20060831_11

#include <string>
#include <time.h>
#include "common/ConfigReader.h"
#include "common/DynamicConfiguration.h"
#include "common/SDLogger.h"
#include "common/common.h"
#include "common/AvgSpeedStat.h"
#include "common/Utility.h"
#include <vector>

//class StatCommand;

using std::string;
using std::vector;

class P2PGlobalStat
{
public:
	const static int SPEED_STAT_SPAN = 5;
	const static int MAX_PART_NUM = 20;
	
	P2PGlobalStat();
	void reset();
	void unfinish_reset();
	string get_stat_details();
	string get_partfile_request_details();
	string get_unfinish_stat_details();
	//StatCommand* get_stat_as_cmd() const;

	void inc_session_num()
	{
		m_cur_sessions++;
		if(m_max_sessions < m_cur_sessions)
			m_max_sessions = m_cur_sessions;
		m_total_sessions++;
	}

	void inc_cut_by_no_slowspeed()
	{
		m_cut_by_slowspeeds ++;
	}
	
	void dec_session_num()
	{
		if(m_cur_sessions > 0)
			m_cur_sessions--;
	}

	int  get_session_num()
	{
	       return m_cur_sessions;
	}

	int  get_cur_band()
	{
	       return m_cur_band;
	}


	void inc_unfinish_session_num()
	{
		m_unfinish_cur_sessions++;
		if(m_unfinish_max_sessions < m_unfinish_cur_sessions)
			m_unfinish_max_sessions = m_unfinish_cur_sessions;
		m_unfinish_total_sessions++;
	}

	void dec_unfinish_session_num()
	{
		if(m_unfinish_cur_sessions > 0)
			m_unfinish_cur_sessions --;
	}

	void add_unfinish_files()
	{
		m_unfinish_files ++;
	}

	void del_unfinish_files()
	{
		if(m_unfinish_files > 0)
		{
			m_unfinish_files --;
		}
	}

	int get_unfinish_files()
	{
		if(m_unfinish_files >= 0)
		{
			return m_unfinish_files;
		}
		else
		{
			return 0;
		}
	}

	void add_p2p_uploaded(bool is_cdn_protocol, int uploaded, _u64 time_ms = 0)
	{
		if(uploaded < 0)
			return ;
		
		if(is_cdn_protocol)
			m_cdn_protocol_uploaded += uploaded;
		else 
			m_old_p2p_uploaded += uploaded;
		
		_u64 cur_time = time_ms;
		if(cur_time == 0)
			cur_time = Utility::current_time_ms();
		m_speed_stat.put(uploaded, cur_time);
	}

	void add_unfinish_p2p_uploaded(bool is_cdn_protocol, int uploaded, _u64 time_ms = 0)
	{
		if(uploaded < 0)
			return ;

		if(is_cdn_protocol)
			m_unfinish_cdn_protocol_uploaded += uploaded;
		else
			m_unfinish_old_p2p_uploaded += uploaded;

		_u64 cur_time = time_ms;
		if(cur_time == 0)
			cur_time = Utility::current_time_ms();
		m_unfinish_speed_stat.put(uploaded, cur_time);
	}

	void inc_send_storagefiles_num()
	{
		m_send_storagefiles++;
	}

	void inc_recv_storagefiles_num()
	{
		m_recv_storagefiles++;
	}

	void inc_partfile_request(int part_index)
	{
		if(part_index >= 0 || part_index < MAX_PART_NUM)
		{
			m_partfile_request_num[part_index]++;
		}
	}
	//jidong.yu add 20071226
	void inc_scatter_data(int read_times,int readv_times,int scatter_times)
	{
		m_read_times += read_times;
		m_readv_times += readv_times;
		m_scatter_times += scatter_times;
	}
	void inc_scatter_fail_data(int fail_times)
	{
		m_fail_times += fail_times;
	}

	void inc_scatter_read_times(int read_times)
	{
		m_readss_times += read_times;
	}


       void inc_scatter_met_req(int fail_times)
	{
		m_cache_met_times += fail_times;
	}

       void inc_scatter_combin_req(int req_times)
	{
		m_combin_reqs += req_times;
	}
	
	string get_scatter_stat()
	{
		char details[1024];

		char* buffer = details;
		// read
		int written = sprintf(buffer, ",(scatter,%d,%d,%d,%d,%d,%d,%d)", m_read_times,m_readv_times,m_scatter_times,
		                         m_fail_times,m_cache_met_times, m_readss_times,m_combin_reqs);
		if(written < 0)
			return "";
		buffer += written;

		m_read_times = 0;
		m_readv_times = 0;
		m_scatter_times = 0;
		m_fail_times = 0;
		m_cache_met_times=0;
		m_readss_times=0;
		m_combin_reqs = 0;
		return (string)details;
	}
	void inc_gather_data(int write_times,int writev_times,int gather_times)
	{
		m_write_times += write_times;
		m_writev_times += writev_times;
		m_gather_times += gather_times;
	}
	void inc_gather_fail_data(int fail_times)
	{
		m_fail_write_times += fail_times;
	}
	string get_gather_stat()
	{
		char details[1024];

		char* buffer = details;
		// read
		int written = sprintf(buffer, ",(gather,%d,%d,%d,%d)", m_write_times,m_writev_times,m_gather_times,m_fail_write_times);
		if(written < 0)
			return "";
		buffer += written;

		m_write_times = 0;
		m_writev_times = 0;
		m_gather_times = 0;
		m_fail_write_times = 0;
		return (string)details;
	}
	//end
public:
	_u32 m_partfile_request_num[MAX_PART_NUM];   // 统计各part的请求量
	
	int m_cur_sessions;
	int m_max_sessions;
	int m_total_sessions;
	int m_cut_by_slowspeeds;

	int m_cur_band;

	int m_total_files;
	int m_active_files;

	int m_local_files;
	int m_send_storagefiles;
	int m_recv_storagefiles;
	
	_u64 m_old_p2p_uploaded;
	_u64 m_cdn_protocol_uploaded;

	int m_unfinish_cur_sessions;
	int m_unfinish_max_sessions;
	int m_unfinish_total_sessions;

	_u64 m_unfinish_old_p2p_uploaded;
	_u64 m_unfinish_cdn_protocol_uploaded;

	int m_unfinish_files;

	int m_pending_session;
	
	AvgSpeedStat<SPEED_STAT_SPAN> m_speed_stat;
	AvgSpeedStat<SPEED_STAT_SPAN> m_unfinish_speed_stat;

	//jidong.yu add 20071226
	//for stat scatter read;
	int m_read_times;
	int m_readv_times;
	int m_scatter_times;
	int m_fail_times;
	int m_cache_met_times;
	int m_readss_times;
	int  m_combin_reqs;


	int m_write_times;
	int m_writev_times;
	int m_gather_times;
	int m_fail_write_times;
	//end
};

class DistServerApp
{
	const static _u16 DEFAULT_REFUSED_TIMES = 3;
	const static int DEFAULT_URL_SPEED_THRESHOLD_KB = 40;

public:
	enum NOTIFY_RESULT_TYPE
	{
		SENDFILE_RESULT = 1,
		FILELIST_RESULT = 2,
		PEERRC_COUNT_RESULT = 3,
		READ_MAPADDR_RESULT = 4,
		MVFILE_RESULT = 5,
		QUERY_PEER_RESULT = 6,
		CACHE_REQ_RESULT = 7,
	};

	enum BANDWIDTH_CONTROL_LEVEL
	{
		NO_BANDWIDTH_LIMIT = 0,
		CONTROL_FILE_BANDWIDTH = 1,
		CONTROL_PEER_SPEED = 2
	};

	enum FADVISE_SUPPORT_STATUS
	{
		STATUS_UNKNOWN = -1,
		FADVISE_NOT_SUPPORT = 0,			
		FADVISE_SUPPORT = 1			
	};
	
	enum DISTSVR_TYPES
	{
		TYPE_SPEEDUP = 0,
		TYPE_OTHER = 1,
	};

	DistServerApp();

	
	void set_config_file(const char* cfg_file)
	{
		if(cfg_file)
			m_cfg_file = cfg_file;
	}
	string get_config_file() const { return m_cfg_file; }

	void set_dyna_config_file(const char* cfg_file)
	{
		if(cfg_file)
			m_dyna_cfg_file = cfg_file;
	}
	string get_dyna_config_file() const { return m_dyna_cfg_file; }
	
	int get_p2s_sequence_no() { return m_p2s_cmd_next_seq_no++; }

	bool load_conf();
	bool load_upload_server_port_conf();
	string get_conf_string(const char* key, const char* default_value = NULL, bool log_warning = true) { return m_cfg_reader.get_string(key, default_value, log_warning); }
	int get_conf_int(const char* key, int default_value = 0, bool log_warning = true)  { return m_cfg_reader.get_int(key, default_value, log_warning); }

	string get_dyna_cfg_string(const char* key, const char* default_value = NULL, bool log_warning = true) { return m_dyna_cfg->get_string(key, default_value, log_warning); }
	int get_dyna_cfg_int(const char* key, int default_value = 0, bool log_warning = true)  { return m_dyna_cfg->get_int(key, default_value, log_warning); }

	void set_dyna_cfg_string(const char* key, const char* value) { m_dyna_cfg->set_string(key, value); m_dyna_cfg->save();}
	void set_dyna_cfg_int(const char* key, int value)  { m_dyna_cfg->set_int(key, value); m_dyna_cfg->save();}
	
	string get_dyna_conf_string(const char* conf_path, const char* key, const char* default_value = NULL) 
	{  
		if(m_dyna_config)
			return m_dyna_config->get_conf_string(conf_path, key, default_value);
		else
			return default_value != NULL ? default_value : "";
	}
	int get_dyna_conf_int(const char* conf_path, const char* key, int default_value = 0)  
	{
		if(m_dyna_config)
			return m_dyna_config->get_conf_int(conf_path, key, default_value);
		else
			return default_value;	
	}	

	bool add_dyna_conf_file(const char* conf_file_path, bool use_parent_conf = true)
	{
		if(m_dyna_config)
			return m_dyna_config->add_config_file(conf_file_path, use_parent_conf);
		else
			return false;	
	}
	bool update_dyna_conf_file(const char* conf_file_path, bool use_parent_conf = true)
	{
		if(m_dyna_config)
			return m_dyna_config->update_config_file(conf_file_path, use_parent_conf);
		else
			return false;	
	}	

	bool prepare_upload_server_socket(bool socket_block_mode = false);
	bool prepare_admin_server_socket(bool socket_block_mode = false);	

	int get_upload_server_listen_port_num();
	int get_upload_server_listen_port(int index = 0);
	int get_upload_server_socket(int index = 0);

	int get_admin_server_socket() const { return m_admin_server_socket; }
	int get_admin_server_listen_port() const;

	_u32 get_server_bind_addr() const { return m_server_bind_addr; }

	int get_notify_pipe_read_fd() const { return m_notify_pipe_fds[0]; }
	int get_notify_pipe_write_fd() const { return m_notify_pipe_fds[1]; }

	// 2007-07-20
	// add by Kang
	int get_notify_sendfile_result_pipe_read_fd() const { return m_notify_sendfile_result_pipe_fds[0]; }
	int get_notify_sendfile_result_pipe_write_fd() const { return m_notify_sendfile_result_pipe_fds[1]; }	
	// end

	int get_system_page_size() 
	{
		if(m_system_page_size == 0)
		{
			m_system_page_size = getpagesize();
		}

		return m_system_page_size;
	}
	int is_fadvise_supported() { return m_fadvise_support_status; }
	void set_fadvise_support_status(int status) { m_fadvise_support_status = status; }

	int get_bandwidth_control_level() const { return m_bandwidth_control_level; }
	void set_bandwidth_control_level(int level) 
	{
		if(m_bandwidth_control_level != level)
		{
			if(get_current_time() - m_bandwidth_chg_time > 60)
			{
				// one level must keep at least 2 minutes
				m_bandwidth_control_level = level; 
				m_bandwidth_chg_time = get_current_time();
			}
		}
	}
	bool is_upload_ver(_u32);
	short get_product_major_ver() ;
	short get_product_build_num();
	int get_product_flag();
	int get_product_version() { return (int)get_product_major_ver() << 16 | get_product_build_num(); }
	int get_client_version() { return 4 << 24 |(int)get_product_major_ver() << 16 | get_product_build_num();  }
	_u16 get_max_refused_times();
	int p2p_total_speed_max();
	int speed_over_limit_sleep_ms();
	bool is_speedup_mode_enabled(); 
	bool send_by_sendfile();
	int max_allowed_connections();

	int get_cdn_type(){ return m_cdn_type;}
	int get_cdn_allow_size(){   return m_max_allow_size;}
	_u32 get_total_disk_space_MB()
	{
		_u32 total_size = 0;
		int pub_file_num = get_conf_int("distserver.pub.dir.num");
		for(int i=0; i<pub_file_num; i++)
		{
			char buf[64] = {0};
			sprintf(buf, "distserver.pub.dir_%d.size.mb", i);
			total_size += get_conf_int(buf, 0);
		}

		return total_size;
	}


	time_t get_current_time() { return time(NULL); }
	void set_current_time() { m_cur_time = time(NULL); }

	void report_external_ip(_u32 local_external_ip);
	int get_external_ip_num() const { return m_server_external_ips.size(); }
	_u32 get_external_ip(int index = 0);

	string get_dynamic_conf_file();
	string get_hub_url_prefix();

	string dump_server_status();
	bool log_finishing_total_upload(string filename, _u64 filesize, _u64 total_upload);
	int is_upload_unfinished_files();
	int get_file_stat_policy();
	bool upload_ver4();
	bool ignore_nocid_session()
	{
		if(m_ignore_nocid_session == 1)
		{
			return true;
		}
		return false;
	}

	bool report_tracker_comlpete_file()
	{
		if(m_report_tracker_comlpete_file == 1)
		{
			return true;
		}
		return false;
	}
	bool only_upload_slowspeed()
	{
		if(m_upload_slow_speed == 1)
		{
			return true;
		}
		return false;
	}
	int peer_speed_diff_kb()
	{
		return m_peer_speed_diff_kb;
	}

	bool enable_bigfile_partly_upload_onetime();
	
	int default_url_speed_threshold_kb();

	string get_urldir();
	string dir_name(const string &filepath);
	bool get_mirror_svr_by_index(int index, const string &filename, string &ip, int &port, string &pubdir, int &transferport);

	int get_mirror_svr_count()
	{
		return m_mirror_svr_list.size();
	}

	bool control_peer_speed();

	int get_dir_num();

	int use_divide_dir();

	string get_storage_svr_ip();
	int get_storage_svr_port();

	bool is_named_by_cid();

	int get_amin_port();

	string get_tmp_dir();

	int get_pubsvr_num();
	bool get_pubsvr_by_index(string &ip, int &port, int index);
	int get_pubsvr_index_by_ip_port(const string &ip, const int &port);

	bool coll_by_storage();
	int get_cut_mintime();
	bool loadseedfile_fromfile();
	bool pub_hotcid();
	string get_repeatfile_dir();
	bool check_file_before_send();
	string get_timeout_file_dir();

	bool get_delete_timeout_file();
	
	bool distserver_reportrc_enabled();

	string get_transfer_client_ip();
	int get_transfer_client_port();

	bool is_storage_distsvr();
	
	int get_distserver_type();

	_u64 get_max_filesize_can_dist();

	string get_big_file_dir();

	int get_upload_allowed_reslevel();
	void set_upload_revise_level(int level);
	int get_upload_revise_level();
	
	int get_transfer_svr_listen_port();

	//added by tianming,20071224
	bool get_read_model(){return m_isScatterRead;}

	bool get_write_model(){return m_is_gather_write;}

	string get_cdnmgr_ip(){return m_cdnmgr_ip;}
	int get_cdnmgr_port(){return m_cdnmgr_port;}
	
private:
	struct MirrorSvr
	{
		string ip;
		int port;
		int transferport;
		string pubdir;
		string urldir;
	};
private:
	int m_distserver_type;

	int m_transfer_svr_listen_port;

	int m_upload_allowed_reslevel;
	int m_upload_revise_level;
	
	_u64 m_max_filesize_can_dist;
	string m_big_file_dir;
	
	// 2007-07-09
	// add by Kang
	bool m_control_peer_speed;
	// end 

	// 2007-07-07-26
	bool m_is_storage_distsvr;
	bool m_distserver_reportrc_enabled;
	string m_transfer_client_ip;
	int m_transfer_client_port;
	// end
	
	string m_cfg_file;
	ConfigReader m_cfg_reader;
	DynamicConfiguration* m_dyna_config;
	string m_dyna_cfg_file;
	ConfigWriter *m_dyna_cfg;

	string m_finishing_total_upload_file;

	int m_upload_unfinished_files;
	int m_upload_slow_speed;
	int m_peer_speed_diff_kb;

	vector <int> m_upload_server_socket;
	vector <int> m_upload_server_port;	
	int m_admin_server_socket;
	int m_admin_server_port;
	_u32 m_server_bind_addr;

	int m_max_allowed_conns;
	int m_bandwidth_control_level;
	time_t m_bandwidth_chg_time;

	int m_system_page_size;

	short m_send_by_sendfile;
	short m_product_major_ver;
	short m_product_build_num;
	int m_product_flag;
	int m_max_refused_times;
	int m_total_speed_max;
	int m_speed_over_limit_sleep_ms;

	_u32 m_low_upload_ver;
	int m_ver_policy;
	_u32 m_web_low_upload_ver;
	int m_web_ver_policy;

	int m_file_stat;
	
	int m_ignore_nocid_session;
	int m_report_tracker_comlpete_file;
	int m_default_speed_threshold_kb;
	int m_enable_bigfile_partly_upload_onetime;

	string m_dynamic_conf_file;
	string m_hub_url_prefix;

	int m_p2s_cmd_next_seq_no;
	time_t m_cur_time;
	vector<_u32> m_server_external_ips;

	// 2007-07-20
	// add by Kang
	int m_notify_sendfile_result_pipe_fds[2];
	// end

	int m_notify_pipe_fds[2];
	int m_notify_pipe_fds_bak[2];	
	int m_fadvise_support_status;

	vector <MirrorSvr> m_mirror_svr_list;

	vector<ServerAddr> m_pubsvr_list;

	int m_divide_dir;
	int m_dir_num;

	string m_storage_ip;
	int m_storage_port;

	int m_named_bycid;

	string m_storage_tmp_dir;

	int m_cut_min_time;

	int m_loadseedfile_fromfile;

	int m_is_pub_hotcid;

	string m_repeatfile_dir;

	int m_check_file_before_send;

       bool m_delete_time_out_file;
	string m_timeout_file_dir;
	
	//added by tianming,20071224
	//the flag of scatter read 
	bool m_isScatterRead;
	bool m_is_gather_write;

       int m_cdn_type;
	int m_max_allow_size;   

	//added by tianming 20090203
	string m_cdnmgr_ip;
	int m_cdnmgr_port;
	
	
public:
	P2PGlobalStat m_p2p_stat;

private:
	DECL_LOGGER(logger);
};

inline
int DistServerApp::default_url_speed_threshold_kb()
{
	if(-1 == m_default_speed_threshold_kb)
	{
		m_default_speed_threshold_kb = get_conf_int("distserver.default.url.speed.threshold.kb", DEFAULT_URL_SPEED_THRESHOLD_KB);
	}

	return m_default_speed_threshold_kb;
}

inline
bool DistServerApp::enable_bigfile_partly_upload_onetime()
{
	if(-1 == m_enable_bigfile_partly_upload_onetime)
	{
		m_enable_bigfile_partly_upload_onetime = get_conf_int("distserver.enable.bigfile.partly.upload.onetime", 1);	
	}

	return m_enable_bigfile_partly_upload_onetime == 1;
}
#endif // __DIST_SERVER_APP_H_20060831_11


