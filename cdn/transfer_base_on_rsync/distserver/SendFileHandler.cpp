//============================================================
// SendFileHandler.cpp : implementation of class SendFileHandler
//                          
// Created: 2009-01-20
//============================================================

#include "SendFileHandler.h"
#include "DistServerApp.h"

#include "admin/AdminCmdReportPartFile.h"
#include "admin/AdminCmdReportPartFileResp.h"
#include "admin/AdminCmdRptTransStat.h"
#include "admin/AdminCmdRptTransStatRsp.h"
#include "common/Utility.h"
#include "common/FileUtil.h"
#include "common/common.h"
#include "datamgr/DataMgrHandler.h"

extern DistServerApp g_app;

IMPL_LOGGER(SendFileHandler, logger);

SendFileHandler::SendFileHandler(SDQueue<persist_task >* request_queue, string file_save_dir, string cdn_server, int cdn_port) : SDThread()
	,m_file_save_dir(file_save_dir), m_cdn_server(cdn_server), m_cdn_port(cdn_port)
{
	LOG4CPLUS_THIS_WARN(logger, "FILE:" <<__FILE__ << " LINE:" << __LINE__);
	
	m_req_queue = request_queue;

	LOG4CPLUS_THIS_WARN(logger, "FILE:" <<__FILE__ << " LINE:" << __LINE__);
	read_conf();
}

void SendFileHandler::read_conf()
{
	//I don't know what parameters i need now
	start_hour = g_app.get_conf_int("transfer.start.task.hour", 0);
	end_hour = g_app.get_conf_int("transfer.end.task.hour", 23);

	m_rsync_server_modle_name = g_app.get_conf_string("transfer.rsync.server.module.name", "kankan");
	m_rsync_param = g_app.get_conf_string("transfer.rsync.param", "zrtopg");
	m_rsync_user_name = g_app.get_conf_string("transfer.rsync.user.name", "cdn");
	m_rsync_server_port = g_app.get_conf_string("transfer.rsync.server.port", "8873");
	m_rsync_local_pass_file = g_app.get_conf_string("transfer.rsync.local.passfile", "/usr/local/sandai/transfer_server_base_on_rsync/rsync/conf/rsyncd.pass");

	m_rsync_cmd_path = g_app.get_conf_string("transfer.rsync.exe.path", "/usr/local/sandai/transfer_server_base_on_rsync/rsync/bin/rsync ");
}

void SendFileHandler::run()
{
	LOG4CPLUS_INFO(logger, "sendfile thread started now.");

	while (true)
	{
		if(should_stop())
			break;

		if (!can_start_task())
		{
			sleep(5);
			continue;
		}
			
			persist_task req ;//= new persist_task();
			bool succ = m_req_queue->pop(req, 200);

			if (!succ )
			{
				//delete req;
				//req = NULL;
				
				if(should_stop())
					break;
				else
				{
					//need sleep here?
					continue;
				}
			}
			
	     	 	do_sendfile(req);

			#if 0
			if (req != NULL)
			{
				delete req;
				req = NULL;
			}
			#endif
		
	}

	LOG4CPLUS_WARN(logger, "sendfile thread stopped!");
}

bool SendFileHandler::can_start_task()
{
	time_t t1 = time(NULL);
	struct tm lt;
	localtime_r(&t1, &lt);

	int cur_hour =  lt.tm_hour;

	if (cur_hour >= start_hour && cur_hour <= end_hour)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void SendFileHandler::do_sendfile(persist_task req)
{
	//call rsync to transfer files
	string cmd = m_rsync_cmd_path;
	cmd.append(" -");
	cmd.append(m_rsync_param);
	cmd.append(" ");
	cmd.append(m_rsync_user_name);
	cmd.append("@");
	cmd.append(req.file_src_ip);
	cmd.append("::");
	cmd.append(m_rsync_server_modle_name);
	//解决路径中特殊字符的问题，为路径添加引号
	//++added by tianming 20090302
	cmd.append("\"");
	cmd.append(req.file_abs_path);
	cmd.append("\"");
		
	cmd.append(" ");
	
	string fileName = Utility::extract_filename(req.file_abs_path.c_str());

	string local_file_path = m_file_save_dir;
	local_file_path.append(fileName);
	LOG4CPLUS_DEBUG(logger, "local_file_path="<<local_file_path);

	cmd.append("\"");
	cmd.append(local_file_path);
	cmd.append("\"");
		
	cmd.append(" --port=");
	cmd.append(m_rsync_server_port);
	cmd.append(" --password-file=");
	cmd.append(m_rsync_local_pass_file);
	LOG4CPLUS_WARN(logger, "rsync cmd="<<cmd);
		
	int retcode = system(cmd.c_str());
	LOG4CPLUS_WARN(logger, "rsync retcode="<<retcode);
	if (retcode == 0)
	{
		req.task_status = 1;
		//备份
		req.task_end_time = Utility::get_date_time_string(Utility::current_time_secs());
		DataMgrHandler::get_instance()->add_request(req);

		//为了兼容cdn的处理，在文件名后面增加 .td的后缀
		if (!Utility::ends_with(local_file_path.c_str(), ".td"))
		{
			string old_path = local_file_path;
			local_file_path.append(".td");
			//++by liuyinfeng 2015-04-16 to ensure rename success
			//rename(old_path.c_str(), local_file_path.c_str());
			string cp_cmd("cp -f ");
			cp_cmd = cp_cmd + old_path + " " + local_file_path;
			LOG4CPLUS_WARN(logger, "old_path " << old_path << " local_file_path " << local_file_path << " cp_cmd " << cp_cmd);
			
			typedef void (*sighandler_t)(int);
			sighandler_t old_handler = signal(SIGCHLD, SIG_DFL);//确保SIGCHLD信号的处理方式为SIG_DFL
			int status = system(cp_cmd.c_str());
			signal(SIGCHLD, old_handler);
			if (status < 0)
            {
                LOG4CPLUS_WARN(logger, "system() failed, cp_cmd = " << cp_cmd << " errno = " << errno);
                return;
            }			
			if (WIFEXITED(status))
			{
				LOG4CPLUS_WARN(logger, "normal termination, exit status = " << WEXITSTATUS(status));
			}
			else if(WIFSIGNALED(status))
			{
				LOG4CPLUS_WARN(logger, "abnormal termination, signal number = " << WTERMSIG(status));
				return;
			}
			else if(WIFSTOPPED(status)) 
			{ 
				LOG4CPLUS_WARN(logger, "process stopped, signal number = " << WSTOPSIG(status));
				return;
			}
			
			if (0 != unlink(old_path.c_str()))
            {
                LOG4CPLUS_WARN(logger, "unlink " << old_path << " failed, errno = " << errno);
                return;
            }
			//--
		}
		
		//通知cdn发行
		AdminCmdReportPartFile *pcmd_req = new AdminCmdReportPartFile();
		memcpy(pcmd_req->m_cid, req.cid, MAX_CID_LEN);
		char gcid[MAX_CID_LEN];
		int buff_len = MAX_CID_LEN;
		int bcid_num = 0;
		retcode = FileUtil::calc_file_gcid(local_file_path.c_str(), (byte*)gcid, buff_len, bcid_num);
		if (retcode != 0)
		{
			LOG4CPLUS_WARN(logger, "failed to calc gcid for file=" <<local_file_path<< ", retcode=" << retcode);
			delete pcmd_req;
			pcmd_req = NULL;
			return;
		}
		else
		{
			LOG4CPLUS_DEBUG(logger, "succeed to calc gcid for file=" <<local_file_path);
		}
		memcpy (pcmd_req->m_gcid, gcid, MAX_CID_LEN);
		pcmd_req->m_filepath = local_file_path;
		pcmd_req->m_filesize = req.filesize;
		pcmd_req->m_stat = 1;
		pcmd_req->m_down_ratio = 100;
		AdminCommand* ret_cmd  = send_cmd(pcmd_req, m_cdn_server, m_cdn_port);
		delete pcmd_req;
		pcmd_req = NULL;

		if (ret_cmd == NULL)
		{
			LOG4CPLUS_WARN(logger, "failed to notify cdn to dist file=" <<local_file_path);
			
			//向cdn mgr报告错误
			AdminCmdRptTransStat *pcmd_stat = new AdminCmdRptTransStat();
			memcpy(pcmd_stat->m_cid, req.cid, MAX_CID_LEN);
			pcmd_stat->m_FileSize = req.filesize;
			pcmd_stat->m_errcode = AdminCmdRptTransStat::FAILED_NOTIFY_CDN_DIST;
			ret_cmd  = send_cmd(pcmd_stat, g_app.get_cdnmgr_ip(), g_app.get_cdnmgr_port());
			delete pcmd_stat;
			pcmd_stat = NULL;

			if (ret_cmd != NULL)
			{
				LOG4CPLUS_DEBUG(logger, "AdminCmdRptTransStatRsp retocde=" << ((AdminCmdRptTransStatRsp*)ret_cmd)->m_status);
				delete ret_cmd;
				ret_cmd = NULL;
			}
		}
		else
		{
			LOG4CPLUS_DEBUG(logger, "AdminCmdReportPartFileResp retocde=" << ((AdminCmdReportPartFileResp*)ret_cmd)->m_result);
			delete ret_cmd;
			ret_cmd = NULL;
		}
	}
	else
	{
		req.task_status = retcode*(-1);
		//备份
		DataMgrHandler::get_instance()->add_request(req);
		
		//通知cdnmgr
		AdminCmdRptTransStat *pcmd_stat = new AdminCmdRptTransStat();
		memcpy(pcmd_stat->m_cid, req.cid, MAX_CID_LEN);
		pcmd_stat->m_FileSize = req.filesize;
		pcmd_stat->m_errcode = AdminCmdRptTransStat::FAILED_GET_FILE;
		AdminCommand* ret_cmd  = send_cmd(pcmd_stat, g_app.get_cdnmgr_ip(), g_app.get_cdnmgr_port());
		delete pcmd_stat;
		pcmd_stat = NULL;

		if (ret_cmd != NULL)
		{
			LOG4CPLUS_DEBUG(logger, "AdminCmdRptTransStatRsp retocde=" << ((AdminCmdRptTransStatRsp*)ret_cmd)->m_status);
			delete ret_cmd;
			ret_cmd = NULL;
		}
	}

	//if finished,update the task status
}

AdminCommand * SendFileHandler::send_cmd(AdminCommand * cmd, string server_ip, int server_port)
{
	AdminCommand *  ret_cmd = NULL;
	if(cmd == NULL)
		return ret_cmd;

	string real_ip = Utility::get_first_ip_from_domainname(server_ip.c_str());
	int sock_fd = Utility::connect_by_ip_port(real_ip.c_str(), server_port);
	if (sock_fd == -1)
	{
		LOG4CPLUS_WARN(logger, "failed to connect,ip_domain=" <<server_ip<<",real_ip="<< real_ip << ", server_port=" << server_port<<", errno=" << errno);
		return ret_cmd;
	}

	Utility::set_socket_recv_timeout(sock_fd, 12);
	Utility::set_socket_send_timeout(sock_fd, 12);

	int buffer_len = cmd->encoded_length();
	char* temp_buff = new char[buffer_len];
	cmd->encode((byte*)temp_buff, buffer_len);

	int ret = Utility::send_data(sock_fd, temp_buff, buffer_len);
	if (ret == -1)
	{
		// send error
		delete[] temp_buff;
		LOG4CPLUS_WARN(logger, "send admin response error on fd " << sock_fd << ", errno=" << errno);
		close(sock_fd);
		return ret_cmd;
	}
	else
	{
		delete[] temp_buff;

		if (cmd->get_cmd_type_id() != AdminCommandFactory::CMD_TYPE_REPORT_TRANS_STAT )
		{
			ret_cmd = AdminCommandFactory::recv_admin_command(sock_fd);
		}
		else
		{
			ret_cmd = AdminCommandFactory::recv_admin_command_cdnmgr(sock_fd);
		}
		
		close(sock_fd);
		return ret_cmd;
	}
}
