//============================================================
// SendFileThreadPool.h : interface of class SendFileThreadPool
//		(sendfile() thread pool manager)
//                          
// Created: 2009-01-20
//============================================================

#ifndef __SEND_FILE_THREADPOLL_H_20061222_39
#define  __SEND_FILE_THREADPOLL_H_20061222_39

#include "SendFileHandler.h"
#include "common/SDQueue.h"
#include <set>
#include <vector>
#include <string>

//using namespace __gnu_cxx;

using std::vector;

class persist_task;

class SendFileThreadPool
{
private:
	SendFileThreadPool();
	~SendFileThreadPool() ;
	
public:
	static SendFileThreadPool* get_instance();

	bool add_request(persist_task req);
	void start_threads();

protected:
	//virtual void run();

private:
	void read_conf();	
	int find_ok_queue();
	bool is_exist_req(persist_task req);
	bool is_cid_on_cdn(persist_task req);
	AdminCommand* send_cmd(AdminCommand * cmd, string server_ip, int server_port);

private:
	int m_thread_num;
	int m_req_queue_capacity;

	string m_file_save_dir;

	string m_cdn_server;
	int m_cdn_port;
	
	vector< SDQueue<persist_task >* > m_req_queues;
	set<string> m_all_req_cids;
	
private:
	DECL_LOGGER(logger);

private:
	static SendFileThreadPool* __instance;

};

#endif // #ifndef __SEND_FILE_THREADPOLL_H_20061222_39

