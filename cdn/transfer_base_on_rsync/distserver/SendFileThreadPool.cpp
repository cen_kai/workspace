//============================================================
// SendFileThreadPool.cpp : implementation of class SendFileThreadPool
//		(sendfile() thread pool manager)
//                          
// Created: 2009-01-20
//============================================================

#include "DistServerApp.h"
#include "SendFileThreadPool.h"
#include "SendFileHandler.h"
#include "datamgr/DataTask_define.h"
#include "datamgr/DataMgrHandler.h"
#include "common/Utility.h"
#include "admin/AdminCmdRptTransStat.h"
#include "admin/AdminCmdRptTransStatRsp.h"
#include "admin/AdminCmdQueryFileList.h"
#include "admin/AdminCmdQueryListResp.h"


extern DistServerApp g_app;

SendFileThreadPool* SendFileThreadPool::__instance = NULL;

IMPL_LOGGER(SendFileThreadPool, logger);

SendFileThreadPool::SendFileThreadPool() 
{
	read_conf();
	
	if (m_thread_num <= 0 || m_req_queue_capacity <= 0)
	{
		LOG4CPLUS_THIS_ERROR(logger, "invalid  thread number: " << m_thread_num<<",or m_req_queue_capacity:"<<m_req_queue_capacity);
		return;
	}
	
	m_req_queues.reserve(m_thread_num);
	
	for (int i = 0; i < m_thread_num; i++)
	{
		SDQueue<persist_task >* queue = new SDQueue<persist_task>(m_req_queue_capacity);
		m_req_queues.push_back(queue);
	}

}

SendFileThreadPool::~SendFileThreadPool() 
{
	for(int i = 0; i < m_thread_num; i++)
	{
		delete m_req_queues[i];
		m_req_queues[i] = NULL;
	}

	m_req_queues.clear();
}

//判断是否存在，不存在则添加进去
bool SendFileThreadPool::is_exist_req(persist_task req)
{
	LOG4CPLUS_DEBUG(logger, "is_exist_req entering...");

	bool result = false;
	string cid_str = Utility::bytes_to_hex((byte*)req.cid, MAX_CID_LEN);
	LOG4CPLUS_DEBUG(logger, "is_exist_req,cid_str="<<cid_str);
	set<string>::iterator ite =  m_all_req_cids.find(cid_str);
	if (ite != m_all_req_cids.end())
	{
		result = true;
	}
	else
	{
		m_all_req_cids.insert(cid_str);
	}

	return result;
}

bool SendFileThreadPool::add_request(persist_task req)
{
	LOG4CPLUS_DEBUG(logger, "add_request entering...");

#if 0	
	//首先判断该任务是否是重复的
	//当前内存中是否有
	if (is_exist_req(req))
	{
		LOG4CPLUS_DEBUG(logger, "add_request,this task has existed");
		return true;
	}
#endif
	//cdn上是否有，后面再实现
	if (is_cid_on_cdn(req))
	{
		req.task_status = 2;
		DataMgrHandler::get_instance()->add_request(req);
		return true;
	}
	
	//在收到请求时，先做备份
	if (req.task_from != 0)
	{
		DataMgrHandler::get_instance()->add_request(req);
	}
	
	int queue_index= find_ok_queue();
	if (queue_index != -1)
	{	
		return m_req_queues[queue_index]->push(req);
	}
	else
	{
		LOG4CPLUS_THIS_WARN(logger, "!! failed to find idle queue for new request.");
		return false;
	}

	return true;
}

//找到任务最少的队列返回
int SendFileThreadPool::find_ok_queue()
{
	LOG4CPLUS_DEBUG(logger, "find_ok_queue entering...");
	
	int index = -1;
	int req_num = m_req_queue_capacity;
	int size = m_req_queues.size();
	for (int i = 0; i < size; ++i)
	{
		int temp_num = m_req_queues[i]->getSize();
		if (temp_num == 0)
		{
			index = i;
			LOG4CPLUS_DEBUG(logger, "find_ok_queue,found an empty queue");
			break;
		}
		
		if (temp_num < req_num)
		{
			req_num = temp_num;
			index = i;
			continue;
		}
	}
	
	return index;
}

SendFileThreadPool* SendFileThreadPool::get_instance()
{
	LOG4CPLUS_DEBUG(logger, "get_instance entering...");
	
	if(__instance == NULL)
	{
		__instance = new SendFileThreadPool();
	}

	return __instance;
}

void SendFileThreadPool::read_conf()
{
	m_req_queue_capacity = g_app.get_conf_int("transfer.max.requests", 1024);
	m_thread_num = g_app.get_conf_int("transfer.thread.number", 5);
	m_file_save_dir = g_app.get_conf_string("transfer.file.save.dir", "/data/");

	if (!Utility::ends_with(m_file_save_dir.c_str(), "/"))
	{
		m_file_save_dir.append("/");
	}

	m_cdn_server = g_app.get_conf_string("transfer.cdn.server.ip", "127.0.0.1");
	m_cdn_port = g_app.get_conf_int("transfer.cdn.server.port", 3078);
}

void SendFileThreadPool::start_threads()
{
	LOG4CPLUS_DEBUG(logger, "start_threads entering...");
	
	for(int i = 0; i < m_thread_num; i++)
	{
		SendFileHandler* send_handler = new SendFileHandler(m_req_queues[i], m_file_save_dir, m_cdn_server, m_cdn_port);
		send_handler->set_detachable(true);
		
		send_handler->start();
	}

	LOG4CPLUS_THIS_INFO(logger, "total " << m_thread_num << " sendfile thread started now.");
}

bool SendFileThreadPool::is_cid_on_cdn(persist_task req)
{
	LOG4CPLUS_DEBUG(logger, "is_cid_on_cdn entering...");

	bool result = false;

	AdminCmdQueryFileList *pcmd_req = new AdminCmdQueryFileList();
	memcpy(pcmd_req->m_cid, req.cid, MAX_CID_LEN);
	pcmd_req->m_filepath = req.file_abs_path;
	pcmd_req->m_filesize = req.filesize;
	pcmd_req->m_by_what = AdminCmdQueryFileList::BYWHAT_CID;
	AdminCommand* ret_cmd = send_cmd(pcmd_req, m_cdn_server, m_cdn_port);
	delete pcmd_req;
	pcmd_req = NULL;
	
	if (ret_cmd == NULL)
	{
		//通知cdnmgr，和cdn通讯失败
		AdminCmdRptTransStat *pcmd_stat = new AdminCmdRptTransStat();
		memcpy(pcmd_stat->m_cid, req.cid, MAX_CID_LEN);
		pcmd_stat->m_FileSize = req.filesize;
		pcmd_stat->m_errcode = AdminCmdRptTransStat::FAILED_QUERY_LOCAL_CDN;
		ret_cmd = send_cmd(pcmd_stat, g_app.get_cdnmgr_ip(), g_app.get_cdnmgr_port());
		delete pcmd_stat;
		pcmd_stat = NULL;

		if (ret_cmd != NULL)
		{
			LOG4CPLUS_DEBUG(logger, "AdminCmdRptTransStatRsp retocde=" << ((AdminCmdRptTransStatRsp*)ret_cmd)->m_status);
			delete ret_cmd;
			ret_cmd = NULL;
		}
	}
	else
	{
		int size = ((AdminCmdQueryListResp*)ret_cmd)->m_file_list.size();
		if (size > 0)
		{
			AdminCmdQueryListResp::FileInfo& temp_file =  ((AdminCmdQueryListResp*)ret_cmd)->m_file_list[0];
			if (temp_file.m_status == AdminCmdQueryListResp::FILE_ACTIVE || temp_file.m_status == AdminCmdQueryListResp::FILE_INACTIVE)
			{
				result = true;
			}
			else
			{
				LOG4CPLUS_INFO(logger, "file is not existed or md5 is wrong ");
			}
		}

		delete ret_cmd;
		ret_cmd = NULL;
	}

	return result;
}

AdminCommand* SendFileThreadPool::send_cmd(AdminCommand * cmd, string server_ip, int server_port)
{
	AdminCommand* ret_cmd = NULL;
	if(cmd == NULL)
		return ret_cmd;

	string real_ip = Utility::get_first_ip_from_domainname(server_ip.c_str());
	int sock_fd = Utility::connect_by_ip_port(real_ip.c_str(), server_port);
	if (sock_fd == -1)
	{
		LOG4CPLUS_WARN(logger, "failed to connect,ip_domain=" <<server_ip<<",real_ip="<< real_ip << ", server_port=" << server_port<<", errno=" << errno);
		return ret_cmd;
	}

    
    //设置超时
    Utility::set_socket_send_timeout(sock_fd, 10);
    Utility::set_socket_recv_timeout(sock_fd, 10);

	int buffer_len = cmd->encoded_length();
	char* temp_buff = new char[buffer_len];
	cmd->encode((byte*)temp_buff, buffer_len);

	int ret = Utility::send_data(sock_fd, temp_buff, buffer_len);
	if (ret == -1)
	{
		// send error
		delete[] temp_buff;
		LOG4CPLUS_WARN(logger, "send admin response error on fd " << sock_fd << ", errno=" << errno);
		close(sock_fd);
		return ret_cmd;
	}
	else
	{
		delete[] temp_buff;
		if (cmd->get_cmd_type_id() != AdminCommandFactory::CMD_TYPE_REPORT_TRANS_STAT )
		{
			ret_cmd = AdminCommandFactory::recv_admin_command(sock_fd);
		}
		else
		{
			ret_cmd = AdminCommandFactory::recv_admin_command_cdnmgr(sock_fd);
		}
		
		close(sock_fd);
		return ret_cmd;
	}
}

