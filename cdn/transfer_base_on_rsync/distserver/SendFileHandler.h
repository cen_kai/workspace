//============================================================
// SendFileHandler.h : interface of class SendFileHandler
//		(sendfile() thread)
//                          
// Author: JeffLuo
// Created: 2006-12-09
//============================================================

#ifndef __SENDFILE_HANDLER_H_20061209_26
#define  __SENDFILE_HANDLER_H_20061209_26


#include "common/SDThread.h"
#include "common/SDLogger.h"
#include "common/common.h"
#include "common/SDQueue.h"
#include <pthread.h>
#include <vector>
#include "datamgr/DataTask_define.h"
#include <string>

#define MAX_CID_LEN 20

using std::vector;
using std::string;

class persist_task;
class AdminCommand;
const static int REQUEST_WAIT_MAX_TIME = 1500;

class SendFileHandler : public SDThread
{
public:
	SendFileHandler(SDQueue<persist_task >* request_queue, string file_save_dir, string cdn_server, int cdn_port);

protected:
	virtual void run();	

private:
	void read_conf();
	void do_sendfile(persist_task req);
	AdminCommand *  send_cmd(AdminCommand * cmd, string server_ip, int server_port);
	bool can_start_task();
	
private:
	SDQueue<persist_task >* m_req_queue;
	string m_file_save_dir;

	string m_rsync_server_modle_name;
	string m_rsync_param;
	string m_rsync_user_name;
	string m_rsync_server_port;
	string m_rsync_local_pass_file;

	string m_rsync_cmd_path;

	int start_hour;
	int end_hour;

	string m_cdn_server;
	int m_cdn_port;
	   
private:
	DECL_LOGGER(logger);

};

/*
EXIT VALUES
       0      Success

       1      Syntax or usage error

       2      Protocol incompatibility

       3      Errors selecting input/output files, dirs

       4      Requested action not supported: an attempt was made to manipulate 64-bit files on a platform that cannot support them; or an option  was  specified
              that is supported by the client and not by the server.

       5      Error starting client-server protocol

       6      Daemon unable to append to log-file

       10     Error in socket I/O

       11     Error in file I/O

       12     Error in rsync protocol data stream

       13     Errors with program diagnostics

       14     Error in IPC code

       20     Received SIGUSR1 or SIGINT

       21     Some error returned by waitpid()

       22     Error allocating core memory buffers

       23     Partial transfer due to error

       24     Partial transfer due to vanished source files

       25     The --max-delete limit stopped deletions

       30     Timeout in data send/receive
*/

#endif // #ifndef __SENDFILE_HANDLER_H_20061209_26


