//============================================================
// EventHandlerFactory.cpp : implementation of class EventHandlerFactory
//                                 Event handler(event callback) manager
// Created: 2009-01-20
//============================================================

#include "EventHandlerFactory.h"
#include "common/IOEventServer.h"
#include "DistServerApp.h"
#include "admin/AdminServerEvent.h"
#include "admin/AdminClientEventHandler.h"

extern DistServerApp g_app;

void EventHandlerFactory::init()
{
	IEventCallback* ev_cb = NULL;

	ev_cb = new AdminServerEvent(&g_app);
	m_event_handler_map[ADMIN_SERVER_EVENT] = ev_cb;	

	ev_cb = new AdminClientEventHandler(&g_app);
	m_event_handler_map[ADMIN_CLIENT_EVENT] = ev_cb;		
}

void EventHandlerFactory::cleanup()
{
	map<int, IEventCallback*>::iterator it;
	for(it = m_event_handler_map.begin(); it != m_event_handler_map.end(); it++)
	{
		IEventCallback* ev_cb = it->second;
		delete ev_cb;
	}

	m_event_handler_map.clear();
}

IEventCallback* EventHandlerFactory::get_event_handler(int handlerID)
{
	map<int, IEventCallback*>::iterator it = m_event_handler_map.find(handlerID);
	if(it == m_event_handler_map.end())
		return NULL;

	return it->second;
}

