//============================================================
// AdminServerEvent.h : class AdminServerEvent interface and implementation
//                                 (handle server socket events, mainly accepting new socket)
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#ifndef __ADMIN_SERVER_EVENT_H_20061204_18
#define  __ADMIN_SERVER_EVENT_H_20061204_18

#include "common/common.h"
#include "common/IOEventServer.h"
#include "distserver/DistServerApp.h"
#include "admin/AdminSessionManager.h"
#include <arpa/inet.h>
#include "common/Utility.h"

extern AdminSessionManager g_admin_session_mgr;
extern EventHandlerFactory g_event_handler_mgr;

class AdminServerEvent : public DefaultEventCallBack
{
public:
	
	AdminServerEvent(DistServerApp* app) : m_server_app(app) 
	{
		read_conf();
	}
	
	virtual void on_fd_readable(int server_fd, EventData* event_info)
	{
		while(true)
		{
			struct sockaddr peer_addr;
			int sock_len = sizeof(peer_addr);
			int fd = accept(server_fd, &peer_addr, (socklen_t*)&sock_len);
			if(fd == -1)
			{
				if(errno != EAGAIN){
					// report to monitor
					LOG4CPLUS_ERROR(logger, "accept socket error, errno is "<<errno);
				}
				
				break;
			}

			// TODO: check if it's from allowed ip
			

			if(!init_client_socket(fd))
				continue;

			EventInfo ev_info(g_event_handler_mgr.get_event_handler(EventHandlerFactory::ADMIN_CLIENT_EVENT),  \
				m_client_read_timeout_ms);
			if(!get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info)){
				// report to monitor
				LOG4CPLUS_ERROR(logger, "failed to add socket " << fd << " readable event to event-server!");
			}


			AdminClientSession* session = new AdminClientSession();
			session->m_socket_fd = fd;
			session->m_sock_addr = ((struct sockaddr_in*)&peer_addr)->sin_addr.s_addr;
			if(!g_admin_session_mgr.add_session(session)){
				// report to monitor
				LOG4CPLUS_ERROR(logger, "failed to add admin client session(fd=" << fd << ") to session manager!");
			}

		}
	}
	
	virtual void on_fd_error(int fd, EventData* event_info)
	{
		// report to monitor
		LOG4CPLUS_ERROR(logger, "admin server socket " << fd << " is in error state!!");
	}

private:
	bool init_client_socket(int sock_fd)
	{
		// set non-block mode
		if(!Utility::set_fd_block(sock_fd, false)){
			LOG4CPLUS_WARN(logger, "failed to set client socket " << sock_fd << " non-block mode!");
			return false;
		}

		return true;
	}

	void read_conf()
	{
		if(m_server_app)
		{
			m_client_read_timeout_ms = m_server_app->get_conf_int("distserver.peer.read.timeout.ms", 12000);
		}
		else
		{
			m_client_read_timeout_ms = 10000; // 10 seconds
		}
	}
	
private:
	DistServerApp* m_server_app;
	
	int m_client_read_timeout_ms;
	
	DECL_LOGGER(logger);
};


IMPL_LOGGER(AdminServerEvent, logger);

#endif //  __ADMIN_SERVER_EVENT_H_20061204_18


