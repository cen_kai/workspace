//==============================================================
// AdminCommandFactory.h : interface of class AdminCommandFactory 
//			   (admin command factory, create admin cmd object from I/O byte-stream)
//
// Author: JeffLuo
// Created: 2006-12-04
//===============================================================

#ifndef __ADMIN_COMMAND_FACTORY_H_20061204_15
#define  __ADMIN_COMMAND_FACTORY_H_20061204_15

#include "common/common.h"
#include "common/SDLogger.h"
#include "AdminCommand.h"

class AdminCommand;

class AdminCommandFactory
{
private:
	AdminCommandFactory() { } // private ctor, not allow to instantiate it

public:
	const static string CMD_NAME_MODIFYSTATUS_RESP;
	const static string CMD_NAME_DELETEFILES_RESP;
	
	const static int ADMIN_CMD_HEADER_LEN = 16;
	const static int ADMIN_CMD_HEADER_LEN_CDNMGR = 12;
	const static int CUR_ADMIN_PROTOCOL = 10;
	const static int MAX_ADMIN_CMD_LENGTH = 48 * 1024;

	// cmd type id constants
	enum CMD_TYPEID_CONST
	{
		//传输server在收到一个请求后，查询本地cdn，确认该文件本地已存在
		CMD_TYPE_QUERYLIST = 101,
		CMD_TYPE_QUERYLIST_RESP = 102,	
		
		//传输完后，通知本地cdn发行文件
		CMD_TYPE_REPORTPARTFILE = 215,
		CMD_TYPE_REPORTPARTFILE_RESP = 216,

		//接收本地cdn发送的传输文件的请求
		CMD_TYPE_TRANSFER_FILE_REQ = 409,
		CMD_TYPE_TRANSFER_FILE_RSP = 410,
	
		//向cdn mgr上报传输过程中的状态
		CMD_TYPE_REPORT_TRANS_STAT = 43,
		CMD_TYPE_REPORT_TRANS_STAT_RSP = 44,

		//传输server通知目标传输server过来取数据
		CMD_TYPE_GET_FILE_REQ = 1003,
		CMD_TYPE_GET_FILE_REQ_RSP = 1004

	};


	enum CMD_REPORT_TO_MONITOR
	{
		CMD_TYPE_REPORT_HEARTBEAT = 001,
		CMD_TYPE_REPORT_ALARM = 100,
		CMD_TYPE_REPORT_KAYDATA = 200
	};
	

	static int get_cmd_header_len()  { return ADMIN_CMD_HEADER_LEN; }
	static AdminCommand* decode_admin_command(byte* header_buf, int header_buf_len, byte* body_buf, int body_buf_len);
	static void free_admin_command(AdminCommand* cmd)
	{
		if(cmd != NULL)
			delete cmd;
	}

	static AdminCommand* recv_admin_command(int sock_fd);
	static AdminCommand* recv_admin_command_cdnmgr(int sock_fd);
	static AdminCommand* send_admin_command(int sock_fd, AdminCommand* cmd);	

private:
	static bool is_valid_protocol_ver(int protocol_ver)
	{
		return protocol_ver == CUR_ADMIN_PROTOCOL || protocol_ver == 40;
	}
	static AdminCommand* create_admin_cmd(int cmd_typeid);

private:
	DECL_LOGGER(logger);
};

#endif // #ifndef __ADMIN_COMMAND_FACTORY_H_20061204_15


