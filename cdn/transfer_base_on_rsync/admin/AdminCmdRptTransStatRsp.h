#ifndef __COMMAND_RPT_TRANS_STAT_RSP_H_20090123
#define  __COMMAND_RPT_TRANS_STAT_RSP_H_20090123

#include <string>
#include "common/common.h"
#include "common/FixedBuffer.h"
#include "AdminCommandFactory.h"
#include "AdminCommand.h"

class AdminCmdRptTransStatRsp : public AdminCommand
{
public:
	const static int ACCEPT = 0;
	const static int FILENOEXIST = 1;
	const static int OTHERFAULT =2;
	
	const static string CMD_NAME;
	
	AdminCmdRptTransStatRsp() 
	{
	}
	~AdminCmdRptTransStatRsp()
	{
	}

	virtual string get_cmd_name() const { return CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_REPORT_TRANS_STAT_RSP; }
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const;

public:
	unsigned char m_status;
	
	
protected:
	 	DECL_LOGGER(logger);
};


#endif //__COMMAND_RPT_TRANS_STAT_RSP_H_20090123
