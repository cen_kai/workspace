//==============================================================
// AdminSessionManager.cpp : implementation of class AdminSessionManager 
//			  	 (adminSession container using hashtable)
//
// Author: JeffLuo
// Created: 2006-12-04
//===============================================================

#include "AdminSessionManager.h"
#include <algorithm>


IMPL_LOGGER(AdminSessionManager, logger);

AdminSessionManager::AdminSessionManager() : m_session_maps(INIT_HASHTABLE_BUCKET_NUM)
{

}

AdminClientSession* AdminSessionManager::find_session_by_fd(int socket_fd)
{
	ht_iterator it = m_session_maps.find(socket_fd);
	if(it == m_session_maps.end())
		return NULL;

	return it->second;
}

bool AdminSessionManager::add_session(AdminClientSession* session)
{
	if(session == NULL)
		return false;
	
	pair<ht_iterator, bool> insert_result = m_session_maps.insert(make_pair(session->m_socket_fd, session));
	return insert_result.second;
}

bool AdminSessionManager::remove_session(int socket_fd)
{
	ht_iterator it = m_session_maps.find(socket_fd);
	if(it == m_session_maps.end())
		return false;

	AdminClientSession* session = it->second;
	m_session_maps.erase(it);
	delete session;

	LOG4CPLUS_INFO(logger, "admin client session identified by fd " << socket_fd << " removed now.");	
	
	return true;
}



