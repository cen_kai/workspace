
#include "AdminCmdRptTransStatRsp.h"

IMPL_LOGGER(AdminCmdRptTransStatRsp, logger);
const string AdminCmdRptTransStatRsp::CMD_NAME = string("AdminCmdRptTransStatRsp");

bool AdminCmdRptTransStatRsp::decode_parameters(byte* buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);

       m_status =  fixed_buf.get_byte();

	#if 0
	if (m_status != AdminCmdTransferFileRsp::ACCEPT && m_status != AdminCmdTransferFileRsp::FILENOEXIST
		&& m_status != AdminCmdTransferFileRsp::OTHERFAULT)
	{
	      LOG4CPLUS_WARN(logger, "invalid m_status : " <<(int) m_status);
	       return false;
	}
	#endif

	LOG4CPLUS_WARN(logger, "AdminCmdRptTransStatRsp m_status=" <<(int) m_status);
		
	return true;
}

int AdminCmdRptTransStatRsp::encoded_length() const
{
	//int len = AdminCommand::ADMIN_CMD_HEADER_LEN;
	int len = 12;

	len += sizeof(char);
	
	len += sizeof(char);

	return len;
}

bool AdminCmdRptTransStatRsp::encode(byte* buffer, int& buffer_len)
{
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
	{
		return false;
	}

	byte* buf_ptr = buffer + len;
	int remain_len = buffer_len - len;

	FixedBuffer fixed_buf((char*)buf_ptr, remain_len);

	fixed_buf.put_short((short)get_cmd_type_id());

	fixed_buf.put_int(m_status);

	
	int pos = fixed_buf.position();

	buffer_len = len + pos;
	return true;
}

string AdminCmdRptTransStatRsp::get_cmd_content() const
{
	char buffer[1024];

	char* buf_ptr = buffer;

	buf_ptr += sprintf(buf_ptr, "[AdminCmdTransferFileRsp:");
	buf_ptr += sprintf(buf_ptr, "]");

	return (string)buffer;
}





