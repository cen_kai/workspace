
#ifndef __COMMAND_RPT_TRANS_STAT_H_20090123
#define  __COMMAND_RPT_TRANS_STAT_H_20090123

#include <string>
#include "common/common.h"
#include "common/FixedBuffer.h"
#include "AdminCommandFactory.h"
#include "AdminCommand.h"

class AdminCmdRptTransStat : public AdminCommand
{
     public:
	const static string CMD_NAME;

	enum TRANS_ERRCODE
	{
		FAILED_CONNECT_TARGET_TRANSERVER = 100,  //连接目标传输server失败
		FAILED_SEND_REQ_2_TARGET_TRANSERVER, //转发取文件的请求失败
		FAILED_NOTIFY_CDN_DIST, //通知本地cdn发行失败
		FAILED_GET_FILE, //通过rsync获取文件失败
		FAILED_QUERY_LOCAL_CDN //查询本地cdn的文件失败
	};
	
	AdminCmdRptTransStat() 
	{
	}
	~AdminCmdRptTransStat()
	{
	}

	virtual string get_cmd_name() const { return CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_REPORT_TRANS_STAT; }
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const;

public:
	char m_cid[MAX_CID_LEN]; 
	_u64 m_FileSize;
	
	int m_errcode;
	
	
protected:
	 	DECL_LOGGER(logger);
};




#endif //__COMMAND_RPT_TRANS_STAT_H_20090123

