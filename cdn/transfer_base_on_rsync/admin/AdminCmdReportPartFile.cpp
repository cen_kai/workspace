//============================================================
// AdminCmdReportPartFile.cpp implementation of class AdminCmdReportPartFile
//                           
// Author: JeffLuo
// Created: 2006-09-08
//============================================================

#include "AdminCmdReportPartFile.h"

IMPL_LOGGER(AdminCmdReportPartFile, logger);

const string AdminCmdReportPartFile::CMD_NAME = "ReportPartFile";

bool AdminCmdReportPartFile::decode_parameters(byte* buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);
	//m_cmdtypeID
//	m_cmdtypeID = fixed_buf.get_short();
	//m_stat
	m_stat = fixed_buf.get_byte();
	//m_filepath
	m_filepath = fixed_buf.get_string();
	//m_filesize
	m_filesize = fixed_buf.get_int64();
	//m_cid
	int cidlen = fixed_buf.get_int();
	if(cidlen != MAX_CID_LEN)
	{
		LOG4CPLUS_WARN(logger, "error cid len: " << cidlen);
		return false;
	}
	fixed_buf.get_bytes(m_cid, cidlen);
	//m_gcid
	int gcidlen = fixed_buf.get_int();
	if(cidlen != MAX_CID_LEN)
	{
		LOG4CPLUS_WARN(logger, "error gcid len: " << gcidlen);
		return false;
	}
	fixed_buf.get_bytes(m_gcid, gcidlen);
	// down ratio
	m_down_ratio = fixed_buf.get_byte();
	
	// file block list
	int blocks = fixed_buf.get_int();
	if(blocks > 0)
	{
		if(blocks > 4096)
		{
			LOG4CPLUS_WARN(logger, "found " << blocks << " blocks in cmd 'INTERESTEDRESP'!!");
			if(blocks > 65536)
				return false;
		}
		
		m_block_list.resize(blocks);
		
		int remain_len = buffer_len - fixed_buf.position(), consumed_bytes = 0;
		byte* buf_ptr = buffer + fixed_buf.position();
		for(int i = 0; i < blocks; i++)
		{
			FileBlockInfo& block = m_block_list[i];
			block.decode(buf_ptr, remain_len, consumed_bytes);
			
			buf_ptr += consumed_bytes;
			remain_len -= consumed_bytes;
			consumed_bytes = 0;
		}
	}	
	else
	{
		LOG4CPLUS_INFO(logger, "found " << blocks << " blocks in cmd 'ReportPartFile'!!");
		//return false;
	}

	return true;
}

int AdminCmdReportPartFile::encoded_length() const
{
	int len = AdminCommandFactory::get_cmd_header_len();

	// cmd typeid
	len += sizeof(short);

	//m_stat
	len += sizeof(byte);
	
	// Filepathname
	len += sizeof(int);
	len += m_filepath.length();

	// filesize
	len += sizeof(_u64);
	
	// CID
	len += sizeof(int);
	len += MAX_CID_LEN;

	// GCID
	len += sizeof(int);
	len += MAX_CID_LEN;

	// down ratio
	len += sizeof(byte);

	// file block list
	len += sizeof(int);
	len += calc_block_list_encoded_len();

	LOG4CPLUS_WARN(logger, "encoded_length, len=" << len);
	return len;
}

bool AdminCmdReportPartFile::encode(byte* buffer, int& buffer_len)
{
	if(buffer == NULL || buffer_len < encoded_length())
		return false;
	
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
		return false;

//	int header_len = len;
	
	byte* buf = buffer + len;
	int remain_len = buffer_len - len;
	
	FixedBuffer fixed_buf((char*)buf, remain_len);
	
	fixed_buf.put_short((short)get_cmd_type_id());
	fixed_buf.put_byte(m_stat);
	fixed_buf.put_string(m_filepath);
	fixed_buf.put_int64(m_filesize);
	fixed_buf.put_string((char *)m_cid, MAX_CID_LEN);
	fixed_buf.put_string((char *)m_gcid, MAX_CID_LEN);
	fixed_buf.put_byte(m_down_ratio);
	
	// file block list
	int blocks = m_block_list.size();
	fixed_buf.put_int(blocks);
	
	buf += fixed_buf.position();
	remain_len -= fixed_buf.position();
	for(int i = 0; i < blocks; i++)
	{
		FileBlockInfo& block = m_block_list[i];
		int new_len = buffer_len - remain_len;
		block.encode(buf, new_len);
		buf += new_len;
		remain_len += new_len;
	}

	buffer_len = buf - buffer;

	LOG4CPLUS_WARN(logger, "encode, buffer_len=" << buffer_len);
	return true;
}

string AdminCmdReportPartFile::get_cmd_content() const
{
	char buffer[1024];

	char* buf_ptr = buffer;

	buf_ptr += sprintf(buf_ptr, "[AdminCmdReportPartFile, ");

	buf_ptr += sprintf(buf_ptr, "m_down_ratio=%d, ", (int)m_down_ratio);
	buf_ptr += sprintf(buf_ptr, "m_block_num=%d, ", m_block_list.size());
	buf_ptr += sprintf(buf_ptr, "m_stat=%d, ", (int)m_stat);
	buf_ptr += sprintf(buf_ptr, "m_filepath=%s, ", m_filepath.c_str());
	buf_ptr += sprintf(buf_ptr, "m_filesize=%llu ", m_filesize);

	buf_ptr += sprintf(buf_ptr, "]");

	return (string)buffer;	
}
