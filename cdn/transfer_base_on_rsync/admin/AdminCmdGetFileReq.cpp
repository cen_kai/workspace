

#include "AdminCmdGetFileReq.h"

IMPL_LOGGER(AdminCmdGetFileReq, logger);

const string AdminCmdGetFileReq::CMD_NAME = string("AdminCmdGetFileReq");

bool AdminCmdGetFileReq::decode_parameters(byte* buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);

       int t_len = 0;
	
       t_len = fixed_buf.get_int();
	if(t_len != MAX_CID_LEN)
	{
		LOG4CPLUS_WARN(logger, "invalid cid length: " << t_len);
		return false;
	}
	fixed_buf.get_bytes((byte*)m_cid, t_len);

        m_FileSize = fixed_buf.get_int64();

      //m_server_ip = fixed_buf.get_int();
      m_bind_port = fixed_buf.get_short();

	//add by yangxianfeng
	//20070525
	if(fixed_buf.remain_len() > sizeof(int))
	{
		m_FileName = fixed_buf.get_string();
	}

	
	return true;
}

int AdminCmdGetFileReq::encoded_length() const
{
	int len = AdminCommand::ADMIN_CMD_HEADER_LEN;

	len += sizeof(short);

	len += sizeof(_u64);

	len += (sizeof(int) + MAX_CID_LEN);

	//len += sizeof(int);

	len += sizeof(short);

	len += (sizeof(int) + m_FileName.length());
	LOG4CPLUS_DEBUG(logger, "len=" << len);
	
	return len;
}

bool AdminCmdGetFileReq::encode(byte* buffer, int& buffer_len)
{
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
	{
		return false;
	}

	byte* buf_ptr = buffer + len;
	int remain_len = buffer_len - len;

	FixedBuffer fixed_buf((char*)buf_ptr, remain_len);

	fixed_buf.put_short((short)get_cmd_type_id());


	fixed_buf.put_string(m_cid, MAX_CID_LEN);
	fixed_buf.put_int64(m_FileSize);
 
	//fixed_buf.put_int(m_server_ip);
	fixed_buf.put_short(m_bind_port);
	fixed_buf.put_string(m_FileName);
	
	int pos = fixed_buf.position();

	buffer_len = len + pos;
	LOG4CPLUS_DEBUG(logger, "buffer_len=" << buffer_len);
	return true;
}

string AdminCmdGetFileReq::get_cmd_content() const
{
	char buffer[1024];

	char* buf_ptr = buffer;

	buf_ptr += sprintf(buf_ptr, "[CommandTransfer_FileREQ:");
/*	buf_ptr += sprintf(buf_ptr, ", m_server_ip=%s", m_server_ip);
	buf_ptr += sprintf(buf_ptr, ", m_peerid=%s", m_peerid);
	buf_ptr += sprintf(buf_ptr, ", m_stat_time=%s", Utility::get_time_string(m_stat_time).c_str());
	buf_ptr += sprintf(buf_ptr, ", m_time_span=%d", m_time_span);
	buf_ptr += sprintf(buf_ptr, ", m_cur_user_count=%d", m_cur_user_count);
	buf_ptr += sprintf(buf_ptr, ", m_max_user_count=%d", m_max_user_count);
	buf_ptr += sprintf(buf_ptr, ", m_uploaded_bytes=%llu", m_uploaded_bytes);
*/
	buf_ptr += sprintf(buf_ptr, "]");

	return (string)buffer;
}




