//==============================================================
// AdminClientSession.cpp : implementation of class AdminClientSession 
//			   (interaction between web admin client and distserver)
//
// Author: JeffLuo
// Created: 2006-12-04
//===============================================================

#include "AdminClientSession.h"
#include "AdminCommandFactory.h"
#include "common/Utility.h"
#include "common/IOEventServer.h"

#include "common/ConfigReader.h"
#include "distserver/DistServerApp.h"
#include "common/FileUtil.h"
#include <sys/stat.h>
#include <sys/types.h>

#include "distserver/EventHandlerFactory.h"

#include "AdminCmdTransferFileReq.h"
#include "AdminCmdTransferFileRsp.h"

#include "AdminCmdGetFileReq.h"
#include "AdminCmdGetFileRsp.h"

#include "AdminCmdRptTransStat.h"
#include "AdminCmdRptTransStatRsp.h"

#include "distserver/SendFileThreadPool.h"
#include "datamgr/DataTask_define.h"


extern DistServerApp g_app;
extern IOEventServer *g_main_ev_server;
extern EventHandlerFactory g_event_handler_mgr;

IMPL_LOGGER(AdminClientSession, logger);


AdminClientSession::AdminClientSession()
{
	m_socket_fd = -1;
	m_sock_addr = 0;

	m_protocol_version = 0;
	m_start_time = Utility::current_time_ms();

	m_cur_body_buf = NULL;
	reset_recv_buffer();
	
	m_resp_encode_buf = NULL;
	m_resp_buf_len = 0;
	m_cur_resp_len = 0;
	m_cur_resp_transfered = 0;
}

AdminClientSession::~AdminClientSession()
{
	if(m_cur_body_buf != NULL)
		delete []m_cur_body_buf;
	if(m_resp_encode_buf != NULL)
		delete []m_resp_encode_buf;
}

void AdminClientSession::reset_recv_buffer()
{
	m_cur_header_buf[0] = '\0';
	m_cur_header_len = -1;
	m_cur_header_recv = 0	;
	if(m_cur_body_buf)
	{
		delete []m_cur_body_buf;
		m_cur_body_buf = NULL;
	}
	m_cur_body_len = -1;
	m_cur_body_recv = 0;
}

int AdminClientSession::on_fd_readable(int fd)
{
	while (true)
	{
		int ret = read_cmd(fd);
		if(ret != READ_OK)
			return ret;

		// decode cmd parameters after receive a complete command
		AdminCommand* cmd = AdminCommandFactory::decode_admin_command(m_cur_header_buf, m_cur_header_len, m_cur_body_buf, m_cur_body_len);
		if(cmd == NULL)
			return -1;
		reset_recv_buffer();
//		LOG4CPLUS_DEBUG(logger, "successfully create admin command and content is '" << cmd->get_cmd_content() << "'.");
		ret = handle_request(cmd);
		AdminCommandFactory::free_admin_command(cmd);
		if(ret != HANDLE_OK)
			return ret;
	}

	LOG4CPLUS_WARN(logger, "unexpected branch in on_fd_readable() method!");
	return -1;
}

int AdminClientSession::read_cmd(int fd)
{
	int ret = 0;
	// step 1: read protocol version
	if(m_cur_header_len <= 0 && m_cur_header_recv < (int)sizeof(int))
	{
		// first 4 bytes (protocol version) not readed
		int bytes_read = sizeof(int) - m_cur_header_recv;
		ret = Utility::recv_nonblock_data(fd, (char*)(m_cur_header_buf + m_cur_header_recv), bytes_read);
		LOG4CPLUS_DEBUG(logger, "read protocl version returns " << ret << ", and " << bytes_read << " readed.");
		if(ret == Utility::NONBLOCK_RECV_AGAIN)
		{
			m_cur_header_recv += bytes_read;
			return READ_PENDING;
		}
		else if(ret <= 0)
		{
			// can not recv expected bytes, and connection broken or reset by remote peer
			if(ret == 0)
			{
				LOG4CPLUS_INFO(logger, "connection closed by admin client gracefully on fd " << fd);
				return READ_EOF;
			}
			else
			{
				LOG4CPLUS_WARN(logger, "recv() returns " << ret << " on fd " << fd << " (protocol ver), errno=" << errno);
				return READ_ERROR;
			}
		}
		else
		{
			// successfully recv protocol version
			int protocol_ver = *(int*)m_cur_header_buf;
			if (!is_valid_protocol_version(protocol_ver))
			{
				LOG4CPLUS_WARN(logger, "!! invalid admin protocol version: " << protocol_ver);
				return READ_INVALID_DATA;
			}

			m_cur_header_recv += bytes_read;
			if (protocol_ver == 10)
			{
				m_cur_header_len = AdminCommandFactory::get_cmd_header_len();
			}
			else
			{
				//兼容cdnmgr的协议格式
				m_cur_header_len = 12;
			}
		}
	}

	// step 2: read header remainder
	if(m_cur_header_len > 0 && m_cur_header_recv < m_cur_header_len)
	{
		// recv cmd header remainder (since protocol_ver received)
		int bytes_read =  m_cur_header_len - m_cur_header_recv;
		ret = Utility::recv_nonblock_data(fd, (char*)(m_cur_header_buf + m_cur_header_recv), bytes_read);
		LOG4CPLUS_DEBUG(logger, "read header remainder returns " << ret << ", and " << bytes_read << " readed.");		
		if(ret == Utility::NONBLOCK_RECV_AGAIN)
		{
			m_cur_header_recv += bytes_read;
			return READ_PENDING;
		}
		else if(ret <= 0)
		{
			// failed to recv expected bytes(connection broken or reset by remote peer)
			if(ret == 0)
			{
				LOG4CPLUS_INFO(logger, "connection closed by admin client gracefully on fd " << fd);
				return READ_EOF;
			}
			else
			{
				LOG4CPLUS_WARN(logger, "recv() returns " << ret << " on fd " << fd << " (header remain), errno=" << errno);
				return READ_ERROR;
			}
		}
		else
		{
			LOG4CPLUS_DEBUG(logger, "successfully receive header remainder part.");
			// successfully recv header remain part
			m_cur_header_recv += bytes_read;
			if(m_cur_header_recv != m_cur_header_len)
			{
				LOG4CPLUS_WARN(logger, "?? impossible! received header bytes is " << m_cur_header_recv <<", total header len is  " << m_cur_header_len);
				return READ_ERROR;
			}
			int magic_num = 0;
			if(!is_valid_magic_num(&magic_num))
			{
				LOG4CPLUS_WARN(logger, "invalid magic number: " << magic_num);
				return READ_INVALID_DATA;
			}

			m_cur_body_len = fetch_cmd_body_len();
			LOG4CPLUS_DEBUG(logger, "m_cur_body_len=" << m_cur_body_len);			
			if(m_cur_body_len < 1 || m_cur_body_len > MAX_CMD_BODY_LEN)
			{
				LOG4CPLUS_WARN(logger, "!! invalid command body length: " << m_cur_body_len);
				return READ_INVALID_DATA;					
			}
			m_cur_body_buf = new byte[m_cur_body_len];
		}
	}

	// step 3: read command body
	if(m_cur_body_len > 0 && m_cur_body_recv < m_cur_body_len)
	{
		// recv cmd body
		int bytes_read =  m_cur_body_len - m_cur_body_recv;
		ret = Utility::recv_nonblock_data(fd, (char*)(m_cur_body_buf + m_cur_body_recv), bytes_read);
		LOG4CPLUS_DEBUG(logger, "read body returns " << ret << ", and " << bytes_read << " readed.");				
		if(ret == Utility::NONBLOCK_RECV_AGAIN)
		{
			m_cur_body_recv += bytes_read;
			return READ_PENDING;
		}
		else if(ret <= 0)
		{
			// failed to recv expected bytes(connection broken or reset by remote peer)
			if(ret == 0)
			{
				LOG4CPLUS_DEBUG(logger, "connection closed by admin client gracefully on fd " << fd);
				return READ_EOF;
			}
			else
			{
				LOG4CPLUS_WARN(logger, "recv() returns " << ret << " on fd " << fd << " (body), errno=" << errno);
				return READ_ERROR;
			}
		}
		else
		{
			// successfully recv cmd body
			m_cur_body_recv += bytes_read;
			LOG4CPLUS_DEBUG(logger, "successfully recv a complete command.");

			return READ_OK;
		}
	}

	LOG4CPLUS_WARN(logger, "!! unexpected branch in recv_cmd: m_cur_header_len=" << m_cur_header_len 
		<< ", m_cur_header_recv="<<m_cur_header_recv << ", m_cur_body_len=" << m_cur_body_len 
		<< ", m_cur_body_recv=" << m_cur_body_recv);
	return READ_ERROR;
}


int AdminClientSession::on_fd_writable(int fd)
{
	return send_response_remainder();
}

int AdminClientSession::on_fd_timeout(int fd)
{
	return HANDLE_ERROR;
}

int AdminClientSession::handle_request(AdminCommand * cmd)
{
	if(cmd == NULL)
		return HANDLE_ERROR;

	int cmd_typeid = cmd->get_cmd_type_id();
	switch(cmd_typeid)
	{
	#if 0
	//these two cmd should be sent by cdn 
	case AdminCommandFactory::CMD_TYPE_QUERYLIST:
		return handle_querylist_cmd(cmd);

	case AdminCommandFactory::CMD_TYPE_REPORTPARTFILE:
		return handle_reportpartfile_cmd(cmd);
		#endif

		//本地cdn发的传输文件的请求
	case AdminCommandFactory::CMD_TYPE_TRANSFER_FILE_REQ:
		return handle_transmit_trans_file_req(cmd);	

		//源传输server发的传输文件的请求
	case AdminCommandFactory::CMD_TYPE_GET_FILE_REQ:
		return handle_get_file(cmd);	
		
	default:
		LOG4CPLUS_WARN(logger, "unexpected cmd '" << cmd->get_cmd_name() << "' when handle request.");
		return HANDLE_ERROR;
	}
}

int AdminClientSession::handle_transmit_trans_file_req(AdminCommand *cmd)
{
	if(cmd == NULL || cmd->get_cmd_type_id() != AdminCommandFactory::CMD_TYPE_TRANSFER_FILE_REQ)
	{
		return HANDLE_ERROR;
	}

	LOG4CPLUS_INFO(logger, "begin to handle_transmit_trans_file_req!");

	AdminCmdTransferFileReq *pcmd = (AdminCmdTransferFileReq *)cmd;
	LOG4CPLUS_INFO(logger, "begin to handle_transmit_trans_file_req:" << pcmd->get_cmd_content());
	AdminCmdTransferFileRsp resp;
	resp.m_status = 0;

	//cid is invalid,need calc again
	char cid[MAX_CID_LEN];
	int buff_len = MAX_CID_LEN;
	int retcode = FileUtil::calc_file_cid(pcmd->m_FileName.c_str(), (byte*)cid, buff_len);
	if (retcode != 0)
	{
		LOG4CPLUS_WARN(logger, "faild to calc cid for file=" << pcmd->m_FileName);
		return send_response(&resp);
	}
	else
	{
		LOG4CPLUS_DEBUG(logger, "succeed to calc cid for file=" << pcmd->m_FileName<<",cid="<<Utility::bytes_to_hex((const byte*)cid, MAX_CID_LEN));
	}
	
	//转发请求，阻塞式，确保转发成功
	AdminCmdGetFileReq *pcmd_req = new AdminCmdGetFileReq();
	memcpy(pcmd_req->m_cid, cid, MAX_CID_LEN);
	//memcpy(pcmd_req->m_cid, pcmd->m_cid, MAX_CID_LEN);
	pcmd_req->m_FileName = pcmd->m_FileName;
	pcmd_req->m_FileSize = pcmd->m_FileSize;
	pcmd_req->m_bind_port = g_app.get_admin_server_listen_port();//get_conf_int("transfer.admin.server.port", 19010);
	string server_ip_str = Utility::ip_ntoa(pcmd->m_server_ip);
	//AdminCommand *  ret_cmd = send_cmd(pcmd_req, server_ip_str, (int)pcmd->m_server_port);
	//所有的传输server都使用相同的端口
	AdminCommand *  ret_cmd = send_cmd(pcmd_req, server_ip_str, pcmd_req->m_bind_port);
	delete pcmd_req;
	pcmd_req = NULL;

	if (ret_cmd == NULL)
	{
		//向cdn mgr报告错误
		AdminCmdRptTransStat *pcmd_stat = new AdminCmdRptTransStat();
		memcpy(pcmd_stat->m_cid, pcmd->m_cid, MAX_CID_LEN);
		pcmd_stat->m_FileSize = pcmd->m_FileSize;
		pcmd_stat->m_errcode = AdminCmdRptTransStat::FAILED_SEND_REQ_2_TARGET_TRANSERVER;
		ret_cmd = send_cmd(pcmd_stat, g_app.get_cdnmgr_ip(), g_app.get_cdnmgr_port());
		delete pcmd_stat;
		pcmd_stat = NULL;

		if (ret_cmd != NULL)
		{
			LOG4CPLUS_DEBUG(logger, "AdminCmdRptTransStatRsp retocde=" << ((AdminCmdRptTransStatRsp*)ret_cmd)->m_status);
			delete ret_cmd;
			ret_cmd = NULL;
		}
	}
	else
	{
		LOG4CPLUS_DEBUG(logger, "AdminCmdGetFileRsp retocde=" << ((AdminCmdGetFileRsp*)ret_cmd)->m_status);
		delete ret_cmd;
		ret_cmd = NULL;
	}
	
	return send_response(&resp);
}

int AdminClientSession::handle_get_file(AdminCommand *cmd)
{
	if (cmd == NULL || cmd->get_cmd_type_id() != AdminCommandFactory::CMD_TYPE_GET_FILE_REQ)
	{
		return HANDLE_ERROR;
	}

	LOG4CPLUS_INFO(logger, "begin to handle_get_file!");


	AdminCmdGetFileReq *pcmd = (AdminCmdGetFileReq *)cmd;
	AdminCmdGetFileRsp resp;
	resp.m_status = 0;

	//将请求丢进获取文件的线程池中
	persist_task req;// = new persist_task();
	memcpy(req.cid, pcmd->m_cid, MAX_CID_LEN);
	LOG4CPLUS_DEBUG(logger, "cid=" << Utility::bytes_to_hex((const byte*)req.cid, MAX_CID_LEN));
	req.filesize = pcmd->m_FileSize;
	req.file_abs_path = pcmd->m_FileName;

	struct in_addr temp_addr;
	memcpy(&temp_addr, &m_sock_addr, sizeof(m_sock_addr));
	req.file_src_ip = inet_ntoa(temp_addr);
	
	req.task_start_time = Utility::get_date_time_string(Utility::current_time_secs());
	req.task_end_time = req.task_start_time;
	req.task_status = 0;
	SendFileThreadPool::get_instance()->add_request(req);
	
	return send_response(&resp);
}

int AdminClientSession::send_response(AdminCommand * resp)
{
	if(resp == NULL)
		return HANDLE_ERROR;

	int buffer_len = resp->encoded_length();
	alloc_resp_buffer(buffer_len);
	resp->encode(m_resp_encode_buf, buffer_len);
	m_cur_resp_len = buffer_len;
	m_cur_resp_transfered = 0;

	int ret = Utility::send_nonblock_data(m_socket_fd, (char*)m_resp_encode_buf, m_cur_resp_len);
	if (ret == -1)
	{
		// send error
		LOG4CPLUS_WARN(logger, "send admin response error on fd " << m_socket_fd << ", errno=" << errno);
		return HANDLE_ERROR;
	}
	else if(ret < m_cur_resp_len)
	{
		// only part of data send out
		m_cur_resp_transfered = ret;
		LOG4CPLUS_DEBUG(logger, "part of response data sent, remain " << (m_cur_resp_len - ret) << " bytes of data to send.");
		return HANDLE_PENDING;
	}
	else
	{
		// all bytes send out successfully
		m_cur_resp_transfered = ret;
		LOG4CPLUS_DEBUG(logger, "successfully send response of '" << resp->get_cmd_name() << "' (length=" << m_cur_resp_len << ").");		
		LOG4CPLUS_DEBUG(logger, "Response data: " << Utility::bytes_to_hex(m_resp_encode_buf, m_cur_resp_len));				
		return HANDLE_OK;
	}

	LOG4CPLUS_WARN(logger, "!! unexpected send_response() execution branch.");
	return HANDLE_ERROR;
}

int AdminClientSession::send_response_remainder()
{
	if(m_cur_resp_len > 0 && m_cur_resp_transfered < m_cur_resp_len)
	{
		// send remain part of reponse encoded buffer 
		int ret = Utility::send_nonblock_data(m_socket_fd, (char*)(m_resp_encode_buf + m_cur_resp_transfered), m_cur_resp_len - m_cur_resp_transfered);
		if(ret == -1)
		{
			// send error
			LOG4CPLUS_WARN(logger, "send response error on fd " << m_socket_fd << ", errno=" << errno);
			return HANDLE_ERROR;
		}
		else if(ret >= 0)
		{
			m_cur_resp_transfered += ret;
			if(m_cur_resp_transfered < m_cur_resp_len)
			{
				// only part of data send out
				return HANDLE_PENDING;
			}
		}
		else
		{
			return HANDLE_ERROR;
		}
	}

	return HANDLE_OK;
}

AdminCommand * AdminClientSession::send_cmd(AdminCommand * cmd, string server_ip, int server_port)
{
	AdminCommand *  ret_cmd = NULL;
	if (cmd == NULL)
		return ret_cmd;

	//string server_ip_str = Utility::ip_ntoa(server_ip);
	string real_ip = Utility::get_first_ip_from_domainname(server_ip.c_str());
	int sock_fd = Utility::connect_by_ip_port(real_ip.c_str(), server_port);
	if (sock_fd == -1)
	{
		LOG4CPLUS_WARN(logger, "failed to connect,ip_domain=" <<server_ip<<",real_ip="<< real_ip << ", server_port=" << server_port<<", errno=" << errno);
		return ret_cmd;
	}
	
	Utility::set_socket_recv_timeout(sock_fd, 12);
	Utility::set_socket_send_timeout(sock_fd, 12);

	int buffer_len = cmd->encoded_length();
	alloc_resp_buffer(buffer_len);
	cmd->encode(m_resp_encode_buf, buffer_len);
	m_cur_resp_len = buffer_len;
	m_cur_resp_transfered = 0;

	int ret = Utility::send_data(sock_fd, (char*)m_resp_encode_buf, m_cur_resp_len);
	if (ret == -1)
	{
		// send error
		LOG4CPLUS_WARN(logger, "send admin response error on fd " << m_socket_fd << ", errno=" << errno);
		close(sock_fd);
		return ret_cmd;
	}
	else
	{
		// all bytes send out successfully
		m_cur_resp_transfered = ret;

		if (cmd->get_cmd_type_id() != AdminCommandFactory::CMD_TYPE_REPORT_TRANS_STAT )
		{
			ret_cmd = AdminCommandFactory::recv_admin_command(sock_fd);
		}
		else
		{
			ret_cmd = AdminCommandFactory::recv_admin_command_cdnmgr(sock_fd);
		}
		
		close(sock_fd);
		return ret_cmd;
	}
}
