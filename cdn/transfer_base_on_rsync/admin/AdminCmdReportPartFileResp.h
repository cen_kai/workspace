//============================================================
// AdminCmdReportPartFileResp.h : interface of class AdminCmdReportPartFileResp
//                           
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#ifndef __ADMIN_CMD_REPORTPARTFILE_RESP_H_20061204_12
#define  __ADMIN_CMD_REPORTPARTFILE_RESP_H_20061204_12

#include "common/common.h"
#include "common/SDLogger.h"
#include "AdminCommand.h"
#include "AdminCommandFactory.h"

class AdminCmdReportPartFileResp : public AdminCommand
{
public:
	const static string CMD_NAME;
	
	AdminCmdReportPartFileResp() { }
	~AdminCmdReportPartFileResp() { }

	virtual string get_cmd_name() const { return AdminCmdReportPartFileResp::CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_REPORTPARTFILE_RESP; }
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const { return get_cmd_name(); }

private:
	
public:
	int m_result;
		
private:
	DECL_LOGGER(logger);
	
};

#endif // __ADMIN_CMD_REPORTPARTFILE_RESP_H_20061204_12


