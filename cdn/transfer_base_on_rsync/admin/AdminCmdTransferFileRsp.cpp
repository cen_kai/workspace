
#include "AdminCmdTransferFileRsp.h"

IMPL_LOGGER(AdminCmdTransferFileRsp, logger);
const string AdminCmdTransferFileRsp::CMD_NAME = string("AdminCmdTransferFileRsp");

bool AdminCmdTransferFileRsp::decode_parameters(byte* buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);

       m_status =  fixed_buf.get_int();

	if (m_status != AdminCmdTransferFileRsp::ACCEPT && m_status != AdminCmdTransferFileRsp::FILENOEXIST
		&& m_status != AdminCmdTransferFileRsp::OTHERFAULT)
	{
	      LOG4CPLUS_WARN(logger, "invalid m_status : " <<(int) m_status);
	       return false;
	}
		
	return true;
}

int AdminCmdTransferFileRsp::encoded_length() const
{
	int len = AdminCommand::ADMIN_CMD_HEADER_LEN;

	len += sizeof(short);
	
	len += sizeof(int);

	return len;
}

bool AdminCmdTransferFileRsp::encode(byte* buffer, int& buffer_len)
{
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
	{
		return false;
	}

	byte* buf_ptr = buffer + len;
	int remain_len = buffer_len - len;

	FixedBuffer fixed_buf((char*)buf_ptr, remain_len);

	fixed_buf.put_short((short)get_cmd_type_id());

	fixed_buf.put_int(m_status);

	
	int pos = fixed_buf.position();

	buffer_len = len + pos;
	return true;
}

string AdminCmdTransferFileRsp::get_cmd_content() const
{
	char buffer[1024];

	char* buf_ptr = buffer;

	buf_ptr += sprintf(buf_ptr, "[AdminCmdTransferFileRsp:");
/*	buf_ptr += sprintf(buf_ptr, ", m_server_ip=%s", m_server_ip);
	buf_ptr += sprintf(buf_ptr, ", m_peerid=%s", m_peerid);
	buf_ptr += sprintf(buf_ptr, ", m_stat_time=%s", Utility::get_time_string(m_stat_time).c_str());
	buf_ptr += sprintf(buf_ptr, ", m_time_span=%d", m_time_span);
	buf_ptr += sprintf(buf_ptr, ", m_cur_user_count=%d", m_cur_user_count);
	buf_ptr += sprintf(buf_ptr, ", m_max_user_count=%d", m_max_user_count);
	buf_ptr += sprintf(buf_ptr, ", m_uploaded_bytes=%llu", m_uploaded_bytes);
*/
	buf_ptr += sprintf(buf_ptr, "]");

	return (string)buffer;
}





