//============================================================
// AdminCmdQueryFileList.h : interface of class AdminCmdQueryFileList
//                           
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#ifndef __ADMIN_CMD_QUERYFILELIST_H_20061204_16
#define  __ADMIN_CMD_QUERYFILELIST_H_20061204_16

#include "common/common.h"
#include "common/SDLogger.h"
#include "AdminCommand.h"
#include "AdminCommandFactory.h"

class AdminCmdQueryFileList : public AdminCommand
{
public:
	const static string CMD_NAME;

	const static int BYWHAT_CID = 0;
	const static int BYWHAT_ACTIVE_LIST = 1;
	const static int BYWHAT_INACTIVE_LIST = 2;
	const static int BYWHAT_ALL_LIST = 3;
	
	AdminCmdQueryFileList() { }
	~AdminCmdQueryFileList() { }

	virtual string get_cmd_name() const { return CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_QUERYLIST; }
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const;

	bool is_valid_parameters()
	{
		if(m_by_what == BYWHAT_CID)
			return m_filesize > 0;
		else if(m_by_what >= BYWHAT_ACTIVE_LIST && m_by_what <= BYWHAT_ALL_LIST)
			return true;
		else
			return false;
	}

public:
	byte m_by_what;
	byte m_cid[MAX_CID_LEN];
	_u64 m_filesize;
	string m_filepath;
	
private:
	DECL_LOGGER(logger);
	
};

#endif // __ADMIN_CMD_QUERYFILELIST_H_20061204_16


