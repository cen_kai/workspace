//============================================================
// AdminCommand.h : abstract class AdminCommand
//                           (defines a common interface for all admin commands)
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#ifndef __ADMIN_COMMAND_H_20061204_13
#define  __ADMIN_COMMAND_H_20061204_13

#include <string>
#include "common/common.h"
#include "common/FixedBuffer.h"
#include "distserver/DistServerApp.h"

using std::string;

extern DistServerApp g_app;

class AdminCommand
{
public:
	const static int CUR_PROTOCOL_VER = 10;
	const static int ADMIN_CMD_HEADER_LEN = 16;	
	const static int MAGIC_NUM_V10 = 0x4c27d9e5;
	const static int MAGIC_NUM_V11 = 0x3b74d9a5;

	const static int RESP_RESULT_OK = 0;
	const static int RESP_RESULT_FAIL = 1;

	const static int FILE_EXISTS = 2;
	const static int FILE_REDOWNLOAD = 3;

	const static int DOWNLOADING = 0;
	const static int DOWNLOADSUCC = 1;
	const static int DOWNLOADFAIL = 2;
	const static int DOWNLOADFAIL_NO_FILE = 3;
	const static int STATUS_SPLIT_SUCC = 101;  // 分文件成功
	
	AdminCommand() :  m_protocol_ver(CUR_PROTOCOL_VER), m_timestamp(g_app.get_current_time()) { }
	virtual ~AdminCommand() { }

	virtual string get_cmd_name() const  = 0;
	virtual int get_cmd_type_id() const  = 0;	
	virtual bool decode_parameters(byte* buffer, int buffer_len) = 0;
	virtual int encoded_length() const = 0;
	virtual bool encode(byte* buffer, int& buffer_len) = 0;
	virtual string get_cmd_content() const = 0;

	bool encode_cmd_header(byte* buffer, int& buffer_len, int magic_num = MAGIC_NUM_V10)
	{
		if(buffer == NULL || buffer_len < ADMIN_CMD_HEADER_LEN)
			return false;

		FixedBuffer fixed_buf((char*)buffer, buffer_len);
		fixed_buf.put_int(m_protocol_ver);
		fixed_buf.put_int(magic_num);
		fixed_buf.put_int(m_timestamp);
		fixed_buf.put_int(encoded_length() - ADMIN_CMD_HEADER_LEN);

		buffer_len = fixed_buf.position();
		return true;
	}

	bool encode_cmd_header_cdnmgr(byte* buffer, int& buffer_len, int magic_num = MAGIC_NUM_V10)
	{
		if(buffer == NULL || buffer_len < ADMIN_CMD_HEADER_LEN)
			return false;

		FixedBuffer fixed_buf((char*)buffer, buffer_len);
		fixed_buf.put_int(40); //ver
		fixed_buf.put_int(0); //seq
		//fixed_buf.put_int(m_timestamp);
		fixed_buf.put_int(encoded_length() - 12);

		buffer_len = fixed_buf.position();
		return true;
	}

public:
	int m_protocol_ver;
	int m_timestamp;
};

#endif //#ifndef __ADMIN_COMMAND_H_20061204_13



