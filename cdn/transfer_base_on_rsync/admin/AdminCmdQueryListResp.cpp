//============================================================
// AdminCmdQueryListResp.cpp : implementation of class AdminCmdQueryListResp
//                           
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#include "AdminCmdQueryListResp.h"
#include "common/FixedBuffer.h"

IMPL_LOGGER(AdminCmdQueryListResp, logger);

const string AdminCmdQueryListResp::CMD_NAME = string("QUERYLIST_RESP");

bool AdminCmdQueryListResp::decode_parameters(byte * buffer, int buffer_len)
{
	LOG4CPLUS_INFO(logger, "decode_parameters() entering for  '" << CMD_NAME << "'");

	m_file_list.clear();
	LOG4CPLUS_INFO(logger, "before decode_parameters(),size=" << m_file_list.size());

	FixedBuffer fixed_buf((char*)buffer, buffer_len);

	m_result = fixed_buf.get_int();

	if (m_result != 0)
	{
		return true;
	}

	int arraysize = fixed_buf.get_int();
	if (arraysize == 0 )
	{
		return true;
	}

	FileInfo temp_info;
	for (int i = 0; i < arraysize; ++i)
	{
		int stru_len = fixed_buf.get_int();
		int cid_len = fixed_buf.get_int();
		fixed_buf.get_bytes(temp_info.m_cid, cid_len);
		temp_info.m_filesize = fixed_buf.get_int64();
		temp_info.m_status = fixed_buf.get_byte();
		temp_info.m_uploaded_bytes = fixed_buf.get_int64();
		temp_info.m_down_url = fixed_buf.get_string();
		m_file_list.push_back(temp_info);

		LOG4CPLUS_INFO(logger, "decode_parameters(),url=" << temp_info.m_down_url);
	}

	LOG4CPLUS_INFO(logger, "after decode_parameters() finished,size=" << m_file_list.size());	
	
	return true;
}

int AdminCmdQueryListResp::encoded_length() const
{
	int len = AdminCommandFactory::get_cmd_header_len();

	// cmd_type_id
	len += sizeof(short);

	// m_result
	len += sizeof(int);

	if(m_result != 0)
	{
		// failure
		len += sizeof(int);
	}
	else
	{
		len += sizeof(int);
		for(int i = 0, size= m_file_list.size(); i < size; i++)
		{
			const FileInfo& file = m_file_list[i];
			len += file.encoded_length();
		}
	}
	
	return len;
}

bool AdminCmdQueryListResp::encode(byte * buffer, int & buffer_len)
{
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
		return false;

	byte* buf = buffer + len;
	int remain_len = buffer_len - len;
	FixedBuffer fixed_buf((char*)buf, remain_len);

	// cmd_type_id
	fixed_buf.put_short((short)get_cmd_type_id());

	// m_result
	fixed_buf.put_int(m_result);
	
	if(m_result != 0)
	{
		// failed
		fixed_buf.put_int(0);
	}
	else
	{
		int size = (int)m_file_list.size();
		fixed_buf.put_int(size);


		buf += fixed_buf.position();
		remain_len -= fixed_buf.position();
//		LOG4CPLUS_DEBUG(logger, "buffer position=" << fixed_buf.position() << ", offset=" << (buf - buffer));		
		for(int i = 0; i < size; i++)
		{
			FileInfo& file = m_file_list[i];

			int len = remain_len;
			file.encode(buf, len);
			buf += len;
			remain_len -= len;
		}
	}

	buffer_len = buf - buffer;
	
	return true;
}


