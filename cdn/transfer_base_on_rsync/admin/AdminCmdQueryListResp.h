//============================================================
// AdminCmdQueryListResp.h : interface of class AdminCmdQueryListResp
//                           
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#ifndef __ADMIN_CMD_QUERYLISTRESP_H_20061204_22
#define  __ADMIN_CMD_QUERYLISTRESP_H_20061204_22

#include "common/common.h"
#include "common/SDLogger.h"
#include "AdminCommand.h"
#include "AdminCommandFactory.h"
#include <vector>

using std::vector;

class AdminCmdQueryListResp : public AdminCommand
{
public:
	struct FileInfo
	{
		byte m_cid[MAX_CID_LEN];
		_u64 m_filesize;
		byte m_status;
		_u64 m_uploaded_bytes;
		string m_down_url;

		int encoded_length() const
		{
			int len = sizeof(int); // length prefix (total struct length)
			len += (sizeof(int) + MAX_CID_LEN); // m_cid
			len += sizeof(_u64); // m_filesize
			len += sizeof(byte); // m_status
			len += sizeof(_u64); // m_uploaded_bytes
			len += (sizeof(int) + m_down_url.length());

			return len;
		}
		bool encode(byte* buffer, int& buffer_len)
		{
			int total_len = encoded_length();
			if(buffer == NULL || buffer_len < total_len)
				return false;

			FixedBuffer fixed_buf((char*)buffer, buffer_len);
			fixed_buf.put_int(total_len - sizeof(int));
			fixed_buf.put_string((char*)m_cid, MAX_CID_LEN);
			fixed_buf.put_int64(m_filesize);
			fixed_buf.put_byte(m_status);
			fixed_buf.put_int64(m_uploaded_bytes);
			fixed_buf.put_string(m_down_url);

			buffer_len = fixed_buf.position();
			return true;
		}
	};

	enum FILE_STATUS
	{
		FILE_ACTIVE = 1,
		FILE_INACTIVE = 2,
		FILE_NON_EXIST = 3,
		FILE_MD5_CHECK_FAILED = 4
	};

public:
	const static string CMD_NAME;
	
	AdminCmdQueryListResp() { }
	~AdminCmdQueryListResp() { }

	virtual string get_cmd_name() const { return CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_QUERYLIST_RESP; }
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const { return get_cmd_name(); }

	void add_file(byte* cid, _u64 filesize, bool active, _u64 uploaded_bytes, const string& down_url)
	{
		FileInfo file;
		memcpy(file.m_cid, cid, MAX_CID_LEN);
		file.m_filesize = filesize;
		file.m_status = active ? FILE_ACTIVE : FILE_INACTIVE;
		file.m_uploaded_bytes = uploaded_bytes;
		file.m_down_url = down_url;

		m_file_list.push_back(file);
	}
	
	void add_nonexist_file(byte* cid, _u64 filesize)
	{
		FileInfo file;
		memcpy(file.m_cid, cid, MAX_CID_LEN);
		file.m_filesize = filesize;
		file.m_status = FILE_NON_EXIST;
		file.m_uploaded_bytes = 0;
		
		m_file_list.push_back(file);		
	}

	void add_md5failed_file(byte* cid, _u64 filesize)
	{
		FileInfo file;
		memcpy(file.m_cid, cid, MAX_CID_LEN);
		file.m_filesize = filesize;
		file.m_status = FILE_MD5_CHECK_FAILED;
		file.m_uploaded_bytes = 0;
		
		m_file_list.push_back(file);		
	}
	

public:
	int m_result;
	vector<FileInfo> m_file_list;
		
private:
	DECL_LOGGER(logger);
	
};

#endif // __ADMIN_CMD_QUERYLISTRESP_H_20061204_22

