
#ifndef __COMMAND_GET_FILE_REQ_H_20090123
#define  __COMMAND_GET_FILE_REQ_H_20090123

#include <string>
#include "common/common.h"
#include "common/FixedBuffer.h"
#include "AdminCommandFactory.h"
#include "AdminCommand.h"

class AdminCmdGetFileReq : public AdminCommand
{
     public:
	const static string CMD_NAME;
	
	AdminCmdGetFileReq() 
	{
	}
	~AdminCmdGetFileReq()
	{
	}

	virtual string get_cmd_name() const { return CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_GET_FILE_REQ; }
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const;

public:
	char m_cid[MAX_CID_LEN]; 
	_u64 m_FileSize;
	//int m_server_ip;
	short m_bind_port;
	
	string m_FileName;
	
	
protected:
	 	DECL_LOGGER(logger);
};




#endif //__COMMAND_GET_FILE_REQ_H_20090123

