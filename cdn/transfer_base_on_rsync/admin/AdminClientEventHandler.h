//============================================================
// AdminClientEventHandler.h : class AdminClientEventHandler interface and implementation
//                                 (handling admin request from web admin)
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#ifndef __ADMINCLIENT_EVENT_HANDLER_H_20061204_13
#define  __ADMINCLIENT_EVENT_HANDLER_H_20061204_13

#include "AdminSessionManager.h"
#include "distserver/EventHandlerFactory.h"
#include <unistd.h>
#include <errno.h>

extern AdminSessionManager g_admin_session_mgr;
extern EventHandlerFactory g_event_handler_mgr;

class AdminClientEventHandler : public DefaultEventCallBack
{
	
public:
	AdminClientEventHandler(DistServerApp* app) : m_server_app(app) 
	{
		read_conf();
	}	


	virtual void on_fd_readable(int fd, EventData* event_info)
	{
		LOG4CPLUS_DEBUG(logger, "admin client fd " << fd << " is readable now.");
		
		AdminClientSession* session = g_admin_session_mgr.find_session_by_fd(fd);
		if(session == NULL)
		{
			LOG4CPLUS_WARN(logger, "!! Can not find admin client session context when fd " << fd << " is readable");
			get_event_server()->notify_fd_closed(fd);
			return;
		}

		int ret = session->on_fd_readable(fd);
		if(ret < 0)
		{
			// read error
			LOG4CPLUS_WARN(logger, "!! AdminClientEventHandler close fd because  on_fd_readable error,fd " << fd << " is read wrong");
			if(g_admin_session_mgr.remove_session(fd))
			{
				close(fd);
			}
			get_event_server()->notify_fd_closed(fd);
		}
		else
		{
			if(ret == 0 && session->have_response_to_send())
			{
				// response can not send out this time, we should wait for fd's writable event
				EventInfo ev_info(this, m_client_send_timeout_ms);
				get_event_server()->add_event(fd, IOEventServer::EVENT_WRITABLE, &ev_info);
			}
			else
			{
				// current cmd handled successfully, try to read next cmd
				EventInfo ev_info(this, m_client_read_timeout_ms);
				get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info);
			}
		}
	}

	virtual void on_fd_writable(int fd, EventData* event_info)
	{
		LOG4CPLUS_DEBUG(logger, "admin client fd " << fd << " is writable now.");
	
		AdminClientSession* session = g_admin_session_mgr.find_session_by_fd(fd);
		if(session == NULL)
		{
			LOG4CPLUS_WARN(logger, "!! Can not find admin client session context when fd " << fd << " is writable.");
			get_event_server()->notify_fd_closed(fd);			
			return;
		}
			
		int ret = session->on_fd_writable(fd);
		if(ret < 0)
		{
			// some error happened or request can not be handled(for example, requested file not available),
			//  so we should close connection now
			LOG4CPLUS_WARN(logger, "!! AdminClientEventHandler close fd because  on_fd_writable error,fd " << fd << " is write wrong");
			if(g_admin_session_mgr.remove_session(fd))
			{
				close(fd);
			}
			get_event_server()->notify_fd_closed(fd);			
		}
		else
		{
			if(ret == 0)
			{
				// response can not send out this time, we should wait for fd's writable event
				EventInfo ev_info(this, m_client_send_timeout_ms);
				get_event_server()->add_event(fd, IOEventServer::EVENT_WRITABLE, &ev_info);
			}
			else
			{
				int recv_timeout = m_client_read_timeout_ms;
				EventInfo ev_info(this, recv_timeout);
				get_event_server()->add_event(fd, IOEventServer::EVENT_READABLE, &ev_info);
			}
		}
		
	}

	virtual void on_fd_timeout(int fd, EventData* event_info)
	{
		LOG4CPLUS_INFO(logger, "admin client socket (fd=" << fd << ") timeout.");	

		AdminClientSession* session = g_admin_session_mgr.find_session_by_fd(fd);
		if(session == NULL)
		{
			LOG4CPLUS_WARN(logger, "!! Can not find admin client session context when fd " << fd << " timeout.");
			get_event_server()->notify_fd_closed(fd);			
			return;
		}	

		int ret = session->on_fd_timeout(fd);
		if(ret < 0)
		{
			// abnormal timeout, should close connection
			LOG4CPLUS_WARN(logger, "!! AdminClientEventHandler close fd because  on_fd_timeout error,fd " << fd << " is time out wrong");
			if(g_admin_session_mgr.remove_session(fd))
			{
				close(fd);
			}
			get_event_server()->notify_fd_closed(fd);			
		}
		else if(ret == 0)
		{
		
		}
		else if(ret > 0)
		{
		
		}

	}

	
	virtual void on_fd_error(int fd, EventData* event_info)
	{
		LOG4CPLUS_WARN(logger, "admin client socket " << fd << " is in error state, close it now!");
		if(g_admin_session_mgr.remove_session(fd))
		{
			close(fd);
		}
		get_event_server()->notify_fd_closed(fd);		
	}

private:
	void read_conf()
	{
		if(m_server_app)
		{
			m_client_read_timeout_ms = m_server_app->get_conf_int("distserver.peer.read.timeout.ms", 12000);
			m_client_send_timeout_ms = m_server_app->get_conf_int("distserver.peer.send.timeout.ms", 10000);
		}
		else
		{
			m_client_read_timeout_ms = 12000; // 12 seconds
			m_client_send_timeout_ms = 10000;
		}	
	}	

private:
	DistServerApp* m_server_app;

	int m_client_read_timeout_ms;	
	int m_client_send_timeout_ms;	
	
	DECL_LOGGER(logger);	
};

IMPL_LOGGER(AdminClientEventHandler, logger);

#endif // #ifndef __ADMINCLIENT_EVENT_HANDLER_H_20061204_13


