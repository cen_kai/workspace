

#include "AdminCmdRptTransStat.h"

IMPL_LOGGER(AdminCmdRptTransStat, logger);

const string AdminCmdRptTransStat::CMD_NAME = string("AdminCmdRptTransStat");

bool AdminCmdRptTransStat::decode_parameters(byte* buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);

       int t_len = 0;
	
       t_len = fixed_buf.get_int();
	if(t_len != MAX_CID_LEN)
	{
		LOG4CPLUS_WARN(logger, "invalid cid length: " << t_len);
		return false;
	}
	fixed_buf.get_bytes((byte*)m_cid, t_len);

       m_FileSize = fixed_buf.get_int64();
	   
	m_errcode = fixed_buf.get_byte();
	
	return true;
}

int AdminCmdRptTransStat::encoded_length() const
{
	//int len = AdminCommand::ADMIN_CMD_HEADER_LEN;
	int len = 12;

	len += sizeof(char); //cmd_id

	len += (sizeof(int) + MAX_CID_LEN); //cid
	
	len += sizeof(_u64); //filesize

	len += sizeof(int); //errcode

	LOG4CPLUS_DEBUG(logger, "len=" << len);
	
	return len;
}

bool AdminCmdRptTransStat::encode(byte* buffer, int& buffer_len)
{
	int len = buffer_len;
	if(!encode_cmd_header_cdnmgr(buffer, len))
	{
		return false;
	}

	byte* buf_ptr = buffer + len;
	int remain_len = buffer_len - len;

	FixedBuffer fixed_buf((char*)buf_ptr, remain_len);

	fixed_buf.put_byte((unsigned char)get_cmd_type_id());

	fixed_buf.put_string(m_cid, MAX_CID_LEN);

	fixed_buf.put_int64(m_FileSize);
 
	fixed_buf.put_int(m_errcode);
	
	int pos = fixed_buf.position();

	buffer_len = len + pos;
	LOG4CPLUS_DEBUG(logger, "buffer_len=" << buffer_len);
	return true;
}

string AdminCmdRptTransStat::get_cmd_content() const
{
	char buffer[1024];

	char* buf_ptr = buffer;

	buf_ptr += sprintf(buf_ptr, "[AdminCmdRptTransStat:");
	buf_ptr += sprintf(buf_ptr, "]");

	return (string)buffer;
}




