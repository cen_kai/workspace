// AdminSessionManager.h : interface of class AdminSessionManager 
//			   (thread-safe AdminSession container using hashtable)
//
// Author: JeffLuo
// Created: 2006-12-04
//===============================================================

#ifndef __ADMIN_SESSION_MANAGER_H_20061204_15
#define  __ADMIN_SESSION_MANAGER_H_20061204_15

#include <ext/hash_map>
#include "AdminClientSession.h"
#include "common/SDLogger.h"

using namespace __gnu_cxx;


class AdminSessionManager
{
	typedef hash_map<int, AdminClientSession*> Hashtable;
	typedef hash_map<int, AdminClientSession*>::iterator  ht_iterator;
	const static int INIT_HASHTABLE_BUCKET_NUM = 16;
public:
	AdminSessionManager();

	AdminClientSession* find_session_by_fd(int socket_fd);	
	bool remove_session(int socket_fd);
	bool add_session(AdminClientSession* session);

	int active_sesssion_num() const { return m_session_maps.size(); }
	
private:
	Hashtable m_session_maps;

private:
	DECL_LOGGER(logger);
};

#endif // #ifndef __ADMIN_SESSION_MANAGER_H_20061204_15


