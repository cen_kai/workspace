//============================================================
// AdminCmdReportPartFile.h : interface of class AdminCmdReportPartFile
//                         
// Author: JeffLuo
// Created: 2006-09-08
//============================================================

#ifndef __ADMIN_CMD_REPORTPARTFILE_H_20060908_37
#define  __ADMIN_CMD_REPORTPARTFILE_H_20060908_37

#include "AdminCommand.h"
#include "AdminCommandFactory.h"
#include "common/SDLogger.h"
#include "common/FixedBuffer.h"
#include <vector>

using std::vector;

class AdminCmdReportPartFile : public AdminCommand
{
	class FileBlockInfo
	{
	public:
		const static _u64 MAX_FILESIZE_32BIT = (_u64)4 * 1024 * 1024 * 1024; // 4GB
		
		_u64 m_file_pos;
		_u64 m_file_len;

		FileBlockInfo() : m_file_pos(0), m_file_len(0) { }

		int calc_encoded_length() const
		{
			int pos_store_len = m_file_pos > MAX_FILESIZE_32BIT ? 8 : 4;
			int len_store_len = m_file_len > MAX_FILESIZE_32BIT ? 8 : 4;
			return 1 + pos_store_len + len_store_len;
		}

		bool encode(byte* buffer, int& buffer_len)
		{
			if(buffer == NULL || buffer_len < calc_encoded_length())
				return false;
			
			byte* cur_ptr = buffer;
			int real_len = 0;		
			
			byte pos_store_len = m_file_pos > MAX_FILESIZE_32BIT ? 8 : 4;
			byte len_store_len = m_file_len > MAX_FILESIZE_32BIT ? 8 : 4;
			byte store_type = (len_store_len << 4) | (pos_store_len & 0xF);		

			*cur_ptr = store_type;
			cur_ptr += sizeof(byte);
			real_len += sizeof(byte);

			int offset = copy_int64(cur_ptr, m_file_pos);
			cur_ptr += offset;
			real_len += offset;

			offset = copy_int64(cur_ptr, m_file_len);
			cur_ptr += offset;
			real_len += offset;

			buffer_len = real_len;

			return true;
		}

		bool decode(byte* buffer, int buffer_len, int& consumed_bytes)
		{
			FixedBuffer fixed_buf((char*)buffer, buffer_len);

			byte store_type = fixed_buf.get_byte();
			int pos_store_len = store_type & 0xF;
			int len_store_len = (store_type >> 4) & 0xF;

			int readed_bytes = 0;
			int shift_bits = 0;
			_u64 data_pos = 0;
			while(readed_bytes < pos_store_len)
			{
				byte b = fixed_buf.get_byte();
				data_pos |= (((_u64)b) << shift_bits);
				readed_bytes++;
				shift_bits += 8;
			}

			readed_bytes = 0;
			shift_bits = 0;
			_u64 date_len = 0;
			while(readed_bytes < len_store_len)
			{
				byte b = fixed_buf.get_byte();
				date_len |= (((_u64)b) << shift_bits);
				readed_bytes++;
				shift_bits += 8;
			}

			m_file_pos = data_pos;
			m_file_len= date_len;

			consumed_bytes = fixed_buf.position();
			return true;
		}

	private:
		int copy_int64(byte* buf, _u64 data)
		{
			if(data > MAX_FILESIZE_32BIT)
			{
				memcpy(buf, &data, sizeof(_u64));
				return sizeof(_u64);	
			}
			else
			{
				_u32 temp_data = (_u32)data;
				memcpy(buf, &temp_data, sizeof(_u32));
				return sizeof(_u32);
			}
		}
	};
public:
	const static string CMD_NAME;

	AdminCmdReportPartFile() 
	{ 
		m_down_ratio = 0;
	}
	
	virtual ~AdminCmdReportPartFile() { }

	virtual string get_cmd_name() const  { return CMD_NAME; }
	virtual int get_cmd_type_id() const { return AdminCommandFactory::CMD_TYPE_REPORTPARTFILE;}	
	virtual bool decode_parameters(byte* buffer, int buffer_len);
	virtual int encoded_length() const;
	virtual bool encode(byte* buffer, int& buffer_len);
	virtual string get_cmd_content() const;

	void add_file_block(_u64 block_start_pos, _u64 block_len)
	{
		FileBlockInfo block;
		block.m_file_pos = block_start_pos;
		block.m_file_len = block_len;
		m_block_list.push_back(block);
	}

	FileBlockInfo* get_block_info(int block_index)
	{
		if(block_index >= 0 && block_index < (int)m_block_list.size())
			return &m_block_list[block_index];
		else
			return NULL;
	}
	
public:

	ushort m_cmdtypeID;
	byte m_stat;  // status
	string m_filepath;
	_u64 m_filesize;
	byte m_cid[MAX_CID_LEN + 1];
	byte m_gcid[MAX_CID_LEN + 1];
	byte m_down_ratio; // 0 ~ 100

	vector<FileBlockInfo> m_block_list;

private:
	int calc_block_list_encoded_len() const
	{
		int len = 0;
		for(int i = 0, size = m_block_list.size(); i < size; i++)
		{
			const FileBlockInfo& block = m_block_list[i];
			len += block.calc_encoded_length();
		}

		return len;
	}
		
private:
	DECL_LOGGER(logger);
};

#endif // __ADMIN_CMD_REPORTPARTFILE_H_20060908_37



