//==============================================================
// AdminClientSession.h : interface of class AdminClientSession 
//			   (interaction between web admin client and distserver)
//
// Author: JeffLuo
// Created: 2006-12-04
//===============================================================

#ifndef __ADMINCLIENT_SESSION_H_20061204_16
#define  __ADMINCLIENT_SESSION_H_20061204_16

#include "common/common.h"
#include "common/Utility.h"
#include <list>
#include <unistd.h>
#include "common/SDLogger.h"
#include "distserver/DistServerApp.h"
#include "AdminCommand.h"

using std::list;

extern DistServerApp g_app;

class AdminClientSession
{
public:
	//const static byte ZERO_CID[MAX_CID_LEN] = {0};

	const static int CMD_HEADER_MAX_LEN = 16;	
	const static int MAX_CMD_BODY_LEN = 16 * 1024;

	const static int MIN_BUF_ALLOC_SIZE = 1024; 
	const static int BUF_ALLOC_ALIGN_SIZE = 256;	

	enum READ_CMD_STATUS{
		READ_PENDING = 0,
		READ_OK = 1,
		READ_ERROR = -1,
		READ_EOF = -2,
		READ_INVALID_DATA = -3
	};
	enum HANDLE_CMD_STATUS{
		HANDLE_PENDING = 0,
		HANDLE_OK = 1,
		HANDLE_ERROR = -1,
	};	


	AdminClientSession();
	~AdminClientSession();

	int on_fd_readable(int fd);	
	int on_fd_writable(int fd);
	int on_fd_timeout(int fd);	

	bool have_response_to_send()
	{
		if(m_cur_resp_len > 0 && m_cur_resp_transfered < m_cur_resp_len)
			return true;

		return false;
	}	

private:
	void reset_recv_buffer();
	int read_cmd(int fd);
	int handle_request(AdminCommand* cmd);
	//int handle_querylist_cmd(AdminCommand* cmd);
	//int handle_reportpartfile_cmd(AdminCommand * cmd);
	int handle_transmit_trans_file_req(AdminCommand *cmd);
	int handle_get_file(AdminCommand *cmd);
	
	
	int fetch_cmd_body_len()
	{
		return *(int*)(m_cur_header_buf + sizeof(int) * 3);
	}	
	bool is_valid_protocol_version(int protocol_ver) { return protocol_ver == 10 || protocol_ver == 40;}	
	bool is_valid_magic_num(int* sig = NULL)
	{
		int protocol_ver = *(int*)(m_cur_header_buf);
		int magic_num = *(int*)(m_cur_header_buf + sizeof(int));
		if(sig != NULL)
			*sig = magic_num;
		
		if(protocol_ver == 10)
		{
			if((magic_num == AdminCommand::MAGIC_NUM_V10) || (magic_num == AdminCommand::MAGIC_NUM_V11))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
			return false;
	}

	bool alloc_resp_buffer(int required_size)
	{
		int old_buf_size = m_resp_encode_buf == NULL ? 0 : m_resp_buf_len;
		if(required_size <= old_buf_size)
			return false;

		int new_buf_size = required_size;
		if(required_size <= MIN_BUF_ALLOC_SIZE)
			new_buf_size = MIN_BUF_ALLOC_SIZE;
		else
			new_buf_size = (required_size / BUF_ALLOC_ALIGN_SIZE + 1) * BUF_ALLOC_ALIGN_SIZE;

		if(m_resp_encode_buf)
			delete []m_resp_encode_buf;
		m_resp_encode_buf = new byte[new_buf_size];
		m_resp_buf_len = new_buf_size;

		if(old_buf_size > 0)
		{
			LOG4CPLUS_INFO(logger, "now response buffer size is " << new_buf_size << " (from " << old_buf_size << ").");
		}

		return true;
	}

	int send_response(AdminCommand* resp);	
	int send_response_remainder();	
	AdminCommand *  send_cmd(AdminCommand * cmd, string server_ip, int server_port);
	
public:
	int m_socket_fd;
	unsigned int m_sock_addr;

	short m_protocol_version;
	_u64 m_start_time;

private:
	// active header info
	byte m_cur_header_buf[CMD_HEADER_MAX_LEN];
	short m_cur_header_len;
	short m_cur_header_recv;

	// active body info
	byte* m_cur_body_buf;
	int m_cur_body_len;
	int m_cur_body_recv;	

	byte* m_resp_encode_buf;
	int m_resp_buf_len;
	int m_cur_resp_len;
	int m_cur_resp_transfered;

private:
	DECL_LOGGER(logger);	
};


#endif // #ifndef __ADMINCLIENT_SESSION_H_20061204_16


