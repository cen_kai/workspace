// AdminCommandFactory.cpp : implementation of class AdminCommandFactory 
//			   (Admin command factory, create admin cmd object from I/O byte-stream)
//
// Author: JeffLuo
// Created: 2006-12-04
//===============================================================

#include "AdminCommandFactory.h"
#include "common/FixedBuffer.h"

#include "AdminCmdQueryFileList.h"
#include "AdminCmdQueryListResp.h"

#include "AdminCmdReportPartFile.h"
#include "AdminCmdReportPartFileResp.h"

#include "AdminCmdTransferFileReq.h"
#include "AdminCmdTransferFileRsp.h"

#include "AdminCmdGetFileReq.h"
#include "AdminCmdGetFileRsp.h"

#include "AdminCmdRptTransStat.h"
#include "AdminCmdRptTransStatRsp.h"

IMPL_LOGGER(AdminCommandFactory, logger);

const  string AdminCommandFactory::CMD_NAME_MODIFYSTATUS_RESP = string("MODIFYSTATUS_RESP");
const  string AdminCommandFactory::CMD_NAME_DELETEFILES_RESP = string("DELETEFILES_RESP");

AdminCommand* AdminCommandFactory::decode_admin_command(byte* header_buf, int header_buf_len, byte* body_buf, int body_buf_len)
{
	if(header_buf == NULL || header_buf_len <= 0 || body_buf == NULL || body_buf_len <= 0)
		return NULL;

	AdminCommand* ret_cmd = NULL;

	int protocol_ver = *(int*)header_buf;
	byte* body_buf_ptr = body_buf;
	int cur_body_buf_len = body_buf_len;

	short cmd_typeid = 0;
	if(protocol_ver == 10)
	{
		cmd_typeid = *(short*)body_buf_ptr;
	}
	else
	{
		cmd_typeid = (short)(*(unsigned char*)body_buf_ptr);
	}

	LOG4CPLUS_INFO(logger, "cmd_typeid = " << cmd_typeid);
	
	AdminCommand* cmd = create_admin_cmd(cmd_typeid);

	if(cmd == NULL)
	{
		LOG4CPLUS_WARN(logger, "can not create admin command by typeid " << cmd_typeid);
		return NULL;
	}
	LOG4CPLUS_DEBUG(logger, "successfully create admin command '" << cmd->get_cmd_name() << "'.");

	if (protocol_ver == 10)
	{
		body_buf_ptr += sizeof(short);
		cur_body_buf_len -= sizeof(short);
	}
	else
	{
		body_buf_ptr += sizeof(char);
		cur_body_buf_len -= sizeof(char);
	}
	
	if(!cmd->decode_parameters(body_buf_ptr, cur_body_buf_len))
	{
		LOG4CPLUS_WARN(logger, "failed to decode parameters of admin command '" << cmd->get_cmd_name() << "'");
		delete cmd;			
		return NULL;			
	}

	ret_cmd = cmd;

	LOG4CPLUS_DEBUG(logger, "successfully create admin command and content is '" << ret_cmd->get_cmd_content() << "'.");
	return ret_cmd;
}

AdminCommand* AdminCommandFactory::create_admin_cmd(int cmd_typeid)
{
	AdminCommand* cmd = NULL;
	switch(cmd_typeid)
	{ 
	case CMD_TYPE_QUERYLIST:
		cmd = new AdminCmdQueryFileList();
		break;
		
	case CMD_TYPE_QUERYLIST_RESP:
		cmd = new AdminCmdQueryListResp();
		break;

	case CMD_TYPE_REPORTPARTFILE:
		cmd = new AdminCmdReportPartFile();
		break;
	case CMD_TYPE_REPORTPARTFILE_RESP:
		cmd = new AdminCmdReportPartFileResp();
		break;	
		
	case CMD_TYPE_TRANSFER_FILE_REQ:
		cmd = new AdminCmdTransferFileReq();
		break;
		
	case CMD_TYPE_TRANSFER_FILE_RSP:
		cmd = new AdminCmdTransferFileRsp();
		break;
		
	case CMD_TYPE_REPORT_TRANS_STAT:
		cmd = new AdminCmdRptTransStat();
		break;
		
	case CMD_TYPE_REPORT_TRANS_STAT_RSP:
		cmd = new AdminCmdRptTransStatRsp();
		break;
		
	case CMD_TYPE_GET_FILE_REQ:
		cmd = new AdminCmdGetFileReq();
		break;
		
	case CMD_TYPE_GET_FILE_REQ_RSP:
		cmd = new AdminCmdGetFileRsp();
		break;
	
	default:	
		LOG4CPLUS_WARN(logger, "unexpected cmd typeid: " << cmd_typeid);
	}

	return cmd;
}

AdminCommand* AdminCommandFactory::recv_admin_command(int sock_fd)
{
	byte header_buf[ADMIN_CMD_HEADER_LEN];	

	int ret = Utility::recv_data(sock_fd, (char *)header_buf, ADMIN_CMD_HEADER_LEN);
	if(ret == -1 || ret == 0)
	{
		LOG4CPLUS_WARN(logger, "failed to receive admin command header, ret=" << ret << ", errno=" << errno);
		return NULL;
	}

	// protocol ver
	int protocol_ver = *(int*)header_buf;
	if(CUR_ADMIN_PROTOCOL != protocol_ver)
	{
		LOG4CPLUS_WARN(logger, "recv_admin_command,invalid admin protocol version: " << protocol_ver);
		return NULL;
	}
	int magic_num = *(int*)(header_buf + sizeof(int));
	if(magic_num != AdminCommand::MAGIC_NUM_V10 && magic_num != AdminCommand::MAGIC_NUM_V11)
	{
		LOG4CPLUS_WARN(logger, "invalid admin cmd magic number: " << magic_num);
		return NULL;
	}

	// skip timestamp field

	// cmd body length
	int body_len = *(int*)(header_buf + sizeof(int) * 3);
	if(body_len <= 0 || body_len >= MAX_ADMIN_CMD_LENGTH)
	{
		LOG4CPLUS_WARN(logger, "invalid admin command body length: " << body_len);
		return NULL;
	}

	// receive command body
	byte* body_buf = new byte[body_len];
	ret = Utility::recv_data(sock_fd, (char*)body_buf, body_len);
	if(ret == -1 || ret == 0)
	{
		LOG4CPLUS_WARN(logger, "failed to receive admin command body, ret=" << ret << ", errno=" << errno);
		delete[] body_buf;
		return NULL;
	}

	LOG4CPLUS_DEBUG(logger, "successfully receive admin-cmd response, body-length=" << body_len);
	AdminCommand* cmd = decode_admin_command(header_buf, ADMIN_CMD_HEADER_LEN, body_buf, body_len);
	delete [] body_buf;

	return cmd;
}

AdminCommand* AdminCommandFactory::recv_admin_command_cdnmgr(int sock_fd)
{
	byte header_buf[ADMIN_CMD_HEADER_LEN_CDNMGR];	

	int ret = Utility::recv_data(sock_fd, (char *)header_buf, ADMIN_CMD_HEADER_LEN_CDNMGR);
	if(ret == -1 || ret == 0)
	{
		LOG4CPLUS_WARN(logger, "failed to receive admin command header, ret=" << ret << ", errno=" << errno);
		return NULL;
	}

	// protocol ver
	int protocol_ver = *(int*)header_buf;
	if(CUR_ADMIN_PROTOCOL != protocol_ver && 40 != protocol_ver)
	{
		LOG4CPLUS_WARN(logger, "recv_admin_command_cdnmgr,invalid admin protocol version: " << protocol_ver);
		return NULL;
	}

	// cmd body length
	int body_len = *(int*)(header_buf + sizeof(int) * 2);
	if(body_len <= 0 || body_len >= MAX_ADMIN_CMD_LENGTH)
	{
		LOG4CPLUS_WARN(logger, "invalid admin command body length: " << body_len);
		return NULL;
	}

	// receive command body
	byte* body_buf = new byte[body_len];
	ret = Utility::recv_data(sock_fd, (char*)body_buf, body_len);
	if(ret == -1 || ret == 0)
	{
		LOG4CPLUS_WARN(logger, "failed to receive admin command body, ret=" << ret << ", errno=" << errno);
		delete[] body_buf;
		return NULL;
	}

	LOG4CPLUS_DEBUG(logger, "successfully receive admin-cmd response, body-length=" << body_len);
	AdminCommand* cmd = decode_admin_command(header_buf, ADMIN_CMD_HEADER_LEN_CDNMGR, body_buf, body_len);
	delete [] body_buf;

	return cmd;
}

AdminCommand* AdminCommandFactory::send_admin_command(int sock_fd, AdminCommand* cmd)
{
	if(cmd == NULL)
		return NULL;

	byte buffer[MAX_ADMIN_CMD_LENGTH];
	int encoded_len = MAX_ADMIN_CMD_LENGTH;
	cmd->encode(buffer, encoded_len);

	
	// send data
	if(Utility::send_data(sock_fd, (char *)buffer, encoded_len) < encoded_len)
	{
		LOG4CPLUS_WARN(logger, "failed to send " << encoded_len << " bytes of admin command " << cmd->get_cmd_name());
		return NULL;
	}

	LOG4CPLUS_INFO(logger, "send admin command '" << cmd->get_cmd_name() << "' of " << encoded_len << " bytes successfully.");

	// receive response
	return recv_admin_command(sock_fd);
}


