//============================================================
// AdminCmdReportPartFileResp.cpp : implementation of class AdminCmdReportPartFileResp
//                           
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#include "AdminCmdReportPartFileResp.h"
#include "common/FixedBuffer.h"

IMPL_LOGGER(AdminCmdReportPartFileResp, logger);

const string AdminCmdReportPartFileResp::CMD_NAME = string("ReportPartFileResp");

bool AdminCmdReportPartFileResp::decode_parameters(byte * buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);

	m_result = fixed_buf.get_int();
	
	return true;
}

int AdminCmdReportPartFileResp::encoded_length() const
{
	int len = AdminCommandFactory::get_cmd_header_len();

	// cmd_type_id
	len += sizeof(short);

	// m_result
	len += sizeof(int);
	
	return len;
}

bool AdminCmdReportPartFileResp::encode(byte * buffer, int & buffer_len)
{
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
		return false;

	byte* buf_ptr = buffer + len;
	int remain_len = buffer_len - len;

	// cmd type
	*(short*)buf_ptr = (short)get_cmd_type_id();
	buf_ptr += sizeof(short);
	remain_len -= sizeof(short);	

	// m_result
	*(int*)buf_ptr = m_result;
	buf_ptr += sizeof(int);
	remain_len -= sizeof(int);

	buffer_len = buf_ptr - buffer;
	
	return true;
}
