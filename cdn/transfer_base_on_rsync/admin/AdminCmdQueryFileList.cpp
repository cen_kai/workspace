//============================================================
// AdminCmdQueryFileList.cpp : implementation of class AdminCmdQueryFileList
//                           
// Author: JeffLuo
// Created: 2006-12-04
//============================================================

#include "AdminCmdQueryFileList.h"
#include "common/FixedBuffer.h"
#include "common/Utility.h"

IMPL_LOGGER(AdminCmdQueryFileList, logger);

const string AdminCmdQueryFileList::CMD_NAME = string("QUERYLIST");

bool AdminCmdQueryFileList::decode_parameters(byte * buffer, int buffer_len)
{
	FixedBuffer fixed_buf((char*)buffer, buffer_len);

	m_by_what = fixed_buf.get_byte();

	int cid_len = fixed_buf.get_int();
	if(cid_len != MAX_CID_LEN)
	{
		LOG4CPLUS_WARN(logger, "invalid CID length: " << cid_len);
		return false;
	}
	if(!fixed_buf.get_bytes(m_cid, cid_len))
	{
		LOG4CPLUS_WARN(logger, "read CID failed.");
		return false;
	}

	m_filesize = fixed_buf.get_int64();
	if(fixed_buf.remain_len() > 0)
		m_filepath = fixed_buf.get_string();

	return true;
}

int AdminCmdQueryFileList::encoded_length() const
{
	int len = AdminCommandFactory::get_cmd_header_len();

	// cmd typeid
	len += sizeof(short);

	// by what
	len += sizeof(byte);

	// CID
	len += sizeof(int);
	len += MAX_CID_LEN;

	// filesize
	len += sizeof(_u64);

	// filepath
	len += (sizeof(int) + m_filepath.length());

	return len;
}

bool AdminCmdQueryFileList::encode(byte * buffer, int & buffer_len)
{
	LOG4CPLUS_DEBUG(logger, "AdminCmdQueryFileList encode()");
	int len = buffer_len;
	if(!encode_cmd_header(buffer, len))
		return false;

	byte* buf = buffer + len;
	int remain_len = buffer_len - len;
	FixedBuffer fixed_buf((char*)buf, remain_len);

	fixed_buf.put_short((short)get_cmd_type_id());
	
	fixed_buf.put_byte(m_by_what);
	fixed_buf.put_string((char *)m_cid, MAX_CID_LEN);
	fixed_buf.put_int64(m_filesize);
	fixed_buf.put_string(m_filepath);


	int pos = fixed_buf.position();
	buffer_len = len + pos;
	
	return true;
}

string AdminCmdQueryFileList::get_cmd_content() const
{
	char buffer[1024];

	char* buf_ptr = buffer;
	buf_ptr += sprintf(buf_ptr, "[QUERYLIST, m_by_what=%d", (int)m_by_what);
	buf_ptr += sprintf(buf_ptr, ", m_cid=%s", Utility::bytes_to_hex(m_cid, MAX_CID_LEN).c_str());
	buf_ptr += sprintf(buf_ptr, ", filesize=%llu", m_filesize);
	buf_ptr += sprintf(buf_ptr, ", filepath=%s", m_filepath.c_str());	

	buf_ptr += sprintf(buf_ptr, "]");

	return (string)buffer;
}


