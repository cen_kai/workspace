/********************************************************************
	created:		2005/08/21
	created:		21:8:2005   17:41
	filename: 		d:\work\newcvs\chub\impl\src\common\sdthreadpool.h
	file path:		d:\work\newcvs\chub\impl\src\common
	file base:		sdthreadpool
	file ext:		h
	author:			lifeng
	description:	a simple thread pool
*********************************************************************/

#ifndef SANDAI_C_THREADPOOL_H_200508211741
#define SANDAI_C_THREADPOOL_H_200508211741

#include <pthread.h>
#include "SDLogger.h"
#include "Counter.h"

class SDThread
{
	const static int STACK_SIZE = (256 * 1024);

public:
	SDThread(int stack_size = 0);
	virtual ~SDThread();

	void set_detachable(bool detachable) { m_detachable = detachable; }

	bool start(bool force_restart = false);
	void wait_terminate();

	void request_stop() { m_should_stop = true; }

	void set_activethread_counter(Counter<int>* counter_p)
	{
		m_thread_counter = counter_p;
	}

	bool is_running() { return m_is_started; }

protected:
	virtual void run() = 0;
	bool should_stop() const { return m_should_stop; }

	virtual void set_active()
	{
		if(m_thread_counter != NULL)
			m_thread_counter->increase();
	}
	virtual void set_inactive()
	{
		if(m_thread_counter != NULL)
			m_thread_counter->decrease();
	}


private:
	DECL_LOGGER(logger);

	static void* threadProc(void* para);

	pthread_t m_thread_id;
	bool m_should_stop;
	bool m_detachable;
	int m_stack_size;
	bool m_is_started;

	Counter<int>* m_thread_counter;
};

#endif
