//============================================================
// FileUtil.cpp : implementation of utility class for thunder file hash functions
//
// Author: JeffLuo
// Created: 2006-08-28
//============================================================

#include "FileUtil.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <openssl/sha.h>
#include <openssl/md5.h>
#include <errno.h>
#include <string>
#include "Utility.h"
#include "common.h"
#include "file_cid.h"

using std::string;

#define XCID_FILE_MAGIC		0x0539C4D7
#define XCID_FILE_MAGIC_V2		0x1327B5C4

IMPL_LOGGER(PartInfo, logger);

IMPL_LOGGER(FileUtil, logger);

/*
   Params:	
   	filepath: target file to be calculated
   return values: 
       0:   calc successful
       -1: invalid parameter
       -2: open file failed
       -3: file is not large enough to calc CID (>= 60K)
       -4: cid buffer too small
       -5: other errors
*/
//兼容小文件
int FileUtil::calc_file_cid(const char * filepath, byte * cid_buf, int& buffer_len)
{
    if(filepath == NULL || cid_buf == NULL || buffer_len < CID_SIZE)
        return -1;

    file_cid tool;
    const int HEX_TXT_CID_SIZE = 41;
    char hex_txt_cid[HEX_TXT_CID_SIZE] = {0};
    bool ok = tool.calculate_file_cid(filepath, hex_txt_cid, HEX_TXT_CID_SIZE);
    if(!ok)
        return -2;
    Utility::hex_text_to_bytes(string(hex_txt_cid), cid_buf, buffer_len);
    buffer_len = CID_SIZE;

    return 0;
}

// !!NOTICE: if bcid_buf_ret is not NULL, then caller have responsibility to free its memory
int FileUtil::calc_file_gcid(const char * filepath, byte * gcid_buf, int& buffer_len, int& bcid_num, byte** bcid_buf_ret )
{
	if(filepath == NULL || gcid_buf == NULL || buffer_len < CID_SIZE)
		return -1;

	int fd = LOG_OPEN(filepath, O_RDONLY);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << filepath << "', errno=" << errno);
		return -2;
	}

	struct stat64 stat_buf;	
	int ret = fstat64(fd, &stat_buf);
	if(ret < 0)
	{
		LOG4CPLUS_WARN(logger, "fstat() returns " << ret <<", errno=" << errno);
		return -2;
	}
	
	// calc part size and part count
	_u64 file_size = (_u64)stat_buf.st_size;
	int block_size = calc_gcid_partsize(file_size);
    //++by liuyinfeng 20150420
    LOG4CPLUS_WARN(logger, "file_size: " << file_size);
	LOG4CPLUS_WARN(logger, "block_size: " << block_size);
	//--

	if(block_size <= 0)
	{
		LOG4CPLUS_WARN(logger, "invalid block_size:" << block_size);
		return -4;
	}
	
	int block_count = (int)(( file_size + block_size - 1 )/block_size);
	bcid_num = block_count;

	if(bcid_buf_ret != NULL)
		*bcid_buf_ret = new byte[MAX_CID_LEN * block_count];

	const int MAX_TEMP_DATA_SIZE = (16<<10); // 16K
	byte temp_data[MAX_TEMP_DATA_SIZE];
	byte sha_buf[CID_SIZE];

	SHA_CTX bctx, gctx;	
	SHA1_Init(&gctx);
	
	// read each part of the file
	for (int i=0; i<block_count; i++)
	{
			SHA1_Init(&bctx);
			
			int blksize = block_size;
			if (i == block_count-1)
				blksize = file_size - i*block_size;
				
			int left_bytes = blksize;
			while (left_bytes > 0)
			{
				int read_bytes = (left_bytes < MAX_TEMP_DATA_SIZE ? left_bytes : MAX_TEMP_DATA_SIZE);
				ssize_t readed = read(fd, temp_data, read_bytes);
				if(readed != read_bytes)
				{
					LOG4CPLUS_WARN(logger, "try to read " << read_bytes << ", but read() returns " << readed << ", errno=" << errno);
					LOG_CLOSE(fd);
					return -3;
				}
				SHA1_Update(&bctx, temp_data, readed);
				left_bytes -= readed;
			}
    	
			SHA1_Final(sha_buf, &bctx);     	
			SHA1_Update(&gctx, sha_buf, CID_SIZE);    	
			
			if(bcid_buf_ret != NULL)
				memcpy(*bcid_buf_ret + i * MAX_CID_LEN, sha_buf, MAX_CID_LEN);
	}
  
	LOG_CLOSE(fd);

	SHA1_Final(gcid_buf, &gctx);
	buffer_len = CID_SIZE;

	return 0;
}

int FileUtil::calc_gcid_partsize(_u64 filesize)
{
    if(filesize <= 0)
        return 0;

    const int max_block_size = (2<<20); // 2M
    int block_size = GCID_PART_SIZE;
    const int max_count = 512;
    int count = (int)(( filesize + block_size - 1 )/block_size);
    while( (count>max_count) && (block_size<max_block_size) )
    {
        block_size <<= 1; // double the size
        count = (int)(( filesize + block_size - 1 )/block_size);
    }

    //
    return block_size;
}

// 根据xcid 文件名和相关信息，创建xcid 文件
int FileUtil::create_xcid_file(const char * xcid_path, byte * cid, byte * gcid, byte * bcid, int bcid_block_num, _u64 filesize)
{
	if(xcid_path == NULL || cid == NULL || gcid == NULL || bcid == NULL || bcid_block_num <= 0)
		return -1;

	int fd = LOG_OPEN(xcid_path, O_WRONLY | O_CREAT | O_TRUNC, COMMON_FILE_MODE);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << xcid_path << "', errno=" << errno);
		return -2;
	}		
	LOG4CPLUS_DEBUG(logger, "in create create_xcid_file " << xcid_path << " will create gcid is " << Utility::bytes_to_hex(gcid, MAX_CID_LEN));
	int magic = XCID_FILE_MAGIC_V2;
	ssize_t written = write(fd, &magic,  sizeof(int));
	if(written < 0)
	{
		LOG4CPLUS_WARN(logger, "write() returns " << written << " when try to write 4 bytes, errno=" << errno);
		LOG_CLOSE(fd);
		return -3;
	}

	// bcid block num
	written = write(fd, &bcid_block_num,  sizeof(int));

	// CID
	written = write(fd, cid,  MAX_CID_LEN);

	// GCID
	written = write(fd, gcid, MAX_CID_LEN);
	
	// BCID
	written = write(fd, bcid, bcid_block_num * MAX_CID_LEN);

	// the following fields added after XCID_FILE_MAGIC_V2
	
	// Filesize
	written = write(fd, &filesize, sizeof(_u64));
	// checksum
	int checksum_zero = 0;
	written = write(fd, &checksum_zero, sizeof(int));
	
	LOG_CLOSE(fd);		

	return 0;
}

int FileUtil::read_bcid_block_num(const char * xcid_path)
{
	if(xcid_path == NULL)
		return -1;

	int fd = LOG_OPEN(xcid_path, O_RDONLY);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << xcid_path << "', errno=" << errno);
		return -1;
	}		

	struct stat stat_buf;
	if(-1 == fstat(fd, &stat_buf))
		return -1;

	int min_filesize = sizeof(int) * 2 + MAX_CID_LEN * 3;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small!");
		LOG_CLOSE(fd);
		return -1;
	}

	int magic = 0;
	ssize_t readed = read(fd, &magic,  sizeof(int));
	if(readed < 0)
	{
		LOG4CPLUS_WARN(logger, "read() returns " << readed << " when try to read 4 bytes, errno=" << errno);
		LOG_CLOSE(fd);
		return -1;
	}
	if(magic != XCID_FILE_MAGIC && magic != XCID_FILE_MAGIC_V2)
	{
		LOG4CPLUS_WARN(logger, "magic num read from xcid file is " << magic << ", expected magic is [" << XCID_FILE_MAGIC << " or " << XCID_FILE_MAGIC_V2 << "].");
		LOG_CLOSE(fd);
		return 0;
	}

	int block_num = 0;
	readed = read(fd, &block_num,  sizeof(int));
	if(block_num <= 0)
	{
		LOG4CPLUS_WARN(logger, "bcid block num read from xcid file is " << block_num);
		LOG_CLOSE(fd);		
		return 0;
	}
	min_filesize = sizeof(int) * 2 + MAX_CID_LEN * 2 + block_num * MAX_CID_LEN;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small when bcid block num is " << block_num);
		LOG_CLOSE(fd);
		return 0;
	}

	// skip CID
	lseek(fd, MAX_CID_LEN, SEEK_CUR);
	
	byte gcid[MAX_CID_LEN];
	// read GCID
	readed = read(fd, gcid, MAX_CID_LEN);
	
	byte* bcid_buf = new byte[block_num * MAX_CID_LEN];
	// read BCID
	readed = read(fd, bcid_buf, block_num * MAX_CID_LEN);
	LOG_CLOSE(fd);
	
	// verify bcid
	SHA_CTX gctx;
	byte calc_gcid[MAX_CID_LEN];

	SHA1_Init(&gctx);	
	SHA1_Update(&gctx, bcid_buf, block_num * MAX_CID_LEN);	
	SHA1_Final(calc_gcid, &gctx);  
	delete []bcid_buf;

	if(0 == memcmp(gcid, calc_gcid, MAX_CID_LEN))
	{
		LOG4CPLUS_INFO(logger, "BCID block num is " << block_num << " by file " << xcid_path);	
		return block_num;
	}
	else
	{
		LOG4CPLUS_WARN(logger, "readed GCID not match that of calculated from BCID blocks!!");
		return 0;
	}
}

bool FileUtil::read_filesize_from_xcid(const char * xcid_path, _u64& filesize)
{
	filesize= 0;
	if(xcid_path == NULL)
		return false;

	int fd = LOG_OPEN(xcid_path, O_RDONLY);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << xcid_path << "', errno=" << errno);
		return false;
	}		

	struct stat stat_buf;
	if(-1 == fstat(fd, &stat_buf))
		return false;

	int min_filesize = sizeof(int) * 2 + MAX_CID_LEN * 3;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small!");
		LOG_CLOSE(fd);
		return false;
	}

	int magic = 0;
	ssize_t readed = read(fd, &magic,  sizeof(int));
	if(readed < 0)
	{
		LOG4CPLUS_WARN(logger, "read() returns " << readed << " when try to read 4 bytes, errno=" << errno);
		LOG_CLOSE(fd);
		return false;
	}
	if(magic == XCID_FILE_MAGIC)
	{
		LOG4CPLUS_INFO(logger, "xcid file magic is " << XCID_FILE_MAGIC << ", no filesize field saved in this file.");
		LOG_CLOSE(fd);
		return true;
	}
	
	if(magic != XCID_FILE_MAGIC_V2)
	{
		LOG4CPLUS_WARN(logger, "magic num read from xcid file is " << magic << ", expected magic is " << XCID_FILE_MAGIC_V2 << ".");
		LOG_CLOSE(fd);
		return false;
	}

	// read bcid block-num
	int block_num = 0;
	readed = read(fd, &block_num,  sizeof(int));
	if(block_num <= 0)
	{
		LOG4CPLUS_WARN(logger, "bcid block num read from xcid file is " << block_num);
		LOG_CLOSE(fd);		
		return false;
	}
	
	min_filesize = sizeof(int) * 2 + sizeof(_u64) + MAX_CID_LEN * 2 + block_num * MAX_CID_LEN;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small when bcid block num is " << block_num);
		LOG_CLOSE(fd);
		return false;
	}

	// skip CID, GCID, BCID
	lseek(fd, MAX_CID_LEN * 2 + block_num * MAX_CID_LEN, SEEK_CUR);

	bool ret_val = false;

	// read filesize
	_u64 temp_filesize = 0;
	readed = read(fd, &temp_filesize, sizeof(_u64));
	if(readed == sizeof(_u64))
	{
		ret_val = true;
		filesize = temp_filesize;
	}
	LOG_CLOSE(fd);

	return ret_val;
}

// 读xcid 文件的内容，判断该xcid 文件是否有错
// 对比创建xcid 文件的过程(调用函数create_xcid_file())
bool FileUtil::read_cid_gcid_filesize(const char* xcid_path, byte* cid_buf, int cid_buf_len, byte* gcid_buf, int gcid_buf_len, _u64& filesize)
{
	if(xcid_path == NULL || cid_buf == NULL || cid_buf_len < MAX_CID_LEN || gcid_buf == NULL || gcid_buf_len < MAX_CID_LEN)
		return false;

	int fd = LOG_OPEN(xcid_path, O_RDONLY);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << xcid_path << "', errno=" << errno);
		return false;
	}		

	struct stat stat_buf;
	if(-1 == fstat(fd, &stat_buf))
		return false;

	int min_filesize = sizeof(int) * 2 + MAX_CID_LEN * 3;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small!");
		LOG_CLOSE(fd);
		return false;
	}

	int magic = 0;
	ssize_t readed = read(fd, &magic,  sizeof(int));
	if(readed != sizeof(int))
	{
		LOG4CPLUS_WARN(logger, "read() returns " << readed << " when try to read 4 bytes, errno=" << errno);
		LOG_CLOSE(fd);
		return false;
	}
	if(magic == XCID_FILE_MAGIC)
	{
		LOG4CPLUS_INFO(logger, "xcid file magic is " << XCID_FILE_MAGIC << ", no filesize field saved in this file.");
		LOG_CLOSE(fd);
		return false;
	}
	
	if(magic != XCID_FILE_MAGIC_V2)
	{
		LOG4CPLUS_WARN(logger, "magic num read from xcid file is " << magic << ", expected magic is " << XCID_FILE_MAGIC_V2 << ".");
		LOG_CLOSE(fd);
		return false;
	}

	// read bcid block-num
	int block_num;
	readed = read(fd, &block_num,  sizeof(int));
	if(readed != sizeof(int) || block_num <= 0)
	{
		LOG4CPLUS_WARN(logger, "bcid block num read from xcid file is " << block_num);
		LOG_CLOSE(fd);		
		return false;
	}
	
	min_filesize = sizeof(int) * 2 + sizeof(_u64) + MAX_CID_LEN * 2 + block_num * MAX_CID_LEN;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small when bcid block num is " << block_num);
		LOG_CLOSE(fd);
		return false;
	}

	// read cid
	readed = read(fd, cid_buf, MAX_CID_LEN);
	if(readed != MAX_CID_LEN)
	{
		LOG_CLOSE(fd);
		return false;
	}

	// read gcid
	readed = read(fd, gcid_buf, MAX_CID_LEN);
	if(readed != MAX_CID_LEN)
	{
		LOG_CLOSE(fd);
		return false;
	}

	// skip BCID
	lseek(fd, block_num * MAX_CID_LEN, SEEK_CUR);

	// read filesize
	readed = read(fd, &filesize, sizeof(_u64));
	if(readed != sizeof(_u64) || filesize == 0)
	{
		LOG_CLOSE(fd);
		return false;
	}
	
	LOG_CLOSE(fd);
	return true;
}



bool FileUtil::read_bcid(const char * xcid_path, byte * bcid_buf, int buffer_len)
{
	if(xcid_path == NULL || bcid_buf == NULL || buffer_len < MAX_CID_LEN)
		return false;

	int fd = LOG_OPEN(xcid_path, O_RDONLY);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << xcid_path << "', errno=" << errno);
		return false;
	}		

	struct stat stat_buf;
	if(-1 == fstat(fd, &stat_buf))
		return false;

	int min_filesize = sizeof(int) * 2 + MAX_CID_LEN * 3;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small!");
		LOG_CLOSE(fd);
		return false;
	}

	int magic = 0;
	ssize_t readed = read(fd, &magic,  sizeof(int));
	if(readed < 0)
	{
		LOG4CPLUS_WARN(logger, "read() returns " << readed << " when try to read 4 bytes, errno=" << errno);
		LOG_CLOSE(fd);
		return false;
	}
	if(magic != XCID_FILE_MAGIC && magic != XCID_FILE_MAGIC_V2)
	{
		LOG4CPLUS_WARN(logger, "magic num read from xcid file is " << magic << ", expected magic is [" << XCID_FILE_MAGIC << " or " << XCID_FILE_MAGIC_V2 << "].");
		LOG_CLOSE(fd);
		return 0;
	}

	int block_num = 0;
	readed = read(fd, &block_num,  sizeof(int));
	if(block_num <= 0)
	{
		LOG4CPLUS_WARN(logger, "bcid block num read from xcid file is " << block_num);
		LOG_CLOSE(fd);		
		return false;
	}
	min_filesize = sizeof(int) * 2 + MAX_CID_LEN * 2 + block_num * MAX_CID_LEN;
	if(stat_buf.st_size < min_filesize)
	{
		LOG4CPLUS_WARN(logger, "xcid file(size=" << stat_buf.st_size << ") is too small when bcid block num is " << block_num);
		LOG_CLOSE(fd);
		return false;
	}
	if(buffer_len < block_num * MAX_CID_LEN)
	{
		LOG4CPLUS_WARN(logger, "buffer length " << buffer_len << " is too small when bcid block num is " << block_num);
		LOG_CLOSE(fd);
		return false;
	}

	// skip CID, GCID
	lseek(fd, MAX_CID_LEN * 2, SEEK_CUR);
		
	// read BCID
	readed = read(fd, bcid_buf, block_num * MAX_CID_LEN);
	LOG_CLOSE(fd);		
	bool succ = (readed == block_num * MAX_CID_LEN);
	
	return succ;
}

bool FileUtil::calc_file_md5(const char * filepath, byte * md5_ret, int & buffer_len)
{
	if(filepath == NULL || md5_ret == NULL || buffer_len < MD5_RET_SIZE)
		return false;

	buffer_len = 0;
	
	int fd = LOG_OPEN(filepath, O_RDONLY);
	if(fd < 0)
	{
		LOG4CPLUS_WARN(logger, "failed to open file '" << filepath << "' when try to calc md5sum, errno=" << errno);
		return false;
	}	

	MD5_CTX ctx;
	MD5_Init(&ctx);	

	const int BUFFER_SIZE = 16384; // 16 KB
	char buffer[BUFFER_SIZE];  
	ssize_t bytes_readed = 0;
	size_t bytes_toread = 0;
	bool eof = false;	
	while(!eof)
	{
		bytes_toread = BUFFER_SIZE;
		bytes_readed = read(fd, buffer,  bytes_toread);
		if(bytes_readed == -1)
		{
			LOG4CPLUS_WARN(logger, "read file '" << filepath << "' error when try to calc md5sum, errno=" << errno);		
			break;
		}
		else if(bytes_readed > 0)
		{
			MD5_Update(&ctx, buffer, bytes_readed);
		}

		if(bytes_readed < (ssize_t)bytes_toread)
			eof = true;
	}
	LOG_CLOSE(fd);

	if(!eof)
		return false;

	MD5_Final(md5_ret, &ctx);
	buffer_len = MD5_RET_SIZE;

	return true;
}

// read md5sum from text file (mostly only one line in this file)
bool FileUtil::read_md5sum(const char * md5file, char * buffer, int buffer_len, bool log_warning)
{
	if(md5file == NULL || buffer == NULL || buffer_len < MD5_RET_SIZE * 2 + 1)
		return false;
	
	char temp_buf[MAX_PATH_LEN];
	
	FILE* fp = fopen(md5file, "r");
	if(!fp)
	{
		if(log_warning)
		{
			LOG4CPLUS_WARN(logger, "failed to open md5 file " << md5file);
		}
		return false;
	}

	string ret_line;
	while(true) 
	{
		char* pret = fgets(temp_buf, MAX_PATH_LEN, fp);
		if(!pret)
			break;
		
		string line = Utility::trim_string(string(temp_buf));
		if(!line.empty())
		{
			ret_line = line;
			break;
		}
	}
	fclose(fp);

	if(ret_line.length() < MD5_RET_SIZE * 2)
		return false;

	strncpy(buffer, ret_line.c_str(), MD5_RET_SIZE * 2);
	buffer[MD5_RET_SIZE * 2] = '\0';

	LOG4CPLUS_INFO(logger, "md5sum is [" << buffer << "] read from file " << md5file);

	return true;
}

bool FileUtil::is_true_head(const HeadFile &head)
{
	if(head.m_ver != HEAD_VER_10001 && head.m_ver != HEAD_VER_10002)
	{
		LOG4CPLUS_DEBUG(logger, "error head.m_ver is " << head.m_ver << " and the right is" << HEAD_VER_10001 << " or " << HEAD_VER_10002);
		return false;
	}
	if(head.m_type != FILE_INDEX && head.m_type != FILE_PARTS)
	{
		LOG4CPLUS_DEBUG(logger, "error head.m_type is " << head.m_type << " and the right is" << FILE_PARTS);
		return false;
	}
	if(head.m_magic_num != PART_FILE_MAGIC)
	{
		LOG4CPLUS_DEBUG(logger, "error head.m_magic_num is " << head.m_magic_num << " and the right is" << PART_FILE_MAGIC);
		return false;
	}
	return true;
}

