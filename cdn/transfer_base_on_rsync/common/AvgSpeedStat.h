//============================================================
// AvgSpeedStat.h : interface of class AvgSpeedStat
//                           
// Author: qiu
// Created: 2006-09-14
//============================================================

#ifndef __AVG_SPEED_STAT_20060914_37
#define __AVG_SPEED_STAT_20060914_37

#include "common/common.h"

template <_u32 _MAX_TIME>
class AvgSpeedStat
{
	struct TimeSliceItem
	{
		TimeSliceItem() : m_first_time_ms(0), m_last_time_ms(0), m_total_bytes(0) { }

		_u64 m_first_time_ms;
		_u64 m_last_time_ms;
		_u64 m_total_bytes;
	};

public:
	const static int PER_SECOND = 0;
	const static int PER_MINUTE = 1;

public:
	AvgSpeedStat(int time_precision = PER_SECOND)
	{
		memset(&m_timed_items[0], 0, sizeof(TimeSliceItem) * _MAX_TIME);
	
		set_time_precision(time_precision);
		m_timeslice_index = 0;
		m_transfer_start_time = 0;
	}

	void set_time_precision(int time_precision)
	{
		if(time_precision == PER_SECOND || time_precision == PER_MINUTE)
			m_time_precision = time_precision;
		else
			m_time_precision = PER_SECOND;
	}	

	void put(_u64 bytes, _u64 time_ms);
	_u32 get_avg_speed(_u32 timeslice_num, _u64 time_ms);
	_u64 get_transfer_start_time() { return m_transfer_start_time; }

private:
	_u32 get_timeslice_ms() const 
	{
		return (m_time_precision == PER_MINUTE ? 60000 : 1000);
	}

private:
	TimeSliceItem m_timed_items[_MAX_TIME];
	_u32 m_timeslice_index;
	int m_time_precision;
	_u64 m_transfer_start_time;

private:
	static log4cplus::Logger logger;
};


template <_u32 _MAX_TIME>
log4cplus::Logger AvgSpeedStat<_MAX_TIME>::logger = log4cplus::Logger::getInstance("AvgSpeedStat");



template <_u32 _MAX_TIME>
void AvgSpeedStat<_MAX_TIME>::put(_u64 bytes, _u64 time_ms)
{
	if(m_transfer_start_time == 0)
		m_transfer_start_time = time_ms;

	if (m_timed_items[m_timeslice_index].m_last_time_ms == 0)
	{
		m_timed_items[m_timeslice_index].m_total_bytes = bytes;
		m_timed_items[m_timeslice_index].m_first_time_ms = time_ms;		
		m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;
		return;
	}

	_u32 timeslice_ms = get_timeslice_ms();		
	if (time_ms - m_timed_items[m_timeslice_index].m_last_time_ms >= _MAX_TIME * timeslice_ms)
	{
		memset(&m_timed_items[0], 0, sizeof(TimeSliceItem) * _MAX_TIME);
		
		m_timed_items[0].m_total_bytes = bytes;
		m_timed_items[0].m_first_time_ms = time_ms;				
		m_timed_items[0].m_last_time_ms = time_ms;
		m_timeslice_index = 0;

		return;
	}	

	_u32 cur_time_index = (_u32)(time_ms / timeslice_ms);
	_u32 last_time_index = (_u32)(m_timed_items[m_timeslice_index].m_last_time_ms / timeslice_ms);		
	if(last_time_index > cur_time_index || cur_time_index - last_time_index > _MAX_TIME)
	{
		LOG4CPLUS_THIS_WARN(logger, "put(): cur_time_index=" << cur_time_index << ", last_time_index=" << last_time_index);
	}
	if(cur_time_index == last_time_index)
	{
		m_timed_items[m_timeslice_index].m_total_bytes += bytes;
		m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;
	}
	else if(cur_time_index > last_time_index)
	{
		for(_u32 i = 0; i < cur_time_index - last_time_index - 1; i++)
		{
			m_timeslice_index++;
			if(m_timeslice_index >= _MAX_TIME)
				m_timeslice_index = 0;
			
			m_timed_items[m_timeslice_index].m_total_bytes = 0;			
			m_timed_items[m_timeslice_index].m_first_time_ms = time_ms;						
			m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;			
		}

		m_timeslice_index++;
		if(m_timeslice_index >= _MAX_TIME)
			m_timeslice_index = 0;
		m_timed_items[m_timeslice_index].m_total_bytes = bytes;			
		m_timed_items[m_timeslice_index].m_first_time_ms = time_ms;					
		m_timed_items[m_timeslice_index].m_last_time_ms = time_ms;			
	}	
}

template <_u32 _MAX_TIME>
_u32 AvgSpeedStat<_MAX_TIME>::get_avg_speed(_u32 timeslice_num, _u64 time_ms)
{
	if (m_timed_items[m_timeslice_index].m_last_time_ms == 0)
		return 0;

	if(timeslice_num > _MAX_TIME)
		timeslice_num = _MAX_TIME;

	_u32 timeslice_ms = get_timeslice_ms();	
	if (time_ms - m_timed_items[m_timeslice_index].m_last_time_ms > timeslice_num * timeslice_ms)
	{		
		return 0;
	}	

	_u32 cur_time_index = (_u32)(time_ms / timeslice_ms);
	_u32 last_time_index = (_u32)(m_timed_items[m_timeslice_index].m_last_time_ms / timeslice_ms);	
	if(last_time_index > cur_time_index || cur_time_index - last_time_index > timeslice_num)
	{
		LOG4CPLUS_THIS_WARN(logger, "get_avg_speed(): cur_time_index=" << cur_time_index << ", last_time_index=" << last_time_index);
	}

	_u64 sum_bytes = 0;
	_u64 total_time_ms = 0;
	if(cur_time_index > last_time_index)
	{
		timeslice_num -= (cur_time_index - last_time_index);
		total_time_ms += (cur_time_index - last_time_index) * timeslice_ms;
		if(timeslice_num <= 0 || timeslice_num > _MAX_TIME)
			return 0;
	}

	int cur_index = m_timeslice_index;
	for(_u32 i = 0; i < timeslice_num; i++)
	{
		sum_bytes += m_timed_items[cur_index].m_total_bytes;

		if(i == timeslice_num - 1)
		{
			// the first time-slice
			_u64 start = m_timed_items[cur_index].m_first_time_ms;
			_u64 end = ((start / timeslice_ms) + 1) * timeslice_ms;
			total_time_ms += (end - start);
		}
		else if(i == 0 && cur_time_index == last_time_index)
		{
			// the last time-slice
			_u64 end = m_timed_items[cur_index].m_last_time_ms;
			total_time_ms += (end % timeslice_ms);
		}
		else
		{
			// time-slice in the middle
			total_time_ms += timeslice_ms;
		}

		cur_index--;
		if(cur_index < 0)
			cur_index = _MAX_TIME - 1;
	}

	// return speed in KB/sec
	if(total_time_ms > 0)
		return (_u32)(sum_bytes * 1000 / (total_time_ms * 1024) );  
	else
		return 0;
}


#endif // __AVG_SPEED_STAT_20060914_37

