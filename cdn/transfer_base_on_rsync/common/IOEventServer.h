//============================================================
// IOEventServer.h : interface of class IOEventServer
//                                monitor socket I/O event using poll
// Author: JeffLuo
// Created: 2006-08-29
//============================================================

#ifndef __IO_EVENT_MONITOR_H_20060829_11
#define  __IO_EVENT_MONITOR_H_20060829_11

#include <sys/socket.h>
#include <sys/poll.h>
#include <ext/hash_map>
#include <vector>
#include "common/common.h"
#include "common/SDLogger.h"
#include "common/SDMutexLock.h"
#include "common/SimpleStack.h"

using namespace std;

class IOEventServer;
class IEventCallback;

struct EventData
{
	int m_event_flag;
	int m_timeout_ms;
};

struct EventInfo : public EventData
{
	EventInfo()
	{
		m_ev_cb = NULL;
		m_timeout_ms = 0;
		m_event_flag = 0;
	}
	EventInfo(IEventCallback* ev_cb, int timeoutMs,  int event_flag = 0)
	{
		m_ev_cb = ev_cb;
		m_timeout_ms = timeoutMs;
		m_event_flag = event_flag;
	}
	
	IEventCallback* m_ev_cb;
};

class IEventCallback
{
public:
	virtual void on_fd_readable(int fd, EventData* attach_info) = 0;
	virtual void on_fd_writable(int fd, EventData* attach_info) = 0;
	virtual void on_fd_error(int fd, EventData* attach_info) = 0;
	virtual void on_fd_timeout(int fd, EventData* attach_info) = 0;

	virtual IOEventServer* get_event_server() = 0;
	virtual void set_event_server(IOEventServer* ev_server) = 0;

	virtual ~IEventCallback() { }
};

class IOEventServer
{
public:
	const static int INVALID_FD = -1;

	const static int EVENT_NONE = 0;
	const static int EVENT_READABLE = 1;
	const static int EVENT_WRITABLE = 2;
	const static int EVENT_PERSIST = 64; // means the event will not removed from poll even if the event triggered	
	const static int EVENT_ALL = EVENT_READABLE |EVENT_WRITABLE|EVENT_PERSIST;

	virtual ~IOEventServer() { }

	virtual bool add_event(int fd, int event_type, EventInfo* attach_info) = 0;
	virtual bool add_timer_event(EventInfo* attach_info) = 0;
	virtual bool del_event(int fd, int event_type) = 0;

	virtual void notify_fd_closed(int fd) = 0;
	
	virtual void run_event_loop() = 0;
	virtual void request_exit() = 0;
};

class DefaultEventCallBack : public IEventCallback
{
public:
	DefaultEventCallBack() : m_ev_server(NULL) { }
	
	virtual void on_fd_readable(int fd, EventData* attach_info)
	{
		LOG4CPLUS_THIS_WARN(logger, "!! fd " << fd << " becomes readable.");
	}
	virtual void on_fd_writable(int fd, EventData* attach_info)
	{
		LOG4CPLUS_THIS_WARN(logger, "!! fd " << fd << " becomes writable.");
	}
	virtual void on_fd_error(int fd, EventData* attach_info)
	{
		LOG4CPLUS_THIS_WARN(logger, "!! fd " << fd << " is in error state.");
	}
	virtual void on_fd_timeout(int fd, EventData* attach_info)
	{
		LOG4CPLUS_THIS_WARN(logger, "!! fd " << fd << " timeout(" << attach_info->m_timeout_ms << ").");
	}

	virtual IOEventServer* get_event_server() { return m_ev_server; }
	virtual void set_event_server(IOEventServer* ev_server) { m_ev_server = ev_server; }


private:
	IOEventServer* m_ev_server;

	DECL_LOGGER(logger);
};

#endif // #ifndef __IO_EVENT_MONITOR_H_20060829_11

