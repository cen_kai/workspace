//============================================================
// FixedBuffer.h : interface of class FixedBuffer
//                          
// Author: JeffLuo
// Created: 2006-11-08
//============================================================

#ifndef __FIXED_BUFFER_H_20061108_26
#define  __FIXED_BUFFER_H_20061108_26

#include "common/common.h"
#include <string>

using std::string;

class FixedBuffer
{
	const static string EMPTY_STRING;
public:
	FixedBuffer(char* buffer, int buffer_size, bool reverse_byteorder = false);

	inline int capacity() const { return m_buffer_size; }
	inline int remain_len() const { return m_buffer_size - m_offset; }
	inline int position() const { return m_offset; }
	inline bool set_position(int new_pos)
	{
		if(new_pos > -1 && new_pos < m_buffer_size)
		{
			m_offset = new_pos;
			return true;
		}
		else
			return false;
	}
	inline int get_limit() const { return m_limit; }
	inline void flip()
	{
		m_limit = m_offset;
		m_offset = 0;
	}

	inline bool put_int(int x);
	inline bool put_int64(_u64 x);
	inline bool put_short(short x);
	inline bool put_byte(byte x);
	inline bool put_string(const string& str);
	inline bool put_string(const char* str, int str_len = -1);
	inline bool skip(int offset);

	inline int get_int();
	inline _u64 get_int64();
	inline short get_short();
	inline byte get_byte();
	inline string get_string();
	inline bool get_bytes(byte* buf, int len);

private:
	inline void copy_bytes(byte* dest, const byte * src, int len)
	{
		if(!m_reverse_byteorder)
		{
			memcpy(dest, src, len);
		}
		else
		{
			for(int i = 0; i < len; i++)
			{
				*(dest + i) = *(src + len -i - 1);
			}
		}
	}

private:
	char* m_buffer;
	int m_buffer_size;
	int m_offset;
	bool m_reverse_byteorder;

	int m_limit;
};

bool FixedBuffer::put_byte(byte x)
{
	if(remain_len() < (int)sizeof(byte))
		return false;

	*(m_buffer + m_offset) = x;
	m_offset += sizeof(byte);
	if(m_limit < m_offset)
		m_limit = m_offset;

	return true;
}

bool FixedBuffer::put_int(int x)
{
	if(remain_len() < (int)sizeof(int))
		return false;

	copy_bytes((byte*)(m_buffer + m_offset), (byte*)&x, sizeof(int));
	m_offset += sizeof(int);
	if(m_limit < m_offset)
		m_limit = m_offset;	

	return true;
}

bool FixedBuffer::put_int64(_u64 x)
{
	if(remain_len() < (int)sizeof(_u64))
		return false;

	copy_bytes((byte*)(m_buffer+m_offset), (byte*)&x, sizeof(_u64));
	m_offset += sizeof(_u64);
	if(m_limit < m_offset)
		m_limit = m_offset;

	return true;
}

bool FixedBuffer::put_short(short x)
{
	if(remain_len() < (int)sizeof(short))
		return false;

	copy_bytes((byte*)(m_buffer+m_offset), (byte*)&x, sizeof(short));
	m_offset += sizeof(short);
	if(m_limit < m_offset)
		m_limit = m_offset;

	return true;
}

bool FixedBuffer::put_string(const char * str, int str_len)
{
	int len = str_len;
	if(len == -1)
		len = (str != NULL ? strlen(str) : 0);

	if(remain_len() < (int)sizeof(int) + len)
		return false;

	copy_bytes((byte*)(m_buffer+m_offset), (byte*)&len, sizeof(int));
	m_offset += sizeof(int);
	if(len > 0)
		memcpy(m_buffer + m_offset, str, len);
	m_offset += len;

	if(m_limit < m_offset)
		m_limit = m_offset;

	return true;
}

bool FixedBuffer::put_string(const string & str)
{
	int len = str.length();

	if(remain_len() < (int)sizeof(int) + len)
		return false;

	copy_bytes((byte*)(m_buffer+m_offset), (byte*)&len, sizeof(int));
	m_offset += sizeof(int);
	if(len > 0)
		memcpy(m_buffer + m_offset, str.c_str(), len);
	m_offset +=  len;

	if(m_limit < m_offset)
		m_limit = m_offset;

	return true;
}

bool FixedBuffer::skip(int offset)
{
	int new_pos = m_offset + offset;
	if(new_pos > -1 && new_pos < m_buffer_size)
	{
		m_offset += offset;
		if(m_limit < m_offset)
			m_limit = m_offset;
		
		return true;
	}
	else
	{
		return false;
	}
}

byte FixedBuffer::get_byte()
{
	byte b = *(m_buffer + m_offset);
	m_offset += sizeof(byte);

	if(m_limit < m_offset)
		m_limit = m_offset;

	return b;
}

int FixedBuffer::get_int()
{
	int x;

	copy_bytes((byte*)&x, (byte*)(m_buffer+m_offset), sizeof(int));
	m_offset += sizeof(int);

	if(m_limit < m_offset)
		m_limit = m_offset;

	return x;
}

_u64 FixedBuffer::get_int64()
{
	_u64 x;

	copy_bytes((byte*)&x, (byte*)(m_buffer+m_offset), sizeof(_u64));
	m_offset += sizeof(_u64);

	if(m_limit < m_offset)
		m_limit = m_offset;

	return x;
}

short FixedBuffer::get_short()
{
	short x;

	copy_bytes((byte*)&x, (byte*)(m_buffer+m_offset), sizeof(short));
	m_offset += sizeof(short);

	if(m_limit < m_offset)
		m_limit = m_offset;

	return x;
}

string FixedBuffer::get_string()
{
	int str_len = get_int();
	if(str_len == 0)
		return EMPTY_STRING;

	string str(str_len, 0);
	memcpy(&str[0], m_buffer + m_offset, str_len);
	m_offset += str_len;

	if(m_limit < m_offset)
		m_limit = m_offset;

	return str;
}

// NOTICE: get_bytes do not read prefix length, but get_string() DO
bool FixedBuffer::get_bytes(byte * buf, int len)
{
	if(len < 0 || remain_len() < len)
		return false;

	memcpy(buf, m_buffer + m_offset, len);
	m_offset += len;

	if(m_limit < m_offset)
		m_limit = m_offset;

	return true;
}

#endif // __FIXED_BUFFER_H_20061108_26

