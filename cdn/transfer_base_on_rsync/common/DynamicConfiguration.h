//============================================================
// DynamicConfiguration.h : interface of class DynamicConfiguration
//                           (dynamic configuration based on filepath of key)
// Author: JeffLuo
// Created: 2006-11-25
//============================================================

#ifndef __DYNAMIC_CONFIG_H_20061125_35
#define  __DYNAMIC_CONFIG_H_20061125_35

#include <string>
#include <map>
#include "ConfigReader.h"
#include "SDLogger.h"

using std::string;
using std::map;

class DynamicConfiguration
{
	class DynaConfigItem;

	struct DynaConfigItem
	{	
		DynaConfigItem() : m_loaded(false), m_cfg_reader(NULL), m_parent(NULL) { }
		
		bool m_loaded;
		ConfigReader* m_cfg_reader;
		DynaConfigItem* m_parent;
	};

	typedef map<string, DynaConfigItem*>::iterator map_iterator;
	const static char* DEFAULT_CONF_FILE_SUFFIX;

public:
	DynamicConfiguration(const char* root_conf_path, const char* default_dir_conf_filename = NULL);

	bool add_config_file(const char* conf_file_path, bool use_parent_conf = true);
	bool update_config_file(const char* conf_file_path, bool use_parent_conf = true);

	string get_conf_string(const char* conf_path, const char* key, const char* default_value = NULL);	
	int get_conf_int(const char* conf_path, const char* key, int default_value = 0);

private:
	void load_parents_conf(const char* child_conf_path, DynaConfigItem** child_conf);

private:
	DynaConfigItem m_root_cfg;
	string m_dir_conf_filename;

	map<string, DynaConfigItem*> m_cfg_list;
	map<string, DynaConfigItem*> m_dir_cfg_list;	


	DECL_LOGGER(logger);			
};

#endif //  __DYNAMIC_CONFIG_H_20061125_35

