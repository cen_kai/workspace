#ifndef SIMULATORRULE_H
#define SIMULATORRULE_H

#include "Singleton.hpp"
#include "RuleEngine.hpp"
#include "Logger.hpp"
#include "PingAn.hpp"

using std::string;
                                     
static const string KNOWN_PIPES_V1 = "known_pipes:true";
static const string KNOWN_QEMU_DRIVERS_V1 = "known_qemu_drivers:true";
static const string KNOWN_FILES_V1 = "known_files:true";

class SimulatorRule: public Rule, public Singleton<SimulatorRule>
{
public:
  SimulatorRule()
  {
    Rule::mRuleName = pingan::SIMULATOR_RULE;
  }

  void process(const SecurityRequest& req, SecurityResponse& resp)
  {
    TRACE;
    uint16_t result = 0;
    
    // LOG_BEGIN(Logger::INFO)
    //   LOG_PROP("req.mDeviceInfo", req.mDeviceInfo);
    // LOG_END("");
      
    size_t n = req.mDeviceInfo.find(KNOWN_PIPES_V1);
    if(std::string::npos != n)
      {
        resp.updateScore(mRuleName, result);
      }
    n = req.mDeviceInfo.find(KNOWN_QEMU_DRIVERS_V1);
    if(std::string::npos != n)
      {
        resp.updateScore(mRuleName, result);
      }
  }

  virtual ~SimulatorRule()
  {
  }
};

#endif /* SIMULATORRULE_H */
