/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef MYSQL_CONNECTION_POOL_HPP
#define MYSQL_CONNECTION_POOL_HPP

#include "ObjectPool.hpp"
#include "MysqlClient.hpp"
#include <string>

using std::string;

class MysqlConnectionPool : public ObjectPool<MysqlClient *>, public Singleton<MysqlConnectionPool>
{
public:

  // MysqlConnectionPool( const char *host         = NULL,
  //                      const char *user         = NULL,
  //                      const char *passwd       = NULL,
  //                      const char *db           = NULL,
  //                      const unsigned int port  = 3306);

  MysqlConnectionPool(){};
  ~MysqlConnectionPool();
  void init();
  void start();

  void stop();

  // static void createInstance(const char *host         = NULL,
  //                            const char *user         = NULL,
  //                            const char *passwd       = NULL,
  //                            const char *db           = NULL,
  //                            const unsigned int port = 3306);

  // static MysqlConnectionPool* getInstance();
  // static void delInstance();
  // static MysqlConnectionPool* m_instance;  

private:

  MysqlConnectionPool(const MysqlConnectionPool&);
  MysqlConnectionPool& operator=(const MysqlConnectionPool&);

  string  m_host;
  string  m_user;
  string  m_passwd;
  string  m_db;
  unsigned int m_port ;
  int mPool;
};


#endif // MYSQL_CONNECTION_POOL_HPP
