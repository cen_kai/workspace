#ifndef CRYPT_TASK_HPP
#define CRYPT_TASK_HPP

#include "Task.hpp"
#include <sys/types.h>
#include <string>

using std::string;
class CryptTask : public Task
{

public:
  CryptTask( string& id, string& message);
  ~CryptTask();
  void run();
  
  string mServData;
private:
  int getHeadLen();
  CryptTask(const CryptTask&);
  CryptTask& operator=(const CryptTask&);

  string m_id;
  int16_t m_type;
  char* m_pData;
  string mOrient;

  int m_len;
};

#endif // CRYPT_TASK_HPP
