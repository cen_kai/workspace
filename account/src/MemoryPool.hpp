/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef _MemPool_hpp_
#define _MemPool_hpp_

#include <stddef.h>
#include <string.h>

class MemoryBlock
{
public:
	MemoryBlock( int size ):
		m_pNext(NULL),
		m_pData( new char[size] ),
		m_nBlockSize(size)
	{
		memset( m_pData,0,size );
	}

	~MemoryBlock()
	{
		if ( m_pData != NULL )
		{
			delete[] m_pData;
			m_pData = NULL;
		}
	}

	char *getData()
	{
		return m_pData;
	}

  int getBlockSize()
	{
		return m_nBlockSize;
	}

public:
	MemoryBlock	*m_pNext;

private:
	char	*m_pData;
	int		m_nBlockSize;
};

class MemoryPool
{
public:
	MemoryPool( int oneBlockSize, int count );
	~MemoryPool(void);

	MemoryBlock *getBlock();
	void setBlock( MemoryBlock *pBlock );
  int getCurBlockCount();
  int getMaxBlockCount();
  int getBlockSize();

private:
	MemoryBlock	*m_Head;
	int m_nOneBlockSize;
  int m_freeBlockSize;
	int	m_curBlockCount;
  int m_maxBlockCount;
};


#endif
