/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "EPoll.hpp"


EPoll::EPoll(int flags, EventHandler *handler)
{
#ifdef EPOLL_CLOEXEC
  mBackendFd = epoll_create1 (EPOLL_CLOEXEC);

  if (mBackendFd < 0 && (errno == EINVAL || errno == ENOSYS))
#endif
    mBackendFd = epoll_create (256);
  
  mInit = false;
  
  if (mBackendFd < 0)
    return;
  
  fcntl (mBackendFd, F_SETFD, FD_CLOEXEC);

  mBackendMintime = 1e-3; /* epoll does sometimes return early, this is just to avoid the worst */

  mEventmax = 64; /* initial number of events receivable per poll */
  mEpollEvents = (struct epoll_event *)malloc (sizeof (struct epoll_event) * mEventmax);

  mHandler = handler;
  
  mInit = true;
}



EPoll::~EPoll(){
  
  if(mEpollEvents)
    free(mEpollEvents);
}

bool EPoll::poll(ev_tstamp timeout){
  if(!mInit){
    printf("init failture");
    return false;
  }
  int i;
  int eventcnt;

  /* epoll wait times cannot be larger than (LONG_MAX - 999UL) / HZ msecs, which is below */
  /* the default libev max wait time, however. */

  eventcnt = epoll_wait (mBackendFd, mEpollEvents, mEventmax, timeout * 1e3);

  if (eventcnt < 0)
    {
      if (errno != EINTR)
        perror("epoll");
        //ev_syserr ("(libev) epoll_wait");

      return false;
    }

  for (i = 0; i < eventcnt; ++i)
    {
      struct epoll_event *ev = mEpollEvents + i;

      int fd = (uint32_t)ev->data.u64; /* mask out the lower 32 bits */

      int got  = (ev->events & (EPOLLOUT | EPOLLERR | EPOLLHUP) ? EV_WRITE : 0)
               | (ev->events & (EPOLLIN  | EPOLLERR | EPOLLHUP) ? EV_READ  : 0);

      mHandler->doEvent(fd, got);
    }
  
  if(eventcnt == 0)
    {
      mHandler->doEvent(-2, 0); //这时表示时间到处理定时事件
    }
  
  return true;
}

void EPoll::modify ( int fd, int old , int nev  )
{
  struct epoll_event ev;
  unsigned char oldmask;

  /*
   * we handle EPOLL_CTL_DEL by ignoring it here
   * on the assumption that the fd is gone anyways
   * if that is wrong, we have to handle the spurious
   * event in epoll_poll.
   * if the fd is added again, we try to ADD it, and, if that
   * fails, we assume it still has the same eventmask.
   */
  if (!nev)
    return;

  /* store the generation counter in the upper 32 bits, the fd in the lower 32 bits */
  ev.data.u64 = (uint64_t)(uint32_t)fd;

  ev.events   = (nev & EV_READ  ? EPOLLIN  : 0)
              | (nev & EV_WRITE ? EPOLLOUT : 0);

  if (!epoll_ctl (mBackendFd, !old ? EPOLL_CTL_ADD : EPOLL_CTL_MOD, fd, &ev))
    return;

  if (errno == ENOENT)
    {
// TODO: handle error
      if (!epoll_ctl (mBackendFd, EPOLL_CTL_ADD, fd, &ev))
        return;
    }
  else if (errno == EEXIST)
    {
      /* EEXIST means we ignored a previous DEL, but the fd is still active */
      /* if the kernel mask is the same as the new mask, we assume it hasn't changed */
// TODO: handle error
      if (!epoll_ctl (mBackendFd, EPOLL_CTL_MOD, fd, &ev))
        return;
    }
  else if (errno == EPERM)
    {
      /* EPERM means the fd is always ready, but epoll is too snobbish */
      /* to handle it, unlike select or poll. */
// TODO: 处理错误      
      return;
    }

  mHandler->doEvent(fd, -1); //这里处理修改失败的情况

}

