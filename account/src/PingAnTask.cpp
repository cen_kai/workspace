/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "PingAnTask.hpp"
#include "Json.hpp"
#include "MysqlConnectionPool.hpp"
#include "ConnectManager.hpp"
#include "MessageManager.hpp"
#include "BlackPhoneManager.hpp"
#include "Logger.hpp"
#include <iostream>

using std::cout;
using std::endl;

void BlackDeviceTask::run()
{
  
}

static void getDbPhones(vector<string>& addwords, vector<string>& delwords, MYSQL_RES *result)
{
  MYSQL_ROW row;
  //  string res;
  int num_fields = mysql_num_fields(result);
  if(num_fields != 2)
    {
      // LOG_STATIC(Logger::INFO, "db result error")
      //   LOG_END_STATIC("");
      cout<<"fields error"<<endl;
      return;
    }
  
  while ((row = mysql_fetch_row(result))) 
    {
      string s;
      //SentiveWord *word = new SentiveWord();
      s = (char*)row[0];
      char y[2] = {'y', 0};
      int ret = strcasecmp(y, row[1]);
      if(ret != 0)
        delwords.push_back(s);
      else
        addwords.push_back(s);
    }
  
    // LOG_BEGIN(Logger::INFO)
    //   LOG_PROP("new words size", addwords.size())
    //   LOG_PROP("expire words size", delwords.size())
    //   LOG_END_STATIC("");

  return;
}


void BlackPhoneTask::run()
{
  string sql, m_result;
  string tablename = "phone_black";
  //  select word, weight, type , update_time from forbid_word where update_time > 0;
  sql.append("select mobile, status from ")
    .append(tablename)
    .append(" where modifyTime >= ")
    .append("FROM_UNIXTIME(")
    .append(TToStr(mLastTime))
    .append(")")
    .append(";");

  LOG_BEGIN(Logger::INFO)
    LOG_PROP("sql", sql);
  LOG_END("");
  
  MYSQL_RES *result;
  
  MysqlConnectionPool* conPool =  MysqlConnectionPool::getInstance();
  MysqlClient *c = conPool->acquire();
  if ( !c->querty(sql.c_str(), sql.length(), &result) )
    {
      std::string errorMsg("Could not execute query.");
      LOG ( Logger::ERR, errorMsg.c_str() );
      return;
    }

  vector<string> addPhones; // TODO: 去重
  vector<string> delPhones;
  getDbPhones(addPhones, delPhones, result);
  
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("addSize", addPhones.size());
  LOG_PROP("delPhones", delPhones.size());
  LOG_END("");
  
  BlackPhoneManager *bpm = BlackPhoneManager::getInstance();
  bpm->update(addPhones, delPhones);
  mysql_free_result(result);
  conPool->release(c);

}

BlackPhoneTask::~BlackPhoneTask()
{
  TRACE;
}
