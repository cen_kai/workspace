#include "CryptTask.hpp"
#include "CryptRSA.hpp"
#include "CryptAES.hpp"
#include "Logger.hpp"
#include "GNUMemoryPool.hpp"
#include "ThreadPool.hpp"
#include "BlackPhoneManager.hpp"

#include "base64.h"
#include "RuleEngine.hpp"
#include "PingAn.hpp"
#include "MessageManager.hpp"

#include <arpa/inet.h>
#include <string>
#include <algorithm>
using namespace pingan;

CryptTask::CryptTask( string& id,  string &message)
  :mServData()
  ,m_id(id)
  ,m_pData(NULL)
  ,mOrient(message)
  ,m_len(0)
{
}

CryptTask::~CryptTask()
{
  if(m_pData){
    m_pData = NULL;
  }
}

int CryptTask::getHeadLen(){
  TRACE;
  if(m_len < 10)
    {
      LOG( Logger::ERR, std::string("I'm a task, msg len error").c_str());
      return -1;
    }
  int32_t mesglen =0, headerlen = 0;
  int *i, *j;
  i = (int*) m_pData;
  mesglen = ntohl(*i);
  uint16_t* k = (uint16_t*)(++i);
  
  uint16_t ocrc = ntohs(*k);
  uint16_t icrc = crc16_ccitt((uint8_t *)(m_pData + 6), m_len - 6);
  // LOG_BEGIN(Logger::INFO)
  //   LOG_PROP("ocrc", ocrc)
  //   LOG_PROP("icrc", icrc)
  //   LOG_PROP("len", *i)
  // LOG_END("");
  
  i = (int*) (m_pData + 6);
  headerlen = ntohl(*i);
  // LOG_BEGIN(Logger::INFO)
  //   LOG_PROP("headerlen", headerlen)
  //   LOG_PROP("mesglen", mesglen)
  //   LOG_PROP("mlen", m_len)
  // LOG_END("");
  
  if(ocrc != icrc)
    {
      LOG( Logger::ERR, std::string("I'm a task, crc error").c_str());
      return -1;
    }

  return headerlen;
}

#define MEM_BUFF_SIZE 20480
__thread char memorySrc[MEM_BUFF_SIZE];
__thread char memoryDst[MEM_BUFF_SIZE];

//0 ͨ��, 1 ��� , 2 �˹����, 3�ܾ�

const int  ACCEPT = 0;
const int  MONITOR = 1;
const int  REVIEW = 2;
const int  REFUSE = 3;

static const map<std::string, int> rulePropsal {
  {BLACK_PHONE_RULE, REFUSE},
  {SIMULATOR_RULE, MONITOR},
  {DEVICE_ACCOUNT_RULE, MONITOR},
  {DEVICE_PRINT_RULE, MONITOR},
  {TIME_DIFF_RULE, MONITOR}
};

void CryptTask::run()
{

  size_t result_len = 0;
  LOG( Logger::FINEST, std::string("I'm a task, provessing message").c_str());
  unsigned char bufen[256] = {0};
  unsigned char bufkey[32] = {0};

  //  GNUMemoryPool *mem_pool =  GNUMemoryPool::getInstance(); // 
  // memset(memorySrc, 0, 2048);
  // memset(memoryDst, 0, 2048);
  
  //  m_pData = (char*)mem_pool->get(m_len);
  //  char *result = (char *)mem_pool->get(m_len);

  //  GNUMemoryPool *mem_pool =  GNUMemoryPool::getInstance(); // 
  memset(memorySrc, 0, MEM_BUFF_SIZE);
  memset(memoryDst, 0, MEM_BUFF_SIZE);
  char *result = memoryDst;
  //  m_pData = (char*)mem_pool->get(m_len);
  //  char *result = (char *)mem_pool->get(m_len);
  json::Object respObj;
  respObj[ID] = m_id;
  respObj[CMD] = ACCOUNT_CHECK;

  if(mOrient.length() < 1 && mServData.length() > 5)
    {
      try
        {
          json::Value js = json::Deserialize(mServData);
          json::Object obj = js.ToObject();
          if(obj[MOBILE].GetType() != json::NULLVal)
            {
              json::Array array;
              respObj[STATUS] = STATUS_200;

              BlackPhoneManager *bpm = BlackPhoneManager::getInstance();
              string s = obj[MOBILE].ToString();
              bool rrr = bpm->find(s);
              if(rrr)
                {
                  respObj[RES] = 10;
                  array.push_back(2);
                }
              else
                respObj[RES] = 0;
              respObj[PARAM] = array;    
            }
        }
      catch(...)
        {
          LOG_BEGIN(Logger::ERR)
            LOG_PROP("servData", mServData)
            LOG_END("json parse error");
          respObj[STATUS] = STATUS_407;
        }
      string resp = json::Serialize(respObj);
      MessageManager *mm = MessageManager::getInstance();
      mm->sndResult(m_id, resp);
      return; 
    }
  
  m_pData = memorySrc;
  std::string output;
  bool rret = Base64Decode(mOrient, &output);

  if(rret = false)
    m_len = -1;
  m_len = output.length();
  memcpy(m_pData, output.data(), output.length());
  
  if(m_len < 10)
    {
      LOG_BEGIN(Logger::ERR)
        LOG_END("message error");
      respObj[STATUS] = STATUS_407;
    }
  else
    {
      CryptRSA *cryptRSA = CryptRSA::getInstance();

      int len = getHeadLen();
      if(len == -1 )
        {
          LOG( Logger::ERR, std::string("Data error").c_str());
          respObj[STATUS] = STATUS_407;
        }
      else
        {
          memcpy(bufen, m_pData + 10 , len);
          int ret = cryptRSA->privKeyDecrypt(bufen, len, bufkey);
          string key((char *)bufkey);
          if(key.size() < 1)
            {
              LOG_BEGIN(Logger::ERR)
                LOG_PROP("ret", ret)
                LOG_PROP("len", len)
                LOG_END("Rsa key failture!");
              respObj[STATUS] = STATUS_407;
            }
          else
            {
              CryptAES cryptAES;  
              u_char *ciphertext = (u_char *)m_pData + len + 10;
              size_t cip_len = m_len - len - 10;
              
              cryptAES.aesDecrypt(ciphertext,cip_len, key, (u_char*)result, result_len);
              string jsonData(result, result_len);
  
              json::Value js = json::Deserialize(jsonData);
              json::Object obj = js.ToObject();

              SecurityRequest request;
              SecurityResponse response;

              try{
                request.setDataSource(jsonData, mServData);
                RuleEngine *engine = RuleEngine::getInstance();
                engine->doProcess(request, response);
    
                respObj[STATUS] = STATUS_200;
                json::Array array;
                int res  = 0;
                for(auto &it: response.mScores)
                  {
                    auto ir = rulePropsal.find(it);
                    if(ir !=rulePropsal.end() && ir->second > res)
                      res = ir->second;
                    array.push_back(it);
                  }
                
                if(res == MONITOR)
                  res = 0;
                
                respObj[RES] = res;
                obj[PARAM] = array;
                char servTime[40] = {};
                time_t rawtime;
                struct tm * timeinfo;
                time (&rawtime);
                timeinfo = localtime (&rawtime);
                //"2016-04-01 05:06:10"
                strftime (servTime,40,"%F %T",timeinfo);
                obj[SERV_TIME] = string(servTime);
                string ress = json::Serialize(obj);
                MSG(ress.c_str());
              }
              catch(...)
                {
                  LOG_BEGIN(Logger::ERR)
                    LOG_PROP("jsonData", jsonData)
                    LOG_END("json parse error");
                  respObj[STATUS] = STATUS_500;
                }
            }
        }
    }

  MessageManager *mm = MessageManager::getInstance();
  string resp = json::Serialize(respObj);
  mm->sndResult(m_id, resp);

}

