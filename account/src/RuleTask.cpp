#include "RuleTask.hpp"
#include "Logger.hpp"
#include "RuleEngine.hpp"

RuleTask::RuleTask(SecurityRequest *request,  SecurityResponse *response)
  :mRequest(request)
  ,mResponse(response)
{
  TRACE;
}

RuleTask::~RuleTask()
{
  TRACE;
  if(mResponse)
    delete mResponse;
  if(mRequest)
    delete mRequest;
}

void RuleTask::run()
{
  TRACE;
  RuleEngine *engine = RuleEngine::getInstance();
  engine->doProcess(*mRequest, *mResponse);
}
