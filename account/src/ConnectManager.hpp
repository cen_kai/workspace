/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef CONNECT_MANAGER_HPP
#define CONNECT_MANAGER_HPP

#include "Event.hpp"
#include "TcpConnection.hpp"
#include "Logger.hpp"
#include "Singleton.hpp"
#include "EventLoop.hpp"
#include "PingAnMessage.hpp"

#include <map>

using std::map;

typedef std::map< int, TcpConnection* > ConnectionMap; //map[fd] = TcpConnection
typedef std::map< int, TcpConnection* >::iterator ConnectionIt; 
typedef std::map< string, int > IdMap; //map[id] = fd;
typedef std::map< string, int > IdIt;

class ConnectManager: public IOWatcher , public Singleton<ConnectManager>
{
public:
  
  ConnectManager();
  virtual ~ConnectManager();
  bool doIO(IOEvent *evt, int events) ;
  void setListenConnection(TcpConnection * con);
  bool doAccept(IOEvent* evt, int events);
  bool doRead(int fd);
  bool doWrite(int fd);
  bool sndResp(string id , string resp);

  void addConnection(int fd, TcpConnection *con);
  TcpConnection* getConnection(int fd);
  
  void removeConnection(int socket);
  void delTimeoutConnection();
  bool start();
  bool stop();
  bool init(TcpConnection* listenConection);
  
private:
  ConnectManager& operator=(const ConnectManager&);
  ConnectManager(const ConnectManager&);
  bool bStart;
  ConnectionMap mConnections;
  TcpConnection *mListenConnction;
  TcpConnection *mDirConnetion;
  IdMap mIds;
};

#endif
