/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef OBJECT_POOL_HPP
#define OBJECT_POOL_HPP

#include "ConcurrentDeque.hpp"
#include "Logger.hpp"

template <typename T>
class ObjectPool
{
public:

  ObjectPool()
    : m_pool()
    , m_numberOfUsedObjects(0)
  {
    TRACE;
  }

  virtual ~ObjectPool()
  {
    TRACE;
  }

  void add(const T object) // throws CancelledException
  {
    TRACE;
    m_pool.push(object);
  }


  T acquire() // throws CancelledException
  {
    TRACE;
    T tmp = m_pool.waitAndPop();
    m_numberOfUsedObjects++;
    return tmp;
  }

  size_t size()
  {
    return m_pool.size();
  }
  
  T tryAcquire() // throws CancelledException
  {
    // TRACE;
    T t;
    bool ret = m_pool.tryPop(t);
    if(ret){
      m_numberOfUsedObjects++;
      return t;
    }else
      return NULL;
  }
  
  void release(T object)
  {
    TRACE;
    if ( m_pool.cancelled() ) {
      m_numberOfUsedObjects--;
      return;
    }

    m_pool.push(object);
    m_numberOfUsedObjects--;
  }

  void clear()
  {
    TRACE;
    m_pool.cancel();
  }


private:

  ConcurrentDeque<T> m_pool;
  int m_numberOfUsedObjects;
};

#endif // OBJECT_POOL_HPP
