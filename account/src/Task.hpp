/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef TASK_HPP
#define TASK_HPP

#include <time.h>
#include <sys/types.h>

class Task
{

public:
  
    virtual ~Task() {};
    virtual void run () = 0;

protected:


};


#endif // TASK_HPP
