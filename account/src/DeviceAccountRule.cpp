#include "DeviceAccountRule.hpp"
#include "Logger.hpp"
#include "EventLoop.hpp"
#include "PingAn.hpp"

#include <iostream>

DeviceAccountRule::DeviceAccountRule()
  :mAccount()
{
  Rule::mRuleName = pingan::DEVICE_ACCOUNT_RULE;
}

DeviceAccountRule::~DeviceAccountRule()
{

  for(auto it = mAccount.begin(); it != mAccount.end(); ++it)
      delete it->second;

  mAccount.clear();
}

void DeviceAccountRule::process(const SecurityRequest& req, SecurityResponse& resp)
{
  if(req.mDeviceID.size() < 1)
    {
      return ;
    }

  EventLoop *ev = EventLoop::getInstance();
  time_t t = ev->getCurTime();
  ReaderMutexLock lock(&mLock);
  auto it = mAccount.find(req.mDeviceID);
  AccountSet *item = nullptr;
  
  if(it == mAccount.end())
  	{
   	 item = new AccountSet();
   	 mAccount[req.mDeviceID] = item;
  }
  else
    item = it->second;
  uint16_t res = 0;
  item->mAccounts.insert(req.mClientNo);
  item->mlastime = t;

  if(item->mAccounts.size() > 5)
    {
      resp.updateScore(mRuleName, res);
    }
}

void DeviceAccountRule::aging()
{
  WriterMutexLock lock(&mLock);
  EventLoop *ev = EventLoop::getInstance();
  time_t t = ev->getCurTime();
  std::cout<<"getCurtime: "<<t<<std::endl;
  std::cout<<"getSystemTime: "<<time(NULL)<<std::endl;
  for(auto it = mAccount.begin() ; it != mAccount.end();)
    {
      AccountSet *item = it->second;
      if(( t - item->mlastime) > 86400) //超过一天
        {
          delete item;
          it = mAccount.erase(it);
        }
      else
        ++it;
    }
}


