/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "Logger.hpp"
#include "Log4z.hpp"

#include "Colors.hpp"

#include "Common.hpp"

#include <time.h> //time

using namespace zsummer::log4z;

void Logger::init(std::ostream& log_stream )
{
  //m_ostream = &log_stream;
  ILog4zManager::GetInstance()->Start();
}
Logger::~Logger()
{
  //m_ostream->flush();
  ILog4zManager::GetInstance()->Stop();
}

void Logger::setLogLevel ( const LogLevel loglevel )
{
  m_logLevel = loglevel;
}


void Logger::usePrefix( const bool use )
{
  m_usePrefix = use;
}


void Logger::log_pointer( const void* pointer,
                          const char* file,
                          const int line,
                          const char* function)
{
  if ( !m_usePrefix ) {
    *m_ostream << pointer << std::endl;
    return;
  }
	zsummer::log4z::CStringStream ss(g_log4zstreambuf, LOG_BUF_SIZE);

  ss << getTime() << " "
              << COLOR( FG_GREEN ) << extractFilename(file)
              << COLOR_RESET << ":"
              << COLOR( FG_BROWN ) <<  line << COLOR_RESET << " "
              << COLOR( FG_CYAN ) << function << COLOR_RESET << " "
              << COLOR( FG_BLUE ) << pointer
     << COLOR_RESET ;
  int id = ILog4zManager::GetInstance()->GetMainLogger();
  zsummer::log4z::ILog4zManager::GetInstance()->PushLog(id, LOG_LEVEL_DEBUG, g_log4zstreambuf); 
}


void Logger::log_string( const int level,
                         const void* pointer,
                         const char* msg,
                         const char* file,
                         const int line,
                         const char* function)
{
  // if ( !m_usePrefix ) {
  //   *m_ostream << msg << std::endl;
  //   return;
  // }

  const char *color;
  if ( level <= WARNING ) { color = COLOR_F_FG( F_BOLD, FG_RED ); }
  else if ( level <= INFO ) { color = COLOR_F_FG( F_BOLD, FG_WHITE); }
  else { color = COLOR_F_FG( F_BOLD, FG_BROWN); }
	zsummer::log4z::CStringStream ss(g_log4zstreambuf, LOG_BUF_SIZE);
  ss  << getTime() << " "
              << COLOR( FG_GREEN ) << extractFilename(file)
              << COLOR_RESET << ":"
              << COLOR( FG_BROWN ) <<  line << COLOR_RESET << " "
              << COLOR( FG_CYAN ) << function << COLOR_RESET << " "
              << color << "\"" << msg << "\" "
              << COLOR( FG_BLUE ) << pointer
      << COLOR_RESET;
  int chanel = LOG_LEVEL_DEBUG;
  switch(level)
    {
    case EMERG:
    case ALERT:
      chanel = LOG_LEVEL_FATAL;
      break;
    case CRIT:
    case ERR:
      chanel = LOG_LEVEL_ERROR;
      break;
    case WARNING:
      chanel = LOG_LEVEL_WARN;
      break;
    case NOTICE:
    case INFO:
      chanel = LOG_LEVEL_INFO;
      break;
    case DEBUG:
    case FINEST:
      chanel = LOG_LEVEL_DEBUG;
      break;
    }
  int id = ILog4zManager::GetInstance()->GetMainLogger();
  zsummer::log4z::ILog4zManager::GetInstance()->PushLog(id, chanel, g_log4zstreambuf); 
}
#include <iostream>
void Logger::msg(const char* text)
{
  //  *m_ostream  << COLOR_F( F_BOLD) << text << COLOR_RESET << std::endl;
  zsummer::log4z::ILog4zManager::GetInstance()->PushLog(1, LOG_LEVEL_CONTENT , text); 
}


Logger::LogLevel Logger::m_logLevel = Logger::FINEST;
std::ostream* Logger::m_ostream = 0;
bool Logger::m_usePrefix = true;
