/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef _ConfigFile_hpp_
#define _ConfigFile_hpp_

#include <string>
#include <map>
#include <list>

#include "Singleton.hpp"

using std::string;
using std::map;
using std::list;

class ConfigFile
{
public:
	/* Parse config file 'configFile'. If the process environment
	* is provided, environment variables can be used as expansion symbols.
	*/
	ConfigFile(string configFile, char** envp = NULL);

	~ConfigFile();

	// get string config entry
	string getString(string name, string def);

	/* get boolean config entry
	* A value of Yes/yes/YES/true/True/TRUE leads to true,
	* all other values leads to false.
	*/
	bool getBool(string name, bool def);

	// get double config entry; value is parsed using atof()
	double getDouble(string name, double def);

	// get int config entry; value is parsed using atoi()
	int getInt(string name, int def);

  static void init(string& filename)
  {
    sFilename = filename;
  }

	// get the symbol map (e.g. for iterating over all symbols)
	inline map<string, string>& getSymbols()
	{
		return symbols;
	}

	// get config sub group
	inline ConfigFile* group(string name)
	{
		return groups[name];
	}

	// get config sub group map (e.g. for iterating over all groups)
	inline map<string, ConfigFile*>& getGroups()
	{
		return groups;
	}

  static void createInstance(string& fname);

  static ConfigFile* getInstance();
  static void destroy();
  static ConfigFile* m_instance;  
  
private:
	// private constructor for sub groups
	ConfigFile(string name, string parentDebugInfo);

	// helper functions for parsing
	void add(string name, string value);
	void split(string in, string& left, string& right, char c);
	void trim(string& s);
	void symbolExpand(string& s);
	void symbolExpand(map<string, string>& symbols, string& s);
	void envSymbolExpand(string& s);

	// config group symbol map
	map<string, string> symbols;

	// environment symbol map
	map<string, string> envSymbols;

	// config sub group map
	map<string, ConfigFile*> groups;

	// stack of config groups for parsing (only used in top config element)
	list<ConfigFile*> groupStack;

	// debug info used for logging messages
	string debugInfo;

  static string sFilename;
};

#endif
