#include "BlackPhoneManager.hpp"
#include "Logger.hpp"
#include "PingAnTask.hpp"

#include <iostream>

BlackPhoneManager::BlackPhoneManager()
  :mPhones()
  ,mStart(false)
  ,mLock()
{

}

BlackPhoneManager::~BlackPhoneManager()
{
  mStart = false;
  mPhones.clear();
}

bool BlackPhoneManager::start()
{
  if(mStart == true)
    return true;

  BlackPhoneTask blackPhoneTask(0);
  blackPhoneTask.run();
  
  mStart= true;
}

bool BlackPhoneManager::stop()
{
  if(mStart == false)
    return true;
  
  mPhones.clear();
  mStart = false;
}

static int64_t phoneStr2long(const string& phoneNo)
{
  string s;
  if(phoneNo.find('+') != std::string::npos)
    s = phoneNo.substr(3);
  else
    s = phoneNo;
      
  int ret = strtoll(s.data(), nullptr, 10);
  return ret;
}

void BlackPhoneManager::update(vector<string>& add, vector<string>& del)
{
  WriterMutexLock lock(&mLock);
  
  for(auto it = add.begin(); it != add.end(); ++it)
    {
      int64_t ret = phoneStr2long(*it);
      mPhones.insert(ret);
    }

  for(auto it = del.begin(); it != del.end(); ++it)
    {
      int64_t ret =phoneStr2long(*it);
      mPhones.erase(ret);
    }
}

bool BlackPhoneManager::find(const string& phoneNo)
{
  TRACE;
  ReaderMutexLock lock(&mLock);
  std::cout<<"---------> lock after"<<std::endl;
  int64_t t = phoneStr2long(phoneNo);
  auto it = mPhones.find(t);
  if(it != mPhones.end())
    return true;
  else
    return false;
}

