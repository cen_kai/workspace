/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef SOCKET_SERVER_HPP
#define SOCKET_SERVER_HPP

#include "StreamConnection.hpp"
#include "ConfigFile.hpp"
#include "Event.hpp"
#include "TimerManager.hpp"
#include "MessageManager.hpp"
#include "ConnectManager.hpp"

using std::set;
using std::string;
using std::map;

class SocketServer
{
public:

  SocketServer ();
  
  virtual ~SocketServer();
  bool start();
  void stop();
  
private:

  SocketServer(const SocketServer&);
  
  SocketServer& operator=(const SocketServer&);

  bool bStart;  
  TcpConnection  *mListenConntion;
  TimerManager *mTimerManager;
  ConnectManager *mConnectionManger;
  MessageManager *mMessageManager;
  int mMaxPendingQueueLen;
};

#endif // SOCKET_SERVER_HPP
