#include "CryptAES.hpp"
#include "Logger.hpp"

CryptAES::CryptAES(){
  TRACE;
}

CryptAES::~CryptAES(){
  TRACE;
}

string* CryptAES::aesDecrypt(const std::string& ciphertext, const std::string& key)
{
  TRACE;
  EVP_CIPHER_CTX ctx;
  EVP_CIPHER_CTX_init(&ctx);
  int ret = EVP_DecryptInit_ex(&ctx, EVP_aes_128_ecb(), NULL, (const unsigned char*)key.data(), NULL);
  //  assert(ret == 1);
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_DecryptInit_ex err");
  }
  unsigned char* result = new unsigned char[ciphertext.length() + 64]; // 弄个足够大的空间
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("lenData", ciphertext.length() + 64)
    LOG_END("");
  
  int len1 = 0;
  ret = EVP_DecryptUpdate(&ctx, result, &len1, (const unsigned char*)ciphertext.data(), ciphertext.length());
  //  assert(ret == 1);
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_DecryptUpdate err");
  }
  int len2 = 0;
  ret = EVP_DecryptFinal_ex(&ctx, result+len1, &len2);
  
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_DecryptFinal_ex err");
  }
  //  assert(ret == 1);
  //  std::cout << len1 << "," << len2 << std::endl;
  LOG_BEGIN(Logger::DEBUG)
    LOG_PROP("len1", len1)
    LOG_PROP("len2", len2)
  LOG_END("");

  ret = EVP_CIPHER_CTX_cleanup(&ctx);
  // assert(ret == 1);
  
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_CIPHER_CTX_cleanup err");
  }

  //  std::string *res = new string(((char*)result, len1+len2));
  std::string *res = new string((char*)result, len1+len2);
  delete[] result;
  return res;
}


void CryptAES::aesDecrypt(u_char *ciphertext, size_t encsiz, const std::string& key, u_char *result, size_t &resLen)
{
  TRACE;
  EVP_CIPHER_CTX ctx;
  EVP_CIPHER_CTX_init(&ctx);
  int ret = EVP_DecryptInit_ex(&ctx, EVP_aes_128_ecb(), NULL, (const unsigned char*)key.data(), NULL);
  //  assert(ret == 1);
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_DecryptInit_ex err");
  }
  //unsigned char* result = new unsigned char[ciphertext.length() + 64]; // 弄个足够大的空间
  /*
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("lenData", ciphertext.length() + 64)
    LOG_END("");
  */
  int len1 = 0;
  ret = EVP_DecryptUpdate(&ctx, result, &len1, (const unsigned char*)ciphertext, encsiz);
  //  assert(ret == 1);
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_DecryptUpdate err");
  }
  int len2 = 0;
  ret = EVP_DecryptFinal_ex(&ctx, result+len1, &len2);
  
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_DecryptFinal_ex err");
  }
  //  assert(ret == 1);
  //  std::cout << len1 << "," << len2 << std::endl;
  // LOG_BEGIN(Logger::DEBUG)
  //   LOG_PROP("len1", len1)
  //   LOG_PROP("len2", len2)
  //   LOG_PROP("result", result)
  // LOG_END("");

  ret = EVP_CIPHER_CTX_cleanup(&ctx);
  // assert(ret == 1);
  
  if(ret != 1){
    LOG_BEGIN(Logger::ERR)
      LOG_END("EVP_CIPHER_CTX_cleanup err");
  }

  //  std::string *res = new string(((char*)result, len1+len2));
  //  std::string *res = new string((char*)result, len1+len2);
  resLen = len1 + len2;
  //  delete[] result;
  //  return res;
}

