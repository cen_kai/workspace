#include "MacManager.hpp"
#include "Common.hpp"

#include <algorithm>

static uint32_t getMacOrgan(string &mac)
{
  string tmp(mac);
  if(tmp.length() == 17)
    std::remove(tmp.begin(), tmp.end(), ':');

  if(tmp.length() != 12)
      return 0;

  string imac = tmp.substr(0, 6);
  char *p = NULL;
  uint32_t result = strtol(imac.data(), &p, 16);
  return result;
}

MacManager::MacManager()
  :mMacOrgans()
{
  
}

MacManager::~MacManager()
{
  mMacOrgans.clear();
}

bool MacManager::start()
{
  //TODO 启动初始化任务
  return true;
}

bool MacManager::stop()
{
  mMacOrgans.clear();
  return false;
}

bool MacManager::findMac(string& mac)
{
  ReaderMutexLock lock(&mLock);
  
  if(mac.length() < 1)
    return 0;
  
  uint32_t organ = getMacOrgan(mac);
  if(binary_search(mMacOrgans.begin(), mMacOrgans.end(), organ))
      return organ;
  else
    return false;
}

bool MacManager::update(MacSet& updateMacs, MacSet& deleteMacs)
{
  WriterMutexLock lock(&mLock);
  MacSetIt upIt = updateMacs.begin();
  
  for(; upIt != updateMacs.end(); ++upIt)
      mMacOrgans.insert(*upIt);
  
  for(upIt = deleteMacs.begin();upIt != deleteMacs.end(); ++upIt)
    mMacOrgans.erase(*upIt);
  
  return true;
}

    
