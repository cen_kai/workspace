/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef CONCURRENTQUEUE_HPP
#define CONCURRENTQUEUE_HPP

#include <deque>
#include <algorithm>

#include "Mutex.hpp"
#include "ConditionVariable.hpp"
#include "ScopedLock.hpp"
#include "Common.hpp"
#include "Logger.hpp"


class CancelledException {};


template <typename T>
class ConcurrentDeque
{
public:

  ConcurrentDeque()
    : m_queue()
    , m_cancelled(false)
    , m_mutex()
    , m_condVar(m_mutex)
  {
    TRACE;
  }

  ~ConcurrentDeque()
  {
    TRACE;
    freeDeque();
  }


  void push( T value)
  {
    TRACE;
    ScopedLock sl(m_mutex);
    if (m_cancelled) throw CancelledException();
    m_queue.push_back( value );
    m_condVar.signal();
  }


  bool tryPop(T &popped_value)
  {
    TRACE;
    ScopedLock sl(m_mutex);
    if (m_cancelled) throw CancelledException();
    if ( m_queue.empty() ) return false;

    popped_value = m_queue.front();
    m_queue.pop_front();
    return true;
  }

  size_t size()
  {
    m_queue.size();
  }
  
  T waitAndPop()
  {
    TRACE;
    ScopedLock sl(m_mutex);

    while ( m_queue.empty() and not m_cancelled) {
      m_condVar.wait();
    }
    if (m_cancelled) throw CancelledException();

    T retVal = m_queue.front(); // cctor
    m_queue.pop_front();
    return retVal;
  }


  bool empty() const
  {
    TRACE;
    ScopedLock sl(m_mutex);
    if (m_cancelled) throw CancelledException();
    return m_queue.empty();
  }

  void cancel()
  {
    TRACE;
    ScopedLock sl(m_mutex);
    m_cancelled = true;
    m_condVar.broadcast();
    freeDeque();
  }

  void freeDeque()
  {
    TRACE;
    typename std::deque<T>::iterator it;
    for ( it = m_queue.begin(); it != m_queue.end(); ++it )
      delete *it;

    m_queue.clear();
  }
  /*
  void freeDeque()
  {
    TRACE;
    m_queue.clear();
  }
  */
  bool cancelled()
  {
    TRACE;
    return m_cancelled;
  }

private:


  ConcurrentDeque& operator=( const ConcurrentDeque& );
  ConcurrentDeque( const ConcurrentDeque& );

  std::deque<T> m_queue;
  bool m_cancelled;
  mutable Mutex m_mutex;
  ConditionVariable m_condVar;

};

#endif // CONCURRENTQUEUE_HPP
