/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "WorkerThread.hpp"

#include "Logger.hpp"


WorkerThread::WorkerThread( ThreadPool& tp )
  : m_tp(tp)
{
  TRACE;
}


void* WorkerThread::run()
{
  TRACE;
  while ( m_isRunning )
  {
    Task* task(0);
    try {
      task = m_tp.popTask();
      task->run();
      delete task;
    } catch (CancelledException) {
      LOG( Logger::FINEST, "Now I die.");
    }
  }
  return 0;
}
