#ifndef DEVICEPRINTRULE_H
#define DEVICEPRINTRULE_H

#include "Singleton.hpp"
#include "RuleEngine.hpp"

class DevicePrintRule: public Singleton<DevicePrintRule>, public Rule
{
public:
  DevicePrintRule();
  void process(const SecurityRequest& req, SecurityResponse& resp);
  virtual ~DevicePrintRule();
};


#endif /* DEVICEPRINTRULE_H */
