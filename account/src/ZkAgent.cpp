#include "ZkAgent.hpp"
#include "Logger.hpp"
#include "ConfigFile.hpp"
#include <string>

using std::string;

ZkAgent::ZkAgent()
{
  TRACE;
  
  ConfigFile *config = ConfigFile::getInstance();
  std::string zookaddr = config->getString("zookaddr", "null");
  //  std::string zookaddr = "10.211.55.2:2181";
  mZookeeper = new ZooKeeper(zookaddr, this, 30000);
  
  mPath.clear();
  mPath.append("/apps/");
  std::string localhost = config->getString("localhost", "null");
  std::string localport = config->getString("localport", "10003");
  std::string service = config->getString("service_name", "null");
  // FIXME: service、localhost, localport为空的情形
  mPath.append(service).append("/").append(localport).append("@").append(localhost);
}

ZkAgent::~ZkAgent()
{
  TRACE;
  if(mZookeeper)
    delete mZookeeper;
}

void ZkAgent::OnConnected()
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_END("OnConnected zk .");

  try
    {
      mZookeeper->Create(mPath, "low", 1);
    }
  catch(ZooException)
    {
      LOG_BEGIN(Logger::INFO)
        LOG_END("create zk exception");
    }
}

void ZkAgent::OnConnecting()
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_END("OnConnecting zk");
}

void ZkAgent::OnSessionExpired()
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_END("OnSessionExpired zk");
}

void ZkAgent::OnCreated(const char* path)
{
  TRACE;

  LOG_BEGIN(Logger::INFO)
    LOG_PROP("path", path)
    LOG_END("zk OnCreated .");

}

void ZkAgent::OnDeleted(const char* path)
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("path", path)
    LOG_END("zk OnDeleted");
}

void ZkAgent::OnChanged(const char* path)
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("path", path)
    LOG_END("zk OnChanged .");
}

void ZkAgent::OnChildChanged(const char* path)
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("path", path)
    LOG_END("zk OnChildChanged .");
}

void ZkAgent::OnNotWatching(const char* path)
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("path", path)
    LOG_END("zk OnNotWatching .");
}

void ZkAgent::setSnodeValue(ZkSNodeValue value)
{
  // FIXME: stat disconnect
  switch(value)
    {
    case SNODE_LOW:
      mZookeeper->Set(mPath, "low");
      break;
    case SNODE_NORMAL:
      break;
    case SNODE_HIGH:
      mZookeeper->Set(mPath, "high");
      break;
    }
}
