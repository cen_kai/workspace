#include "DeviceManager.hpp"

DeviceManager::DeviceManager()
  :mLock()
  ,mStart(false)
  ,mDevices()
  ,mDeviceIDs()
  ,mBlueMacDevices()
  ,mWifiMacDevices()
  ,mPhoneNoDevices()
  ,mIMEIDevices()
  ,mIMSIDevices()
  ,mSimSerialDevices()
{
  
}

DeviceManager::~DeviceManager()
{
  mWifiMacDevices.clear();
  mBlueMacDevices.clear();
  mPhoneNoDevices.clear();
  mIMEIDevices.clear();
  mIMSIDevices.clear();
  mSimSerialDevices.clear();
  mDeviceIDs.clear();
}

bool DeviceManager::start()
{
  mStart = true;
  //TODO 添加设备数据初始化任务
  return true;
}

bool DeviceManager::stop()
{
  mWifiMacDevices.clear();
  mBlueMacDevices.clear();
  mPhoneNoDevices.clear();
  mIMEIDevices.clear();
  mIMSIDevices.clear();
  mSimSerialDevices.clear();
  mDeviceIDs.clear();
  mStart = false;
  return true;
}

AndroidDeviceInfo* DeviceManager::findByBlueMac(uint64_t blueMac)
{
  ReaderMutexLock lock(&mLock);
  DeviceMacMapIt it = mBlueMacDevices.find(blueMac);
  if(it == mBlueMacDevices.end())
    return NULL;
  AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
  *device = *(it->second);
  return device;
}

AndroidDeviceInfo* DeviceManager::findByWifiMac(uint64_t wifiMac)
{
  ReaderMutexLock lock(&mLock);
  DeviceMacMapIt it = mWifiMacDevices.find(wifiMac);
  if(it == mWifiMacDevices.end())
    return NULL;
  AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
  *device = *(it->second);
  return device;
}

DeviceInfo* DeviceManager::findByPhoneNo(uint64_t phoneNumber)
{
  ReaderMutexLock lock(&mLock);
  DevicePhoneMapIt it = mPhoneNoDevices.find(phoneNumber);
  if(it == mPhoneNoDevices.end())
    return NULL;
  if(it->second->mPlatform == ANDROID_PLATFORM)
    {
      AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
      AndroidDeviceInfo *orient = dynamic_cast<AndroidDeviceInfo*>(it->second);
      
      if(orient == NULL)
        {
          recycleDeviceInfo(device);
          return NULL;
        }
      *device = *orient;
      return device;
    }
  else if(it->second->mPlatform == IOS_PLATFORM)
    {
      IOSDeviceInfo *device = obtainIOSDeviceInfo();
      IOSDeviceInfo *orient = dynamic_cast<IOSDeviceInfo*>(it->second);
      if(orient == NULL)
        return NULL;

      *device = *orient;
      return device;
    }
  else
    return NULL;
}

AndroidDeviceInfo* DeviceManager::findByIMEI(uint64_t imei)
{
  ReaderMutexLock lock(&mLock);
  DeviceIMEIMapIt it = mIMEIDevices.find(imei);
  if(it == mIMEIDevices.end())
    return NULL;
  AndroidDeviceInfo *orient = dynamic_cast<AndroidDeviceInfo*>(it->second);
  if(orient == NULL)
    return NULL;
  
  AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
  *device = *orient;
  return device;
}

AndroidDeviceInfo* DeviceManager::findByIMSI(uint64_t imsi)
{
  ReaderMutexLock lock(&mLock);

  DeviceIMSIMapIt it = mIMSIDevices.find(imsi);
  if(it == mIMSIDevices.end())
    return NULL;
  AndroidDeviceInfo *orient = dynamic_cast<AndroidDeviceInfo*>(it->second);
  if(orient == NULL)
    return NULL;

  AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
  *device = *orient;
  return device;
}

AndroidDeviceInfo* DeviceManager::findBySimSerial(uint64_t simserial)
{
      ReaderMutexLock lock(&mLock);

  DeviceSimSerialMapIt it = mSimSerialDevices.find(simserial);
  if(it == mSimSerialDevices.end())
    return NULL;
  
  AndroidDeviceInfo *orient = dynamic_cast<AndroidDeviceInfo*>(it->second);
  if(orient == NULL)
    return NULL;

  AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
  *device = *orient;
  return device;
}

DeviceInfo* DeviceManager::findByDeviceID(const string& deviceID)
{
  ReaderMutexLock lock(&mLock);

  DeviceIDIt it = mDeviceIDs.find(deviceID);
  if(it == mDeviceIDs.end())
    return NULL;
  if(it->second->mPlatform == ANDROID_PLATFORM)
    {
      AndroidDeviceInfo *device = obtainAndroidDeviceInfo();
      AndroidDeviceInfo *orient = dynamic_cast<AndroidDeviceInfo*>(it->second);
      if(orient == NULL)
        {
          recycleDeviceInfo(device);
          return NULL;
        }
      *device = *orient;
      return device;
    }
  else if(it->second->mPlatform == IOS_PLATFORM)
    {
      IOSDeviceInfo *device = obtainIOSDeviceInfo();
      IOSDeviceInfo *orient = dynamic_cast<IOSDeviceInfo*>(it->second);
      *device = *orient;
      return device;
    }
  else
    return NULL;
}

AndroidDeviceInfo* DeviceManager::obtainAndroidDeviceInfo()
{
  DeviceInfo* tmp = mAndroidPool.tryAcquire();
  AndroidDeviceInfo* device = dynamic_cast<AndroidDeviceInfo*>(tmp) ;
  if(device == NULL)
    device = new AndroidDeviceInfo();
  return device;
}

void DeviceManager::recycleDeviceInfo(DeviceInfo* deviceInfo)
{
  if(deviceInfo == NULL)
    return;
  
  if(deviceInfo->mPlatform == ANDROID_PLATFORM)
    {
      if(mAndroidPool.size() > 100)
        delete deviceInfo;
      else
        mAndroidPool.add(deviceInfo);
    }
  else if(deviceInfo->mPlatform == IOS_PLATFORM)
    {
      if(mIOSPool.size() > 100)
        delete deviceInfo;
      else
        mIOSPool.add(deviceInfo);
    }
}

bool DeviceManager::update(DeviceSet& updateDevices, DeviceSet& deleteDevices)
{
  WriterMutexLock lock(&mLock);
  for(DeviceSetIt dsi = updateDevices.begin(); dsi != updateDevices.end(); ++dsi)
    {
      DeviceInfo* device = *dsi;
      //加入mDevies
      DeviceMapIt deviceMapIt = mDevices.find(device->mID);
      if(deviceMapIt == mDevices.end())
        {
          mDevices[device->mID] = device;
        }
      else
        {
          deviceMapIt->second->mLocations.clear();
          mDeviceIDs.erase(device->mDeviceID);
          if(device->mPlatform == ANDROID_PLATFORM)
            {
              AndroidDeviceInfo* android = dynamic_cast<AndroidDeviceInfo*>(device);
              mBlueMacDevices.erase(android->mBlueMac);
              mWifiMacDevices.erase(android->mWifiMac);
              mPhoneNoDevices.erase(android->mPhoneNumber);
              mIMEIDevices.erase(android->mIMEI);
              mIMSIDevices.erase(android->mIMSI);
              mSimSerialDevices.erase(android->mSimSerial);
            }
          else
            {
              //IOS专用
            }
        }
        
      //add into mDeviceIDs, 要求deviceID为空时必须是EMTPYT_STRING
      if(device->mDeviceID != EMPTY_STRING)
        {
          DeviceIDIt deviceIDIt = mDeviceIDs.find(device->mDeviceID);
          if(deviceIDIt == mDeviceIDs.end())
            {
              mDeviceIDs[device->mDeviceID] = device;
            }
          else
            {
              LOG_BEGIN(Logger::ERR)  LOG_PROP("duplicate deviceID", device->mDeviceID) LOG_END("");
            }
        }

      //加入mPhoneNoDevices, 默认为0
      if(device->mPhoneNumber != 0)
        {
          DevicePhoneMapIt devicePhoneMapIt = mPhoneNoDevices.find(device->mPhoneNumber);
          if(devicePhoneMapIt == mPhoneNoDevices.end())
            {
              mPhoneNoDevices[device->mPhoneNumber] = device;
            }
          else
            {
              LOG_BEGIN(Logger::WARNING)  LOG_PROP("duplicate phoneno:", device->mPhoneNumber) LOG_END("");
            }
        }
      
      if(device->mPlatform == ANDROID_PLATFORM)
        {
          AndroidDeviceInfo* android = dynamic_cast<AndroidDeviceInfo*> (device);
          //加入mMacDevices 默认0
          if(android->mBlueMac != 0)
            {
              DeviceMacMapIt deviceMacMapIt = mBlueMacDevices.find(android->mBlueMac);
              if(deviceMacMapIt == mBlueMacDevices.end())
                {
                  mBlueMacDevices[android->mBlueMac] = dynamic_cast<AndroidDeviceInfo*>(device);
                }
              else
                {
                  LOG_BEGIN(Logger::WARNING)  LOG_PROP("duplicate blueMac:", android->mBlueMac) LOG_END("");
                }
            }
          
          if(android->mWifiMac != 0)
            {
              DeviceMacMapIt deviceMacMapIt = mWifiMacDevices.find(android->mWifiMac);
              if(deviceMacMapIt == mWifiMacDevices.end())
                {
                  mWifiMacDevices[android->mWifiMac] = dynamic_cast<AndroidDeviceInfo*>(device);
                }
              else
                {
                  LOG_BEGIN(Logger::WARNING)  LOG_PROP("duplicate blueMac:", android->mWifiMac) LOG_END("");
                }
            }
          
          //加入mIMEIDevices
          if(android->mIMEI != 0)
            {
              DeviceIMEIMapIt deviceIMEIMapIt = mIMEIDevices.find(android->mIMEI);
              if(deviceIMEIMapIt == mIMEIDevices.end())
                {
                  mIMEIDevices[android->mIMEI] = dynamic_cast<AndroidDeviceInfo*>(device);
                }
              else
                {
                  LOG_BEGIN(Logger::WARNING)  LOG_PROP("duplicate blueMac:", android->mIMEI) LOG_END("");
                }
            }
          //加入mIMSIDevices
          if(android->mIMSI != 0)
            {
              DeviceIMSIMapIt deviceIMSIMapIt = mIMSIDevices.find(android->mIMSI);
              if(deviceIMSIMapIt == mIMSIDevices.end())
                {
                  mIMSIDevices[android->mIMSI] = dynamic_cast<AndroidDeviceInfo*>(device);
                }
              else
                {
                  LOG_BEGIN(Logger::WARNING)  LOG_PROP("duplicate blueMac:", android->mIMSI) LOG_END("");
                }
            }
          //加入mSimSerialDevices
          if(android->mSimSerial != 0)
            {
              DeviceSimSerialMapIt deviceSimSerialMapIt = mSimSerialDevices.find(android->mSimSerial);
              if(deviceSimSerialMapIt == mSimSerialDevices.end())
                {
                  mSimSerialDevices[android->mSimSerial] = dynamic_cast<AndroidDeviceInfo*>(device);
                }
              else
                {
                  LOG_BEGIN(Logger::WARNING)  LOG_PROP("duplicate mSimSerial:", android->mIMSI) LOG_END("");
                }
            }
        }

      if(device->mPlatform == IOS_PLATFORM)
        {
          //todo ios相关处理
        }
    }

  for(DeviceSetIt dsi = deleteDevices.begin(); dsi != deleteDevices.end(); ++dsi)
    {
      DeviceInfo* device = *dsi;
      device->mLocations.clear();
      mDeviceIDs.erase(device->mDeviceID);
      if(device->mPlatform == ANDROID_PLATFORM)
        {
          AndroidDeviceInfo* android = dynamic_cast<AndroidDeviceInfo*> (device);
          mBlueMacDevices.erase(android->mBlueMac);
          mWifiMacDevices.erase(android->mWifiMac);
          mPhoneNoDevices.erase(android->mPhoneNumber);
          mIMEIDevices.erase(android->mIMEI);
          mIMSIDevices.erase(android->mIMSI);
          mSimSerialDevices.erase(android->mSimSerial);
        }
      else
        {
          //IOS专用
        }
    }  
  return true;
}

IOSDeviceInfo* DeviceManager::obtainIOSDeviceInfo()
{
  TRACE;
  DeviceInfo *tmp = mIOSPool.tryAcquire();

  IOSDeviceInfo* device = dynamic_cast<IOSDeviceInfo*>(tmp) ;
  if(device == NULL)
    device = new IOSDeviceInfo();

  return device;
  
}
