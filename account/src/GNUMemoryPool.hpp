/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef _MemoryPoolTemple_hpp_
#define _MemoryPoolTemple_hpp_

#include "MemoryPool.hpp"
#include "ScopedLock.hpp"
#include <stddef.h>

class GNUMemoryPool
{
public:
	GNUMemoryPool(int msgCount);
	~GNUMemoryPool();
	void *get(size_t siz);
	void set( void *t );

  static void createInstance(size_t siz);

  static GNUMemoryPool* getInstance();

  static void destroy();
  static GNUMemoryPool* m_instance;  
private:
	int	m_nMsgCount;
	MemoryPool	*m_pPool;
  mutable Mutex m_mutex;
};

#endif
