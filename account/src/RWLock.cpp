/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "RWLock.hpp"

RWLock::RWLock()
{
  SetIsSafe();
  if (is_safe_ && pthread_rwlock_init(&mutex_, NULL) != 0)
    abort();
}

RWLock::~RWLock()
{
  if (is_safe_ && pthread_rwlock_destroy(&mutex_) != 0)
    abort();
}
void RWLock::Lock()
{
  if (is_safe_ && pthread_rwlock_wrlock(&mutex_) != 0)
    abort();
}
void RWLock::Unlock()
{
  if (is_safe_ && pthread_rwlock_unlock(&mutex_) != 0)
    abort(); 
}

void RWLock::ReaderLock()
{
  if (is_safe_ && pthread_rwlock_rdlock(&mutex_) != 0)
    abort(); 
}
void RWLock::ReaderUnlock()
{
  if (is_safe_ && pthread_rwlock_unlock(&mutex_) != 0)
    abort(); 
}
