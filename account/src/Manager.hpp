#ifndef MANAGER_H
#define MANAGER_H

class Manager
{
public:
  Manager();
  virtual ~Manager();

public:
  bool start();
  bool stop();
};


#endif /* MANAGER_H */
