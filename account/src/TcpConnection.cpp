/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "TcpConnection.hpp"

#include "Logger.hpp"
#include "Common.hpp"
#include "PingAnMessage.hpp"

#include "AddrInfo.hpp"
#include <iostream>
using std::endl;
using std::cout;

TcpConnection::TcpConnection (  const std::string&   host,
                                const std::string&   port,
                                Message            *message,
                                const size_t        bufferLength )
  : StreamConnection(host, port)
  , m_socket(AF_INET, SOCK_STREAM) // or AF_INET6 for IPv6
  , m_message(message)
  , m_buffer(0)
  , m_bufferLength(bufferLength)
  , m_state(CLOSED)
{
  TRACE;
  m_socket.createSocket();
  m_buffer = new unsigned char[m_bufferLength];
  m_message->setConnection(this);
}


TcpConnection::~TcpConnection()
{
  TRACE;

  if (m_state == OPEN)
    disconnect();
  
  if(m_message)
    delete m_message;
  delete[] m_buffer;
}


Connection* TcpConnection::clone(const int socket)
{
  TRACE;
  TcpConnection *tcpConnection = new TcpConnection(socket,
                                                   m_message->clone(),
                                                   m_bufferLength );

  return tcpConnection;
}


bool TcpConnection::connect()
{
  TRACE;

  if (m_state == OPEN) {
    LOG(Logger::ERR, "Connection is open already.");
    return false;
  }

  AddrInfo addrInfo;
  if (!addrInfo.getHostInfo(m_host, m_port))
    return false;

  addrInfo.printHostDetails();

  if (!m_socket.connect(addrInfo[0]))
    return false;

  std::string address, service;
  if ( AddrInfo::convertNameInfo( addrInfo[0], address, service) ) {
    LOG_BEGIN(Logger::INFO)
      LOG_PROP("Host", address)
      LOG_PROP("Port", service)
      LOG_PROP("Socket", m_socket.getSocket())
    LOG_END("Connected to peer.");
  }

  m_state = OPEN;
  return true;
}


bool TcpConnection::bind()
{
  TRACE;

  if (m_state == OPEN) {
    LOG(Logger::ERR, "Connection is open already.");
    return false;
  }

  AddrInfo addrInfo;
  if (!addrInfo.getHostInfo(m_host, m_port))
    return false;

  addrInfo.printHostDetails();

  if (!m_socket.bind(addrInfo[0]))
    return false;

  // std::string address, service;
  // if ( AddrInfo::convertNameInfo( addrInfo[0], address, service) ) {
  //   LOG_BEGIN(Logger::INFO)
  //     LOG_PROP("Host", address)
  //     LOG_PROP("Port", service)
  //   LOG_END("Binded to socket.");
  // }
  return true;
}


bool TcpConnection::listen( const int maxPendingQueueLen )
{
  TRACE;

  if (m_state == OPEN) {
    LOG(Logger::ERR, "Connection is open already.");
    return false;
  }

  if (m_socket.listen(maxPendingQueueLen)) {
    m_state = OPEN;
    return true;
  }

  return false;
}


bool TcpConnection::accept(int& client_socket)
{
  TRACE;
  if (m_state == CLOSED){
    LOG_BEGIN(Logger::INFO)
      LOG_PROP("Info message", m_state)
      LOG_END("");
    
    return false;
  }
  bool ret = m_socket.accept(client_socket);
  
  return ret;
}


bool TcpConnection::disconnect()
{
  TRACE;
  if (m_state == CLOSED)
    return false;

  m_state = CLOSED;
  return m_socket.closeSocket();
}


bool TcpConnection::send( const void* message, const size_t length )
{
  TRACE;
  if (m_state == CLOSED){
    TRACE;
    return false;
  }
  return m_socket.send( message, length );//// TODO: 这里需要处理错误，主要处理在内核缓冲区不足无法写入情况
}


bool TcpConnection::receive()
{
  TRACE;

  if (m_state == CLOSED)
    return false;

  while(1)
    {
    
      ssize_t length;
      if (!m_socket.receive(m_buffer, m_bufferLength, &length) )
        return false;

      //      std::cout<<"buffer"<<m_buffer + 4<<endl;
      if (length == 0) {
        LOG_BEGIN(Logger::INFO)
          LOG_PROP("Host", m_host)
          LOG_PROP("Port", m_port)
          LOG_PROP("Socket", m_socket.getSocket())
          LOG_END("Connection closed by peer.");

        return false;
      }

      if(length == -1)
        return true;

      LOG_BEGIN(Logger::INFO)
        LOG_PROP("Host", m_host)
        LOG_PROP("Port", m_port)
        LOG_PROP("Socket", m_socket.getSocket())
        LOG_PROP("Bytes", length)
        LOG_END("Received message from peer.");
    
      m_message->buildMessage( (void*)m_buffer, (size_t)length);
    }
}


int TcpConnection::getSocket() const
{
  //  TRACE;
  return m_socket.getSocket();
}


void TcpConnection::setState(const State state)
{
  TRACE;
  m_state = state;
}


bool TcpConnection::closed() const
{
  TRACE;
  return m_state == CLOSED;
}


Message* TcpConnection::getMessage() const
{
  TRACE;
  return m_message;
}


size_t TcpConnection::getBufferLength() const
{
  TRACE;
  return m_bufferLength;
}


TcpConnection::TcpConnection (  const int      socket,
                                Message       *message,
                                const size_t   bufferLength )
  : StreamConnection("invalid", "invalid")
  , m_socket(socket)
  , m_message(message)
  , m_buffer(0)
  , m_bufferLength(bufferLength)
  , m_state(OPEN)  /// @todo can clone only open ones?
{
  TRACE;

  std::string host, port;
  m_socket.getPeerName(host, port);
  setHost(host);
  setPort(port);

  m_buffer = new unsigned char[m_bufferLength];
  m_message->setConnection(this);
}
