#ifndef BLACKPHONENO_H
#define BLACKPHONENO_H

#include "RuleEngine.hpp"
#include "BlackPhoneManager.hpp"
#include "Logger.hpp"


class BlackPhoneRule: public Rule, public Singleton<BlackPhoneRule>
{
  
public:
  BlackPhoneRule()
  {
    Rule::mRuleName = pingan::BLACK_PHONE_RULE;
  }
  
  void process(const SecurityRequest& req, SecurityResponse& resp)
  {
    TRACE;
    uint16_t result = 0;
    BlackPhoneManager* manager = BlackPhoneManager::getInstance();
    bool ret1 = manager->find(req.mPhoneNo);
    bool ret2 = manager->find(req.mCellNo);
    bool ret3 = manager->find(req.mServCelloNo);
    if(ret1 || ret2 || ret3)
      resp.updateScore(mRuleName, result);
  }
  
  virtual ~BlackPhoneRule()
  {

  }
};


#endif /* BLACKPHONENO_H */






