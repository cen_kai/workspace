/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */



#ifndef FORBID_UPDATE_TASK_HPP
#define FORBID_UPDATE_TASK_HPP

#include "PingAnTask.hpp"
class ForbidUpdateTask : public PingAnTask
{
public:
  ForbidUpdateTask();
  virtual ~ForbidUpdateTask();
};

#endif
