/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef RWLOCK_HPP
#define RWLOCK_HPP

#include <pthread.h>
#include <stdlib.h>

typedef pthread_rwlock_t RWMutex;
class RWLock {
public:

  RWLock();
  ~RWLock();
  void Lock();
  void Unlock();
  void ReaderLock();
  void ReaderUnlock();
  void WriterLock() { Lock(); }
  void WriterUnlock() { Unlock(); }
  void AssertHeld() {}

private:
  RWMutex mutex_;
  volatile bool is_safe_;
  void SetIsSafe() { is_safe_ = true; }
  RWLock(RWLock* ) {}
  RWLock(const RWLock&);
  void operator=(const RWLock&);
};

class MutexLock {
public:
  explicit MutexLock(RWLock *mu) : mu_(mu) { mu_->Lock(); }
  ~MutexLock() { mu_->Unlock(); }
private:
  RWLock * const mu_;

  MutexLock(const MutexLock&);
  void operator=(const MutexLock&);
};

class ReaderMutexLock {
public:
  explicit ReaderMutexLock(RWLock *mu) : mu_(mu) { mu_->ReaderLock(); }
  ~ReaderMutexLock() { mu_->ReaderUnlock(); }
private:
  RWLock * const mu_;

  ReaderMutexLock(const ReaderMutexLock&);
  void operator=(const ReaderMutexLock&);
};

class WriterMutexLock {
public:
  explicit WriterMutexLock(RWLock *mu) : mu_(mu) { mu_->WriterLock(); }
  ~WriterMutexLock() { mu_->WriterUnlock(); }
private:
  RWLock * const mu_;

  WriterMutexLock(const WriterMutexLock&);
  void operator=(const WriterMutexLock&);
};

#endif
