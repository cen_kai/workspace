#ifndef ZKAGENT_H
#define ZKAGENT_H

#include "zookeeper.hpp"
#include "zookeeper_error.hpp"
#include "Singleton.hpp"

using zookeeper::ZooWatcher;
using zookeeper::ZooKeeper;
using zookeeper::ZooException;

enum ZkSNodeValue
  {
    SNODE_LOW = 0,
    SNODE_NORMAL = 1,
    SNODE_HIGH = 2,
  };

class ZkAgent: public ZooWatcher, public Singleton<ZkAgent>
{
  
public:
  ZkAgent();
  virtual ~ZkAgent();
  
  virtual void OnConnected() ;
  virtual void OnConnecting() ;
  virtual void OnSessionExpired() ;

  virtual void OnCreated(const char* path) ;
  virtual void OnDeleted(const char* path) ;
  virtual void OnChanged(const char* path) ;
  virtual void OnChildChanged(const char* path);
  virtual void OnNotWatching(const char* path);
  void setSnodeValue(ZkSNodeValue value);
private:
  ZooKeeper *mZookeeper;
  std::string mPath;
};

#endif /* ZKAGENT_H */
