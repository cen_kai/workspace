/**
 * @file   SocketServer.cpp
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Oct 28 13:39:07 2015
 * 
 * @brief  
 * 
 * 
 */

#include "SocketServer.hpp"
#include "Logger.hpp"
#include "ThreadPool.hpp"
#include "WorkerThread.hpp"
#include "MysqlConnectionPool.hpp"
#include "DeviceManager.hpp"
#include "RuleEngine.hpp"
#include "CryptRSA.hpp"
#include "BlackPhoneManager.hpp"

SocketServer::SocketServer ()
  :bStart(false)
  ,mListenConntion(NULL)
  ,mTimerManager(NULL)
  ,mConnectionManger(NULL)
  ,mMessageManager(NULL)
  ,mMaxPendingQueueLen(100)
{
  TRACE;
  TimerManager::createInstance();
  ConnectManager::createInstance();
  MessageManager::createInstance();
  ThreadPool::createInstance();
  MysqlConnectionPool::createInstance();
  MysqlClient::init();
  RuleEngine::createInstance();
  BlackPhoneManager::createInstance();
}

SocketServer::~SocketServer()
{
  TRACE;

  mTimerManager->destroy();
  mConnectionManger->destroy();
  mMessageManager->destroy();
  //      ThreadPool::delInstance();
  ThreadPool::destroy();
  MysqlConnectionPool::destroy();
  BlackPhoneManager::destroy();
  RuleEngine::destroy();
  
  //  DicManager::destroy();
  delete mListenConntion;

}

bool SocketServer::start()
{
  TRACE;
  
  if(bStart)
    {
      LOG_BEGIN(Logger::INFO) LOG_END("sentive server start!");
      return true;
    }
  ConfigFile *config = ConfigFile::getInstance();

  if( NULL == mListenConntion )
    {
      string host = config->getString("localhost", "null");
      string port = config->getString("localport", "10010");
      if(host == "null" || port == "null")
        {
          LOG_BEGIN(Logger::ERR) LOG_END("please set serv host and port");
          return false;
        }

      PingAnMessage *msgParam = new PingAnMessage();
      mListenConntion = new TcpConnection(host, port , msgParam, 200);
    }

  int reuse = 1;
  int rr = setsockopt( mListenConntion->getSocket(), SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof( reuse ));
  if(rr != 0)
    {
      perror("setsock");
    }
  
  if( !mListenConntion->bind() )
    {
      LOG_BEGIN(Logger::ERR) LOG_END("bind errro");
      return false;
    }
  

  if(NULL == mTimerManager)
      mTimerManager = TimerManager::getInstance();
  
  mTimerManager->init();
  mTimerManager->start();

  if(NULL == mConnectionManger)
      mConnectionManger = ConnectManager::getInstance();
  
  mConnectionManger->init(mListenConntion);
  mConnectionManger->start();

  if(NULL == mMessageManager)
      mMessageManager = MessageManager::getInstance();

  ThreadPool *threadPool = ThreadPool::getInstance();  
  int threads = config->getInt("threadpool", 4);
  for ( int i = 0; i < threads; ++i )
    threadPool->pushWorkerThread(new WorkerThread(*threadPool));

  threadPool->startWorkerThreads();
  // PingAnTask *task = new PingAnTask();
  // threadPool->pushTask(task);
  
  MysqlConnectionPool *mysqlConnectionPool =  MysqlConnectionPool::getInstance();
  mysqlConnectionPool->init();
  mysqlConnectionPool->start();

  BlackPhoneManager *bpm = BlackPhoneManager::getInstance();
  bpm->start();
  
  RuleEngine *engine = RuleEngine::getInstance();
  engine->start();
  
  string filename = config->getString("privatekey", "private.key");
  
  if ( !mListenConntion->listen( mMaxPendingQueueLen ))
    {
      LOG_BEGIN(Logger::ERR) LOG_END("listen errro");
      return false;
    }

  CryptRSA::createInstance(filename);
  GNUMemoryPool::createInstance(200);

  bStart = true;
  return true;
}

void SocketServer::stop()
{
  TRACE;
  if(!bStart)
    return;
  //  mDicManager->stop();
  ThreadPool *threadPool = ThreadPool::getInstance();  
  threadPool->stop();
  MysqlConnectionPool *mysqlConnectionPool =  MysqlConnectionPool::getInstance();
  mysqlConnectionPool->stop();
  
  mTimerManager->stop();
  mConnectionManger->stop();
  mListenConntion->disconnect();
  
  RuleEngine *engine = RuleEngine::getInstance();
  engine->stop();

  BlackPhoneManager *bpm = BlackPhoneManager::getInstance();
  bpm->stop();

  bStart = false;
}
