/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include <set>
#include "StreamConnection.hpp"

using std::set;

class Session{
private:
  StreamConnection *m_conn;
  int start_time; //s
  int keepalive; //s
  int update_time; //s
  set<int> m_timers;
};
