#ifndef BLACKPHONENOMANAGER_H
#define BLACKPHONENOMANAGER_H

#include "Singleton.hpp"
#include "RWLock.hpp"
#include <stdint.h>
#include <string>
#include <vector>
#include <set>

using std::string;
using std::vector;
using std::set;

class BlackPhoneManager: public Singleton<BlackPhoneManager>
{
public:
  BlackPhoneManager();
  virtual ~BlackPhoneManager();
  bool start();
  bool stop();
  void update(vector<string>& add, vector<string>& del);
  bool find(const string& phoneNo);
private:
  set<int64_t> mPhones;
  bool mStart;
  RWLock mLock;
};


#endif /* BLACKPHONENOMANAGER_H */
