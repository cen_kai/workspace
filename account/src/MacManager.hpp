#ifndef MACMANAGER_H
#define MACMANAGER_H

#include "Singleton.hpp"
#include "RWLock.hpp"
#include <vector>
#include <stdint.h>
#include <string>
#include <set>

using std::vector;
using std::string;
using std::set;

typedef set<uint32_t> MacSet;
typedef MacSet::iterator MacSetIt;

class MacManager: public Singleton<MacManager>
{
public:
  MacManager();
  bool start();
  bool stop();
  bool update(MacSet& updateMacs, MacSet& deleteMacs);
  bool findMac(string& mac);
  virtual ~MacManager();
  
private:
  MacSet mMacOrgans;//
  RWLock mLock;
};


#endif /* MACMANAGER_H */
