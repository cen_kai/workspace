#ifndef DEVICE_INFO_HPP
#define DEVICE_INFO_HPP

#include "PingAn.hpp"
#include <stdint.h>
#include <string>
#include <set>

using std::string;
using std::set;
using namespace pingan;

class DeviceInfo
{
public:
  uint32_t mID;
  uint8_t mPlatform;
  string mDeviceID;
  uint64_t mPhoneNumber;
  set<string> mLocations;//常驻地
  
  DeviceInfo(uint8_t platform = ANDROID_PLATFORM);
  DeviceInfo(const DeviceInfo& dst);
  DeviceInfo(DeviceInfo& dst);

  virtual ~DeviceInfo();
};

class AndroidDeviceInfo: public DeviceInfo
{
public:
  AndroidDeviceInfo();
  ~AndroidDeviceInfo();
  AndroidDeviceInfo(const AndroidDeviceInfo& dst);
  AndroidDeviceInfo(AndroidDeviceInfo& dst);
public:

  uint64_t mWifiMac;
  uint64_t mBlueMac;
  uint64_t mIMEI;
  uint64_t mIMSI;
  uint64_t mSimSerial;
};

class IOSDeviceInfo: public DeviceInfo
{
public:
  IOSDeviceInfo();
  ~IOSDeviceInfo();
  IOSDeviceInfo(const IOSDeviceInfo& dst);
};

#endif

