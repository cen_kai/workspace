/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include "EventLoop.hpp"
#include "Logger.hpp"
#include <errno.h>
#include <stdio.h>

static void EV_TV_SET(timeval& tv,ev_tstamp& t){
 tv.tv_sec = (long)t;
 tv.tv_usec = (long)((t - tv.tv_sec) * 1e6); 
}

static void ev_sleep (ev_tstamp delay)
{
  if (delay > 0.)
    {
      struct timeval tv;

      /* here we rely on sys/time.h + sys/types.h + unistd.h providing select */
      /* something not guaranteed by newer posix versions, but guaranteed */
      /* by older ones */
      EV_TV_SET (tv, delay);
      select (0, 0, 0, 0, &tv);
    }
}

static void * ev_realloc_emul (void *ptr, long size) 
{

  if (size)
    return realloc (ptr, size);

  free (ptr);
  return 0;
}

static void *ev_malloc(long size){
  ev_realloc_emul(0, size);
}

static ev_tstamp ev_time (void)
{
  struct timespec ts;
  clock_gettime (CLOCK_REALTIME, &ts);
  return ts.tv_sec + ts.tv_nsec * 1e-9;
}

static ev_tstamp get_clock (void)
{
  struct timespec ts;
  clock_gettime (CLOCK_MONOTONIC, &ts);
  return ts.tv_sec + ts.tv_nsec * 1e-9;
}


EventLoop::EventLoop()
  :mTimerEvents()
  ,mPeriodicEvents()
  ,mPending()
  ,mIOEvents()
  ,mFds()
  ,mFlags(0)
  ,mRtnow(0)
  ,mMonow(0)
  ,mIoBlocktime(0)
  ,mTimeoutBlocktime(0)
  ,mCurpid(0)
  ,poll(0, this)
  ,mLoopDone(0)
  ,ev_rt_now(0)
  ,now_floor(0)
  ,mn_now(0)
  ,rtmn_diff(0)
  ,pipe_write_wanted(0)
  ,pipe_write_skipped(0)
  ,timeout_blocktime(0)
  ,io_blocktime(0)
  ,backend_mintime(0)
  ,mIdleall(0)
  ,mIOPool()
  ,mTimerPool()
  ,mPeriodPool()
{
  TRACE;
}

EventLoop::~EventLoop()
{
  TRACE;
}

void EventLoop::addComm(Event * w){
  TRACE;
  w->mActive = 1;
}


void EventLoop::delComm(Event * w){
  TRACE;
  w->mActive = 0;
}
  
bool EventLoop::addEvent(TimerEvent * w){
  TRACE;
  w->mAt = mn_now + w->mAt;
  mTimerEvents.push_back(w);
  w->mActive = 1;
  return true;
}
  
bool EventLoop::delEvent(TimerEvent * w){
  TRACE;
  w->mAt = w->mAt - mn_now  ;
  mTimerEvents.remove(w);
  w->mActive = 0;
  return true;
}

static int  set_unblock (int sfd)
{

  int flags, s;

  flags = fcntl (sfd, F_GETFL, 0);
  if (flags == -1)
    {
      perror ("fcntl");
      return -1;
    }

  flags |= O_NONBLOCK;
  s = fcntl (sfd, F_SETFL, flags);
  if (s == -1)
    {
      perror ("fcntl");
      return -1;
    }

  return 0;
}

bool EventLoop::addEvent(IOEvent *w){
  TRACE;
  map<int , FileDescribe*>::iterator itfd;
  map<int , list<IOEvent*>*>::iterator itio;

  set_unblock(w->mFd);
  
  if((itfd = mFds.find(w->mFd)) == mFds.end())
    {
      FileDescribe * fds = new FileDescribe();
      mFds[w->mFd] = fds;
      list<IOEvent *> *l = new list<IOEvent *>();
      mIOEvents[w->mFd] = l;
    }
  
  itio = mIOEvents.find(w->mFd);
  list<IOEvent *> *l = itio->second;
  l->push_back(w);

  w->mActive = 1;
  mActivecnt++;
  return true;
}
  
bool EventLoop::delEvent(IOEvent *w){
  TRACE;
  map<int , FileDescribe*>::iterator itfd;
  map<int , list<IOEvent*>*>::iterator itio;
  w->mActive = 0;
  itio = mIOEvents.find(w->mFd);
  if(itio != mIOEvents.end())
    {
      list<IOEvent*>* l = itio->second;
      // TODO: return event消息的内存池和unin一起使用
      l->remove(w);
      if(l->empty())
        {
          itfd = mFds.find(w->mFd);
          if(itfd != mFds.end())
            {
              delete itfd->second;
              mFds.erase(itfd);
            }
        }
      delete l;// list的内存池
    }
  mActivecnt--;
  return true;
}

bool EventLoop::delEvent(int fd){
  
  TRACE;
  map<int , FileDescribe*>::iterator itfd;
  map<int , list<IOEvent*>*>::iterator itio;
  // TODO: 小心内存放开
  list<IOEvent*>::iterator curIt;
  itio = mIOEvents.find(fd);
  if(itio != mIOEvents.end())
    {
      list<IOEvent*>* l = itio->second;
      curIt = l->begin();
      // TODO: return event消息的内存池和unin一起使用
      itfd = mFds.find(fd);
      if(itfd != mFds.end())
        {
          delete itfd->second;
          mFds.erase(itfd);
        }
      
      for(; curIt != l->end();)
        {
          delete (*curIt++);
          mActivecnt--;
        }
      delete l;// list的内存池
      mIOEvents.erase(itio);
    }

  return true;
}


bool EventLoop::addEvent(PeriodicEvent *w){
// TODO: 
}
bool EventLoop::delEvent(PeriodicEvent *w){
// TODO: 
}

enum {
  EVBREAK_CANCEL = 0, /* undo unloop */
  EVBREAK_ONE    = 1, /* unloop once */
  EVBREAK_ALL    = 2  /* unloop all loops */
};

void EventLoop::invokePending()
{
// TODO: 回调所有的watcher
  TRACE;
  list<Event*>::iterator it = mPending.begin();

  for(; it!= mPending.end(); )
    {
      switch((*it)->mType)
        {
        case IO_EVENT:
          {
            IOEvent* evio = (IOEvent*)*it;
            evio->mWatcher->doIO(evio, IO_EVENT);
            ++it;
            break;
          }
        case SIGNAL_EVENT:
          {
            SignalEvent* evs = (SignalEvent*)*it;
            evs->mWatcher->doSignal(evs);
            ++it;
            break;
          }
        case TIMER_EVENT:
          {
            TimerEvent *evt = (TimerEvent*)*it;
            evt->mWatcher->doTimer(evt);
            ++it;
            break;
          }
        default:
          ++it;
          break;
        }
    }
  mPending.clear();
  return;
}

void EventLoop::reify ()
{
  TRACE;
  unsigned i;
  std::map<int, FileDescribe*>::iterator itFds = mFds.begin();
  std::map<int, list<IOEvent*> *>::iterator itlist = mIOEvents.begin();
  for (; itFds != mFds.end() ; ++itFds, ++itlist)
    {
      int fd = itFds->first;
      FileDescribe * anfd = itFds->second;
      // LOG_BEGIN(Logger::INFO)
      //   LOG_PROP("reify fd", fd)
      //   LOG_END("")
      IOEvent *w;
      list<IOEvent*> *events = itlist->second;
      list<IOEvent*>::iterator itEvents= events->begin(); //取出ioevents

      unsigned char o_events = anfd->events;
      unsigned char o_reify  = anfd->reify;

      anfd->reify  = 0;
      anfd->events = 0;

      for(;itEvents != events->end();++itEvents)
        {
          IOEvent *io = *itEvents;
          anfd->events |= io->mEvents;
        }

      if (o_events != anfd->events)
        o_reify = EV__IOFDSET; /* actually |= */

      if (o_reify & EV__IOFDSET){
        // TODO: add log , modify return value
        poll.modify(fd, o_events, anfd->events);
      }
    }
}

void EventLoop::time_update (ev_tstamp max_block)
{
  TRACE;
  int i;
  ev_tstamp odiff = rtmn_diff;

  mn_now = get_clock ();
  if (( mn_now -  now_floor) < 1.*.5)
    {
      ev_rt_now =  rtmn_diff +  mn_now;
      return;
    }

  now_floor =  mn_now;
  ev_rt_now = ev_time();
  for (i = 4; --i; )
    {
      ev_tstamp diff;
      rtmn_diff =  ev_rt_now -  mn_now;

      diff = odiff -  rtmn_diff;

      if ((diff < 0. ? -diff : diff) < 1.)
        return;

      ev_rt_now = ev_time ();
      mn_now = get_clock ();
      now_floor = mn_now;
    }
  periodics_reschedule ();
}

// TODO: 使用范型
ev_tstamp EventLoop::getMinTimerAt (){
  ev_tstamp to  = 60.;
  TRACE;
  if(mTimerEvents.size() > 0){
    
    list<TimerEvent*>::iterator it = mTimerEvents.begin();
    
    for(; it!= mTimerEvents.end(); ++it)
      {
        ev_tstamp tmp = (*it)->mAt - mn_now;
        if(tmp < to) to = tmp;
      }
  }
  return to;
}

ev_tstamp EventLoop:: getMinPeriodAt (){
  ev_tstamp to  = 60.;
  TRACE;
  if(mTimerEvents.size() > 0){
    
    list<PeriodicEvent*>::iterator it = mPeriodicEvents.begin();
    
    for(; it!= mPeriodicEvents.end(); ++it)
      {
        ev_tstamp tmp = (*it)->mAt - ev_rt_now;
        if(tmp < to) to = tmp;
      }
  }
  return to;
}


void EventLoop::timers_reify(){
  // TODO:
  TRACE;
  if(mTimerEvents.size() < 1)
    return;
  
  list<TimerEvent*>::iterator it = mTimerEvents.begin();
  TimerEvent* ev = NULL;
  for (; it != mTimerEvents.end(); )
    {
      ev  = *it;
      if(ev->mAt < mn_now) //时间到
        {
          if(ev->mActive)  //定时到
            mPending.push_back(ev);        

          if(ev->mRepeat) //重复定时器
            {
              ev->mAt +=  ev->mRepeat;
              if(ev->mAt < mn_now) //下一次在等待前就被处理掉
                ev->mAt = mn_now;
              ++it;
            }
          else
            {
              // remove to pending list
              mTimerEvents.erase(it++);
              if(ev->mActive)
                {
                  ev->mPending = 1;
                  ev->mAt = mn_now;
                  ev->mActive = 0;
                }
              delete ev;
            }
        }
      else
        ++it;
    }
}

void EventLoop::periodics_reify(){
  // TODO: 这个周期性的定时器，照着每天时间来的。放后面实现
  TRACE;
}


void EventLoop::periodics_reschedule ()
{
  TRACE;
  // TODO:  暂不实现
}

#define MAX_BLOCKTIME 59.743 /* never wait longer than this time (to detect time jumps) */

enum {
  EVRUN_NOWAIT = 1,
  EVRUN_ONCE = 2
};

void EventLoop::doEvent(int fd, int events)
{
  TRACE;
  map<int , list<IOEvent*> *> ::iterator itlist;
  list<IOEvent*>::iterator itevts ;
  if(fd == -2)
    {
      return ;
    }
  if(((itlist = mIOEvents.find(fd)) == mIOEvents.end())){
    // TODO:
    //printf("error");
    LOG_BEGIN(Logger::INFO)
      LOG_PROP("fd", fd)
      LOG_PROP("events", events)
      LOG_END("can't find fd");
    return ;
  }
  // TODO: 后面使用timerfd实现
  list<IOEvent*> *ioevents = itlist->second;
  for(itevts = ioevents->begin(); itevts != ioevents->end(); ++itevts)
    {
      IOEvent * evt = * itevts;
      
      if(evt->mEvents & events)
        mPending.push_back(evt);
    }

  return;
}

int EventLoop::run()
{
  TRACE;
  mLoopDone = EVBREAK_CANCEL;
  do
    {
      if(mCurpid == 0)
        {
          mCurpid = getpid ();
        }
      
      invokePending();

      if (mLoopDone)
        break;

      /* update fd-related kernel structures */
      reify ();

      /* calculate blocking time */
      {
        ev_tstamp waittime  = 0.;
        ev_tstamp sleeptime = 0.;

        /* remember old timestamp for io_blocktime calculation */
        ev_tstamp prev_mn_now = mn_now;

        /* update time to cancel out callback processing overhead */
        time_update (1e100); // TODO: 
        // TODO:         time_update (1e100);
        /* from now on, we want a pipe-wake-up */
        pipe_write_wanted = 1;

        //ECB_MEMORY_FENCE; /* make sure pipe_write_wanted is visible before we check for potential skips */

        //        if (!(mFlags & EVRUN_NOWAIT || mIdleall || !mActivecnt || !pipe_write_skipped))
        if (!(mFlags & EVRUN_NOWAIT || mIdleall || !mActivecnt ))
          {
            waittime = MAX_BLOCKTIME;

            ev_tstamp to = getMinTimerAt();
            if (waittime > to) waittime = to;

            to = getMinPeriodAt();
            if (waittime > to) waittime = to;

            /* don't let timeouts decrease the waittime below timeout_blocktime */
            if (waittime < timeout_blocktime)
              waittime = timeout_blocktime;

            /* at this point, we NEED to wait, so we have to ensure */
            /* to pass a minimum nonzero value to the backend */
            if (waittime < backend_mintime)
              waittime = backend_mintime;

            /* extra check because io_blocktime is commonly 0 */
            if (io_blocktime)
              {
                sleeptime = io_blocktime - (mn_now - prev_mn_now);

                if (sleeptime > waittime - backend_mintime)
                  sleeptime = waittime - backend_mintime;

                if (sleeptime > 0.)
                  {
                    ev_sleep (sleeptime);
                    waittime -= sleeptime;
                  }
              }
          }
        poll.poll(waittime);

        pipe_write_wanted = 0; /* just an optimisation, no fence needed */

        //ECB_MEMORY_FENCE_ACQUIRE;
        if (pipe_write_skipped)
          {
            // TODO: log  pipe watcher
            //            ev_feed_event ( &pipe_w, EV_CUSTOM);
          }

        /* update ev_rt_now, do magic */
        time_update (waittime + sleeptime);
      }

      /* queue pending timers and reschedule them */
      timers_reify (); /* relative timers called last */

      periodics_reify (); /* absolute timers called first */
      bool tag = mActivecnt && !mLoopDone  && !(mFlags & (EVRUN_ONCE | EVRUN_NOWAIT));
      // LOG_BEGIN(Logger::INFO)
      //   LOG_PROP("mActivecnt", mActivecnt)
      //   LOG_PROP("loopDone", mLoopDone)
      //   LOG_PROP("mFlags", mLoopDone)
      //   LOG_PROP("EVRUN_ONCE", EVRUN_ONCE) 
      //   LOG_PROP("EVRUN_NOWAIT", EVRUN_NOWAIT)
      //   LOG_PROP("tag", tag) 
      // LOG_END("");
      
    }while (mActivecnt && !mLoopDone  && !(mFlags & (EVRUN_ONCE | EVRUN_NOWAIT)));

  if (mLoopDone == EVBREAK_ONE)
    mLoopDone = EVBREAK_CANCEL;

  clearAll();
  return mActivecnt;
  
}
void EventLoop::clearAll()
{
  //  map<int , FileDescribe*>::iterator itfd;
  IOListMapIt itio ;
  FileDescMapIt itfd = mFds.begin();
  for(; itfd != mFds.end();)
    {
      delete itfd->second;
      mFds.erase(itfd++);
    }

  TimerEventListIt timerEventIt = mTimerEvents.begin();
  for(; timerEventIt != mTimerEvents.end(); )
    {
      delete *timerEventIt;
      mTimerEvents.erase(timerEventIt++);
    }

  PeriodicEventsListIt perodicIt = mPeriodicEvents.begin();
  for(;perodicIt != mPeriodicEvents.end();)
    {
      delete *perodicIt;
      mPeriodicEvents.erase(perodicIt++);
    }

  mPending.clear();
  
  IOEventsMapIt iOEventsMapIt = mIOEvents.begin();
  IOEventListIt iOEventListIt;
  for(; iOEventsMapIt != mIOEvents.end() ; ++iOEventsMapIt)
    {
      IOEventList *l = iOEventsMapIt->second;
      for(iOEventListIt = l->begin();iOEventListIt != l->end(); ++iOEventListIt)
          delete *iOEventListIt;
      l->clear();
      delete l;
    }

  mIOEvents.clear();

  IOEvent *ioEvent = mIOPool.tryAcquire();
  while(NULL != ioEvent)
    delete ioEvent;

  TimerEvent* timerEvent = mTimerPool.tryAcquire();
  while(NULL != timerEvent)
    delete timerEvent;

 
  PeriodicEvent* periodicEvent =  mPeriodPool.tryAcquire();
  while(NULL != periodicEvent)
    delete periodicEvent;
  
}

IOEvent* EventLoop::obtainIOEvent()
{
  TRACE;
  IOEvent* e = mIOPool.tryAcquire();
  if(NULL == e)
    e = new IOEvent();
  return e;
}

TimerEvent* EventLoop::obtainTimerEvent()
{
  TRACE;
  TimerEvent* e = mTimerPool.tryAcquire();
  if(NULL == e)
    e = new TimerEvent();
  return e;

}

PeriodicEvent* EventLoop::obtainPeriodicEvent()
{
  TRACE;
  PeriodicEvent *e = mPeriodPool.tryAcquire();
  if(NULL == e)
    e = new PeriodicEvent();
  return e;
}

void EventLoop::init(u_int32_t flags)
{
  TRACE;
  mFlags = flags;
}
void EventLoop::quit()
{
  TRACE;
  mLoopDone = EVBREAK_ALL;
}
