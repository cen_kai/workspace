/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef EPOLL_HPP

#define EPOLL_HPP

#include <sys/epoll.h>

#include "Event.hpp"

class EventHandler{
public:
  // 如果fd是-2 则表示时间到了，没有相应的事件  
  virtual void doEvent(int fd, int events) = 0;
};

class EPoll
{
public:
  EPoll(int flags, EventHandler *handler);

  virtual ~EPoll();
  bool poll (ev_tstamp timeout);
  void modify ( int fd,  int old, int nev);
  
private:
  int mBackendFd;
  ev_tstamp mBackendMintime;
  int mEventmax;
  struct epoll_event * mEpollEvents;
  bool mInit ;
  EventHandler *mHandler;
};

#endif
