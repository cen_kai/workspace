/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef ADDR_INFO_HPP
#define ADDR_INFO_HPP

#include <netdb.h>
#include <string>


class AddrInfo
{
public:

  AddrInfo();
  virtual ~AddrInfo();

  bool getHostInfo(const std::string& host, const std::string& port);
  addrinfo* operator[](const unsigned int pos);
  void printHostDetails() const;

  static bool convertNameInfo(const addrinfo* addrInfo,
                              std::string &retAddr,
                              std::string &retService);

private:

  AddrInfo(const AddrInfo&);
  AddrInfo& operator=(const AddrInfo&);

  struct addrinfo *m_addrInfo;
};

#endif // ADDR_INFO_HPP
