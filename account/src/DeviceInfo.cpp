#include "DeviceInfo.hpp"

DeviceInfo::DeviceInfo(uint8_t platform)
  :mID(0)
  ,mPlatform(platform)
  ,mDeviceID()
  ,mPhoneNumber(0)
  ,mLocations()
{
  
}

DeviceInfo::DeviceInfo(const DeviceInfo& dst)
{
  mDeviceID = dst.mDeviceID;
  mID = dst.mID ;
  mLocations = dst.mLocations ;
  mPhoneNumber = dst.mPhoneNumber; 
  mPlatform = dst.mPlatform;
}

DeviceInfo::DeviceInfo(DeviceInfo& dst)
{
  mDeviceID = dst.mDeviceID;
  mID = dst.mID ;
  mLocations = dst.mLocations ;
  mPhoneNumber = dst.mPhoneNumber; 
  mPlatform = dst.mPlatform;
}

DeviceInfo::~DeviceInfo()
{
  mDeviceID.clear();
  mLocations.clear();
}


IOSDeviceInfo::IOSDeviceInfo()
  :DeviceInfo(IOS_PLATFORM)
{
  
}

IOSDeviceInfo::~IOSDeviceInfo()
{
}

AndroidDeviceInfo::AndroidDeviceInfo()
  :DeviceInfo(ANDROID_PLATFORM)
  ,mWifiMac(0)
  ,mBlueMac(0)
  ,mIMEI(0)
  ,mIMSI(0)
  ,mSimSerial(0)
{
  
}

AndroidDeviceInfo::AndroidDeviceInfo(const AndroidDeviceInfo& dst)
  :DeviceInfo(dst)    
{
  mWifiMac = dst.mWifiMac;
  mBlueMac = dst.mBlueMac;
  mIMEI = dst.mIMEI;
  mIMSI = dst.mIMSI;
  mSimSerial = dst.mSimSerial;
}

AndroidDeviceInfo::AndroidDeviceInfo(AndroidDeviceInfo& dst)
  :DeviceInfo(dst)    
{
  mWifiMac = dst.mWifiMac;
  mBlueMac = dst.mBlueMac;
  mIMEI = dst.mIMEI;
  mIMSI = dst.mIMSI;
  mSimSerial = dst.mSimSerial;
}

AndroidDeviceInfo::~AndroidDeviceInfo()
{
  
}
