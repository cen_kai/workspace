/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef PINGAN_TASK_HPP
#define PINGAN_TASK_HPP

/**
 * @file   PingAnTask.hpp
 * @author one <qianzhongshan354@pingan.com.cn>
 * @date   Mon Oct 12 15:11:25 2015
 * 
 * @brief  
 * 具体业务：包括内部的和外部的
 * 
 */

#include "Task.hpp"
#include "PingAn.hpp"
#include "Logger.hpp"
#include <string>


using std::string;
using namespace pingan;

static const int32_t TASK_BLACK_PHONE = 1;
static const int32_t TASK_BLACK_DEVICE = 2;

class PingAnTask: public Task
{
public:
  PingAnTask(){mType = 0;};
  virtual void run(){ TRACE; LOG_BEGIN(Logger::INFO) LOG_END("=============");};
  virtual ~PingAnTask(){};
  
public :
  int32_t mType;
};

class BlackPhoneTask: public PingAnTask
{
public:
  BlackPhoneTask(time_t t):mLastTime(t){mType = TASK_BLACK_PHONE;}
  virtual ~BlackPhoneTask();
  void run();
  time_t& mLastTime;
};

class BlackDeviceTask: public PingAnTask
{
public:
  BlackDeviceTask(time_t t):mLastTime(t){mType = TASK_BLACK_DEVICE;}
  virtual ~BlackDeviceTask();
  void run();
  time_t& mLastTime;
};

#endif
