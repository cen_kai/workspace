/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef PINGAN_EVENT_HPP
#define PINGAN_EVENT_HPP

#include <stdlib.h>
#include <vector>

using std::vector;
class Event;
typedef double ev_tstamp;
typedef vector<Event*> EventsArray;
typedef vector<Event*>::iterator EventIt;

enum {
  EV_UNDEF = (int)0xFFFFFFFF,
  EV_NONE = 0x00,
  EV_READ = 0x01,
  EV_WRITE = 0x02,
  EV__IOFDSET = 0x80,
  EV_IO = EV_READ,
  EV_TIMER = 0x00000100,

  EV_TIMEOUT = EV_TIMER,

  EV_PERIODIC = 0x00000200,
  EV_SIGNAL = 0x00000400,
  EV_CHILD = 0x00000800,
  EV_STAT = 0x00001000,
  EV_IDLE = 0x00002000,
  EV_PREPARE = 0x00004000,
  EV_CHECK = 0x00008000,
  EV_EMBED = 0x00010000,
  EV_FORK = 0x00020000,
  EV_CLEANUP = 0x00040000,
  EV_ASYNC = 0x00080000,
  EV_CUSTOM = 0x01000000,
  EV_ERROR = (int)0x80000000
};

enum{
  IO_EVENT = (int)0,
  TIMER_EVENT =1,
  SIGNAL_EVENT = 2,
  PERIODIC_EVENT
};

class Event
{
public:
  Event(int type)   :mType(type),mActive(0),mPending(0) ,mPriority(0) ,mData(0) {}
  virtual ~Event(){};

public:
  int mType;
  int mActive;
  int mPending;
  int mPriority;
  void *mData;
};

class IOEvent;
class IOWatcher
{
public:
  virtual bool doIO(IOEvent *evt, int events) =0 ;
};

class IOEvent: public Event
{
public:
  IOEvent()  :Event(IO_EVENT), mFd(-1) ,mEvents(0) ,mWatcher(NULL) {}
  ~IOEvent(){}
  
public:
  int mFd;
  int mEvents;
  IOWatcher * mWatcher;
};

class TimerEvent;
class TimerWatcher
{
public:  
  virtual bool doTimer(TimerEvent* evt) = 0 ;
};

class TimerEvent : public Event
{
public:
  TimerEvent():Event(TIMER_EVENT),mAt(0),mRepeat(0), mWatcher(NULL) {}
  virtual ~TimerEvent(){}
  
public:
  ev_tstamp mAt;
  ev_tstamp mRepeat;
  TimerWatcher *mWatcher;
};

class PeriodicWatcher
{
public:  
  virtual bool doPeriodic() {};
};

class PeriodicEvent:public Event
{
public:
  PeriodicEvent():Event(PERIODIC_EVENT){}
  
public:
  ev_tstamp mAt;
  ev_tstamp mOffset;
  ev_tstamp mInterval;
  PeriodicWatcher *mWatcher;
};

class SignalEvent;
class SignalWatcher
{
public:
  virtual void doSignal(SignalEvent* evt) = 0;
};


class SignalEvent: public Event
{
public:
  SignalEvent();

  int mSignum;
  
  SignalWatcher *mWatcher;
};


class ChildWatcher
{
public:  
  virtual void doChild(int revents) = 0;
};

class ChildEvent:public Event
{
public:
  ChildEvent();
  virtual ~ChildEvent();
  
public:
  int mFlags;
  int mPid;
  int mRpid;
  int mRstatus;
  ChildWatcher *mWatcher;
};

class StatWatcher
{
public:  
  virtual void doStat(int revents) ;
};

class StatEvent:public Event
{
public:
  StatEvent();
  virtual ~StatEvent();
public:
  //TODO 
  StatWatcher *mWatcher;
};

class IdleWatcher
{
  virtual void doIdle(int revents) ;
};

class IdleEvent:public Event
{
public:
  IdleEvent();

public:
  IdleWatcher *mWatcher;
};

class PrepareWatcher
{
  virtual void doPrepare(int revents) ;
};

class PrepareEvent:public Event
{
public:
  PrepareEvent();
  virtual ~PrepareEvent();
public:
  PrepareWatcher *mWatcher;
};


class CheckWatcher
{
  virtual void doCheck(int revents) ;
};

class CheckEvent:public Event
{
public:
  CheckEvent();

public :
  CheckWatcher *mWatcher;
};

class ForkWatcher
{
  virtual void doFork(int revents) ;
};

class ForkEvent:public Event
{
public:
  ForkEvent();
public:
  ForkWatcher *mWatcher;
};

class CleanupWatcher
{
  virtual void doCleanup(int revents) ;
};

class CleanupEvent:public Event
{
public:
  CleanupEvent();
public:
  CleanupWatcher *mWatcher;
};

typedef int sig_atomic_t;

class AsyncWatcher
{
  virtual void doAsync(int revents) ;
};

class AsyncEvent:public Event
{
public:
  AsyncEvent();
public :
  sig_atomic_t volatile mSent;
  AsyncWatcher *mWatcher;
};

#endif
