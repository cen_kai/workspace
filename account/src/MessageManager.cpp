/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include "MessageManager.hpp"
#include "ConnectManager.hpp"
#include "PingAnTask.hpp"
#include "Json.hpp"
#include "ScopedLock.hpp"
#include "CryptTask.hpp"
#include "UUID.hpp"

#include <arpa/inet.h>

MessageManager::MessageManager()
  :mMessages()
  ,mMutex()
  ,mRequestPool()
{

}

MessageManager::~MessageManager()
{
  TRACE;
  mMessages.clear();

  Request * req = mRequestPool.tryAcquire();
  while(req != NULL)
    {
      delete req;
      req = mRequestPool.tryAcquire();
    }
  mRequestPool.clear();
  
}

static void setProtoHeader(unsigned char *buf, string& data)
{
  ProtoHeader *header = (ProtoHeader*) buf;
  uint16_t checksum = crc16_ccitt((unsigned char*)data.data(), data.length());
  header->checksum = htons(checksum);
  header->prototype = htons(1);
  header->totolen = htonl(data.length());
  header->version = 1;

  // std::cout<<std::oct<<" checksum = "<<header->checksum
  //          <<" prototype = "<<header->prototype
  //          <<" totallen = " <<data.length()
  //          <<" version = " <<header->version
  //          <<std::endl;
}

void MessageManager::getKeepaliveResp(char * buf, int& len)
{
  json::Object obj;
  obj[CMD] = KEEPALIVE;
  obj[TYPE] = RESP;
  obj[STATUS] = STATUS_200;
  string s = json::Serialize(obj);
  setProtoHeader((uint8_t*)buf, s);
  memcpy(buf + sizeof(ProtoHeader), s.data(), s.length());
  len = s.length() + sizeof(ProtoHeader);
}

void MessageManager::addRequest(int& fd, string& req)
{
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("req", req)
    LOG_END("");
  static int i= 100;
  try{
  json::Value reqContent = json::Deserialize(req);
  json::Object reqObj = reqContent.ToObject();
  string reqCmd = reqObj[CMD].ToString();
  
  sole::uuid uid =  sole::uuid0();
  string reqID = uid.str();
  
  if(reqCmd == KEEPALIVE)
    {
      // if((i++ % 2) == 0)
      //   {丢包测试代码
      //     return;
      //     if(i > 1000000)
      //       i = 100;
      //   }
      
      char buf[128] = {0};
      int len = 0;
      getKeepaliveResp(buf, len);
      ConnectManager* cm = ConnectManager::getInstance();
      TcpConnection* con = cm->getConnection(fd);
      LOG_BEGIN(Logger::INFO)
        LOG_END("KeepAlive resp");
      con->send(buf, len); // TODO: check return value ,if error , close connection
       return;
    }
  
  string reqParam ;
  if(reqObj[PARAM].GetType() != json::NULLVal)
    {
      reqParam = reqObj[PARAM].ToString();  
    }
  
  EventLoop *loop = EventLoop::getInstance();
  time_t cur = loop->getCurTime();
  
  Request* request = new Request();
  request->mFd = fd;
  request->mCmd = reqCmd;
  request->mID = reqID;
  request->mTimestamp = cur;
  ThreadPool *pool = ThreadPool::getInstance();

  string servData;
  if(reqObj[S_PARAM].GetType() != json::NULLVal)
    {
       servData = reqObj[S_PARAM].ToString();
      //      servData = json::Serialize(servObject);
    }
  CryptTask* task = new CryptTask(reqID, reqParam);
  task->mServData = servData;
  ScopedLock lock(mMutex);
  pool->pushTask(task);
  mMessages[reqID] = request;
  
  }catch(...)
    {
      LOG_BEGIN(Logger::ERR)
        LOG_PROP("req", req)
        LOG_END("");
    }
  
  return;
}

void MessageManager::delRequest(string& id)
{
  return;
  // TODO:
  ScopedLock lock(mMutex);
  MessageIt it = mMessages.find(id);

  if(it != mMessages.end())
    {
      mMessages.erase(it);
    }
}

void MessageManager::doTimeOutRequest()
{
  EventLoop *loop = EventLoop::getInstance();
  time_t cur = loop->getCurTime();
  ScopedLock lock(mMutex);
  MessageIt it = mMessages.begin();
  for(; it != mMessages.end();++it)
    {
      if(it->second->expire(cur))
        mMessages.erase(it++);
    }
}

__thread char s_sndbuffer[2048] = {0};
void MessageManager::sndResult(string& id, string& result)
{
  memset(s_sndbuffer, 0, 2048);
  char *buffer = s_sndbuffer;

  setProtoHeader((uint8_t *)buffer, result);
  memcpy(buffer + sizeof(ProtoHeader) , result.data(), result.length());
  ScopedLock lock(mMutex);
  MessageIt it = mMessages.find(id);
  if(it == mMessages.end())
    {
      LOG_BEGIN(Logger::ERR) LOG_END("sndResult no message ERROR");
      return;
    }
  
  Request  *req = it->second;
  ConnectManager* cm = ConnectManager::getInstance();
  TcpConnection* con = cm->getConnection(req->mFd);
  if(NULL == con)
    {
      LOG_BEGIN(Logger::ERR)
        LOG_PROP("resp", result)
        LOG_PROP("fd", req->mFd)
        LOG_END("");
    }
  else
    {
      LOG_BEGIN(Logger::INFO)
        LOG_PROP("resp", result)
        LOG_END("");
      con->send(buffer, result.size() + sizeof(ProtoHeader));
    }

  delete req;
  mMessages.erase(id);
}

bool MessageManager::start()
{
  
}

bool MessageManager::stop()
{

}

bool MessageManager::init()
{

}

