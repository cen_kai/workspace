#include "RuleEngine.hpp"
#include "Logger.hpp"
#include "SimulatorRule.hpp"
#include "DevicePrintRule.hpp"
#include "BlackPhoneRule.hpp"
#include "DeviceAccountRule.hpp"
#include "TimeDiffRule.hpp"
#include "Logger.hpp"

#include <vector>

using std::vector;

bool Rule::trigger(SecurityRequest& req)
{
  bool ret = true;
  /*
  RuleItemMapIt it = mProperties.begin();
  for(; it != mProperties.end(); ++it)
    {
      bool result = req.findKey(it->first);
      if(result && it->second->mRequired)
        {
          return true;
        }
    }
  */
  return ret;
}

Rule::Rule()
  : mProperties(), mOnOff(false), mRuleName()
{

}

Rule::~Rule()
{
  RuleItemMapIt it = mProperties.begin();
  for(; it != mProperties.end(); ++it)
    delete it->second;
  mProperties.clear();
}

void Rule::process(const SecurityRequest& req, SecurityResponse& resp)
{
  TRACE;
}

RuleEngine::RuleEngine()
  :mRules()
{
}

RuleEngine::~RuleEngine()
{
  RuleMapIt it = mRules.begin();
  for(; it != mRules.end(); )
      delete it->second;
  mRules.clear();
}

bool RuleEngine::doProcess(SecurityRequest& request, SecurityResponse& response)
{
  TRACE;
  RuleMapIt it = mRules.begin();
  for(; it != mRules.end(); ++it)
    {
      /*
      if(it->second->trigger(request))
        {

        }
      */
      it->second->process(request, response);
      // LOG_BEGIN(Logger::INFO)
      //   LOG_PROP("hell", it->second->mRuleName)
      // LOG_END("---------------->");
    }
}

void RuleEngine::addRule(Rule* rule)
{
  if(NULL == rule)
    return ;
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("ruleName", rule->mRuleName);
  LOG_END("");
  RuleMapIt it = mRules.find(rule->mRuleName);
  if(it != mRules.end())
    {
      LOG_BEGIN(Logger::INFO) LOG_PROP("del already exsit:", rule->mRuleName) LOG_END("");
      delete it->second;
    }
  
  mRules[rule->mRuleName] = rule;
  return;
}

void RuleEngine::delRule(  const string&  ruleName )
{
  RuleMapIt it = mRules.find(ruleName);
  if(it != mRules.end())
    {
      LOG_BEGIN(Logger::INFO) LOG_PROP("delRule", ruleName) LOG_END("");
      delete it->second;
    }
}

bool RuleEngine::start()
{
  if(mStart)
    return true;
  
  SimulatorRule::createInstance();
  BlackPhoneRule::createInstance();
  DeviceAccountRule::createInstance();
  TimeDiffRule::createInstance();
  SimulatorRule *simulator = SimulatorRule::getInstance();
  BlackPhoneRule *blackPhone = BlackPhoneRule::getInstance();
  DeviceAccountRule *deviceAccount = DeviceAccountRule::getInstance();
  TimeDiffRule *timeDiff = TimeDiffRule::getInstance();
  addRule(deviceAccount);
  addRule(simulator);
  addRule(blackPhone);
  addRule(timeDiff);
  
  mStart = true;
  return true;
}

bool RuleEngine::stop()
{
  if(!mStart)
    return true;

  SimulatorRule *simulator = SimulatorRule::getInstance();
  BlackPhoneRule *blackPhone = BlackPhoneRule::getInstance();
  DeviceAccountRule *deviceAccount = DeviceAccountRule::getInstance();
  TimeDiffRule *timeDiff = TimeDiffRule::getInstance();
  
  delRule(timeDiff->mRuleName);
  delRule(simulator->mRuleName);
  delRule(blackPhone->mRuleName);
  delRule(deviceAccount->mRuleName);
  mStart = false;
  return true;
}
