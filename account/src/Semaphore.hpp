/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef SEMAPHORE_HPP
#define SEMAPHORE_HPP

#include "Mutex.hpp"
#include "ConditionVariable.hpp"


class Semaphore
{

public:

  Semaphore( int maxCount = 1 );
  ~Semaphore( void );

  bool lock( const long int intervalSec = 0, const long int intervalNSec = 0 );
  bool unLock( void );
  int getCount( void ) const;

private:

  int m_maxCount;
  int m_count;
  mutable Mutex m_mutex;
  ConditionVariable m_condVar;
};

#endif // SEMAPHORE_HPP
