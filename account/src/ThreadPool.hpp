/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef THREADPOOL_HPP
#define THREADPOOL_HPP

#include <vector>

#include "ConcurrentDeque.hpp"
#include "Task.hpp"
#include "Thread.hpp"
#include "Mutex.hpp"
#include "Singleton.hpp"


class ThreadPool: public Singleton<ThreadPool>
{

  public:

    ThreadPool();
    ~ThreadPool();

    void pushTask(Task* task);
    Task* popTask();

    void pushWorkerThread(Thread * thread);
    void startWorkerThreads();

    void stop();
    void join() const;

  private:

    ThreadPool(const ThreadPool&);
    ThreadPool& operator=(const ThreadPool&);

    std::vector<Thread*> m_threads;
    ConcurrentDeque<Task*> m_tasks;
};

// class ThreadPool
// {

// public:

//   ThreadPool();
//   ~ThreadPool();

//   void pushTask(Task* task);
//   Task* popTask();

//   void pushWorkerThread(Thread * thread);
//   void startWorkerThreads();

//   void stop();
//   void join() const;
  
//   static void createInstance();

//   static ThreadPool* getInstance();
//   static void delInstance();
//   static ThreadPool* m_instance;  

// private:

//   ThreadPool(const ThreadPool&);
//   ThreadPool& operator=(const ThreadPool&);

//   std::vector<Thread*> m_threads;
//   ConcurrentDeque<Task*> m_tasks;
// };



#endif // THREADPOOL_HPP */
