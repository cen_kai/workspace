/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef PINGAN_HPP
#define PINGAN_HPP

#include <string>
#include <stdint.h>

using std::string;
namespace pingan{
  //sdk info
  const string  CMD = "cmd";
  const string  TYPE = "type";
  const string  ID = "id";
  const string  CRC = "crc";
  const string  RES = "res";
  const string  PARAM = "param";
  const string  ALL = "all";

  const string  REQ = "req";
  const string  RESP = "resp";
  const string  USRNAME = "usrname";
  const string  GET_ID = "getid";
  const string  KEEPALIVE = "keepalive";
  const string  GET_LIST = "getlist";
  const string  STATUS = "status";
  const string  PORT = "port";

  const string  DATA_COLLECT = "data_collect";
  const string  ID_QUERY = "id_query";
  const string  GET_SERVICE_TYPE = "get_service_type";
  const string SERVICE_TYPE = "service_type";
  const string TIME = "time";
  const string IP = "ip";

  const string  ID_SERVICE = "id_service";
  const string  COLLECT_SERVICE = "collect_service";
  const string  GET_STATIC = "get_static";
  const string  SENSITIVE_SERVICE = "sensitive_service";
  
  const int STATUS_200 = 200; //正常
  const int STATUS_500 = 500; //服务器错误 
  const int STATUS_400 = 400; //请求错误
  const int STATUS_206 = 206; //正常
  const int STATUS_407 = 407; //正常

  const string WEIGHT_COMPUTE = "weight_compute";

  const string SENTIVE_LAST = "SENTIVE_LAST";
  const string SENTIVE_ALL = "SENTIVE_ALL";
  const string FORBID_LAST  = "FORBID_LAST";
  const string FORBID_ALL  = "FORBID_ALL";
  const string EMPTY_STRING  = "null";

  const uint8_t ANDROID_PLATFORM = 1;
  const uint8_t IOS_PLATFORM = 2;
  const uint8_t OTHER_PLATFORM = 3;

  const  string CLIENTNO = "clientNo";
  const  string NETWORKOPERATOR = "networkOperator";
  const  string PHONENO = "phoneNo";
  const  string CLIENTIP = "clientIp";
  const  string SIMSERIAL = "simSerial";
  const  string CURRENTTIME = "currentTime";
  const  string WIFIIP = "wifiIp";
  const  string IMSIID = "imsiId";
  const  string IMEIID = "imeiId";
  const  string DEVICEID = "deviceId";
  const  string LOCATION = "location";
  const  string CELLNO = "cellno";
  const  string S_PARAM = "s_param";
  
  const  string NAME = "name";
  const  string VERSION = "version";
  const  string VENDOR = "vendor";
  const  string ACCELEROMETER = "accelerometer";
  const  string CLIENTSENSORINFO = "clientSensorInfo";
  const  string MAGNETICFIELD = "magneticField";
  const  string ORIENTATION = "orientation";
  const  string GYROSCOPE = "gyroscope";
  const  string LIGHT = "light";
  const  string PRESSURE = "pressure";
  const  string TEMPERATURE = "temperature";
  const  string PROXIMITY = "proximity";
  const  string GRAVITY = "gravity";
  const  string LINEARACCELERATION = "linearAcceleration";
  const  string VECTORROTATION = "vectorRotation";

  const  string CLIENTAPPLIST = "clientAppList";
  const  string APPNAME = "appName";
  const  string INSTIME = "insTime";
  const  string BLUEMAC = "blueMac";
  const  string WIFIMAC = "wifiMac";
  const  string BATSTATUS = "batStatus";

  const  string CHARGING = "charging";
  const  string FULL = "full";
  const  string UNKNOWN = "unknown";
  const  string DISCHARGING = "discharging";

  const  string ISBATUSAGE = "isBatUsage";
  const  string BATLEVEL = "batLevel";
  const  string CONTACTS = "contacts";
  const  string HUMIDITY = "humidity";
  const  string ROTATION = "rotation";
  const  string DEVICE_INFO = "device_info";
  const  string SYS_NAME = "sysName";
  const  string ANDROID = "android";
  const  string KNOWN_PIPES = "known_pipes";
  const  string KNOWN_QEMU_DRIVERS = "known_qemu_drivers";
  const  string KNOWN_FILES = "known_files";
  const  string BOARD = "board";
  const  string BOOTLOADER = "bootloader";
  const  string BRAND = "brand";
  const  string DEVICE = "device";
  const  string HARDWARE = "hardware";
  const  string MODEL = "model";
  const  string PRODUCT = "product";
  const string TEMPLETE = "template";

  const  string APP_VERSION = "appVersion";
  const  string LOGIN_STATUS = "loginStatus";
  const string LOGIN_TIME = "loginTime";
  const  string MAC = "mac";
  const  string MOBILE = "mobile";
  const  string DEVICE_SOURCE = "deviceSource";
  const  string SOURCE = "source";
  const  string USER_AGENT = "userAgent";
  const  string USER_NAME = "userName";
  const  string SOURCE_SYS = "sourceSys";
  const  string HTTP_HEADER_INFO = "httpHeaderInfo";
  const  string NICKNAME = "nickName";

  const  string REGISTER_STATUS = "registerStatus";

  const  string EMAIL = "email";

  const  string LOGIN_TYPE  = "loginType";
  const  string REGISTER_TYPE = "registerType";
  const  string COOKIE  = "cookie";
  const  string CUSTOM_NAME = "customname";

  const  int LOGIN_MSG  = 1;
  const  int REGISTER_MSG  = 2;
  const  int PHONE_MSG  = 3;

  const  string ACCOUNT_CHECK = "account_check";
  //rule name
  const  string SIMULATOR_RULE = "SimulatorRule";
  const  string BLACK_PHONE_RULE = "BlackPhoneRule";
  const  string DEVICE_ACCOUNT_RULE = "DeviceAccountRule";
  const  string DEVICE_PRINT_RULE = "DevicePrintRule";
  const  string TIME_DIFF_RULE = "TimeDiffRule";

  const  string WEB = "web";
  //REGISTER
  const string LOGIN_STR = "login";
  const string REGISTER_STR = "register";
    const string SERV_TIME = "servTime";
}
#endif
