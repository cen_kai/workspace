/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */



#include "GNUMemoryPool.hpp"
#include "MemoryPool.hpp"
#include "Logger.hpp"

GNUMemoryPool::GNUMemoryPool(int msgCount)
  :m_nMsgCount(msgCount)
  ,m_pPool(NULL)
{
  TRACE;
  int maxFragSize = 1000;
  m_pPool = new MemoryPool( maxFragSize,  m_nMsgCount);
}

GNUMemoryPool::~GNUMemoryPool()
{
  TRACE;
  if ( m_pPool != NULL )
    {
      delete m_pPool;
      m_pPool = NULL;
    }
}

void * GNUMemoryPool::get(size_t siz)
{
  TRACE;
  
  MemoryBlock *pBlock = NULL;
  
  ScopedLock sl(m_mutex);
  if(siz < 1490)
    pBlock = m_pPool->getBlock();

  if ( pBlock == NULL )
    {
      return NULL;
    }
  
  MemoryBlock **ppBlock = (MemoryBlock**) (pBlock->getData());
  *ppBlock = pBlock;

  ppBlock ++;
  return (void *) (ppBlock);
}

void GNUMemoryPool::set( void *t )
{
  TRACE;
  if ( t == NULL )
    {
      return;
    }

  MemoryBlock **ppBlock = (MemoryBlock **) t;
  ppBlock --;

  MemoryBlock *pBlock = *ppBlock;
  MemoryPool	*tPool = 0;
    
  if(pBlock->getBlockSize() == 1000)
    tPool = m_pPool;
  if(!tPool){
    LOG_BEGIN(Logger::ERR)
      LOG_END("memory error");
    return;
  }

  ScopedLock sl(m_mutex);
  tPool->setBlock( pBlock );
}

void GNUMemoryPool::createInstance(size_t siz)
{
  if ( not m_instance ) {
    m_instance = new GNUMemoryPool(siz);
  }
}

GNUMemoryPool* GNUMemoryPool::getInstance()
{
  return m_instance;
}

void GNUMemoryPool::destroy()
{
  if ( m_instance ) {
    delete m_instance;
  }
  m_instance = 0;
}

GNUMemoryPool* GNUMemoryPool::  m_instance = NULL;
