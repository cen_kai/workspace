/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef CONDITION_VARIABLE_HPP
#define CONDITION_VARIABLE_HPP

#include "Mutex.hpp"
#include <pthread.h>  // pthread_cond

class ConditionVariable
{

public:

  ConditionVariable(Mutex& mutex);
  ~ConditionVariable();

  int wait(const long int intervalSec = 0, const long int intervaNSec = 0);
  int signal();
  int broadcast();

private:

  ConditionVariable(const ConditionVariable&);
  ConditionVariable& operator=(const ConditionVariable&);

  Mutex& m_mutex;
  pthread_cond_t m_condVar;

};

#endif // CONDITION_VARIABLE_HPP
