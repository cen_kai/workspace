/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include "Connection.hpp"

#include "Logger.hpp"


Connection::Connection(const std::string& host, const std::string& port)
  : m_host(host)
  , m_port(port)
{
  TRACE;
}


Connection::~Connection()
{
  TRACE;
}


std::string Connection::getHost() const
{
  TRACE;
  return m_host;
}


std::string Connection::getPort() const
{
  TRACE;
  return m_port;
}

// std::string Connection::getRhost() const
// {
//   TRACE;
//   return m_rhost;
// }

// std::string Connection::getRport() const
// {
//   TRACE;
//   return m_rport;
// }

void Connection::setHost(const std::string& host)
{
  TRACE;
  m_host = host;
}


void Connection::setPort(const std::string& port)
{
  TRACE;
  m_port = port;
}


