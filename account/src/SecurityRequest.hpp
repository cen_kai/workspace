#ifndef SECURITYREQUEST_H
#define SECURITYREQUEST_H

#include "RWLock.hpp"

#include <stdint.h>
#include <string>
#include <vector>
#include <map>
#include <set>

#include "Json.hpp"

using std::string;
using std::map;
using std::vector;
using std::set;

typedef set<string> ScoreSet;

class SecurityRequest
{
public:
  SecurityRequest();
  //  virtual ~SecurityRequest();
  string mCellNo;
  string mDeviceInfo;
  string mPhoneNo;
  string mWifiMac;
  string mBlueMac;
  string mIMEI;
  string mIMSI;
  string mSimSerial;
  string mDeviceID;
  string mClientNo;
  string mServCelloNo;
  //add by qzs 20160524 for 撞库
  string mEventId;
  string mSystem;
  int mTimeDiff;//本次事件的时间, 当前在只有web端生效
  bool findKey(const string &key) const;
  bool findStr(const string &str) const;
  bool setDataSource(string &data, string&servData);
  json::Object *obj;
};

class SecurityResponse
{
public:
  SecurityResponse(){}
  //  virtual ~SecurityResponse();
  void updateScore(const string& ruleName, uint16_t& score); //TODO 记录规则返回，记录规则值，移余已返回的规则
  void updateRuleCode(const int code); //TODO 记录规则返回，记录规则值，移余已返回的规则
  ScoreSet mScores;
  RWLock mLock;
  void toString();
};

#endif /* SECURITYREQUEST_H */
