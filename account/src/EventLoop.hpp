/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef EVENT_LOOP_HPP
#define EVENT_LOOP_HPP

#include <map>
#include <list>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>

#include "EPoll.hpp"
#include "Event.hpp"
#include "ObjectPool.hpp"
#include "Singleton.hpp"

using std::map;
using std::list;
using std::string;

class FileDescribe
{
public:
  unsigned char events; /* the events watched for */
  unsigned char reify;  /* flag set when this ANFD needs reification (EV_ANFD_REIFY, EV__IOFDSET) */
  unsigned char emask;  /* the epoll backend stores the actual kernel mask in here */
  unsigned char unused;
  unsigned int egen;    /* generation counter to counter epoll bugs */
};

class RespItem
{
public:
  RespItem();
  virtual ~RespItem();

private:
  int mFd;
  string mItem;
};

typedef list<RespItem *> RespList;
typedef RespList::iterator RespIt;
typedef map<int , FileDescribe*> FileDescMap;
typedef map<int , FileDescribe*>::iterator FileDescMapIt;
typedef map<int , list<IOEvent*>*>::iterator IOListMapIt;
typedef list<TimerEvent*> TimerEventList;
typedef list<TimerEvent*>::iterator TimerEventListIt;
typedef list<PeriodicEvent*> PeriodicEventsList;
typedef list<PeriodicEvent*>::iterator PeriodicEventsListIt;
typedef map<int , list<IOEvent*>*> IOEventsMap;
typedef map<int , list<IOEvent*>*>::iterator IOEventsMapIt;
typedef list<IOEvent*> IOEventList;
typedef list<IOEvent*>::iterator IOEventListIt;

class EventLoop :public EventHandler, public Singleton<EventLoop>
{
private:
  TimerEventList mTimerEvents;
  PeriodicEventsList mPeriodicEvents;
  //list<IOEvent*> mIOEvents;
  list<Event*> mPending;

  map<int , list<IOEvent*>*> mIOEvents; //一个FileDescribe对应多个event, 一个event对应一个watcher
  map<int , FileDescribe*> mFds; //一个文件描述符对一个FileDescribe

  u_int32_t mFlags; //针对EventLoop的标记
  
  ev_tstamp mRtnow; //时间
  ev_tstamp mMonow; //时间

  ev_tstamp mIoBlocktime      ; //每次阻塞的时间
  ev_tstamp mTimeoutBlocktime ;
  
  pid_t mCurpid;

  EPoll poll;
  
  sig_atomic_t volatile mLoopDone; //退出标记
  int mActivecnt;

  ev_tstamp ev_rt_now;

  ev_tstamp now_floor;
  ev_tstamp mn_now;
  ev_tstamp rtmn_diff;

  sig_atomic_t volatile pipe_write_wanted;
  sig_atomic_t volatile pipe_write_skipped;
  ev_tstamp timeout_blocktime; //= 0
  ev_tstamp io_blocktime; // = 0
  ev_tstamp  backend_mintime ;// = 1e-3
  int mIdleall;

// Todo: 用管道写这个eventloop, 及时叫醒主线程
  
public:
  EventLoop();
  virtual ~EventLoop();
  void init(u_int32_t flags);
  void quit();
  int run();
  void addComm(Event * w);
  void delComm(Event * w);
  bool addEvent(TimerEvent * w);
  bool delEvent(TimerEvent * w);
  bool addEvent(IOEvent *w);
  bool delEvent(IOEvent *w);
  bool delEvent(int fd);
  bool addEvent(PeriodicEvent *w);
  bool delEvent(PeriodicEvent *w);
  void invokePending();
  void reify ();
  void time_update (ev_tstamp max_block);
  void periodics_reschedule ();
  ev_tstamp getMinTimerAt ();
  ev_tstamp getMinPeriodAt ();
  void timers_reify();
  void periodics_reify();
  virtual void doEvent(int fd, int events) ;
  time_t getCurTime() {return (time_t)ev_rt_now;};
  
  IOEvent* obtainIOEvent();
  TimerEvent* obtainTimerEvent();
  PeriodicEvent* obtainPeriodicEvent();
  void clearAll();
public :
  ObjectPool<IOEvent*> mIOPool;
  ObjectPool<TimerEvent*> mTimerPool;
  ObjectPool<PeriodicEvent*> mPeriodPool;

};


#endif //EVENT_LOOP_HPP
