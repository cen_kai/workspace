#include "DeviceManager.hpp"
#include "DevicePrintRule.hpp"

static const string SYS_IOS = "sysName:ios";
static const string SYS_ANDROID = "sysName:android";
static const string RULE_NAME = "devicePrint";

static int64_t getMac(const string &mac)
{
  string tmp(mac);
  
  if(tmp.length() == 17)
    std::remove(tmp.begin(), tmp.end(), ':');

  if(tmp.length() != 12)
      return 0;
  
  char *p = NULL;
  int64_t result = 0;
  result = strtoll(tmp.data(), &p, 16);
  return result;
}

inline uint64_t str2Uint64( const std::string& s , int size)
{
  uint64_t i = 0;
  if(s.length() == size)
    {
      char *p = NULL;
      i = strtoll(s.data(), &p, 10);
    }
  return i;
}


DevicePrintRule::DevicePrintRule()
{
  mRuleName = pingan::DEVICE_PRINT_RULE;
}

DevicePrintRule::~DevicePrintRule()
{

}

bool compareDevicePrint(AndroidDeviceInfo* deviceInfo, const SecurityRequest& req)
{
  
}

bool compareDevicePrint(IOSDeviceInfo* deviceInfo, const SecurityRequest& req)
{

}

void DevicePrintRule::process(const SecurityRequest& req, SecurityResponse& resp)
{
  string SYS_ANDROID = "sysName:android";
  DeviceManager *manager = DeviceManager::getInstance();
  uint16_t tmp = 10;
  DeviceInfo *device = manager->findByDeviceID(req.mDeviceID);
  if(device != NULL)
    {
      if(device->mPlatform == ANDROID_PLATFORM)
        {
          AndroidDeviceInfo *android = dynamic_cast<AndroidDeviceInfo*>(device);
          if(!compareDevicePrint(android, req))
            {
              resp.updateScore(mRuleName, tmp);
              return;
            }
          //其实这里也可以返回
        }
      if(device->mPlatform == IOS_PLATFORM)
        {
          IOSDeviceInfo *ios = dynamic_cast<IOSDeviceInfo*>(device);
          if(!compareDevicePrint(ios, req))
            {
              resp.updateScore(mRuleName, tmp);
            }
        }
    }
  uint64_t phoneNo = str2Uint64(req.mPhoneNo, 11);
  if(phoneNo > 0)
    {
      device = manager->findByPhoneNo(phoneNo);
      if(device != NULL)
        {
          if(device->mPlatform == ANDROID_PLATFORM)
            {
              AndroidDeviceInfo *deviceInfo = dynamic_cast<AndroidDeviceInfo*>(device);
              if(!compareDevicePrint(deviceInfo, req))
                {
                  resp.updateScore(mRuleName, tmp);
                }
              return;
            }
        }
    }
  
  if(req.findStr(SYS_ANDROID))
    {
      uint16_t tmp = 10;
      //wifiMac
      uint64_t mac  = getMac(req.mWifiMac);
      AndroidDeviceInfo *deviceInfo = manager->findByWifiMac(mac);
      if(deviceInfo != NULL)
        {
          if(!compareDevicePrint(deviceInfo, req))
            {
              resp.updateScore(mRuleName, tmp);
            }
          return;
        }
      //blueMac
      mac = getMac(req.mBlueMac);
      deviceInfo = manager->findByBlueMac(mac);
      if(deviceInfo != NULL)
        {
          if(!compareDevicePrint(deviceInfo, req))
            {
              resp.updateScore(mRuleName, tmp);
            }
          return;
        }
      //手机号
      uint64_t imei = str2Uint64(req.mIMEI, 15);
      if(imei > 0 )
        {
          deviceInfo = manager->findByIMEI(imei);
          if(deviceInfo != NULL)
            {
              if(!compareDevicePrint(deviceInfo, req))
                {
                  resp.updateScore(mRuleName, tmp);
                }
              return;
            }
        }

      uint64_t imsi = str2Uint64(req.mIMSI, 15);
      if(imsi > 0)
        {
          deviceInfo = manager->findByIMEI(imsi);
          if(deviceInfo != NULL)
            {
              if(!compareDevicePrint(deviceInfo, req))
                {
                  resp.updateScore(mRuleName, tmp);
                }
              return;
            }
        }

      uint64_t simSerial = str2Uint64(req.mSimSerial, 15);
      if(simSerial > 0 )
        {
          deviceInfo = manager->findBySimSerial(simSerial);
          if(deviceInfo != NULL)
            {
              if(!compareDevicePrint(deviceInfo, req))
                {
                  resp.updateScore(mRuleName, tmp);
                }
              return;
            }
        }
    }
  
  if(req.findStr(SYS_IOS))
    {
      return;
    }
 
}

