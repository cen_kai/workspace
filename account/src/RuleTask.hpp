#ifndef RULETASK_H
#define RULETASK_H
#include "Task.hpp"
#include "SecurityRequest.hpp"
#include <string>

using std::string;

class RuleTask : public Task
{

public:
  RuleTask(SecurityRequest *request,  SecurityResponse *response);
  ~RuleTask();
  void run();

private:
  RuleTask(const RuleTask&);
  RuleTask& operator=(const RuleTask&);

  SecurityRequest *mRequest;
  SecurityResponse *mResponse;
};


#endif /* RULETASK_H */
