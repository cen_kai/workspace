/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include "MemoryPool.hpp"
#include <assert.h>
#include "Logger.hpp"

MemoryPool::MemoryPool( int oneBlockSize, int count ):
	m_Head(NULL),
	m_nOneBlockSize(oneBlockSize),
	m_maxBlockCount(count),
  m_freeBlockSize(0),
  m_curBlockCount(0)
{
  TRACE;
}

MemoryPool::~MemoryPool(void)
{
  TRACE;
	while ( m_Head != NULL )
	{
		MemoryBlock *pBlock = m_Head;
		m_Head = m_Head->m_pNext;
		delete pBlock;
		pBlock = NULL;
	}
}

MemoryBlock * MemoryPool::getBlock()
{
  TRACE;
  
  LOG_BEGIN(Logger::DEBUG)
    LOG_PROP("Total", m_curBlockCount)
    LOG_PROP("Free", m_freeBlockSize)
    LOG_PROP("Unit", m_nOneBlockSize)
    LOG_END("");
    
  if( NULL == m_Head ){
    MemoryBlock *pBlock = new MemoryBlock(m_nOneBlockSize);

    if(NULL == pBlock){
      //      TODO Warning , 限制大小
      LOG_BEGIN(Logger::ERR)
        LOG_END("Memory expire!!!");
      return NULL;
    }
    m_curBlockCount++;
    
    LOG_BEGIN(Logger::DEBUG)
      LOG_PROP("Allocate Memeory", m_nOneBlockSize)
      LOG_END("Bytes");
    
    return pBlock;
  }
  
	MemoryBlock *pRet = m_Head;
	m_Head = m_Head->m_pNext;
  m_freeBlockSize--;
  
	return pRet;
}

void MemoryPool::setBlock(MemoryBlock *pBlock)
{
  TRACE;
  /*
  if(!m_Head){
    MemoryBlock **ppBlock = &m_Head;
    *ppBlock = pBlock;
    pBlock->m_pNext = 0;
    return;
  } 
  */ 
	MemoryBlock *pTemp = m_Head;
	m_Head = pBlock;
	m_Head->m_pNext = pTemp;
  m_freeBlockSize++;

  LOG_BEGIN(Logger::DEBUG)
    LOG_PROP("Total", m_curBlockCount)
    LOG_PROP("Free", m_freeBlockSize)
    LOG_PROP("Unit", m_nOneBlockSize)
    LOG_END("");
}

int MemoryPool::getCurBlockCount()
{
  TRACE;
  return m_curBlockCount;
}

int MemoryPool::getMaxBlockCount()
{
  TRACE;
  return m_nOneBlockSize;
}

int MemoryPool::getBlockSize(){
  TRACE;
  return m_nOneBlockSize;
}

