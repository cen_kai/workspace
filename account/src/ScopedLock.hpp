/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef SCOPEDLOCK_HPP
#define SCOPEDLOCK_HPP

#include "Mutex.hpp"


class ScopedLock
{

public:

  ScopedLock( Mutex& mutex );
  ~ScopedLock();

private:

  ScopedLock(const ScopedLock&);
  ScopedLock& operator=(const ScopedLock&);

  Mutex& m_mutex;

};


#endif // SCOPEDLOCK_HPP
