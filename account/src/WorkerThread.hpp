/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef WORKER_THREAD_HPP
#define WORKER_THREAD_HPP

#include "Thread.hpp"
#include "ThreadPool.hpp"


class WorkerThread : public Thread
{

public:

  WorkerThread( ThreadPool& tp );

private:

  void* run();

  ThreadPool& m_tp;
};


#endif // WORKER_THREAD_HPP
