#include "Singleton.hpp"
#include "RuleEngine.hpp"
#include "Logger.hpp"
#include "PingAn.hpp"
#include "ConfigFile.hpp"

class TimeDiffRule: public Rule, public Singleton<TimeDiffRule>
{
public:
  TimeDiffRule()
  {
    Rule::mRuleName = pingan::TIME_DIFF_RULE;
  }

  void process(const SecurityRequest& req, SecurityResponse& resp)
  {
    TRACE;
    uint16_t result = 0;
    if(req.mTimeDiff > 0 && (req.mSystem.find(pingan::WEB) != string::npos))
      {
        ConfigFile *file = ConfigFile::getInstance();
        if(req.mEventId == pingan::LOGIN_STR)
          {
            int t = file->getInt("login_time", -1);
            if(t != -1 && t > req.mTimeDiff )
              resp.updateScore(mRuleName, result);
            // TODO: under
            
            // TODO: top
          }  
        else if(req.mEventId == pingan::REGISTER_STR)
          {
            int t = file->getInt("register_time", -1);
            if(t != -1 && t > req.mTimeDiff )
              resp.updateScore(mRuleName, result);
            // TODO: under
            
            // TODO: top
          }
        else
          return;

      }
  }

  virtual ~TimeDiffRule()
  {
  }
};
