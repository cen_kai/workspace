/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef MESSAGE_MANAGER_HPP
#define MESSAGE_MANAGER_HPP
#include <string>
#include <map>
#include <vector>
#include <time.h>

#include "Singleton.hpp"
#include "PingAn.hpp"
#include "ThreadPool.hpp"
#include "ObjectPool.hpp"
#include "GNUMemoryPool.hpp"

using std::string;
using std::map;
using std::vector;

using namespace pingan;
class Request;
typedef map<string, Request*>MessageMap; //id + content
typedef map<string, Request*>::iterator MessageIt;

typedef map<string, int> IdFdMap; //id + fd
//typedef map<int, string> Map; //fd + ids
typedef map<string, int>::iterator IdFdIt;

typedef map<string, time_t> MessageTimestampMap; //id + time_t 这里是时间点
typedef map<string, time_t>::iterator TimestampIt;


class MessageManager: public Singleton<MessageManager>
{
public:
  MessageManager();
  virtual ~MessageManager();

  void addRequest(int& fd, string& req);
  void delRequest(string& id);
  void doTimeOutRequest();
  void sndResult(string& id, string& result);
  void getKeepaliveResp(char * buf, int& len);
  
  bool start();
  bool stop();
  bool init();

private:
  MessageMap mMessages;
  Mutex mMutex;
  //  ObjectPool<RequestsItem*> mRequestsItemPool;
  ObjectPool<Request*> mRequestPool;
};

class Request
{
public:  
  string mID;
  string mCmd;
  int mTimestamp;
  int mFd;
  u_int32_t mHashCode ;
  
  u_int32_t hashCode()
  {
    if(mHashCode == 0 && mID.size()> 0)
      mHashCode = getStrHashCode(mID);
    return mHashCode;
  }

  bool expire(time_t& cur)
  {
    if((cur - mTimestamp) > 3) //10s超时
      return true;
    return false;
  }
  
  bool equals(Request& anObject) {
    if (this == &anObject) {
      return true;
    }
    if(mHashCode != anObject.mHashCode)
      return false;

    if(mID == anObject.mID)
      return true;
    return false;
  }
};


#endif
