/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "Logger.hpp"
#include "TimerManager.hpp"
#include "ConfigFile.hpp"
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h> // inet_ntop
#include <stdio.h>

static  const int type_sentive_last = 1;
static  const int type_sentive_all = 2;
static  const int type_forbid_last = 3;
static  const int type_forbid_all = 4;
static  const int type_message_check = 5;
static  const int type_connect_check = 6;
static  const int type_connect_exit = 7;
static  const int type_keepalive = 8;


// TimerManager::~TimerManager()
// {
//   TRACE;
//   // for(int i = 0 ; i<events.size(); i++)
//   //   {
//   //     Event* ev=  events[i];
//   //     delete ev;
//   //   }
//   //内存放在eventloop中统一进行处理,根据事件类型直接进行处理
//   //  mEvents.clear();
// }

static char buffer[256] = {0};

bool TimerManager::init()
{
  TRACE;
  bool ret = true;
  bStart = false;
  return ret;
}

static void setProtoHeader(unsigned char *buf, string& data)
{
  ProtoHeader *header = (ProtoHeader*) buf;
  uint16_t checksum = crc16_ccitt((unsigned char*)data.data(), data.length());
  header->checksum = htons(checksum);
  header->prototype = htons(1);
  header->totolen = htonl(data.length());
  header->version = 1;
}

void TimerManager::setTimer(TimerEvent *ev, ev_tstamp at, ev_tstamp repeat, const int& data)
{
  ev->mAt = at;
  ev->mRepeat = repeat;
  ev->mData = const_cast<int *>(&data);
  ev->mWatcher = this;
}

void get_service_type_resp(char * buf, int& len){
  json::Object obj;
    
  ConfigFile *config = ConfigFile::getInstance();
  int localport = config->getInt("localport", 10010);
  string localhost = config->getString("localhost", "null");
  obj[PORT] = localport;
  
  obj[CMD] = GET_SERVICE_TYPE;
  obj[IP] = localhost;
  obj[SERVICE_TYPE] = SENSITIVE_SERVICE;
  
  string s = json::Serialize(obj);

  setProtoHeader((uint8_t*)buf, s);
  memcpy(buf + sizeof(ProtoHeader), s.data(), s.length());
  len = s.length() + sizeof(ProtoHeader);
  return;
}

void TimerManager::sndKeepAlive()
{
  int32_t length = 0;
  get_service_type_resp(buffer, length);
  
  if (sendto(mFD,buffer, length, 0, (struct sockaddr *) &mAddr, sizeof(mAddr)) < 0)
  {
    perror("sendto keepalive");
    //    exit(1);
  }  
}



bool TimerManager::start()
{
  TRACE;
  bool ret = true;
  if(bStart)
    return ret;

  EventLoop* loop = EventLoop::getInstance();  

  TimerEvent *connectTimer = loop->obtainTimerEvent();
  setTimer(connectTimer, 30, 30, type_connect_check);   //30s
  
  TimerEvent *messageTimer = loop->obtainTimerEvent();
  setTimer(messageTimer, 5, 5, type_message_check); //5s
  
  TimerEvent *forbidTimer = loop->obtainTimerEvent();
  setTimer(forbidTimer, 300, 300, type_forbid_last); //5s
  
  TimerEvent *sentiveTimer = loop->obtainTimerEvent();
  setTimer(sentiveTimer, 300, 300, type_sentive_last); //5s

  TimerEvent *keepAliveTimer = loop->obtainTimerEvent();
  setTimer(keepAliveTimer, 5, 5, type_keepalive); //60s  1分钟跳一次
  
  //  TimerEvent *exitTimer = loop->obtainTimerEvent();
  //  setTimer(exitTimer, 60, 60, type_connect_exit); //5s
  
  loop->addEvent(connectTimer);
  loop->addEvent(sentiveTimer);
  loop->addEvent(messageTimer);
  loop->addEvent(forbidTimer);
  loop->addEvent(keepAliveTimer);
  //  loop->addEvent(exitTimer);

  // if ((mFD=socket(AF_INET,SOCK_DGRAM,0)) < 0)
  //   {
  //     perror("time manager new socket error");
  //     LOG_BEGIN(Logger::INFO) LOG_END("time manager new socket error");
  //     exit(1);
  //   }
  
  // string multicastAddr = ConfigFile::getInstance()->getString("multicast", "225.1.1.1");
  // int multicastPort = ConfigFile::getInstance()->getInt("multicast", 10000);
  
  // mAddr.sin_family=AF_INET;
  // mAddr.sin_addr.s_addr=inet_addr(multicastAddr.data());
  // mAddr.sin_port=htons(multicastPort);

  bStart = true;
  return ret;
}

bool TimerManager::stop()
{
  TRACE;
  
  //  close(mFD);
  bool ret = true;
  return ret;
}

bool TimerManager::doTimer(TimerEvent* evt)
{
  TRACE;
  if(NULL == evt)
    return true;

  int32_t* type = (int32_t*)evt->mData;
  
  switch(*type)
    {
      
    case type_connect_check:
      {
        LOG_BEGIN(Logger::INFO) LOG_END("Connection check");
        ConnectManager* cm = ConnectManager::getInstance();
        cm->delTimeoutConnection();
        break;
      }
      
    case type_message_check:
      {
        //LOG_BEGIN(Logger::INFO) LOG_END(" Message check");
        MessageManager* mm = MessageManager::getInstance();
        mm->doTimeOutRequest();
        break;
      }

    // case type_forbid_last:
    //   {
    //     LOG_BEGIN(Logger::INFO) LOG_END(" Forbid Last check");
    //     ThreadPool* pool = ThreadPool::getInstance();
    //     EventLoop *loop = EventLoop::getInstance();
    //     time_t time = loop->getCurTime();
    //     Task* task = new ForbidAllTask(time); //后面从eventloop取时间
    //     pool->pushTask(task);
    //     break;
    //   }
      
    // case type_sentive_last:
    //   {
    //     LOG_BEGIN(Logger::INFO) LOG_END(" Sentive Last check");
    //     ThreadPool* pool = ThreadPool::getInstance();
    //     EventLoop *loop = EventLoop::getInstance();
    //     time_t time = loop->getCurTime();
    //     Task* task = new SentiveAllTask(time); //后面从eventloop取时间
    //     pool->pushTask(task);
        
    //     break;
    //   }
    case type_connect_exit:
      {
        EventLoop *loop = EventLoop::getInstance();
        loop->quit();
        break;
      }

    case type_keepalive:
      {
        //        LOG_BEGIN(Logger::INFO) LOG_END("snd keepalive");
        //sndKeepAlive();
        break;
      }
    default:
      LOG_BEGIN(Logger::ERR) LOG_END(" default err");
      break;
    }
  return true;
}

bool TimerManager::doPeriodic(PeriodicEvent* evt)
{
  TRACE;
  if(NULL == evt)
    return true;

  int32_t *type = (int32_t * )evt->mData;
  switch(*type)
    {

    case type_forbid_all:
      {
        // ThreadPool* pool = ThreadPool::getInstance();
        // Task* task = new ForbidAllTask();
        // pool->pushTask(task);
        break;
      }

    case type_sentive_all:
      {
        // ThreadPool* pool = ThreadPool::getInstance();
        // Task* task = new SentiveAllTask();
        // pool->pushTask();
        break;
      }
      
    default:
      break;
    }
  return true;
}
