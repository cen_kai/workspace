#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

/**
 * @file   DeviceManager.hpp
 * @author one <one@localhost.localdomain>
 * @date   Sun Dec  6 05:55:17 2015
 * 
 * @brief  设备管理
 * 
 * 
 */

#include "RWLock.hpp"
#include "Singleton.hpp"
#include "DeviceInfo.hpp"
#include "PingAn.hpp"
#include "Logger.hpp"
#include "ObjectPool.hpp"
#include <map>
#include <set>
#include <vector>

using std::map;
using std::vector;
using namespace pingan;

typedef map<uint32_t, DeviceInfo*> DeviceMap;
typedef DeviceMap::iterator DeviceMapIt;
typedef map<string, DeviceInfo*> DeviceIDMap;
typedef DeviceIDMap::iterator DeviceIDIt;
typedef map<uint64_t, AndroidDeviceInfo*> DeviceMacMap;
typedef DeviceMacMap::iterator DeviceMacMapIt;
typedef map<uint64_t, DeviceInfo*> DevicePhoneMap;
typedef DevicePhoneMap::iterator DevicePhoneMapIt;
typedef map<uint64_t, AndroidDeviceInfo*> DeviceIMEIMap;
typedef DeviceIMEIMap::iterator DeviceIMEIMapIt;
typedef map<uint64_t, AndroidDeviceInfo*> DeviceIMSIMap;
typedef DeviceIMSIMap::iterator DeviceIMSIMapIt;
typedef map<uint64_t, AndroidDeviceInfo*> DeviceSimSerialMap;
typedef DeviceSimSerialMap::iterator DeviceSimSerialMapIt;
typedef set<DeviceInfo*> DeviceSet;
typedef DeviceSet::iterator DeviceSetIt;

class DeviceManager: public Singleton<DeviceManager>
{
public:
  DeviceManager();
  virtual ~DeviceManager();

  bool start();
  bool stop();
  bool update(DeviceSet& updateDevices, DeviceSet& deleteDevices);
  
  AndroidDeviceInfo* findByBlueMac(uint64_t blueMac);
  AndroidDeviceInfo* findByWifiMac(uint64_t wifiMac);
  DeviceInfo* findByPhoneNo(uint64_t phoneNumber);
  AndroidDeviceInfo* findByIMEI(uint64_t imei);
  AndroidDeviceInfo* findByIMSI(uint64_t imsi);
  AndroidDeviceInfo* findBySimSerial(uint64_t simserial);
  DeviceInfo* findByDeviceID(const string& deviceID) ;
  AndroidDeviceInfo* obtainAndroidDeviceInfo();
  void recycleDeviceInfo(DeviceInfo* deviceInfo);
  IOSDeviceInfo* obtainIOSDeviceInfo();

private:
  RWLock mLock;
  bool mStart;
  DeviceMap mDevices;
  DeviceIDMap mDeviceIDs;
  DeviceMacMap mBlueMacDevices;
  DeviceMacMap mWifiMacDevices;
  DevicePhoneMap mPhoneNoDevices;
  DeviceIMEIMap mIMEIDevices;
  DeviceIMSIMap mIMSIDevices;
  DeviceSimSerialMap mSimSerialDevices;
  ObjectPool<DeviceInfo*> mAndroidPool;
  ObjectPool<DeviceInfo*> mIOSPool;
};

#endif /* DEVICEMANAGER_H */

