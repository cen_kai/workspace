/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstring> // rindex
#include <string> // string

#include <time.h> // timespec, CLOCK_REALTIME

#include <typeinfo> //typeid
#include <stdexcept> // runtime_error
#include <sstream> // ostringstream

#include <errno.h> // errno
#include <string.h> // strerror
#include <sys/time.h>
#include <stdint.h>
#include "Logger.hpp"

const long NANO = 1000000000L;  // 10^9


inline timespec addTotimespec( const long int & sec, const long int & nsec)
{

  timespec abs_time;
  clock_gettime ( CLOCK_REALTIME, &abs_time );
  long int nsecSum( abs_time.tv_nsec + nsec );

  if ( nsecSum >= NANO ) {
    abs_time.tv_sec += sec + nsecSum / NANO;
    abs_time.tv_nsec = nsecSum % NANO;
  } else {
    abs_time.tv_sec += sec;
    abs_time.tv_nsec += nsec;
  }
  return abs_time;
}


inline timespec timespecAdd( timespec& t1, const timespec& t2 )
{
  timespec  result;
  result.tv_sec = t1.tv_sec + t2.tv_sec ;
  result.tv_nsec = t1.tv_nsec + t2.tv_nsec ;
  if (result.tv_nsec >= NANO ) {
    result.tv_sec++ ;
    result.tv_nsec = result.tv_nsec - NANO;
  }
  return result;
}

inline timespec timespecSubstract( timespec& t1, const timespec& t2 )
{
  timespec  result;
  if ((t1.tv_sec < t2.tv_sec) ||
      ((t1.tv_sec == t2.tv_sec) && (t1.tv_nsec <= t2.tv_nsec))) {
    result.tv_sec = result.tv_nsec = 0 ;
  } else {
    result.tv_sec = t1.tv_sec - t2.tv_sec ;
    if (t1.tv_nsec < t2.tv_nsec) {
      result.tv_nsec = t1.tv_nsec + NANO - t2.tv_nsec ;
      result.tv_sec-- ;
    } else {
      result.tv_nsec = t1.tv_nsec - t2.tv_nsec ;
    }
  }
  return result;
}


inline const char* extractFilename( const char *path )
{
  char const * f = rindex(path, '/');
  if (f) return ++f;
  return path;
}

template<typename T>
inline std::string stringify(T const& x)
{
  std::ostringstream o;
  o << x;
  return o.str();
}


inline std::string getTime( void )
{
  time_t tim = time( NULL );
  struct tm * now = localtime( &tim );

  std::string ret = stringify(now->tm_hour) + ":" +
                    stringify(now->tm_min) + ":" +
                    stringify(now->tm_sec);

  return ret;
}


template <class T>
inline std::string TToStr(const T t)
{
  std::ostringstream oss;
  oss << t;
  return oss.str();
}

// template <class T>
// inline void StrToT( T &t, const std::string s )
// {
//   std::stringstream ss(s);
//   ss >> t;
// }

template <class T>
inline T StrToT( const std::string& s )
{
  std::stringstream ss(s);
  T t;
  ss >> t;
  return t;
}


inline std::string errnoToString(const char *s)
{
  return std::string(s).append(strerror(errno));
}

/** 
 * Get Current Time (ms)
 *
 * @return 
 */
inline std::string getMicrosecTime(void)
{
  struct timeval t_start; 
  gettimeofday(&t_start, NULL); 
  long start = ((long)t_start.tv_sec)*1000+(long)t_start.tv_usec/1000; 
  return TToStr(start);
}


inline unsigned int  getStrHashCode(std::string& s)
{
  unsigned int h = 0;
  if ( s.size() > 0) {
    for (int i = 0; i < s.size(); i++) {
      h = 31 * h + s[i];
    }
  }
  return h;
}

typedef unsigned char BYTE;           
inline uint16_t crc16_ccitt(BYTE *ucbuf, int iLen)
{
  uint16_t crc = 0xFFFF;          // initial value
  uint16_t polynomial = 0x1021;   // 0001 0000 0010 0001  (0, 5, 12)
  int i,j;
  for (j = 0; j < iLen; ++j) {
    for (i = 0; i < 8; i++) {
      char bit = ((ucbuf[j]   >> (7-i) & 1) == 1);
      char c15 = ((crc >> 15    & 1) == 1);
      crc <<= 1;
      if (c15 ^ bit) crc ^= polynomial;
    }
  }

  crc &= 0xffff;

  return crc;
}



#endif // COMMON_HPP
