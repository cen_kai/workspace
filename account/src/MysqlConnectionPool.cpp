/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include "MysqlConnectionPool.hpp"

#include "Logger.hpp"
#include "ConfigFile.hpp"

// MysqlConnectionPool::MysqlConnectionPool( const char *host,
//                                           const char *user,
//                                           const char *passwd,
//                                           const char *db,
//                                           const unsigned int port
//                                           )
//   : m_host(host)
//   , m_user(user)
//   , m_passwd(passwd)
//   , m_db(db)
//   , m_port(port)
// {
//   TRACE;
// }

MysqlConnectionPool::~MysqlConnectionPool()
{
  TRACE;
  
  clear();
}

void MysqlConnectionPool::init()
{

  ConfigFile *config = ConfigFile::getInstance();
  m_host = config->getString("dbhost", "null");
  m_user = config->getString("dbuser", "null");
  
#ifdef RELEASE
  
  string tmp = config->getString("dbpwd", "null");
  FILE *pf = popen(tmp.c_str(), "r");
  char res[1024] = {0};
  fread(res, 1024, 1, pf);
  printf("release------->\n");
  pclose(pf);
  
  m_passwd = res;
  m_passwd = m_passwd.erase(m_passwd.length()-1 , m_passwd.length());
#else
  printf("debug------------>");
  m_passwd = config->getString("dbpwd", "null");
#endif
  m_db = config->getString("dbuse", "null");
  mPool = config->getInt("dbpool", 2);
  m_port = config->getInt("dbport", 3306);
  
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("dbhost", m_host)
    LOG_PROP("dbuser", m_user)
    LOG_PROP("dbpwd", m_passwd)
    LOG_PROP("dbuse", m_port)
    LOG_PROP("dbpool", mPool)
    LOG_PROP("m_db", m_db)
    LOG_END("");

}
void MysqlConnectionPool::start()
{
  TRACE;

  MysqlClient *client = new MysqlClient ( m_host.data(), m_user.data(), m_passwd.data(), m_db.data(), m_port );
  client->connect();
  add(client);
}

void MysqlConnectionPool::stop(){
  clear();
}

