/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#ifndef TIMER_MANAGER_HPP
#define TIMER_MANAGER_HPP

#include "Singleton.hpp"

#include "Event.hpp"
#include "ConnectManager.hpp"
#include "ThreadPool.hpp"
#include "PingAnTask.hpp"
#include "MessageManager.hpp"

// typedef vector<Event*> EventsArray;
// typedef vector<Event*>::iterator EventIt;

class TimerManager: public Singleton<TimerManager>, public TimerWatcher, public PeriodicWatcher
{
public:
  TimerManager(){}
  // ~TimerManager();
  
  bool init();
  bool start();
  bool stop();
  bool doTimer(TimerEvent* evt) ;
  bool doPeriodic(PeriodicEvent* evt);
  void setTimer(TimerEvent *ev, ev_tstamp at, ev_tstamp repeat, const int& data);
  void sndKeepAlive();
public:
  bool bStart;

  int mFD;
  struct sockaddr_in mAddr;  

  //  EventsArray events;           
};



#endif
