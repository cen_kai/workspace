/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef PING_AN_MESSAGE_HPP
#define PING_AN_MESSAGE_HPP

#include "Logger.hpp"
#include "Common.hpp"

#include "Message.hpp"
#include "TcpConnection.hpp"

#include "ConfigFile.hpp"
#include "PingAn.hpp"
#include "Json.hpp"
#include "MessageManager.hpp"

#include <iostream>
#include <string>

#include <unistd.h> // sleep

using namespace pingan;

#pragma pack (1)
typedef struct ProtoHeader
{
  short prototype;
  int totolen;
  unsigned short checksum;
  unsigned char version;
  int extend;
}ProtoHeader;
#pragma pack ()


class PingAnMessage : public Message
{//设定数据源
public:

  PingAnMessage( void *msgParam = NULL)
    :Message(msgParam)
    ,m_expectedLen(0)
    ,mHeaderReady(false)
    ,messageLenBuffer()
  {
    TRACE;
    memset(&header, 0, sizeof(header));
  }

  ~PingAnMessage()
  {
    TRACE;
  }

  bool validate()
  {
    uint16_t crc = crc16_ccitt((unsigned char *)m_buffer.data(), m_buffer.length());
    std::cout<<crc;
    unsigned short checksum = (unsigned short)ntohs(header.checksum);
    short prototype = ntohs(header.prototype);
    int totallen = ntohl(header.totolen);
    
    // std::cout<<std::hex<<" checksum = "<<checksum;
    // std::cout<<" prototype = "<<prototype;
    // std::cout<<" totallen = "<<std::hex<<totallen;
    // //    std::cout<<" version = "<< 
    // std::cout<<" crc = "<< std::hex<<crc <<std::endl<<std::dec;
    if( 1!= prototype)
      {
        std::cout<<" prototype != 1 "<<prototype;
        return false;
      }
    if(crc != checksum)
      {
        std::cout<<" checksum error "<<prototype;
        return false;
      }
    return true;
  }
  
  bool buildMessage( const void   *msgPart,
                     const size_t  msgLen )
  {

    TRACE;

    int32_t headerLen = sizeof(header);
    int32_t remain = msgLen;
    bool repeat = true;

    char *pMsgPart = (char *)msgPart;
    size_t sMsgLen = msgLen;
    
    while(repeat)  //处理粘包现象
      {

        if(!mHeaderReady)
          {
            if(m_expectedLen + sMsgLen >= headerLen)
              {
                std::cout<<"byte14: "<<std::endl<<std::hex;
                for(int i = 0; i< 14; i++)
                  {
                    printf("%hhx", pMsgPart[i]);
                  }
                // std::cout<<std::dec;
                // std::cout<<"m_expectedLen = " <<m_expectedLen<<"copyLen:"<<headerLen - m_expectedLen<<std::endl;
                memcpy(&header + m_expectedLen, pMsgPart, headerLen - m_expectedLen);    
                remain = remain - (headerLen - m_expectedLen);
                // std::cout<<"head totalen: "<<std::hex<<header.totolen<<std::endl;
                // std::cout<<"prototype: "<<std::hex<<header.prototype<<std::endl;
                // std::cout<<"checksum:  "<<std::hex<<header.checksum<<std::endl;
                // std::cout<<"extend: "<<std::hex<<header.extend<<std::endl;
                // std::cout<<"version: "<<std::hex<<header.version<<std::endl;
                m_expectedLen = ntohl(header.totolen);
                //std::cout<<"Messagelen = " <<m_expectedLen<<std::endl<<std::dec; 
                mHeaderReady = true;
              }
            else
              {
                memcpy((char *)&header + m_expectedLen, pMsgPart, sMsgLen);
                //std::cout<<"---------->"<<m_expectedLen<<std::endl;
                m_expectedLen += sMsgLen;
                remain = 0;
              }
          }

        if(remain < 1)
          {
            //        MSG("remain < 1 ", remain);
            return true;
          }
    
        char *p = (char *)pMsgPart + (sMsgLen - remain);//从剩余位置开始

        if(remain > m_expectedLen)
          {
            m_buffer.append(p, m_expectedLen);
            pMsgPart = p + m_expectedLen;
            sMsgLen = remain - m_expectedLen;
            remain = sMsgLen;
          }
        else
          {
            repeat = false;
            m_buffer.append(p, remain);
          }

        if(m_buffer.length() == m_expectedLen){
          if(validate())
            {
              onMessageReady();
              reset();
            }
          else
            {
              reset();
              return false;
            }
        }else if(m_buffer.length() > m_expectedLen)
          {
            return false;
          }

      }
    return true;

  }

  
  void onMessageReady()
  {
    TRACE;
    
    //    std::cout << "buffer: " << m_buffer << std::endl;
    MessageManager *mm = MessageManager::getInstance();

    int sock = m_connection->getSocket();
    mm->addRequest(sock, m_buffer);
    reset();
  }

  void reset(){
    m_expectedLen = 0;
    mHeaderReady = false;
    memset(&header, 0 ,sizeof(header));
    m_buffer.clear();
  }
  
  Message* clone()
  {
    TRACE;
    return new PingAnMessage();
  }

protected:
  
  size_t getExpectedLength()
  {
    TRACE;
    return m_expectedLen;
  }

private:

  int32_t messageLenBuffer;
  int32_t m_expectedLen;
  ProtoHeader header;
  bool mHeaderReady;
};

#endif
