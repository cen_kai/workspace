#ifndef DEVICEACCOUNTRULE_H
#define DEVICEACCOUNTRULE_H


#include "Singleton.hpp"
#include "RWLock.hpp"
#include "RuleEngine.hpp"
#include <string>
#include <set>
#include <map>
#include <time.h>

using std::string;
using std::set;
using std::map;
/**
 * 脱库攻击
 */
class AccountSet
{
public:
  set<string> mAccounts;
  time_t mlastime;//last login time
};

class DeviceAccountRule: public Rule, public Singleton<DeviceAccountRule>
{
public:
  DeviceAccountRule();
  void process(const SecurityRequest& req, SecurityResponse& resp);
  void aging();
  virtual ~DeviceAccountRule();
  map<string, AccountSet*> mAccount;
  RWLock mLock;
};


#endif /* DEVICEACCOUNTRULE_H */
