/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <string>


class Connection
{
public:

  virtual ~Connection();
  virtual Connection* clone(const int socket) = 0;

  virtual bool bind() = 0;

  virtual bool send( const void* message, const size_t length ) = 0;
  virtual bool receive() = 0;

  std::string getHost() const;
  std::string getPort() const;

  void setHost(const std::string& host);
  void setPort(const std::string& port);
  
  // std::string getRhost() const;
  // std::string getRport() const;
  // void setRhost(const std::string& host);
  // void setRport(const std::string& port);

  virtual int getSocket() const = 0;


protected:

  Connection(const std::string& host, const std::string& port);
  // std::string m_rhost;
  // std::string m_rport;
  std::string m_host;
  std::string m_port;


private:

  Connection(const Connection &);
  Connection& operator= (const Connection &);
};

#endif // CONNECTION_HPP
