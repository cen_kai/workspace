#ifndef RULEENGINE_H
#define RULEENGINE_H

#include "Singleton.hpp"
#include "RWLock.hpp"
#include "SecurityRequest.hpp"
#include <stdint.h>
#include <string>
#include <map>

using std::map;
using std::string;

class RuleItem
{
public:
  RuleItem() :mName(), mRequired(false){}
  string mName;
  bool mRequired;
};

typedef map<string,RuleItem*> RuleItemMap; //规则名称、规则项
typedef RuleItemMap::iterator RuleItemMapIt;

class Rule
{
public:
  Rule();
  virtual ~Rule();
  bool trigger(SecurityRequest& req);
  virtual void process(const SecurityRequest& req, SecurityResponse& resp);
public:
  RuleItemMap mProperties;
  bool mOnOff;
  string mRuleName;
};

typedef map<string,Rule*> RuleMap; //规则名称、规则项
typedef RuleMap::iterator RuleMapIt;

class RuleEngine: public Singleton<RuleEngine>
{
  
public:
  RuleEngine();
  ~RuleEngine();
  bool start();
  bool stop();
  bool doProcess(SecurityRequest& request, SecurityResponse& response);
  void addRule(Rule* rule);
  void delRule(const string &ruleName);
private:
  RuleMap mRules;
  bool mStart;
};


#endif /* RULEENGINE_H */
