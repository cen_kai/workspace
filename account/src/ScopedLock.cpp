/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "ScopedLock.hpp"
#include "Common.hpp"


ScopedLock::ScopedLock( Mutex& mutex) : m_mutex(mutex)
{
  TRACE;
  m_mutex.lock();
}


ScopedLock::~ScopedLock()
{
  TRACE;
  m_mutex.unlock();
}
