/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#include "Event.hpp"
#include "TcpConnection.hpp"
#include "Logger.hpp"
#include "Singleton.hpp"
#include "EventLoop.hpp"
#include "MessageManager.hpp"
#include "ConnectManager.hpp"
#include "ConfigFile.hpp"

ConnectManager::ConnectManager()
  :bStart(false), mConnections() ,mListenConnction(NULL), mIds()
{
  TRACE;
}

ConnectManager::~ConnectManager()
{
  TRACE;
  if(mListenConnction)
    mListenConnction = NULL;
  
  ConnectionIt it = mConnections.begin();
  while(it != mConnections.end())
    {
      delete it->second;
      mConnections.erase(it++);
    }
}

bool ConnectManager::doAccept(IOEvent *evt, int revents)
{
  TRACE;  
  if (EV_ERROR & revents) {
    LOG_BEGIN(Logger::ERR)
      LOG_END("events error.");
    return false;
  }
  
  while(1)
    {
      int client_socket = -1;
  
      if ( !mListenConnction->accept( client_socket ))
        return true;
  
      TcpConnection *streamConnection = dynamic_cast<TcpConnection*>(mListenConnction->clone(client_socket));
  
      LOG_BEGIN(Logger::INFO)
        LOG_PROP("host", streamConnection->getHost())
        LOG_PROP("port", streamConnection->getPort())
        LOG_PROP("socket", client_socket)
        LOG_END("New client connected.");

      addConnection(client_socket,  streamConnection);
    }
  
  return true;
}

bool ConnectManager::doIO(IOEvent *evt, int revents)
{
  TRACE;  
  
  if(evt->mFd == mListenConnction->getSocket())
    {
      doAccept(evt, revents);
      return true;
    }
  
  
  if((mConnections.find(evt->mFd)) != mConnections.end())
    {
      if(EV_READ & evt->mEvents)
        doRead(evt->mFd);
      if(EV_WRITE & evt->mEvents)
        doWrite(evt->mFd);
      return true;
    }
  
  return false;
}

bool ConnectManager::doRead(int fd)
{
  TRACE;  
  ConnectionMap::iterator it;

  if((it = mConnections.find(fd)) != mConnections.end())
    {
      TcpConnection * c = it->second;
      if (!c->receive())
        {
          removeConnection(fd);
          return false;
        }
      return true;
    }
  // TODO: 移除事件
  return true;
}

bool ConnectManager::doWrite(int fd)
{
  // TODO: 写数据, 写数据失败后加入这个监听
  TRACE;
  return true;
}

void ConnectManager::addConnection(int fd, TcpConnection *con)
{
  TRACE;
  LOG_BEGIN(Logger::INFO)
    LOG_PROP("fd", fd)
    LOG_END("");
  if((mConnections.find(fd)) != mConnections.end())
    {
      LOG_BEGIN(Logger::ERR) LOG_END("fd already exsist ");
      return;
    }
    //MSG("fd already in exsist!!");
  ConfigFile *config = ConfigFile::getInstance();
  config->getInt("connections_limit", 1000);
  if(mConnections.size() > 1000)
    {
      LOG_BEGIN(Logger::WARNING) LOG_END("connections bigger than config, close it ");
      delete con;
      return;
    }
  mConnections[fd] = con;
  
  IOEvent* evtt = new IOEvent();
  evtt->mFd = fd;
  evtt->mWatcher = this;
  evtt->mEvents = EV_READ;
  EventLoop * loop = EventLoop::getInstance();
  loop->addEvent(evtt);
  
  return;
}

void ConnectManager::removeConnection(int fd)
{
  TRACE;
  ConnectionMap::iterator it = mConnections.find(fd);
  EventLoop *loop = EventLoop::getInstance();
  loop->delEvent(fd);
  
  if(it != mConnections.end())
    {
      it->second->disconnect();
      delete it->second;
      mConnections.erase(it);
    }
  
  return;
}

void ConnectManager::setListenConnection(TcpConnection * con)
{
  TRACE;
  if(NULL != con)
    mListenConnction = con;
}

bool ConnectManager::start()
{
  TRACE;
  if(bStart)
    return true;
  
  // TODO: 监听注册，暂时不管写
  EventLoop *loop = EventLoop::getInstance();

  IOEvent* acceptEvent = loop->obtainIOEvent();
  acceptEvent->mFd = mListenConnction->getSocket();
  acceptEvent->mEvents = EV_READ;
  acceptEvent->mWatcher = this;
  
  loop->addEvent(acceptEvent);
  
  return true;
}

bool ConnectManager::stop()
{
  TRACE;
  if(!bStart)
    return true;
  
  ConnectionIt it = mConnections.begin();
  while(it != mConnections.end())
    {
      it->second->disconnect();
      delete it->second;
      mConnections.erase(it++);
    }
  bStart = false;
  return true;
}

bool ConnectManager::init(TcpConnection* listenConection)
{
  TRACE;
  mListenConnction = listenConection;
  return true;
}

TcpConnection* ConnectManager::getConnection(int fd)
{
  TRACE;
  ConnectionIt it = mConnections.find(fd);
  if(it != mConnections.end())
      return it->second;

  if(mListenConnction->getSocket() == fd)
    return mListenConnction;

  return NULL;
}


void ConnectManager::delTimeoutConnection()
{
  // TODO: 删除超时连接
}
