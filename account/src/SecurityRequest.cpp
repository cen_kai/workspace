#include "SecurityRequest.hpp"
#include "Json.hpp"
#include "Logger.hpp"
#include "PingAn.hpp"
using namespace pingan;

SecurityRequest::SecurityRequest()
  :mDeviceInfo()
  ,mPhoneNo(),
   mWifiMac(),
   mBlueMac(),
   mIMEI(),
   mIMSI(),
   mSimSerial(),
   mDeviceID(),
   mClientNo()
{
  
}

bool SecurityRequest::findKey(const string &key) const
{
  bool ret = false;
  return ret;
}

bool SecurityRequest::findStr(const string &str) const
{
  bool ret = false;
  return ret;
}
bool SecurityRequest::setDataSource(string& data,/*client data*/ string& servData/*serv data*/)
{
  TRACE;
  bool ret = false;
  
  try{
    if(data.length() > 0)
      {
        json::Value js = json::Deserialize(data);
        json::Object obj = js.ToObject();

        if(obj[PHONENO].GetType() != json::NULLVal)
          this->mPhoneNo = obj[PHONENO].ToString();

        if(obj[SIMSERIAL].GetType() != json::NULLVal)
          this->mSimSerial = obj[SIMSERIAL].ToString();

        if(obj[BLUEMAC].GetType() != json::NULLVal)
          this->mBlueMac = obj[BLUEMAC].ToString();
        //    TRACE;
        if(obj[WIFIMAC].GetType() != json::NULLVal)
          this->mWifiMac = obj[WIFIMAC].ToString();
    
        if(obj[IMSIID].GetType() != json::NULLVal)
          this->mIMSI = obj[IMSIID].ToString();

        if(obj[IMEIID].GetType() != json::NULLVal)
          this->mIMEI = obj[IMEIID].ToString();
        //    TRACE;
        if(obj[DEVICEID].GetType() != json::NULLVal)
          this->mDeviceID = obj[DEVICEID].ToString();
    
        if(obj[DEVICE_INFO].GetType() != json::NULLVal)
          this->mDeviceInfo = obj[DEVICE_INFO].ToString();
    
        if(obj[CLIENTNO].GetType() != json::NULLVal)
          this->mClientNo = obj[CLIENTNO].ToString();
    
        if(obj[CELLNO].GetType() != json::NULLVal)
          this->mCellNo = obj[CELLNO].ToString();

      }

    if(servData.length() > 0)
      {
        json::Value js = json::Deserialize(servData);
        json::Object obj = js.ToObject();
        if(obj[MOBILE].GetType() != json::NULLVal)
          this->mServCelloNo = obj[MOBILE].ToString();
      }
    
    LOG_BEGIN(Logger::INFO)
      LOG_PROP("phoneNo", mPhoneNo)
      LOG_PROP("simserial", mSimSerial)
      LOG_PROP("wifimac", mWifiMac)
      LOG_PROP("blueMac", mBlueMac)
      LOG_PROP("imei", mIMEI)
      LOG_PROP("imsi", mIMSI)
      LOG_PROP("deviceId", mDeviceID)
      LOG_PROP("deviceInfo", mDeviceInfo)
      LOG_PROP("clientNo", mClientNo)
      LOG_PROP("mCellno", mCellNo)
      LOG_PROP("servCellNo", mServCelloNo)
      LOG_END("");
      
    ret = true;
  }
  catch(...)
    {
      LOG_BEGIN(Logger::ERR)
        LOG_PROP("data", data)
        LOG_END("json parse error");
      ret = false;
    }
  
  return ret;
}

void SecurityResponse::updateScore(const string& ruleName, uint16_t& score)
{
  if(ruleName.size()<0)
    {
// FIXME: logger
      return;
    }
  
  mScores.insert(ruleName);
  return;
}

void SecurityResponse::toString()
{
  for(auto it = mScores.begin(); it != mScores.end(); ++it)
    {
      LOG_BEGIN(Logger::INFO)
        LOG_PROP("ruleName", *it)
        LOG_END("");
    }
}
