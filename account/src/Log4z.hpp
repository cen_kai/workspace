#pragma once
#ifndef _ZSUMMER_LOG4Z_H_
#define _ZSUMMER_LOG4Z_H_

#include <string>
#include <sstream>
#include <errno.h>
#include <stdio.h>

//the max logger count.
const static int LOGGER_MAX = 20;

//the max log content length.
const static int LOG_BUF_SIZE = 20480;

typedef int LoggerId;

#define INVALID_LOGGERID -1


enum ENUM_LOG_LEVEL
{
	LOG_LEVEL_DEBUG,
	LOG_LEVEL_INFO,
	LOG_LEVEL_WARN,
	LOG_LEVEL_ERROR,
	LOG_LEVEL_ALARM,
	LOG_LEVEL_FATAL,
  LOG_LEVEL_CONTENT
};


#ifndef _ZSUMMER_BEGIN
#define _ZSUMMER_BEGIN namespace zsummer {
#endif  
#ifndef _ZSUMMER_LOG4Z_BEGIN
#define _ZSUMMER_LOG4Z_BEGIN namespace log4z {
#endif
_ZSUMMER_BEGIN
_ZSUMMER_LOG4Z_BEGIN



//log4z class
class ILog4zManager
{
public:
	ILog4zManager(){};
	virtual ~ILog4zManager(){};


	//log4z Singleton
	static ILog4zManager * GetInstance();

	//Get Main Logger. 
	virtual bool	ConfigMainLogger(std::string path="",
		std::string name ="",
		int nLevel = LOG_LEVEL_DEBUG,
		bool display = true) = 0;
	virtual LoggerId GetMainLogger() = 0;

	///Get other loggers, The first must call  ConfigFromFile() to configure.
	virtual std::string GetExampleConfig() = 0;
	virtual bool ConfigFromFile(std::string cfg) = 0;
	virtual LoggerId GetLoggerFromName(std::string name) =0;

	//Get other loggers.
	virtual LoggerId DynamicCreateLogger(	std::string path="",
							std::string name ="",
							int nLevel = LOG_LEVEL_DEBUG,
							bool display = true) = 0;


	// dynamic change logger's attribute.
	virtual bool ChangeLoggerLevel(LoggerId nLoggerID, int nLevel) = 0;
	virtual bool ChangeLoggerDisplay(LoggerId nLoggerID, bool enable) = 0;

	// get log4z runtime status.
	virtual unsigned long long GetStatusTotalWriteCount() = 0;
	virtual unsigned long long GetStatusTotalWriteBytes() = 0;
	virtual unsigned long long GetStatusWaitingCount() = 0;
	virtual unsigned int GetStatusActiveLoggers() = 0;

	//start and stop method.
	virtual bool Start() = 0;
	virtual bool Stop() = 0;

	//push a base log
	virtual bool PushLog(LoggerId id, int level, const char * log) = 0;

};
#ifndef _ZSUMMER_END
#define _ZSUMMER_END }
#endif  
#ifndef _ZSUMMER_LOG4Z_END
#define _ZSUMMER_LOG4Z_END }
#endif

_ZSUMMER_LOG4Z_END
_ZSUMMER_END

class CStringStream;
extern __thread char g_log4zstreambuf[LOG_BUF_SIZE];
//base log micro.
#define LOG_STREAM(id, level, log)\
{\
	zsummer::log4z::CStringStream ss(g_log4zstreambuf, LOG_BUF_SIZE);\
	ss << log;\
	ss << " ( " << __FILE__ << " ) : "  << __LINE__;\
	zsummer::log4z::ILog4zManager::GetInstance()->PushLog(id, level, g_log4zstreambuf);\
}

//log micro
#define LOG_DEBUG(id, log) LOG_STREAM(id, LOG_LEVEL_DEBUG, log)
#define LOG_INFO(id, log)  LOG_STREAM(id, LOG_LEVEL_INFO, log)
#define LOG_WARN(id, log)  LOG_STREAM(id, LOG_LEVEL_WARN, log)
#define LOG_ERROR(id, log) LOG_STREAM(id, LOG_LEVEL_ERROR, log)
#define LOG_ALARM(id, log) LOG_STREAM(id, LOG_LEVEL_ALARM, log)
#define LOG_FATAL(id, log) LOG_STREAM(id, LOG_LEVEL_FATAL, log)

///fast log micro. It write log to the MainLogger.
#define LOGD( log ) LOG_DEBUG( zsummer::log4z::ILog4zManager::GetInstance()->GetMainLogger(), log )
#define LOGI( log ) LOG_INFO( zsummer::log4z::ILog4zManager::GetInstance()->GetMainLogger(), log )
#define LOGW( log ) LOG_WARN( zsummer::log4z::ILog4zManager::GetInstance()->GetMainLogger(), log )
#define LOGE( log ) LOG_ERROR( zsummer::log4z::ILog4zManager::GetInstance()->GetMainLogger(), log )
#define LOGA( log ) LOG_ALARM( zsummer::log4z::ILog4zManager::GetInstance()->GetMainLogger(), log )
#define LOGF( log ) LOG_FATAL( zsummer::log4z::ILog4zManager::GetInstance()->GetMainLogger(), log )

_ZSUMMER_BEGIN
_ZSUMMER_LOG4Z_BEGIN

class CStringStream
{
public:
	CStringStream(char * buf, int len)
	{
		m_pBegin = buf;
		m_pEnd = buf + len;
		m_pCur = m_pBegin;
	}

	template<class T>
	void WriteData(const char * ft, T t)
	{
		if (m_pCur < m_pEnd)
		{
			int len = 0;
			int count = (int)(m_pEnd - m_pCur);
			len = snprintf(m_pCur, count, ft, t);
			if (len < 0)
			{
				*m_pCur = '\0';
				len = 0;
			}
			else if (len >= count)
			{
				len = count;
				*(m_pEnd-1) = '\0';
			}
			m_pCur += len;
		}
	}

	template<class T>
	CStringStream & operator <<(const T * t)
	{	
		if (sizeof(t) == 8)
		{
			WriteData("%016llx", (unsigned long long)t);
		}
		else
		{
			WriteData("%08llx", (unsigned long long)t);
		}
		return *this;
	}
	template<class T>
	CStringStream & operator <<(T * t)
	{
		return (*this << (const T*) t);
	}

	CStringStream & operator <<(char * t)
	{
		WriteData("%s", t);
		return *this;
	}
	CStringStream & operator <<(const char * t)
	{
		WriteData("%s", t);
		return *this;
	}
	CStringStream & operator <<(bool t)
	{
		if(t)WriteData("%s", "true");
		else WriteData("%s", "false");
		return *this;
	}
	CStringStream & operator <<(char t)
	{
		WriteData("%c", t);
		return *this;
	}

	CStringStream & operator <<(unsigned char t)
	{
		WriteData("%d",(int)t);
		return *this;
	}
	CStringStream & operator <<(short t)
	{
		WriteData("%d", (int)t);
		return *this;
	}
	CStringStream & operator <<(unsigned short t)
	{
		WriteData("%d", (int)t);
		return *this;
	}
	CStringStream & operator <<(int t)
	{
		WriteData("%d", t);
		return *this;
	}
	CStringStream & operator <<(unsigned int t)
	{
		WriteData("%u", t);
		return *this;
	}
	CStringStream & operator <<(long t)
	{
		if (sizeof(long) == sizeof(int))
		{
			WriteData("%d", t);
		}
		else
		{
			WriteData("%lld", t);
		}
		return *this;
	}
	CStringStream & operator <<(unsigned long t)
	{
		if (sizeof(unsigned long) == sizeof(unsigned int))
		{
			WriteData("%u", t);
		}
		else
		{
			WriteData("%llu", t);
		}
		return *this;
	}
	CStringStream & operator <<(long long t)
	{
		WriteData("%lld", t);
		return *this;
	}
	CStringStream & operator <<(unsigned long long t)
	{
		WriteData("%llu", t);
		return *this;
	}
	CStringStream & operator <<(float t)
	{
		WriteData("%.4f", t);
		return *this;
	}
	CStringStream & operator <<(double t)
	{
		WriteData("%.4lf", t);
		return *this;
	}
	CStringStream & operator <<(const std::string t)
	{
		WriteData("%s", t.c_str());
		return *this;
	}

private:
	CStringStream(){}
	CStringStream(CStringStream &){}
	char *  m_pBegin;
	char *  m_pEnd;
	char *  m_pCur;
};


_ZSUMMER_LOG4Z_END
_ZSUMMER_END

#endif





