/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */

#include "Logger.hpp"
#include "Common.hpp"

#include "Message.hpp"
#include "TcpConnection.hpp"
#include "SocketServer.hpp"
#include "ConfigFile.hpp"
#include "PingAn.hpp"
#include "Json.hpp"
#include "ZkAgent.hpp"


#include "EventLoop.hpp"
#include "MysqlConnectionPool.hpp"

#include <iostream>
#include <string>
#include <signal.h>
#include <unistd.h> // sleep


SocketServer *cserver;
EventLoop* cmainloop;
using std::string;

void signal_handler(int signal)
{
  LOG_STATIC( Logger::ERR, "User Exit(1)" );
  //  cserver->stop();
  cmainloop->quit();
}

void string_replace(string&s1,const string&s2,const string&s3)
{
string::size_type pos=0;
string::size_type a=s2.size();
string::size_type b=s3.size();
while((pos=s1.find(s2,pos))!=string::npos)
  {
s1.replace(pos,a,s3);
pos+=b;
}

}
string getCurrentPath()
{
  int MAX_SIZE = 200;
  char current_absolute_path[MAX_SIZE];
  int cnt = readlink("/proc/self/exe", current_absolute_path, MAX_SIZE);
  if (cnt < 0 || cnt >= MAX_SIZE)
    {
      std::cout<<"error: path"<<std::endl;
      exit(-1);
    }
  int i;
  for (i = cnt; i >=0; --i)
    {
      if (current_absolute_path[i] == '/')
        {
          current_absolute_path[i+1] = '\0';
          break;
        }
    }
  std::cout<<"current_absolute_path"<<current_absolute_path<<std::endl;
  return string(current_absolute_path);
}
//获取当前程序绝对路径

int main(int argc, char* argv[] )
{
  /*
  if ( argc != 3 ) {
    std::cerr << "Usage: " << argv[0] <<  " <HOST> <PORT>" << std::endl;
    return 1;
  }
  */
  signal(SIGUSR1, signal_handler);
  string src = getCurrentPath();
  string pattern = "/bin/";
  string march = "/conf/demo.cnf";
  string_replace(src, pattern, march);
//  string_replace(src, pattern, march);
  std::cout<<"path: "<<src<<std::endl;
//  string_replace(src, pattern, march);
//  string config = "demo.cnf";
  ConfigFile::createInstance(src);

  Logger::createInstance();
  Logger::init(std::cout);
  Logger::setLogLevel(Logger::FINEST);

  ZkAgent::createInstance();  
  EventLoop::createInstance();
  EventLoop* mainloop = EventLoop::getInstance();

  SocketServer server;
  server.start();
  
  cserver = &server;
  cmainloop = mainloop;
  
  mainloop->run();
  //  mainloop->quit();

  server.stop();
  ZkAgent::destroy();
  Logger::destroy();
  ConfigFile::destroy();
  EventLoop::destroy();
  std::cout<<"the last command"<<std::endl;
}

