/**
 * Copyright (c) 平安科技(深圳)有限公司
 * @file   
 * @author qianzs <qianzs354@pingan.com.cn>
 * @date   Wed Jul 13:41:14 2015
 * 
 * @brief  
 * 
 * 
 */


#ifndef OBSERVER_HPP
#define OBSERVER_HPP

class Subject;

class Observer
{
public:

  virtual ~Observer();
  virtual void update( Subject&) = 0;
};


#endif // OBSERVER_HPP
